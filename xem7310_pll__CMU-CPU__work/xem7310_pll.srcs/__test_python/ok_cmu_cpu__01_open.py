## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_cmu_cpu__01_open.py : test code for open device

####
## library call
import ok_cmu_cpu__lib as cmu
#
# check configuration : OK_EP_ADRS_CONFIG
#  
EP_ADRS = cmu.conf.OK_EP_ADRS_CONFIG
#  
def form_contents(var,idx):
	return '{} = {}'.format(idx, var[idx])
#
def form_address(var,idx):
	return '{} @ {:#04x}'.format(idx, var[idx])
#  
print(form_contents(EP_ADRS,'board_name'))
print(form_contents(EP_ADRS,'ver'))
print(form_contents(EP_ADRS,'bit_filename'))
#
print(form_address(EP_ADRS,'FPGA_IMAGE_ID'))
#
####

####
## init : dev
dev = cmu.ok_cmu_init()
print(dev)
####

####
ret = cmu.ok_cmu_caller_id()
print(ret)
####

####
## open
#ret = cmu.ok_cmu_open()
ret = cmu.ok_cmu_open('1739000J7V') # CMU-CPU-F5500-FPGA1
#ret = cmu.ok_cmu_open('1908000OVP') # CMU-CPU-F5500-FPGA2
#ret = cmu.ok_cmu_open('173900DUMP')
print(ret)
if not ret[1]:
	ret = cmu.ok_cmu_open()
	print(ret)
####



####
# work above!
####

####
cmu.sleep(3)
####

####
## close
ret = cmu.ok_cmu_close()
print(ret)
####