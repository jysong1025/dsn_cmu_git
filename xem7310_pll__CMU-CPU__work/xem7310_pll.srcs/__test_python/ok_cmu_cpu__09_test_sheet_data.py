## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_cmu_cpu__09_test_sheet_data.py : test code for making test sheet data
#  
# power options: ON/OFF 
# DAC options: any ... 0x0000~0x7FFF ... 0V ~ 10V
# dwave freq options: 100kHz ~ 10MHz
# filter bw options: 120kHz, 1.2MHz, 12MHz, 120MHz
# ADC gain options 
#
# note: down-converting = oversampling and decimation...
# note: down-sampling = analog-style decimation...
#  
#  
#  test case 0-0: no signal / noise level check @ 10.4Msps
#  test case 0-1: no signal / noise level check @ 960ksps
#  test case 1-0: normal sampling with 10.4Msps,  10kHz signal, BW 120kHz, some DAC
#  test case 1-1: normal sampling with 10.4Msps, 100kHz signal, BW 120kHz, some DAC
#  test case 1-2: normal sampling with 10.4Msps,   1MHz signal, BW 1.2MHz, some DAC
#  test case 1-3: normal sampling with 10.4Msps,   4MHz signal, BW  12MHz, some DAC
#  test case 2-0:   down-sampling with  960ksps,   1MHz signal, BW 1.2MHz, some DAC -->  40kHz image
#  test case 2-1:   down-sampling with  1.9Mpps,   2MHz signal, BW 1.2MHz, some DAC --> 100kHz image
#  test case 2-2:   down-sampling with  3.9Msps,   4MHz signal, BW  12MHz, some DAC --> 100kHz image
#  
#  batch : exec(open('ok_cmu_cpu__09_test_sheet_data.py').read())

####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
BIT_FILENAME = ''
#
##TODO: DUMP setup
DUMP_EN = 0
#
#PARAM__ADC_INPUT_DELAY_TAP = 25 #NG wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 21 #NG wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 20 #OK wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 15 #OK wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 14 #OK wirh adc base 210MHz and 2.4ns input delay
#PARAM__ADC_INPUT_DELAY_TAP = 13 #OK wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 12 #OK wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 11 #OK wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 10 #OK wirh adc base 210MHz
PARAM__ADC_INPUT_DELAY_TAP = 8 #OK wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 5 #OK wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 0 #OK wirh adc base 210MHz
#
##TODO: TP_TEST setup
TP_TEST_EN = 1
#  
#TP_TEST_FREQ_TARGET=10e6
#TP_TEST_FREQ_TARGET=1e6
TP_TEST_FREQ_TARGET=100e3
#
#TP_TEST_FILT_BW ='12M'
#TP_TEST_FILT_BW ='1M2'
TP_TEST_FILT_BW ='120K'
#
TP_TEST_AMP_PWR = 'OFF'
#
#TP_TEST_DAC_CODE = 0x2000 ## for 10MHz test 
TP_TEST_DAC_CODE = 0x1000 ## for 1X AMP gain 
#TP_TEST_DAC_CODE = 0x0200 ## for 10X AMP gain
#TP_TEST_DAC_CODE = 0x0040 ## for 100X AMP gain
#
TP_TEST_AMP_GAIN_CTRL = '1X'   ## wave gen 500mVpp
#TP_TEST_AMP_GAIN_CTRL = '10X'  ## wave gen  50mVpp
#TP_TEST_AMP_GAIN_CTRL = '100X' ## wave gen  5mVpp
#
## downsampling for 10MHz
#   FS_TARGET          = 9615384   # 125000000/   13= 9615384 //  9.6Msps
#  test case :   down-sampling with  9.6Msps,   10MHz signal, BW  12MHz, some DAC --> 400kHz image
#
TP_TEST_FS_TARGET  = 15000000 # 15Msps
#TP_TEST_FS_TARGET  = 14e6
#TP_TEST_FS_TARGET = 10416666   # 125000000/   12=10416666 // 10.4Msps ###
#TP_TEST_FS_TARGET =  9615384   # 125000000/   13= 9615384 //  9.6Msps
#TP_TEST_FS_TARGET =  8928571   # 125000000/   14= 8928571 //  8.9Msps
#TP_TEST_FS_TARGET =  8333333    # 125000000/   15= 8333333 //  8.3Msps

#
TP_TEST_FREQ_ZOOM = 200e3
#
####

####
## library call
import ok_cmu_cpu__lib as cmu
#
# check configuration : OK_EP_ADRS_CONFIG
#  
EP_ADRS = cmu.conf.OK_EP_ADRS_CONFIG
#  
def form_contents(var,idx):
	return '{} = {}'.format(idx, var[idx])
#
def form_address(var,idx):
	return '{} @ {:#04x}'.format(idx, var[idx])
#  
print(form_contents(EP_ADRS,'board_name'))
print(form_contents(EP_ADRS,'ver'))
print(form_contents(EP_ADRS,'bit_filename'))
#
print(form_address(EP_ADRS,'FPGA_IMAGE_ID'))
#
####

####
## init : dev
dev = cmu.ok_cmu_init()
print(dev)
####

####
ret = cmu.ok_cmu_caller_id()
print(ret)
####

####
## open
ret = cmu.ok_cmu_open()
print(ret)
####

####
## FPGA_CONFIGURE
if FPGA_CONFIGURE==1: 
	ret = cmu.ok_cmu_conf(BIT_FILENAME)
	print(ret)
####  

####
## read fpga_image_id
fpga_image_id = cmu.cmu_read_fpga_image_id()
#
def form_hex_32b(val):
	return '0x{:08X}'.format(val)
#
print(form_hex_32b(fpga_image_id))
####

####
## read FPGA internal temp and volt
ret = cmu.cmu_monitor_fpga()
print(ret)
####

####
## test counter on 
ret = cmu.cmu_test_counter('ON')
print(ret)
####

###################################################


##TODO: def setup____test_signal()
def setup____test_signal(TEST_CONF={}): 
	## SPO ##
	cmu.cmu_spo_enable()
	cmu.cmu_spo_init()
	#
	# led on
	cmu.cmu_spo_bit__leds(0xFF) 
	#
	# AMP power control
	#cmu.cmu_spo_bit__amp_pwr('ON')
	#cmu.cmu_spo_bit__amp_pwr('OFF')
	try: 
		cmu.cmu_spo_bit__amp_pwr(TEST_CONF['AMP_PWR_CTRL'])
	except: 
		print('> missing AMP_PWR_CTRL in TEST_CONF!')	
	#
	# ADC gain control 
	#cmu.cmu_spo_bit__adc0_gain('1X')
	#cmu.cmu_spo_bit__adc0_gain('10X')
	#cmu.cmu_spo_bit__adc0_gain('100X')
	#cmu.cmu_spo_bit__adc1_gain('1X')
	#cmu.cmu_spo_bit__adc1_gain('10X')
	#cmu.cmu_spo_bit__adc1_gain('100X')
	try: 
		cmu.cmu_spo_bit__adc0_gain(TEST_CONF['AMP_GAIN_CTRL'])
		cmu.cmu_spo_bit__adc1_gain(TEST_CONF['AMP_GAIN_CTRL'])
	except: 
		print('> missing FILT_PATH_CTRL in AMP_GAIN_CTRL!')	
	#
	# filter path control
	## BW 120MHz 
	#cmu.cmu_spo_bit__vi_bw('120M')
	#cmu.cmu_spo_bit__vq_bw('120M')
	## BW 12MHz 
	#cmu.cmu_spo_bit__vi_bw('12M')
	#cmu.cmu_spo_bit__vq_bw('12M')
	## BW 1.2MHz 
	#cmu.cmu_spo_bit__vi_bw('1M2')
	#cmu.cmu_spo_bit__vq_bw('1M2')
	## BW 120kHz 
	#cmu.cmu_spo_bit__vi_bw('120K')
	#cmu.cmu_spo_bit__vq_bw('120K')
	try: 
		cmu.cmu_spo_bit__vi_bw(TEST_CONF['FILT_PATH_CTRL'])
		cmu.cmu_spo_bit__vq_bw(TEST_CONF['FILT_PATH_CTRL'])
	except: 
		print('> missing FILT_PATH_CTRL in TEST_CONF!')	
	#
	## DAC ##
	cmu.cmu_dac_bias_enable()
	cmu.cmu_dac_bias_init()
	#
	# set DAC code 
	DAC1_CODE = 0x1000
	DAC2_CODE = 0x1000
	## for filter-out monitoring (making small)
	#DAC3_CODE = 0x0800
	#DAC4_CODE = 0x0800
	#DAC3_CODE = 0x1000 # normal 
	#DAC4_CODE = 0x1000
	#DAC3_CODE = 0x2000
	#DAC4_CODE = 0x2000
	#DAC3_CODE = 0x2800
	#DAC4_CODE = 0x2800
	#DAC3_CODE = 0x4000
	#DAC4_CODE = 0x4000
	## for 8f-4level monitoring
	#DAC3_CODE = 0x2000 
	#DAC4_CODE = 0x2000
	#
	DAC3_CODE = 0x0000
	DAC4_CODE = 0x0000
	try: 
		DAC3_CODE = TEST_CONF['DAC_CODE']
		DAC4_CODE = TEST_CONF['DAC_CODE']
	except: 
		print('> missing DAC_CODE in TEST_CONF!')		
	#
	cmu.cmu_dac_bias_set_buffer(
			DAC1=DAC1_CODE,
			DAC2=DAC2_CODE,
			DAC3=DAC3_CODE,
			DAC4=DAC4_CODE)
	#
	cmu.cmu_dac_bias_update()
	#
	ret = cmu.cmu_dac_bias_readback()
	print(ret)
	#
	## DWAVE ##
	cmu.cmu_dwave_enable()
	#
	# read dwave base freq
	DWAVE_BASE_FREQ = cmu.cmu_dwave_read_base_freq()
	print('{}: {:#8.3f} MHz\r'.format('DWAVE_BASE_FREQ',DWAVE_BASE_FREQ/1e6))
	#
	try: 
		if (DWAVE_BASE_FREQ!=TEST_CONF['DWAVE_BASE_FREQ']):
			print('>>> {}: {}'.format('Warning!: DWAVE_BASE_FREQ is not matched',TEST_CONF['DWAVE_BASE_FREQ']))
			input('Press Enter key!')
	except: 
		print('> missing DWAVE_BASE_FREQ in TEST_CONF!')		
	#  
	try: 
		CNT_PERIOD = TEST_CONF['CNT_PERIOD']
	except: 
		CNT_PERIOD = 80 
		print('> missing CNT_PERIOD in TEST_CONF!')
	#
	try: 
		CNT_DIFF = TEST_CONF['CNT_DIFF']
	except: 
		CNT_DIFF   = 60
		print('> missing CNT_DIFF in TEST_CONF!')		
	#  
	# set CNT_PERIOD
	cmu.cmu_dwave_wr_cnt_period(CNT_PERIOD)
	#
	# set CNT_DIFF
	cmu.cmu_dwave_wr_cnt_diff(CNT_DIFF)
	#
	# dwave output control
	cmu.cmu_dwave_wr_output_dis__enable_all()
	#cmu.cmu_dwave_wr_output_dis__enable_path_i_only()
	#cmu.cmu_dwave_wr_output_dis__enable_path_q_only()
	#
	# set dwave parameters into core logic
	cmu.cmu_dwave_set_para()
	#
	return None

def turn_on__test_signal(TEST_CONF={}):
	# dwave_pulse_on
	cmu.cmu_dwave_pulse_on()
	#
	# wait for pulse stability
	cmu.sleep(1)
	#
	return None

##TODO: def capture__adc_samples()
def capture__adc_samples(TEST_CONF={}):
	## ADC ##
	cmu.cmu_adc_enable()
	#
	# reset adc status
	cmu.cmu_adc_reset()
	#
	# read adc base freq
	ADC_BASE_FREQ = cmu.cmu_adc_read_base_freq()
	print('{}: {:#8.3f} MHz\r'.format('ADC_BASE_FREQ',ADC_BASE_FREQ/1e6))
	#
	# check adc base freq
	try: 
		if (ADC_BASE_FREQ!=TEST_CONF['ADC_BASE_FREQ']):
			print('>>> {}: {}'.format('Warning!: ADC_BASE_FREQ is not matched',TEST_CONF['ADC_BASE_FREQ']))
			input('Press Enter key!')
	except: 
		print('> missing ADC_BASE_FREQ in TEST_CONF!')		
	#
	# set adc sampling freq
	try:
		ADC_CNT_SAMPLE_PERIOD = TEST_CONF['ADC_CNT_SAMPLE_PERIOD']
	except:
		# base 210MHz and base 125MHz ... common integer...
		# 210 = 7 * 3 * 2 * 5 ... 14 min
		# 210MHz/15 = 14 Msps 
		# 125 = 5 * 5 * 5     ... 12 min
		# 125MHz/15 = 8.33333333 Msps
		ADC_CNT_SAMPLE_PERIOD = 15
		print('> missing ADC_CNT_SAMPLE_PERIOD in TEST_CONF!')		
	#
	cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_SMP_PRD',ADC_CNT_SAMPLE_PERIOD)
	#
	# set the number of adc samples
	try:
		ADC_NUM_SAMPLES = TEST_CONF['ADC_NUM_SAMPLES']
	except:
		ADC_NUM_SAMPLES = 100
		print('> missing ADC_NUM_SAMPLES in TEST_CONF!')		
	cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_UPD_SMP',ADC_NUM_SAMPLES)
	#
	# set adc data line input delay tap
	try:
		ADC_INPUT_DELAY_TAP = TEST_CONF['ADC_INPUT_DELAY_TAP'] 
	except:
		ADC_INPUT_DELAY_TAP = 0
		print('> missing ADC_INPUT_DELAY_TAP in TEST_CONF!')		
	ADC_INPUT_DELAY_TAP_code = (ADC_INPUT_DELAY_TAP<<27)|(ADC_INPUT_DELAY_TAP<<22)
	val = ADC_INPUT_DELAY_TAP_code
	msk = 0xFFC00000
	cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_DLY_TAP_OPT',val,msk)
	#
	# set up  test mode
	PIN_TEST_FRC_HIGH = 0
	PIN_DLLN_FRC_LOW  = 0
	PTTN_CNT_UP_EN    = 0
	ADC_control_code = (PTTN_CNT_UP_EN<<2)|(PIN_DLLN_FRC_LOW<<1)|PIN_TEST_FRC_HIGH
	val = ADC_control_code
	msk = 0x00000007
	cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_DLY_TAP_OPT',val,msk)
	#
	## cmu_adc_set_para
	## remove call cmu.cmu_adc_set_para()
	#
	# initialize adc
	cmu.cmu_adc_init()
	#
	# cmu_check_adc_test_pattern
	ret = cmu.cmu_check_adc_test_pattern()
	print(ret)
	#
	# cmu_adc_is_fifo_empty
	ret = cmu.cmu_adc_is_fifo_empty()
	print(ret)
	#
	# cmu_adc_update
	cmu.cmu_adc_update()
	#
	# cmu_adc_load_from_fifo
	adc_list = cmu.cmu_adc_load_from_fifo()
	#
	# disable
	cmu.cmu_adc_disable()
	#
	try: 
		TEST_CONF['ADC_LIST_INT'] = adc_list
	except: 
		print('> missing ADC_LIST_INT in TEST_CONF!')		
	#
	return adc_list 

def turn_off_test_signal(TEST_CONF={}):
	## DWAVE ##
	# dwave_pulse_off
	cmu.cmu_dwave_pulse_off()
	cmu.cmu_dwave_disable()
	## DAC ##
	# set default values
	cmu.cmu_dac_bias_set_buffer() 
	cmu.cmu_dac_bias_update() 
	cmu.cmu_dac_bias_disable() 
	## SPO ##
	# AMP power off
	cmu.cmu_spo_bit__amp_pwr('OFF')
	# LED off
	cmu.cmu_spo_bit__leds(0x00)
	cmu.cmu_spo_disable()
	#
	return None

	
def display__adc_samples(TEST_CONF={}):
	try:
		FS_TARGET     = TEST_CONF['FS_TARGET'    ]
		FS_TRUE       = TEST_CONF['FS_TRUE'      ]
		DUMP_FILE_PRE = TEST_CONF['DUMP_FILE_PRE']
		ADC_LIST_INT  = TEST_CONF['ADC_LIST_INT' ]
		NUM_SMP_ZOOM  = TEST_CONF['NUM_SMP_ZOOM' ]
	except:
		return False
	#
	#cmu.cmu_adc_display_data_list_int (ADC_LIST_INT,FS_TARGET)
	#fig_filename = cmu.cmu_adc_display_data_list_int__zoom (ADC_LIST_INT,DUMP_FILE_PRE,FS_TARGET,NUM_SMP_ZOOM)
	fig_filename = cmu.cmu_adc_display_data_list_int__zoom (ADC_LIST_INT,DUMP_FILE_PRE,FS_TRUE,NUM_SMP_ZOOM)
	return fig_filename 

def save__adc_samples_into_csv_file(TEST_CONF={}):
	try:
		FS_TARGET     = TEST_CONF['FS_TARGET'    ]
		FS_TRUE       = TEST_CONF['FS_TRUE'      ]
		DUMP_FILE_PRE = TEST_CONF['DUMP_FILE_PRE']
		ADC_LIST_INT  = TEST_CONF['ADC_LIST_INT' ]
	except:
		return False
	# adc_save_data_list_int_to_csv
	#csv_filename = cmu.cmu_adc_save_data_list_int_to_csv(ADC_LIST_INT,DUMP_FILE_PRE,FS_TARGET)
	csv_filename = cmu.cmu_adc_save_data_list_int_to_csv(ADC_LIST_INT,DUMP_FILE_PRE,FS_TRUE)
	return csv_filename 

def save__conf_result_dict_into_dict_file(TEST_CONF={}):
	ret = None
	#
	if TEST_CONF:
		#
		ret = cmu.cmu_adc_save_conf_result_dict_to_file(TEST_CONF)
		#
	#
	return ret
	
#def report__test_sheet_data_from_csv_file(FILENAME):
#	pass
#	return None 

#def report__test_sheet_data_from_dict_file(FILENAME):
#	pass
#	return None 

##TODO: def conduct__test_conf()
def conduct__test_conf(TEST_CONF):
	## conduct test
	setup____test_signal(TEST_CONF)
	turn_on__test_signal(TEST_CONF)
	capture__adc_samples(TEST_CONF)
	turn_off_test_signal(TEST_CONF)
	#
	## show and save figure
	fig_filename = display__adc_samples(TEST_CONF)
	print(fig_filename)
	#
	## save csv
	csv_filename = save__adc_samples_into_csv_file(TEST_CONF)
	print(csv_filename)
	#
	## save dict
	dict_filename = save__conf_result_dict_into_dict_file(TEST_CONF)
	print(dict_filename)
	#
	## readback dict
	#exec( 'readback_dict = ' + open(dict_filename).read() )
	#print(len(readback_dict))
	#print(readback_dict)
	return None


####
# common setup
TEST_CONF = {}
#

####
##TODO: (1) test case 0-0: no signal / noise level check @ 10.4Msps
#
## setup TEST_CONF ##
TEST_CONF['CASE'           ] = '0-0'
TEST_CONF['AMP_PWR_CTRL'   ] = 'OFF'
TEST_CONF['FILT_PATH_CTRL' ] = '1M2'
TEST_CONF['DAC_CODE'       ] = 0x1000
#
DWAVE_FREQ_TARGET = 10e3 ##
DWAVE_BASE_FREQ   = 160e6
CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
#
TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
#
FS_TARGET             = 10.4e6 ##
ADC_BASE_FREQ         = 210e6
ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
ADC_NUM_SAMPLES       = 131072
ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP
#
TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
TEST_CONF['FS_TARGET'            ] = FS_TARGET
TEST_CONF['FS_TRUE'              ] = FS_TRUE
TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
#
TEST_CONF['AMP_GAIN_CTRL'] = '1X'
TEST_CONF['ADC_LIST_INT' ] = []
TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
####
#
if DUMP_EN==1:
	TEST_CONF['DUMP_FILE_PRE'  ] = 'DUMP_' + TEST_CONF['CASE'] + '_'
else:
	TEST_CONF['DUMP_FILE_PRE'  ] = ''
#
## conduct TEST_CONF
conduct__test_conf(TEST_CONF)


####
##TODO: (2) test case 0-1: no signal / noise level check @ 960ksps
#
## setup TEST_CONF ##
TEST_CONF['CASE'           ] = '0-1'
TEST_CONF['AMP_PWR_CTRL'   ] = 'OFF'
TEST_CONF['FILT_PATH_CTRL' ] = '1M2'
TEST_CONF['DAC_CODE'       ] = 0x1000
#
DWAVE_FREQ_TARGET = 10e3 ##
DWAVE_BASE_FREQ   = 160e6
CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
#
TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
#
FS_TARGET             = 960e3 ##
ADC_BASE_FREQ         = 210e6
ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
ADC_NUM_SAMPLES       = 131072
ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP
#
TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
TEST_CONF['FS_TARGET'            ] = FS_TARGET
TEST_CONF['FS_TRUE'              ] = FS_TRUE
TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
#
TEST_CONF['AMP_GAIN_CTRL'] = '1X'
TEST_CONF['ADC_LIST_INT' ] = []
TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
####
#
if DUMP_EN==1:
	TEST_CONF['DUMP_FILE_PRE'  ] = 'DUMP_' + TEST_CONF['CASE'] + '_'
else:
	TEST_CONF['DUMP_FILE_PRE'  ] = ''
#
## conduct TEST_CONF
conduct__test_conf(TEST_CONF)


####
##TODO: (3) test case 1-0: normal sampling with 10.4Msps,  10kHz signal, BW 120kHz, some DAC
#
## setup TEST_CONF ##
TEST_CONF['CASE'           ] = '1-0'
TEST_CONF['AMP_PWR_CTRL'   ] = 'ON'
TEST_CONF['FILT_PATH_CTRL' ] = '120K'
TEST_CONF['DAC_CODE'       ] = 0x1000
#
DWAVE_FREQ_TARGET =  10e3 ##
DWAVE_BASE_FREQ   = 160e6
CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
#
TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
#
FS_TARGET             = 10.4e6 ##
ADC_BASE_FREQ         = 210e6
ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
ADC_NUM_SAMPLES       = 131072
ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP
#
TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
TEST_CONF['FS_TARGET'            ] = FS_TARGET
TEST_CONF['FS_TRUE'              ] = FS_TRUE
TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
#
TEST_CONF['AMP_GAIN_CTRL'] = '1X'
TEST_CONF['ADC_LIST_INT' ] = []
TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
####
#
if DUMP_EN==1:
	TEST_CONF['DUMP_FILE_PRE'  ] = 'DUMP_' + TEST_CONF['CASE'] + '_'
else:
	TEST_CONF['DUMP_FILE_PRE'  ] = ''
#
## conduct TEST_CONF
conduct__test_conf(TEST_CONF)


####
##TODO: (4) test case 1-1: normal sampling with 10.4Msps, 100kHz signal, BW 120kHz, some DAC
#
## setup TEST_CONF ##
TEST_CONF['CASE'           ] = '1-1'
TEST_CONF['AMP_PWR_CTRL'   ] = 'ON'
TEST_CONF['FILT_PATH_CTRL' ] = '120K'
TEST_CONF['DAC_CODE'       ] = 0x1000
#
DWAVE_FREQ_TARGET = 100e3 ##
DWAVE_BASE_FREQ   = 160e6
CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
#
TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
#
FS_TARGET             = 10.4e6 ##
ADC_BASE_FREQ         = 210e6
ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
ADC_NUM_SAMPLES       = 131072
ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP
#
TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
TEST_CONF['FS_TARGET'            ] = FS_TARGET
TEST_CONF['FS_TRUE'              ] = FS_TRUE
TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
#
TEST_CONF['AMP_GAIN_CTRL'] = '1X'
TEST_CONF['ADC_LIST_INT' ] = []
TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
####
#
if DUMP_EN==1:
	TEST_CONF['DUMP_FILE_PRE'  ] = 'DUMP_' + TEST_CONF['CASE'] + '_'
else:
	TEST_CONF['DUMP_FILE_PRE'  ] = ''
#
## conduct TEST_CONF
conduct__test_conf(TEST_CONF)


####
##TODO: (5) test case 1-2: normal sampling with 10.4Msps,   1MHz signal, BW 1.2MHz, some DAC
#
## setup TEST_CONF ##
TEST_CONF['CASE'           ] = '1-2'
TEST_CONF['AMP_PWR_CTRL'   ] = 'ON'
TEST_CONF['FILT_PATH_CTRL' ] = '1M2'
TEST_CONF['DAC_CODE'       ] = 0x1000
#
DWAVE_FREQ_TARGET =   1e6 ##
DWAVE_BASE_FREQ   = 160e6
CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
#
TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
#
FS_TARGET             = 10.4e6 ##
ADC_BASE_FREQ         = 210e6
ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
ADC_NUM_SAMPLES       = 131072
ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP
#
TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
TEST_CONF['FS_TARGET'            ] = FS_TARGET
TEST_CONF['FS_TRUE'              ] = FS_TRUE
TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
#
TEST_CONF['AMP_GAIN_CTRL'] = '1X'
TEST_CONF['ADC_LIST_INT' ] = []
TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
####
#
if DUMP_EN==1:
	TEST_CONF['DUMP_FILE_PRE'  ] = 'DUMP_' + TEST_CONF['CASE'] + '_'
else:
	TEST_CONF['DUMP_FILE_PRE'  ] = ''
#
## conduct TEST_CONF
conduct__test_conf(TEST_CONF)


####
##TODO: (6) test case 1-3: normal sampling with 10.4Msps,   4MHz signal, BW  12MHz, some DAC
#
## setup TEST_CONF ##
TEST_CONF['CASE'           ] = '1-3'
TEST_CONF['AMP_PWR_CTRL'   ] = 'ON'
TEST_CONF['FILT_PATH_CTRL' ] = '12M'
TEST_CONF['DAC_CODE'       ] = 0x1000
#
DWAVE_FREQ_TARGET =   4e6 ##
DWAVE_BASE_FREQ   = 160e6
CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
#
TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
#
FS_TARGET             = 10.4e6 ##
ADC_BASE_FREQ         = 210e6
ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
ADC_NUM_SAMPLES       = 131072
ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP
#
TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
TEST_CONF['FS_TARGET'            ] = FS_TARGET
TEST_CONF['FS_TRUE'              ] = FS_TRUE
TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
#
TEST_CONF['AMP_GAIN_CTRL'] = '1X'
TEST_CONF['ADC_LIST_INT' ] = []
TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
####
#
if DUMP_EN==1:
	TEST_CONF['DUMP_FILE_PRE'  ] = 'DUMP_' + TEST_CONF['CASE'] + '_'
else:
	TEST_CONF['DUMP_FILE_PRE'  ] = ''
#
## conduct TEST_CONF
conduct__test_conf(TEST_CONF)




####
##TODO: (7) test case 2-0:   down-sampling with  960ksps,   1MHz signal, BW 1.2MHz, some DAC -->  40kHz image
#
## setup TEST_CONF ##
TEST_CONF['CASE'           ] = '2-0'
TEST_CONF['AMP_PWR_CTRL'   ] = 'ON'
TEST_CONF['FILT_PATH_CTRL' ] = '1M2'
TEST_CONF['DAC_CODE'       ] = 0x1000
#
DWAVE_FREQ_TARGET =   1e6 ##
DWAVE_BASE_FREQ   = 160e6
CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
#
TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
#
FS_TARGET             = 960e3 ##
ADC_BASE_FREQ         = 210e6
ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
ADC_NUM_SAMPLES       = 131072
ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP
#
TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
TEST_CONF['FS_TARGET'            ] = FS_TARGET
TEST_CONF['FS_TRUE'              ] = FS_TRUE
TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
#
TEST_CONF['AMP_GAIN_CTRL'] = '1X'
TEST_CONF['ADC_LIST_INT' ] = []
TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
####
#
if DUMP_EN==1:
	TEST_CONF['DUMP_FILE_PRE'  ] = 'DUMP_' + TEST_CONF['CASE'] + '_'
else:
	TEST_CONF['DUMP_FILE_PRE'  ] = ''
#
## conduct TEST_CONF
conduct__test_conf(TEST_CONF)


####
##TODO: (8) test case 2-1:   down-sampling with  1.9Mpps,   2MHz signal, BW 1.2MHz, some DAC --> 100kHz image
#
## setup TEST_CONF ##
TEST_CONF['CASE'           ] = '2-1'
TEST_CONF['AMP_PWR_CTRL'   ] = 'ON'
TEST_CONF['FILT_PATH_CTRL' ] = '1M2'
TEST_CONF['DAC_CODE'       ] = 0x1000
#
DWAVE_FREQ_TARGET =   2e6 ##
DWAVE_BASE_FREQ   = 160e6
CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
#
TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
#
FS_TARGET             = 1.9e6 ##
ADC_BASE_FREQ         = 210e6
ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
ADC_NUM_SAMPLES       = 131072
ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP
#
TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
TEST_CONF['FS_TARGET'            ] = FS_TARGET
TEST_CONF['FS_TRUE'              ] = FS_TRUE
TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
#
TEST_CONF['AMP_GAIN_CTRL'] = '1X'
TEST_CONF['ADC_LIST_INT' ] = []
TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
####
#
if DUMP_EN==1:
	TEST_CONF['DUMP_FILE_PRE'  ] = 'DUMP_' + TEST_CONF['CASE'] + '_'
else:
	TEST_CONF['DUMP_FILE_PRE'  ] = ''
#
## conduct TEST_CONF
conduct__test_conf(TEST_CONF)


####
##TODO: (9) test case 2-2:   down-sampling with  3.9Msps,   4MHz signal, BW  12MHz, some DAC --> 100kHz image
#
## setup TEST_CONF ##
TEST_CONF['CASE'           ] = '2-2'
TEST_CONF['AMP_PWR_CTRL'   ] = 'ON'
TEST_CONF['FILT_PATH_CTRL' ] = '12M'
TEST_CONF['DAC_CODE'       ] = 0x1000
#
DWAVE_FREQ_TARGET =   4e6 ##
DWAVE_BASE_FREQ   = 160e6
CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
#
TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
#
FS_TARGET             = 3.9e6 ##
ADC_BASE_FREQ         = 210e6
ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
ADC_NUM_SAMPLES       = 131072
ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP
#
TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
TEST_CONF['FS_TARGET'            ] = FS_TARGET
TEST_CONF['FS_TRUE'              ] = FS_TRUE
TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
#
TEST_CONF['AMP_GAIN_CTRL'] = '1X'
TEST_CONF['ADC_LIST_INT' ] = []
TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
####
#
if DUMP_EN==1:
	TEST_CONF['DUMP_FILE_PRE'  ] = 'DUMP_' + TEST_CONF['CASE'] + '_'
else:
	TEST_CONF['DUMP_FILE_PRE'  ] = ''
#
## conduct TEST_CONF
conduct__test_conf(TEST_CONF)



########



####
## cmu_adc_load_from_csv
#ret = cmu.cmu_adc_load_from_csv(csv_filename)
#adc_list_rb = ret[0]
#fs_rb       = ret[1]
#ns_rb       = ret[2]
#print(adc_list_rb[0][0:5])
#print(adc_list_rb[1][0:5])
#print(fs_rb)
#print(ns_rb)
####

####
## adc_display_data_list_int
#ret = cmu.cmu_adc_display_data_list_int(adc_list_rb,fs_rb)
#print(ret)
####


####
#report__test_sheet_data_from_csv_file(TEST_CONF)


########

####
# generate signal for TP test 
####


####
##TODO: def conduct__tp_test()
def conduct__tp_test(
TP_TEST_FREQ_TARGET=100e3,
TP_TEST_FILT_BW ='120K',
TP_TEST_FS_TARGET = 15e6,
TP_TEST_AMP_PWR = 'ON',
TP_TEST_DAC_CODE = 0x1000,
TP_TEST_AMP_GAIN_CTRL='1X',
TP_TEST_FREQ_ZOOM=100e3):
	#
	TEST_CONF['CASE'           ] = 'TP_' + TP_TEST_AMP_GAIN_CTRL
	TEST_CONF['AMP_PWR_CTRL'   ] = TP_TEST_AMP_PWR ##
	TEST_CONF['FILT_PATH_CTRL' ] = TP_TEST_FILT_BW ##
	TEST_CONF['DAC_CODE'       ] = TP_TEST_DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = TP_TEST_FREQ_TARGET ##
	DWAVE_BASE_FREQ   = 160e6
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	print('{}={}'.format('DWAVE_BASE_FREQ'  ,DWAVE_BASE_FREQ  ))
	print('{}={}'.format('DWAVE_FREQ_TARGET',DWAVE_FREQ_TARGET))
	print('{}={}'.format('DWAVE_FREQ_TRUE'  ,DWAVE_FREQ_TRUE  ))
	print('{}={}'.format('CNT_PERIOD'       ,CNT_PERIOD       ))
	print('{}={}'.format('CNT_DIFF'         ,CNT_DIFF         ))
	#
	#
	FS_TARGET             = TP_TEST_FS_TARGET ##
	ADC_BASE_FREQ         = 210e6
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = 131072
	ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	#
	print('{}={}'.format('ADC_BASE_FREQ'        ,ADC_BASE_FREQ        ))
	print('{}={}'.format('FS_TARGET'            ,FS_TARGET            ))
	print('{}={}'.format('FS_TRUE'              ,FS_TRUE              ))
	print('{}={}'.format('ADC_CNT_SAMPLE_PERIOD',ADC_CNT_SAMPLE_PERIOD))
	print('{}={}'.format('ADC_NUM_SAMPLES'      ,ADC_NUM_SAMPLES      ))
	print('{}={}'.format('ADC_INPUT_DELAY_TAP'  ,ADC_INPUT_DELAY_TAP  ))
	#
	#
	TEST_CONF['AMP_GAIN_CTRL']   = TP_TEST_AMP_GAIN_CTRL ##
	TEST_CONF['ADC_LIST_INT'   ] = []
	TEST_CONF['NUM_SMP_ZOOM']    = int(1/TP_TEST_FREQ_ZOOM*10*TEST_CONF['FS_TARGET']) ##
	#
	print('{}={}'.format('AMP_GAIN_CTRL',TEST_CONF['AMP_GAIN_CTRL']))
	print('{}={}'.format('ADC_LIST_INT' ,TEST_CONF['ADC_LIST_INT' ]))
	print('{}={}'.format('NUM_SMP_ZOOM' ,TEST_CONF['NUM_SMP_ZOOM' ]))
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] = 'DUMP_' + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## callers
	setup____test_signal(TEST_CONF)
	turn_on__test_signal(TEST_CONF)
	capture__adc_samples(TEST_CONF)
	#
	## show and save figure
	fig_filename = display__adc_samples(TEST_CONF)
	print(fig_filename)
	#
	## save csv
	csv_filename = save__adc_samples_into_csv_file(TEST_CONF)
	print(csv_filename)
	#
	## save dict
	dict_filename = save__conf_result_dict_into_dict_file(TEST_CONF)
	print(dict_filename)	
	#
	## TP
	print('> Generating signals for TP test')
	input('')
	#
	turn_off_test_signal(TEST_CONF)
	print('> TP test is done')
	#
	return None
#

if TP_TEST_EN:
	conduct__tp_test(
	TP_TEST_FREQ_TARGET,
	TP_TEST_FILT_BW,
	TP_TEST_FS_TARGET,
	TP_TEST_AMP_PWR,
	TP_TEST_DAC_CODE,
	TP_TEST_AMP_GAIN_CTRL,
	TP_TEST_FREQ_ZOOM)


###################################################

########
# TEST

####
## test counter off
ret = cmu.cmu_test_counter('OFF')
print(ret)
####

####
## test counter reset
ret = cmu.cmu_test_counter('RESET')
print(ret)
####


####
## close
ret = cmu.ok_cmu_close()
print(ret)
####