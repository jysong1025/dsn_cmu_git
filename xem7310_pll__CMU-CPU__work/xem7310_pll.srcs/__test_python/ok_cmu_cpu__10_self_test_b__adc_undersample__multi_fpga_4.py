## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_cmu_cpu__10_self_test_b__adc_undersample.py : 
#
#	test cases
#		...
#		no_signal
#		100kHz 
#		1MHz 
#		10MHz 
#		...
#		15.0Msps = 210MHz/14  = 15.0        Msps
#		9.55Msps = 210MHz/22  = 9.545454545 Msps
#		3.89Msps = 210MHz/54  = 3.888888889 Msps
#		0.97Msps = 210MHz/216 = 0.972222222 Msps
#  
# power options: ON/OFF 
# DAC options: any ... 0x0000~0x7FFF ... 0V ~ 10V
# dwave freq options: 100kHz ~ 10MHz
# filter bw options: 120kHz, 1.2MHz, 12MHz, 120MHz
# ADC gain options 
#
# note: down-converting = oversampling and decimation...
# note: down-sampling = analog-style decimation... vs decimation 
# note: undersampling ...
#  
#  
#  test case 0-0: no signal / noise level check @ 15.0Msps
#  test case 0-1: no signal / noise level check @ 9.55Msps
#  test case 0-2: no signal / noise level check @ 3.89Msps
#  test case 0-3: no signal / noise level check @ 0.97Msps
#  


####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
BIT_FILENAME = 'xem7310__cmu_cpu__top.bit'
#BIT_FILENAME = '../_bit/xem7310__cmu_cpu__top__F8_19_0103.bit' # for DWAVE 80MHz, ADC 10.4Msps max.
#BIT_FILENAME = '../_bit/xem7310__cmu_cpu__top__DWAVE135MHz.bit' # for DWAVE 135MHz, ADC 15Msps max.
#
# FPGA serial to access
#FPGA_SERIAL = '' # for any
#FPGA_SERIAL = '1739000J7V' # for module: CMU-CPU-F5500-FPGA1
#FPGA_SERIAL = '1908000OVP' # for module: CMU-CPU-F5500-FPGA2
#FPGA_SERIAL = '1739000J8A' # for module: CMU-CPU-F5500-FPGA3
FPGA_SERIAL = '1739000J63' # for module: CMU-CPU-F5500-FPGA4
#FPGA_SERIAL = '1739000J8I' # for module: CMU-CPU-F5500-FPGA5
#
# DUMP file prefix
#__DUMP_PRE__ = 'DUMP_BD1__'
#__DUMP_PRE__ = 'DUMP_BD2__'
#__DUMP_PRE__ = 'DUMP_BD3__'
__DUMP_PRE__ = 'DUMP_BD4__'
#__DUMP_PRE__ = 'DUMP_BD5__'
#
__DWAVE_BASE_FREQ = 160e6 # 80e6 # 160e6 # 135e6
#
#DWAVE_PIN_SWAP_DISABLE = 0
DWAVE_PIN_SWAP_DISABLE = 1
DWAVE_IDLE_WARMUP_DISABLE = 0
#
#
__ADC_BASE_FREQ = 210e6 # 125e6
#__ADC_BASE_FREQ = 125e6 

## class for sampling freq 
class c_Fs_info:
	""" variables """
	#
	# ADC sampling rates // TODO: Fs
	FS_0 = 210e6/ 14 # 15.0Msps
	FS_1 = 210e6/ 22 # 9.55Msps # for 10MHz test 
	FS_2 = 210e6/ 54 # 3.89Msps
	FS_3 = 210e6/216 # 0.97Msps
	#
	FS_15M00 = 210e6/14   # 15.0Msps
	FS_14M00 = 210e6/15   # 14.0Msps
	#
	#FS_99K  = 210e6/2120 # 99.057Ksps // 2120	0.099057
	#
	FS_100K0 = 210e6/2100
	FS_150K0 = 210e6/1400
	FS_200K0 = 210e6/1050
	FS_210K0 = 210e6/1000
	FS_250K0 = 210e6/ 840
	FS_280K0 = 210e6/ 750
	FS_300K0 = 210e6/ 700
	FS_420K0 = 210e6/ 500
	FS_500K0 = 210e6/ 420
	FS_600K0 = 210e6/ 350
	FS_700K0 = 210e6/ 300
	FS_750K0 = 210e6/ 280
	FS_1M000 = 210e6/ 210
	FS_1M050 = 210e6/ 200
	FS_1M400 = 210e6/ 150 
	FS_1M500 = 210e6/ 140
	FS_2M000 = 210e6/ 105
	FS_2M100 = 210e6/ 100
	FS_2M500 = 210e6/  84
	FS_2M800 = 210e6/  75
	FS_3M000 = 210e6/  70
	FS_4M200 = 210e6/  50
	FS_5M000 = 210e6/  42
	FS_6M000 = 210e6/  35
	FS_7M000 = 210e6/  30
	FS_7M500 = 210e6/  28
	FS_10M00 = 210e6/  21
	FS_10M50 = 210e6/  20
	#
	
	#   case U_001K_   
	#FS_0K999 = 210e6/210010 #
	FS_0K991 = 210e6/212000 #
	FS_0K988 = 210e6/212500
	FS_0K993 = 210e6/211400
	FS_1K000 = 210e6/210100
	#
	FS_0K99941 = 210e6/210125
	FS_0K99950 = 210e6/210105
	FS_0K99964 = 210e6/210075
	FS_0K99983 = 210e6/210035
	FS_0K99988 = 210e6/210025
	FS_0K99993 = 210e6/210015
	
	
	#   case U_002K_   
	#FS_1K999 = 210e6/105008 #
	FS_1K981 = 210e6/106000 #
	FS_1K976 = 210e6/106250
	FS_1K987 = 210e6/105700
	FS_1K999 = 210e6/105050
	#
	FS_1K99857 = 210e6/105075
	FS_1K99867 = 210e6/105070
	FS_1K99933 = 210e6/105035
	
	
	#   case U_005K_   
	#FS_4K999 = 210e6/42008  #
	FS_4K953 = 210e6/ 42400  #
	FS_4K941 = 210e6/ 42500
	FS_4K967 = 210e6/ 42280
	FS_4K998 = 210e6/ 42020
	#
	FS_4K9970  = 210e6/ 42025
	FS_4K9975  = 210e6/ 42021
	FS_4K9982  = 210e6/ 42015
	
	
	#   case U_010K_   
	FS_9K998 = 210e6/ 21004  #
	FS_9K882 = 210e6/ 21250
	FS_9K934 = 210e6/ 21140
	FS_9K995 = 210e6/ 21010
	#
	FS_9K9929  = 210e6/ 21015
	FS_9K9933  = 210e6/ 21014
	FS_9K9967  = 210e6/ 21007
	
	
	#   case U_020K_   
	FS_19K98 = 210e6/ 10510  #
	FS_19K76 = 210e6/ 10625
	FS_19K86 = 210e6/ 10570
	FS_19K99 = 210e6/ 10505
	#
	FS_19K987 = 210e6/ 10507
	FS_19K994 = 210e6/ 10503
	FS_19K998 = 210e6/ 10501
	
	
	#   case U_050K_   
	FS_49K95 = 210e6/  4204
	FS_49K53 = 210e6/  4240  #
	FS_49K41 = 210e6/  4250
	FS_49K66 = 210e6/  4228
	FS_49K99 = 210e6/  4201 
	FS_49K98 = 210e6/  4202 
	#
	FS_49K96 = 210e6/  4203  
	
	#   case U_100K_   
	FS_99K91 = 210e6/  2102  #
	FS_98K82 = 210e6/  2125
	FS_99K33 = 210e6/  2114
	FS_99K95 = 210e6/  2101
	
	#   case U_200K_   
	FS_198K1 = 210e6/  1060  #
	FS_197K1 = 210e6/  1065
	FS_198K6 = 210e6/  1057
	#
	FS_198K9 = 210e6/  1056
	FS_197K4 = 210e6/  1064
	FS_195K3 = 210e6/  1075
	FS_196K1 = 210e6/  1071
	FS_199K1 = 210e6/  1055
	FS_199K4 = 210e6/  1053
	FS_199K8 = 210e6/  1051
	
	#   case U_500K_  
	FS_495K3 = 210e6/   424  #
	FS_494K1 = 210e6/   425
	FS_498K8 = 210e6/   421
	
	#   case U_001M_  
	FS_995K3 = 210e6/   211
	FS_990K6 = 210e6/   212  #
	FS_985K9 = 210e6/   213
	
	#   case U_002M_  
	FS_1M981 = 210e6/106  #
	
	#   case U_005M_   
	FS_4M884 = 210e6/43   #
	
	#   case U_010M_   
	FS_9M545 = 210e6/22   #
	FS_8M750 = 210e6/24
	FS_5M122 = 210e6/41
	####


#
##TODO: DUMP setup
DUMP_EN = 1     #######
DUMP_DICT_DIS = 1 # 1 for no dict file.

#
# wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 31 # NG 1 2
#PARAM__ADC_INPUT_DELAY_TAP = 30 # NG 2
#PARAM__ADC_INPUT_DELAY_TAP = 29 # NG 2
#PARAM__ADC_INPUT_DELAY_TAP = 28 # NG 2
#PARAM__ADC_INPUT_DELAY_TAP = 27 # NG 2
#PARAM__ADC_INPUT_DELAY_TAP = 26 # NG 1
#PARAM__ADC_INPUT_DELAY_TAP = 25 # OK 78ps*25=1.95 nanoseconds
#PARAM__ADC_INPUT_DELAY_TAP = 20 # OK
#PARAM__ADC_INPUT_DELAY_TAP = 19 # 
#PARAM__ADC_INPUT_DELAY_TAP = 15 # OK 78ps*15=1.17 nanoseconds
#PARAM__ADC_INPUT_DELAY_TAP = 11 # OK 
#PARAM__ADC_INPUT_DELAY_TAP = 10 # 
#PARAM__ADC_INPUT_DELAY_TAP = 9 # 
#PARAM__ADC_INPUT_DELAY_TAP = 8 # 
#PARAM__ADC_INPUT_DELAY_TAP = 5 # 
#PARAM__ADC_INPUT_DELAY_TAP = 0 # OK 
#
PARAM__ADC0_INPUT_DELAY_TAP_L =  0 # 15 0 #   // even - DB
PARAM__ADC0_INPUT_DELAY_TAP_H =  0 # 15 0 #   // odd - DA
PARAM__ADC1_INPUT_DELAY_TAP_L =  0 # 15 0
PARAM__ADC1_INPUT_DELAY_TAP_H =  0 # 15 0 # >=28 NG 
#
#
# reference times
# 
#  undersampling rate  kHz	/ length bits	/ ADC samples	/ ACQ times sec
#  1		12	4096	4.096
#  2		12	4096	2.048
#  5		12	4096	0.8192
#  10		15	32768	3.2768
#  20		15	32768	1.6384
#  50		15	32768	0.65536
#  100		17	131072	1.31072
#  200		17	131072	0.65536
#  500		17	131072	0.262144
#  1000		17	131072	0.131072
#  2000		17	131072	0.065536
#  5000		17	131072	0.0262144
#  10000	17	131072	0.0131072
__ADC_NUM_SAMPLES        = 2**17 #131072
__ADC_NUM_SAMPLES_SHORT  = 2**15
__ADC_NUM_SAMPLES_SHORT2 = 2**12
#__ADC_NUM_SAMPLES_SHORT2 = 2**15 ##
#
__ADC_BITWIDTH=18
#__ADC_BITWIDTH=16
#
# range control option: full range +/-10V
# DAC_volt = 10V /0x7FFF * DAC_code
__DAC_CODE = 0x0000 # 0V for new board ... bias voltages
#__DAC_CODE = 0x400    # 10V /0x7FFF * 0x400  = 0.312509537 volts
#__DAC_CODE = 0x800
#__DAC_CODE = 0xA00
#__DAC_CODE = 0x1000   # 10V /0x7FFF * 0x1000 = 1.25003815 volts
#__DAC_CODE = 0x2000   # 10V /0x7FFF * 0x2000 = 2.5000763 volts
#__DAC_CODE = 0x2A40   # 10V /0x7FFF * 0x2A40 = 3.30088198 volts  # ... "SNR 30dB", 10MHz image signal to 160kHz
#__DAC_CODE = 0x4000   # 10V /0x7FFF * 0x4000 = 5.00015259 volts # ... "SNR 45dB", 10MHz image signal to 160kHz
#__DAC_CODE = 0x5000    # ... SNR 45dB, 10MHz image signal to 160kHz
#__DAC_CODE = 0x6000    # 10V /0x7FFF * 0x6000 = 7.50022889  # ... "SNR 55dB", 10MHz image signal to 160kHz
#__DAC_CODE = 0x7000    # ... SNR 55dB, 10MHz image signal to 160kHz
#__DAC_CODE = 0x7FFF    # ... SNR 55dB, 10MHz image signal to 160kHz
#
# output path selection
TEST_PATH_I_ONLY_EN = 0
TEST_PATH_Q_ONLY_EN = 0


class  c_case_info:
	""" variables """
	#
	# case: no signal
	TEST_CASE_0_0_EN = 0 # <<<
	TEST_CASE_0_1_EN = 0
	TEST_CASE_0_2_EN = 0
	TEST_CASE_0_3_EN = 0
	
	## normal sampling cases 
	#
	# selected list:
	#
	#CASE_NO = 'N_001K_210_14_'   # OK 200ppm  # 15000 cyc
	#CASE_NO = 'N_001K_210_15_' # OK 300ppm
	#
	#CASE_NO = 'N_002K_210_14_'   # OK 200ppm  # 7500 cyc
	#CASE_NO = 'N_002K_210_15_' # OK 300ppm
	#
	#CASE_NO = 'N_005K_210_14_'   # OK 150ppm  # 3000 cyc
	#CASE_NO = 'N_005K_210_15_' # OK 150ppm
	#
	####
	
	#
	TEST_CASE__N_001K_210_15__EN   = 0x001  # FS_14M00 = 210e6/15 # // 210	15	14000	1	NA	14.000	131072	9.362	7	98000.00
	TEST_CASE__N_002K_210_15__EN   = 0x001  # FS_14M00 = 210e6/15 # // 210	15	7000	2	NA	14.000	131072	18.725	17	119000.00
	TEST_CASE__N_005K_210_14__EN   = 0x001  # FS_15M00  = 210e6/14   # //  210	14	3000	5	15.000	131072	43.69066667	43	129000.00
	#
	####
	
	TEST_CASE__N_001K_210_14__EN   = 0x0  # FS_15M00  = 210e6/14   # //  210	14	15000	1	15.000	131072	8.738133333	7	105000.00
	TEST_CASE__N_002K_210_14__EN   = 0x0  # FS_15M00  = 210e6/14   # //  210	14	7500	2	15.000	131072	17.47626667	17	127500.00
	TEST_CASE__N_005K_210_15__EN   = 0x0  # FS_14M00 = 210e6/15 # // 210	15	2800	5	NA	14.000	131072	46.811	43	120400.00
	
	TEST_CASE__N_001K_210_2100__EN = 0  # FS_100K0 = 210e6/2100 # // 210	2100	100	1	0.100	131072	1310.72	1307	130700.00
	TEST_CASE__N_002K_210_1050__EN = 0  # FS_200K0 = 210e6/1050 # // 210	1050	100	2	0.200	131072	1310.72	1307	130700.00
	TEST_CASE__N_005K_210_420__EN  = 0  # FS_500K0 = 210e6/420  # // 210	420	100	5	0.500	131072	1310.72	1307	130700.00
	
	
	TEST_CASE__N_001K_210_150__EN  = 0x000000  # FS_1M400 = 210e6/150 # // 210	150	1400	1	NA	1.400	131072	93.623	89	124600.00
	TEST_CASE__N_001K_210_700__EN  = 0x000000  # FS_300K0 = 210e6/700 # // 210	700	300	1	NA	0.300	131072	436.907	433	129900.00
	TEST_CASE__N_001K_210_750__EN  = 0x000000  # FS_280K0 = 210e6/750 # // 210	750	280	1	NA	0.280	131072	468.114	467	130760.00
	
	TEST_CASE__N_002K_210_75__EN   = 0x000000  # FS_2M800 = 210e6/ 75 # // 210	75	1400	2	NA	2.800	131072	93.623	89	124600.00
	TEST_CASE__N_002K_210_350__EN  = 0x000000  # FS_600K0 = 210e6/350 # // 210	350	300	2	NA	0.600	131072	436.907	433	129900.00
	
	TEST_CASE__N_005K_210_30__EN   = 0x000000  # FS_7M000 = 210e6/ 30 # // 210	30	1400	5	NA	7.000	131072	93.623	89	124600.00
	TEST_CASE__N_005K_210_140__EN  = 0x000000  # FS_1M500 = 210e6/140 # // 210	140	300	5	NA	1.500	131072	436.907	433	129900.00
	TEST_CASE__N_005K_210_210__EN  = 0x000000  # FS_1M000 = 210e6/210 # // 210	210	200	5	NA	1.000	131072	655.360	619	123800.00
	TEST_CASE__N_005K_210_150__EN  = 0x000000  # FS_1M400 = 210e6/150 # // 210	150	280	5	NA	1.400	131072	468.114	467	130760.00
	
	#
	TEST_CASE__N_010K_210_15__EN   = 0x000000  # FS_14M00 = 210e6/ 15 # // 210	15	1400	10	NA	14.000	131072	93.623	89	124600.00
	TEST_CASE__N_010K_210_210__EN  = 0x000000  # FS_1M000 = 210e6/210    # // 210	210	100	10	1.000	131072	1310.72	1307	130700.00
	TEST_CASE__N_010K_210_70__EN   = 0x000000  # FS_3M000 = 210e6/ 70 # // 210	70	300	10	NA	3.000	131072	436.907	433	129900.00
	TEST_CASE__N_010K_210_75__EN   = 0x000000  # FS_2M800 = 210e6/ 75 # // 210	75	280	10	NA	2.800	131072	468.114	467	130760.00
	TEST_CASE__N_010K_210_14__EN   = 0x000000  # FS_15M00  = 210e6/14   # //  210	14	1500	10	15.000	131072	87.38133333	83	124500.00
	
	#
	TEST_CASE__N_020K_210_21__EN   = 0x000000  # FS_10M00 = 210e6/21 # // 210	21	500		20	NA	10.000	131072	262.144	257	128500.00
	TEST_CASE__N_020K_210_105__EN  = 0x000000  # FS_2M000 = 210e6/105    # // 210	105	100	20	2.000	131072	1310.72	1307	130700.00
	TEST_CASE__N_020K_210_35__EN   = 0x000000  # FS_6M000 = 210e6/ 35 # // 210	35	300	20	NA	6.000	131072	436.907	433	129900.00
	
	#
	TEST_CASE__N_050K_210_42__EN   = 0x000000  # FS_5M000 = 210e6/42     # // 210	42	100	50	5.000	131072	1310.72	1307	130700.00
	TEST_CASE__N_050K_210_21__EN   = 0x000000  # FS_10M00 = 210e6/ 21 # // 210	21	200	50	NA	10.000	131072	655.360	619	123800.00
	TEST_CASE__N_050K_210_15__EN   = 0x000000  # FS_14M00 = 210e6/ 15 # // 210	15	280	50	NA	14.000	131072	468.114	467	130760.00
	TEST_CASE__N_050K_210_14__EN   = 0x000000  # FS_15M00  = 210e6/14   # //  210	14	300	50	15.000	131072	436.9066667	433	129900.00
	
	#
	TEST_CASE__N_100K_210_15__EN   = 0x000000  # FS_14M00 = 210e6/15 # // 210	15	140	100		NA	14.000	131072	936.229		929		130060.00
	TEST_CASE__N_100K_210_14__EN   = 0x000000  # FS_15M00  = 210e6/14   # // 210	14	150	100	15.000	131072	873.8133333	863	129450.00
	
	#
	TEST_CASE__N_500K_210_15__EN   = 0x000000  # FS_14M00 = 210e6/15 # // 210	15	28	500		NA	14.000	131072	4681.143	4679	131012.00
	TEST_CASE__N_500K_210_14__EN   = 0x000000  # FS_15M00  = 210e6/14   # // 210	14	30	500	15.000	131072	4369.066667	4363	130890.00
	
	#
	TEST_CASE__N_200K_210_15__EN   = 0x000000  # FS_14M00 = 210e6/15 # // 210	15	70	200		NA	14.000	131072	1872.457	1871	130970.00
	TEST_CASE__N_200K_210_14__EN   = 0x000000  # FS_15M00  = 210e6/14   # // 210	14	75	200	15.000	131072	1747.626667	1747	131025.00
	
	#
	####
	
	#
	TEST_CASE__N_001K_210_1400__EN = 0  # FS_150K0 = 210e6/1400 # //  210	1400	150	1	0.150	131072	873.8133333	863	129450.00
	TEST_CASE__N_002K_210_700__EN  = 0  # FS_300K0 = 210e6/700  # //  210	700	150	2	0.300	131072	873.8133333	863	129450.00
	TEST_CASE__N_005K_210_280__EN  = 0  # FS_750K0 = 210e6/280  # //  210	280	150	5	0.750	131072	873.8133333	863	129450.00
	TEST_CASE__N_010K_210_140__EN  = 0  # FS_1M500  = 210e6/140  # //  210	140	150	10	1.500	131072	873.8133333	863	129450.00
	TEST_CASE__N_020K_210_70__EN   = 0  # FS_3M000   = 210e6/70   # //  210	70	150	20	3.000	131072	873.8133333	863	129450.00
	TEST_CASE__N_050K_210_28__EN   = 0  # FS_7M500  = 210e6/28   # //  210	28	150	50	7.500	131072	873.8133333	863	129450.00
	#
	
	
	#
	TEST_CASE__N_001K_210_1000__EN = 0x00    # FS_210K0 = 210e6/1000 # // 210	1000	210	1	NA	0.210	131072	624.152	619	129990.00
	TEST_CASE__N_002K_210_500__EN  = 0x00    # FS_420K0 = 210e6/ 500 # // 210	500		210	2	NA	0.420	131072	624.152	619	129990.00
	TEST_CASE__N_005K_210_200__EN  = 0x00    # FS_1M050 = 210e6/ 200 # // 210	200		210	5	NA	1.050	131072	624.152	619	129990.00
	TEST_CASE__N_010K_210_100__EN  = 0x00    # FS_2M100 = 210e6/ 100 # // 210	100		210	10	NA	2.100	131072	624.152	619	129990.00
	TEST_CASE__N_020K_210_50__EN   = 0x00    # FS_4M200 = 210e6/  50 # // 210	50		210	20	NA	4.200	131072	624.152	619	129990.00
	TEST_CASE__N_050K_210_20__EN   = 0x00    # FS_10M50 = 210e6/  20 # // 210	20		210	50	NA	10.500	131072	624.152	619	129990.00
	
	TEST_CASE__N_001K_210_840__EN = 0x00    # FS_250K0 = 210e6/840 # // 210	840	250	1	NA	0.250	131072	524.288	523	130750.00
	TEST_CASE__N_002K_210_420__EN = 0x00    # FS_500K0 = 210e6/420 # // 210	420	250	2	NA	0.500	131072	524.288	523	130750.00
	TEST_CASE__N_010K_210_84__EN  = 0x00    # FS_2M500 = 210e6/ 84 # // 210	84	250	10	NA	2.500	131072	524.288	523	130750.00
	TEST_CASE__N_020K_210_42__EN  = 0x00    # FS_5M000 = 210e6/ 42 # // 210	42	250	20	NA	5.000	131072	524.288	523	130750.00
	
	TEST_CASE__N_002K_210_300__EN = 0x00    # FS_700K0 = 210e6/300 # // 210	300	350	2	NA	0.700	131072	374.491	373	130550.00
	TEST_CASE__N_020K_210_30__EN  = 0x00    # FS_7M000 = 210e6/ 30 # // 210	30	350	20	NA	7.000	131072	374.491	373	130550.00
	
	#
	TEST_CASE__N_020K_210_14__EN   = 0  # FS_15M00  = 210e6/14   # //  210	14	750		20	15.000	131072	174.7626667	173	129750.00
	
	#
	TEST_CASE__N_001M_210_14__EN   = 0  # FS_15M00  = 210e6/14   # // 210	14	15	1000	15.000	131072	8738.133333	8737	131055.00
	
	TEST_CASE__N_002M_210_14__EN = 0x00    # FS_15M00 = 210e6/14 # // 210	14	7.5	2000	NA	15.000	131072	17476.267	17471	131032.50
	
	TEST_CASE__N_005M_210_14__EN   = 0  # FS_15M00  = 210e6/14   # // 210	14	3	5000	15.000	131072	43690.66667	43669	131007.00
	TEST_CASE__N_010M_210_14__EN   = 0  # FS_15M00  = 210e6/14   # // 210	14	1.5	10000	15.000	131072	87381.33333	87380	131070.00
	
	
	
	TEST_CASE__N_001M_210_15__EN = 0x00    # FS_14M00 = 210e6/15 # // 210	15	14	1000	NA	14.000	131072	9362.286	9349	130886.00
	
	TEST_CASE__N_002M_210_15__EN   = 0  # FS_14M00  = 210e6/15   # // 210	15	7	2000	14.000	131072	18724.57143	18719	131033.00
	
	TEST_CASE__N_005M_210_15__EN = 0x00    # FS_14M00 = 210e6/15 # // 210	15	2.8	5000	NA	14.000	131072	46811.429	46811	131070.80
	TEST_CASE__N_010M_210_15__EN = 0x00    # FS_14M00 = 210e6/15 # // 210	15	1.4	10000	NA	14.000	131072	93622.857	93607	131049.80
	
	#
	####
	
	
	
	## undersampling cases 
	#
	# selected list 
	#
	#CASE_NO = 'U_010K_210_21010_'    # OK 30ppm
	#CASE_NO = 'U_010K_210_21014_'    # OK 50ppm
	#CASE_NO = 'U_010K_210_21007_'    # OK 50ppm
	#
	#CASE_NO = 'U_020K_210_10507_'    # OK 50ppm
	#CASE_NO = 'U_020K_210_10505_'    # OK 40ppm
	#CASE_NO = 'U_020K_210_10501_'    # OK 50ppm
	#
	#CASE_NO = 'U_050K_210_4203_'     # OK 50ppm
	#CASE_NO = 'U_050K_210_4202_'     # OK 40ppm
	#
	#CASE_NO = 'U_100K_210_2101_'     # OK 50ppm
	#CASE_NO = 'U_100K_210_2102_'     # OK 40ppm 
	#
	#CASE_NO = 'U_200K_210_1051_'     # OK 40ppm 
	#
	#CASE_NO = 'U_500K_210_421_'      # OK 40ppm
	#
	#CASE_NO = 'U_001M_210_211_'      # OK 100ppm 
	#
	#CASE_NO = 'U_002M_210_106_'      # OK 120ppm 
	#
	#CASE_NO = 'U_005M_210_43_'       # OK 170ppm
	#
	#CASE_NO = 'U_010M_210_43_'       # OK 300ppm ## expected
	#
	####
	
	#
	TEST_CASE__U_010K_210_21010__EN  = 0x0001 # FS_9K995   = 210e6/ 21010  # // 210	2100	0.01	21010	0.004759638267	0.009995	131072	62.4152381	61	128100.00
	TEST_CASE__U_020K_210_10505__EN  = 0x0001 # FS_19K99   = 210e6/ 10505  # // 210	2100	0.02	10505	0.009519276535	0.019990	131072	62.4152381	61	128100.00
	TEST_CASE__U_050K_210_4202__EN   = 0x0001 # FS_49K98   = 210e6/  4202  # // 210	2100	0.05	4202	0.02379819134	0.049976	131072	62.4152381	61	128100.00
	TEST_CASE__U_100K_210_2101__EN   = 0x0001 # FS_99K95   = 210e6/  2101  # // 210	2101	2100	0.1		0.047596	0.099952	131072	62.415	61	128100.00
	TEST_CASE__U_200K_210_1051__EN   = 0x0001 # FS_199K8 = 210e6/  1051    # // 210	1050	0.2	1051	0.1902949572	0.199810	131072	124.8304762	113	118650.00
	TEST_CASE__U_500K_210_421__EN    = 0x0001 # FS_498K8 = 210e6/   421    # // 210	420	0.5	421	1.187648456	0.498812	131072	312.0761905	311	130620.00
	TEST_CASE__U_001M_210_211__EN    = 0x0001 # FS_995K3 = 210e6/   211    # // 210	210	1	211	4.739336493	0.995261	131072	624.152381	619	129990.00
	TEST_CASE__U_002M_210_106__EN    = 0x0001 # FS_1M981 = 210e6/   106    # // 210	105		2		106		18.86792453			1.981132	131072	1248.304762	1237	129885.00
	TEST_CASE__U_005M_210_43__EN     = 0x0001 # FS_4M884 = 210e6/    43    # // 210	42		5		43		116.2790698			4.883721	131072	3120.761905	3119	130998.00
	TEST_CASE__U_010M_210_43__EN     = 0x0001 # FS_4M884 = 210e6/    43    # // 210	43		21		10		232.558140			4.883721	131072	6241.524	6229	130809.00
	#
	####
	
	TEST_CASE__U_010K_210_21007__EN  = 0x00000  # FS_9K9967  = 210e6/ 21007  # // 210	21007	3000	0.01	0.003332	0.009997	131072	43.691	61	183000.00
	TEST_CASE__U_010K_210_21014__EN  = 0x00000  # FS_9K9933  = 210e6/ 21014  # // 210	21014	1500	0.01	0.006662	0.009993	131072	87.381	61	91500.00
	TEST_CASE__U_020K_210_10501__EN  = 0x00000  # FS_19K998  = 210e6/ 10501  # // 210	10501	10500	0.02	0.001905	0.019998	131072	12.483	61	640500.00
	TEST_CASE__U_020K_210_10507__EN  = 0x00000  # FS_19K987  = 210e6/ 10507  # // 210	10507	1500	0.02	0.013324	0.019987	131072	87.381	61	91500.00
	TEST_CASE__U_050K_210_4203__EN   = 0x00000  # FS_49K96   = 210e6/  4203  # // 210	4203	1400	0.05	0.035689	0.049964	131072	93.623	61	85400.00
	TEST_CASE__U_100K_210_2102__EN   = 0x00000  # FS_99K91   = 210e6/  2102  # // 210	1050	0.1		2102	0.09514747859		0.099905	131072	124.8304762	113		118650.00


	# poor variance rather than normal sampling
	TEST_CASE__U_001K_210_210100__EN = 0x000   # FS_1K000 = 210e6/210100 # // 210	2100	0.001	210100	0.0004759638267	0.001000	131072	62.4152381	61	128100.00
	TEST_CASE__U_002K_210_105050__EN = 0x000   # FS_1K999 = 210e6/105050 # // 210	2100	0.002	105050	0.0009519276535	0.001999	131072	62.4152381	61	128100.00
	TEST_CASE__U_005K_210_42020__EN  = 0x000   # FS_4K998 = 210e6/ 42020 # // 210	2100	0.005	42020	0.002379819134	0.004998	131072	62.4152381	61	128100.00
	
	#
	TEST_CASE__U_001M_210_212__EN    = 0  # FS_990K6 = 210e6/   212 # // 210	105		1		212		9.433962264			0.990566	131072	1248.304762	1237	129885.00
	TEST_CASE__U_010M_210_41__EN     = 0  # FS_5M122 = 210e6/    41    # // 210	41	1.05	10	243.902439(2*Fs image)	5.121951	131072	6241.524	6229	130809.00
	
	
	#
	TEST_CASE__U_001K_210_210125__EN = 0x0000000  # FS_0K99941 = 210e6/210125 =  999.41 # // 1		210	210125	1680	0.001	0.000595	0.000999	16384	9.752	7	11760.00
	TEST_CASE__U_001K_210_210105__EN = 0x0000000  # FS_0K99950 = 210e6/210105 =  999.50 # // 1		210	210105	2000	0.001	0.000500	0.001000	16384	8.192	7	14000.00
	TEST_CASE__U_001K_210_210075__EN = 0x0000000  # FS_0K99964 = 210e6/210075 =  999.64 # // 1		210	210075	2800	0.001	0.000357	0.001000	16384	5.851	5	14000.00
	TEST_CASE__U_001K_210_210035__EN = 0x0000000  # FS_0K99983 = 210e6/210035 =  999.83 # // 1		210	210035	6000	0.001	0.000167	0.001000	16384	2.731	2	12000.00
	TEST_CASE__U_001K_210_210025__EN = 0x0000000  # FS_0K99988 = 210e6/210025 =  999.88 # // 1		210	210025	8400	0.001	0.000119	0.001000	16384	1.950	1	8400.00
	TEST_CASE__U_001K_210_210015__EN = 0x0000000  # FS_0K99993 = 210e6/210015 =  999.93 # // 1		210	210015	14000	0.001	0.000071	0.001000	16384	1.170	1	14000.00
	TEST_CASE__U_002K_210_105075__EN = 0x0000000  # FS_1K99857 = 210e6/105075 = 1998.57 # // 2		210	105075	1400	0.002	0.001428	0.001999	16384	11.703	7	9800.00
	TEST_CASE__U_002K_210_105070__EN = 0x0000000  # FS_1K99867 = 210e6/105070 = 1998.67 # // 2		210	105070	1500	0.002	0.001332	0.001999	16384	10.923	7	10500.00
	TEST_CASE__U_002K_210_105035__EN = 0x0000000  # FS_1K99933 = 210e6/105035 = 1999.33 # // 2		210	105035	3000	0.002	0.000666	0.001999	16384	5.461	7	21000.00
	TEST_CASE__U_005K_210_42025__EN  = 0x0000000  # FS_4K9970  = 210e6/ 42025 =  4997.0 # // 5		210	42025	1680	0.005	0.002974	0.004997	16384	9.752	7	11760.00
	TEST_CASE__U_005K_210_42021__EN  = 0x0000000  # FS_4K9975  = 210e6/ 42021 =  4997.5 # // 5		210	42021	2000	0.005	0.002499	0.004998	16384	8.192	7	14000.00
	TEST_CASE__U_005K_210_42015__EN  = 0x0000000  # FS_4K9982  = 210e6/ 42015 =  4998.2 # // 5		210	42015	2800	0.005	0.001785	0.004998	16384	5.851	7	19600.00
	TEST_CASE__U_010K_210_21015__EN  = 0x0000000  # FS_9K9929  = 210e6/ 21015 =  9992.9 # // 10		210	21015	1400	0.01	0.007138	0.009993	131072	93.623	61	85400.00
	
	TEST_CASE__U_020K_210_10503__EN  = 0x0000000  # FS_19K994  = 210e6/ 10503 = 19994.3 # // 20		210	10503	3500	0.02	0.005713	0.019994	131072	37.449	61	213500.00
	
	TEST_CASE__U_050K_210_4250__EN   = 0x00000000  # FS_49K41 = 210e6/  4250  # // 210	84	0.05	4250	0.5882352941	0.049412	131072	1560.380952	1559	130956.00	
	TEST_CASE__U_050K_210_4204__EN   = 0x00000000  # FS_49K95 = 210e6/  4204 # // 210	1050	0.05	4204	0.0475737393		0.049952	131072	124.8304762	113		118650.00
	TEST_CASE__U_050K_210_4201__EN   = 0x00000000  # FS_49K99 = 210e6/  4201 # // 210	4200	0.05	4201	0.01190192811	0.049988	131072	31.20761905	31	130200.00
	
	TEST_CASE__U_100K_210_2125__EN   = 0x00000000  # FS_98K82 = 210e6/  2125  # // 210	84	0.1		2125	1.176470588		0.098824	131072	1560.380952	1559	130956.00	
	
	TEST_CASE__U_200K_210_1056__EN   = 0x00000000  # FS_198K9 = 210e6/  1056 # // 210	175	0.2	1056	1.136363636		0.198864	131072	748.9828571	743	130025.00
	TEST_CASE__U_200K_210_1053__EN   = 0x00000000  # FS_199K4 = 210e6/  1053 # // 210	350	0.2	1053	0.5698005698	0.199430	131072	374.4914286	373	130550.00
	
	
	TEST_CASE__U_500K_210_424__EN    = 0x00000000  # FS_495K3 = 210e6/   424 # // 210	105		0.5		424		4.716981132			0.495283	131072	1248.304762	1237	129885.00
	
	#
	TEST_CASE__U_010M_210_24__EN     = 0x000000000  # FS_8M750 = 210e6/    24 # // 10000	210	24	7	10	1250.000000	8.750000	131072	18724.571	18719	131033.00
	TEST_CASE__U_010M_210_28__EN     = 0x000000000  # FS_7M500 = 210e6/    28 # // 10000	210	28	3	10	2500.000000	7.500000	131072	43690.667	43669	131007.00
	
	TEST_CASE__U_010M_210_22__EN     = 0x00000000  # FS_9M545 = 210e6/    22 # // 210	21		10		22		454.5454545			9.545455	131072	6241.52381	6229	130809.00
	
	#
	TEST_CASE__U_001K_210_212000__EN = 0  # FS_0K991 = 210e6/212000 # // 210	105	0.001	212000	0.009433962264	0.000991	131072	1248.304762	1237	129885.00
	TEST_CASE__U_002K_210_106000__EN = 0  # FS_1K981 = 210e6/106000 # // 210	105	0.002	106000	0.01886792453	0.001981	131072	1248.304762	1237	129885.00
	TEST_CASE__U_005K_210_42400__EN  = 0  # FS_4K953 = 210e6/ 42400 # // 210	105	0.005	42400	0.04716981132	0.004953	131072	1248.304762	1237	129885.00
	TEST_CASE__U_010K_210_21004__EN  = 0  # FS_9K998 = 210e6/ 21004 # // 210	5250	0.01	21004	0.001904399162		0.009998	131072	24.96609524	23		120750.00
	TEST_CASE__U_020K_210_10510__EN  = 0  # FS_19K98 = 210e6/ 10510 # // 210	1050	0.02	10510	0.01902949572		0.019981	131072	124.8304762	113		118650.00
	#
	TEST_CASE__U_050K_210_4240__EN   = 0  # FS_49K53 = 210e6/  4240 # // 210	105	0.05	4240	0.4716981132	0.049528	131072	1248.304762	1237	129885.00
	#
	TEST_CASE__U_200K_210_1060__EN   = 0  # FS_198K1 = 210e6/  1060 # // 210	105		0.2		1060	1.886792453			0.198113	131072	1248.304762	1237	129885.00
	#
	TEST_CASE__U_200K_210_1064__EN   = 0  # FS_197K4 = 210e6/  1064 # // 210	75	0.2	1064	2.631578947		0.197368	131072	1747.626667	1747	131025.00
	TEST_CASE__U_200K_210_1075__EN   = 0  # FS_195K3 = 210e6/  1075 # // 210	42	0.2	1075	4.651162791		0.195349	131072	3120.761905	3119	130998.00
	TEST_CASE__U_200K_210_1071__EN   = 0  # FS_196K1 = 210e6/  1071 # // 210	50	0.2	1071	3.921568627		0.196078	131072	2621.44	2621	131050.00
	TEST_CASE__U_200K_210_1055__EN   = 0  # FS_199K1 = 210e6/  1055 # // 210	210	0.2	1055	0.9478672986	0.199052	131072	624.152381	619	129990.00
	#
	# 84 cyc undersample
	TEST_CASE__U_200K_210_1065__EN   = 0  # FS_197K1 = 210e6/  1065  # // 210	70	0.2		1065	2.816901408		0.197183	131072	1872.457143	1747	122290.00	
	
	# 150 cyc undersample
	TEST_CASE__U_050K_210_4228__EN   = 0  # FS_49K66 = 210e6/  4228  # // 210	150	0.05	4228	0.3311258278	0.049669	131072	873.8133333	863	129450.00
	TEST_CASE__U_100K_210_2114__EN   = 0  # FS_99K33 = 210e6/  2114  # // 210	150	0.1		2114	0.6622516556	0.099338	131072	873.8133333	863	129450.00
	TEST_CASE__U_200K_210_1057__EN   = 0  # FS_198K6 = 210e6/  1057  # // 210	150	0.2		1057	1.324503311		0.198675	131072	873.8133333	863	129450.00
	####

####
## library call
import ok_cmu_cpu__lib as cmu
#
# check configuration : OK_EP_ADRS_CONFIG
#  
EP_ADRS = cmu.conf.OK_EP_ADRS_CONFIG
#  
def form_contents(var,idx):
	return '{} = {}'.format(idx, var[idx])
#
def form_address(var,idx):
	return '{} @ {:#04x}'.format(idx, var[idx])
#  
print(form_contents(EP_ADRS,'board_name'))
print(form_contents(EP_ADRS,'ver'))
print(form_contents(EP_ADRS,'bit_filename'))
#
print(form_address(EP_ADRS,'FPGA_IMAGE_ID'))
#
####

####
## init : dev
dev = cmu.ok_cmu_init()
print(dev)
####

####
ret = cmu.ok_cmu_caller_id()
print(ret)
####

####
## open
ret = cmu.ok_cmu_open(FPGA_SERIAL)
print(ret)
####
DevSR =  ret[1]
if not DevSR:  # no device opened 
	# retry
	print('>>> cmu open failed!')
	#
	MAX_RETRY = 10 ###
	INTERVAL_RETRY = 1 # sec 
	cnt_retry_open = 0
	while True:
		cnt_retry_open += 1
		print('>>> retry: {}'.format(cnt_retry_open))
		cmu.sleep(INTERVAL_RETRY)
		ret = cmu.ok_cmu_open(FPGA_SERIAL)
		DevSR =  ret[1]
		if DevSR: # device opened 
			print('>>> dev opend: {}'.format(DevSR))
			break 
		#
		if cnt_retry_open >= MAX_RETRY:
			# max reached 
			print('>>> cmu open retry failed!')
			#exit(0)
			#raise SystemExit(0)
			#exit()
			break
	#

####
## device opened 
if DevSR: 
	
	####
	## FPGA_CONFIGURE
	if FPGA_CONFIGURE==1: 
		ret = cmu.ok_cmu_conf(BIT_FILENAME)
		print(ret)
	####  
	
	####
	## read fpga_image_id
	fpga_image_id = cmu.cmu_read_fpga_image_id()
	#
	def form_hex_32b(val):
		return '0x{:08X}'.format(val)
	#
	print(form_hex_32b(fpga_image_id))
	####
	
	####
	## read FPGA internal temp and volt
	ret = cmu.cmu_monitor_fpga()
	print(ret)
	#
	TEMP_FPGA = ret[0]
	print(TEMP_FPGA)
	####
	
	####
	## test counter on 
	ret = cmu.cmu_test_counter('ON')
	print(ret)
	####
	
	###################################################
	## subfunctions
	
	##TODO: def read_board_condition(TEST_CONF)
	def read_board_condition(TEST_CONF={}):
		## read and add temperature to info
		ret = cmu.cmu_monitor_fpga()
		print(ret)
		#
		TEMP_FPGA = ret[0]
		#
		TEST_CONF['TEMP_FPGA'] = TEMP_FPGA
		return None
	
	##TODO: def setup____test_signal()
	def setup____test_signal(TEST_CONF={}): 
		## SPO ##
		cmu.cmu_spo_enable()
		cmu.cmu_spo_init()
		#
		# led on
		cmu.cmu_spo_bit__leds(0xFF) 
		#
		# AMP power control
		#cmu.cmu_spo_bit__amp_pwr('ON')
		#cmu.cmu_spo_bit__amp_pwr('OFF')
		try: 
			cmu.cmu_spo_bit__amp_pwr(TEST_CONF['AMP_PWR_CTRL'])
		except: 
			print('> missing AMP_PWR_CTRL in TEST_CONF!')	
		#
		# ADC gain control 
		#cmu.cmu_spo_bit__adc0_gain('1X')
		#cmu.cmu_spo_bit__adc0_gain('10X')
		#cmu.cmu_spo_bit__adc0_gain('100X')
		#cmu.cmu_spo_bit__adc1_gain('1X')
		#cmu.cmu_spo_bit__adc1_gain('10X')
		#cmu.cmu_spo_bit__adc1_gain('100X')
		try: 
			cmu.cmu_spo_bit__adc0_gain(TEST_CONF['AMP_GAIN_CTRL'])
			cmu.cmu_spo_bit__adc1_gain(TEST_CONF['AMP_GAIN_CTRL'])
		except: 
			print('> missing FILT_PATH_CTRL in AMP_GAIN_CTRL!')	
		#
		# filter path control
		## BW 120MHz 
		#cmu.cmu_spo_bit__vi_bw('120M')
		#cmu.cmu_spo_bit__vq_bw('120M')
		## BW 12MHz 
		#cmu.cmu_spo_bit__vi_bw('12M')
		#cmu.cmu_spo_bit__vq_bw('12M')
		## BW 1.2MHz 
		#cmu.cmu_spo_bit__vi_bw('1M2')
		#cmu.cmu_spo_bit__vq_bw('1M2')
		## BW 120kHz 
		#cmu.cmu_spo_bit__vi_bw('120K')
		#cmu.cmu_spo_bit__vq_bw('120K')
		try: 
			cmu.cmu_spo_bit__vi_bw(TEST_CONF['FILT_PATH_CTRL'])
			cmu.cmu_spo_bit__vq_bw(TEST_CONF['FILT_PATH_CTRL'])
		except: 
			print('> missing FILT_PATH_CTRL in TEST_CONF!')	
		#
		## DAC ##
		cmu.cmu_dac_bias_enable()
		cmu.cmu_dac_bias_init()
		#
		# set DAC code 
		DAC1_CODE = __DAC_CODE
		DAC2_CODE = __DAC_CODE
		## for filter-out monitoring (making small)
		#DAC3_CODE = 0x0800
		#DAC4_CODE = 0x0800
		#DAC3_CODE = 0x1000 # normal 
		#DAC4_CODE = 0x1000
		#DAC3_CODE = 0x2000
		#DAC4_CODE = 0x2000
		#DAC3_CODE = 0x2800
		#DAC4_CODE = 0x2800
		#DAC3_CODE = 0x4000
		#DAC4_CODE = 0x4000
		## for 8f-4level monitoring
		#DAC3_CODE = 0x2000 
		#DAC4_CODE = 0x2000
		#
		DAC3_CODE = 0x0000
		DAC4_CODE = 0x0000
		try: 
			DAC3_CODE = TEST_CONF['DAC_CODE']
			DAC4_CODE = TEST_CONF['DAC_CODE']
		except: 
			print('> missing DAC_CODE in TEST_CONF!')		
		#
		cmu.cmu_dac_bias_set_buffer(
				DAC1=DAC1_CODE,
				DAC2=DAC2_CODE,
				DAC3=DAC3_CODE,
				DAC4=DAC4_CODE)
		#
		cmu.cmu_dac_bias_update()
		#
		ret = cmu.cmu_dac_bias_readback()
		print(ret)
		#
		## DWAVE ##
		#
		# pin swap control 
		if DWAVE_PIN_SWAP_DISABLE == 1:
			cmu.cmu_dwave_pair_ch_pin_swap_disable()
		else:
			cmu.cmu_dwave_pair_ch_pin_swap_enable()
		#
		# R idle warm up 
		if DWAVE_IDLE_WARMUP_DISABLE == 1:
			cmu.cmu_dwave_r_idle_warmup_disable()
		else:
			cmu.cmu_dwave_r_idle_warmup_enable()
		#
		# enable
		cmu.cmu_dwave_enable()
		#
		# read dwave base freq
		DWAVE_BASE_FREQ = cmu.cmu_dwave_read_base_freq()
		print('{}: {:#8.3f} MHz\r'.format('DWAVE_BASE_FREQ',DWAVE_BASE_FREQ/1e6))
		#
		try: 
			if (DWAVE_BASE_FREQ!=TEST_CONF['DWAVE_BASE_FREQ']):
				print('>>> {}: {}'.format('Warning!: DWAVE_BASE_FREQ is not matched',TEST_CONF['DWAVE_BASE_FREQ']))
				input('Press Enter key!')
		except: 
			print('> missing DWAVE_BASE_FREQ in TEST_CONF!')		
		#  
		try: 
			CNT_PERIOD = TEST_CONF['CNT_PERIOD']
		except: 
			CNT_PERIOD = 80 
			print('> missing CNT_PERIOD in TEST_CONF!')
		#
		try: 
			CNT_DIFF = TEST_CONF['CNT_DIFF']
		except: 
			CNT_DIFF   = 60
			print('> missing CNT_DIFF in TEST_CONF!')		
		#  
		# set CNT_PERIOD
		cmu.cmu_dwave_wr_cnt_period(CNT_PERIOD)
		#
		# set CNT_DIFF
		cmu.cmu_dwave_wr_cnt_diff(CNT_DIFF)
		#
		# dwave output control
		if TEST_PATH_I_ONLY_EN:
			cmu.cmu_dwave_wr_output_dis__enable_path_i_only()
		elif TEST_PATH_Q_ONLY_EN:
			cmu.cmu_dwave_wr_output_dis__enable_path_q_only()
		else:
			cmu.cmu_dwave_wr_output_dis__enable_all()
		#
		# set dwave parameters into core logic
		cmu.cmu_dwave_set_para()
		#
		return None
	
	def turn_on__test_signal(TEST_CONF={}):
		# dwave_pulse_on
		cmu.cmu_dwave_pulse_on()
		#
		# wait for pulse stability
		cmu.sleep(1)
		#
		return None
	
	##TODO: def capture__adc_samples()
	def capture__adc_samples(TEST_CONF={}):
		## ADC ##
		#
		# reset adc status
		cmu.cmu_adc_reset()
		#
		# enable
		cmu.cmu_adc_enable()
		#
		# read adc base freq
		ADC_BASE_FREQ = cmu.cmu_adc_read_base_freq()
		print('{}: {:#8.3f} MHz\r'.format('ADC_BASE_FREQ',ADC_BASE_FREQ/1e6))
		#
		# check adc base freq
		try: 
			if (ADC_BASE_FREQ!=TEST_CONF['ADC_BASE_FREQ']):
				print('>>> {}: {}'.format('Warning!: ADC_BASE_FREQ is not matched',TEST_CONF['ADC_BASE_FREQ']))
				input('Press Enter key!')
		except: 
			print('> missing ADC_BASE_FREQ in TEST_CONF!')		
		#
		# set adc sampling freq
		try:
			ADC_CNT_SAMPLE_PERIOD = TEST_CONF['ADC_CNT_SAMPLE_PERIOD']
		except:
			# base 210MHz and base 125MHz ... common integer...
			# 210 = 7 * 3 * 2 * 5 ... 14 min
			# 210MHz/15 = 14 Msps 
			# 125 = 5 * 5 * 5     ... 12 min
			# 125MHz/15 = 8.33333333 Msps
			ADC_CNT_SAMPLE_PERIOD = 15
			print('> missing ADC_CNT_SAMPLE_PERIOD in TEST_CONF!')		
		#
		cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_SMP_PRD',ADC_CNT_SAMPLE_PERIOD)
		#
		# set the number of adc samples
		try:
			ADC_NUM_SAMPLES = TEST_CONF['ADC_NUM_SAMPLES']
		except:
			ADC_NUM_SAMPLES = 100
			print('> missing ADC_NUM_SAMPLES in TEST_CONF!')		
		cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_UPD_SMP',ADC_NUM_SAMPLES)
		#
		# set adc data line input delay tap //$$ TODO: ADC_INPUT_DELAY_TAP rev
		# ADC_INPUT_DELAY_TAP_H
		# ADC_INPUT_DELAY_TAP_L
		try:
			#ADC_INPUT_DELAY_TAP = TEST_CONF['ADC_INPUT_DELAY_TAP'] 
			ADC0_INPUT_DELAY_TAP_L = TEST_CONF['ADC0_INPUT_DELAY_TAP_L'] 
			ADC0_INPUT_DELAY_TAP_H = TEST_CONF['ADC0_INPUT_DELAY_TAP_H'] 
			ADC1_INPUT_DELAY_TAP_L = TEST_CONF['ADC1_INPUT_DELAY_TAP_L'] 
			ADC1_INPUT_DELAY_TAP_H = TEST_CONF['ADC1_INPUT_DELAY_TAP_H'] 
		except:
			#ADC_INPUT_DELAY_TAP = 0
			print('> missing ADC_INPUT_DELAY_TAP in TEST_CONF!')		
		#ADC_INPUT_DELAY_TAP_code = (ADC_INPUT_DELAY_TAP<<27)|(ADC_INPUT_DELAY_TAP<<22)
		#ADC_INPUT_DELAY_TAP_code = (ADC_INPUT_DELAY_TAP<<27)|(ADC_INPUT_DELAY_TAP<<22)|(ADC_INPUT_DELAY_TAP<<17)|(ADC_INPUT_DELAY_TAP<<12)
		ADC_INPUT_DELAY_TAP_code = (ADC1_INPUT_DELAY_TAP_H<<27)|(ADC1_INPUT_DELAY_TAP_L<<22)|(ADC0_INPUT_DELAY_TAP_H<<17)|(ADC0_INPUT_DELAY_TAP_L<<12)
		val = ADC_INPUT_DELAY_TAP_code
		#msk = 0xFFC00000
		msk = 0xFFFFF000
		cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_DLY_TAP_OPT',val,msk)
		#
		# set up  test mode
		PIN_TEST_FRC_HIGH = 0
		PIN_DLLN_FRC_LOW  = 0
		PTTN_CNT_UP_EN    = 0
		ADC_control_code = (PTTN_CNT_UP_EN<<2)|(PIN_DLLN_FRC_LOW<<1)|PIN_TEST_FRC_HIGH
		val = ADC_control_code
		msk = 0x00000007
		cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_DLY_TAP_OPT',val,msk)
		#
		#
		# initialize adc
		ret = cmu.cmu_adc_init()
		if ret == 0:
			return None
		#while ret == 0:
		#	ret = cmu.cmu_adc_init() # repeat init 
		#	input('> Retry cmu_adc_init... Enter...')
		#
		# cmu_check_adc_test_pattern
		ret = cmu.cmu_check_adc_test_pattern()
		print(ret)
		if ret == False:
			# input('> Please check ADC ... test pattern is bad! ')
			return None
		#
		# cmu_adc_is_fifo_empty
		ret = cmu.cmu_adc_is_fifo_empty()
		print(ret)
		#
		# cmu_adc_update
		ret = cmu.cmu_adc_update()
		if ret == 0:
			return None
		#while ret == 0:
		#	input('> Retry cmu_adc_update... Enter...')
		#	ret = cmu.cmu_adc_update() # repeat update 
		#
		#
		# cmu_adc_load_from_fifo
		#adc_list = cmu.cmu_adc_load_from_fifo(ADC_NUM_SAMPLES=131072, ADC_BITWIDTH=18)
		#adc_list = cmu.cmu_adc_load_from_fifo(__ADC_NUM_SAMPLES, __ADC_BITWIDTH)
		adc_list = cmu.cmu_adc_load_from_fifo(ADC_NUM_SAMPLES, __ADC_BITWIDTH)
		#
		# disable
		cmu.cmu_adc_disable()
		#
		try: 
			TEST_CONF['ADC_LIST_INT'] = adc_list
		except: 
			print('> missing ADC_LIST_INT in TEST_CONF!')		
		#
		return adc_list 
	
	def turn_off_test_signal(TEST_CONF={}):
		## DWAVE ##
		# dwave_pulse_off
		cmu.cmu_dwave_pulse_off()
		cmu.cmu_dwave_disable()
		## DAC ##
		# set default values
		cmu.cmu_dac_bias_set_buffer() 
		cmu.cmu_dac_bias_update() 
		cmu.cmu_dac_bias_disable() 
		## SPO ##
		# AMP power off
		cmu.cmu_spo_bit__amp_pwr('OFF')
		# LED off
		cmu.cmu_spo_bit__leds(0x00)
		cmu.cmu_spo_disable()
		#
		return None
	
		
	def display__adc_samples(TEST_CONF={}):
		try:
			#FS_TARGET     = TEST_CONF['FS_TARGET'    ]
			FS_TRUE       = TEST_CONF['FS_TRUE'      ]
			DUMP_FILE_PRE = TEST_CONF['DUMP_FILE_PRE']
			ADC_LIST_INT  = TEST_CONF['ADC_LIST_INT' ]
			NUM_SMP_ZOOM  = TEST_CONF['NUM_SMP_ZOOM' ]
		except:
			return False
		#
		#cmu.cmu_adc_display_data_list_int (ADC_LIST_INT,FS_TARGET)
		#fig_filename = cmu.cmu_adc_display_data_list_int__zoom (ADC_LIST_INT,DUMP_FILE_PRE,FS_TARGET,NUM_SMP_ZOOM)
		fig_filename = cmu.cmu_adc_display_data_list_int__zoom (ADC_LIST_INT,DUMP_FILE_PRE,FS_TRUE,NUM_SMP_ZOOM)
		return fig_filename 
	
	def save__adc_samples_into_csv_file(TEST_CONF={}):
		try:
			#FS_TARGET     = TEST_CONF['FS_TARGET'    ]
			FS_TRUE        = TEST_CONF['FS_TRUE'      ]
			DUMP_FILE_PRE  = TEST_CONF['DUMP_FILE_PRE']
			ADC_LIST_INT   = TEST_CONF['ADC_LIST_INT' ]
			TEMP_FPGA      = TEST_CONF['TEMP_FPGA']
			WARN_RETRY_CNT = TEST_CONF['warning__adc_retry_count']
		except:
			return False
		# adc_save_data_list_int_to_csv
		#csv_filename = cmu.cmu_adc_save_data_list_int_to_csv(ADC_LIST_INT,DUMP_FILE_PRE,FS_TARGET)
		#csv_filename = cmu.cmu_adc_save_data_list_int_to_csv(ADC_LIST_INT,DUMP_FILE_PRE,FS_TRUE)
		csv_filename = cmu.cmu_adc_save_data_list_int_to_csv(ADC_LIST_INT,DUMP_FILE_PRE,FS_TRUE,TEMP_FPGA,WARN_RETRY_CNT)
		return csv_filename 
	
	def save__conf_result_dict_into_dict_file(TEST_CONF={}):
		ret = None
		#
		if TEST_CONF:
			#
			ret = cmu.cmu_adc_save_conf_result_dict_to_file(TEST_CONF)
			#
		#
		return ret
		
	
	##TODO: def conduct__test_conf() // retry  capture__adc_samples
	def conduct__test_conf(TEST_CONF):
		## conduct test
		#
		read_board_condition(TEST_CONF)
		#
		setup____test_signal(TEST_CONF)
		turn_on__test_signal(TEST_CONF)
		ret = capture__adc_samples(TEST_CONF)
		while ret == None:
			#input('adc data failed... retry... Enter...')
			# retry count up
			TEST_CONF['warning__adc_retry_count'] += 1
			print('adc data failed... retry. : {}'.format(TEST_CONF['warning__adc_retry_count']))
			ret = capture__adc_samples(TEST_CONF)
			if TEST_CONF['warning__adc_retry_count'] == 100:
				print('no more retry. : {}'.format(TEST_CONF['warning__adc_retry_count']))
				break
		#
		#
		turn_off_test_signal(TEST_CONF)
		#
		## show and save figure
		fig_filename = display__adc_samples(TEST_CONF)
		print(fig_filename)
		#
		## save csv
		csv_filename = save__adc_samples_into_csv_file(TEST_CONF)
		print(csv_filename)
		#
		## save dict
		if DUMP_DICT_DIS == 0:
			dict_filename = save__conf_result_dict_into_dict_file(TEST_CONF)
			print(dict_filename)
		#
		return None
	
	#######################################################################
	
	####
	# common setup
	#TEST_CONF = {}
	#
	
	
	##TODO: (1) test case 0-0: no signal / noise level check @ 15.0Msps
	if c_case_info.TEST_CASE_0_0_EN:
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = '0-0' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'OFF' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9  ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	##TODO: TEST_CASE N_001K
	if c_case_info.TEST_CASE__N_001K_210_2100__EN :  # FS_100K0 = 210e6/2100 # // 210	2100	100	1	0.100	131072	1310.72	1307	130700.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001K_210_2100' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_100K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_001K_210_14__EN   :  # FS_15M00  = 210e6/14   # //  210	14	15000	1	15.000	131072	8.738133333	7	105000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001K_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_001K_210_700__EN  :  # FS_300K0 = 210e6/700 # // 210	700	300	1	NA	0.300	131072	436.907	433	129900.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001K_210_700' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_300K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_001K_210_750__EN  :  # FS_280K0 = 210e6/750 # // 210	750	280	1	NA	0.280	131072	468.114	467	130760.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001K_210_750' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_280K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_001K_210_150__EN  :  # FS_1M400 = 210e6/150 # // 210	150	1400	1	NA	1.400	131072	93.623	89	124600.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001K_210_150' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1M400  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_001K_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	14000	1	NA	14.000	131072	9.362	7	98000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001K_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_001K_210_1400__EN :  # FS_150K0 = 210e6/1400 # //  210	1400	150	1	0.150	131072	873.8133333	863	129450.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001K_210_1400' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_150K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_001K_210_1000__EN :  # FS_210K0 = 210e6/1000 # // 210	1000	210	1	NA	0.210	131072	624.152	619	129990.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001K_210_1000' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_210K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_001K_210_840__EN  :  # FS_250K0 = 210e6/840 # // 210	840	250	1	NA	0.250	131072	524.288	523	130750.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001K_210_840' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_250K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	##TODO: TEST_CASE N_002K
	if c_case_info.TEST_CASE__N_002K_210_1050__EN :  # FS_200K0 = 210e6/1050 # // 210	1050	100	2	0.200	131072	1310.72	1307	130700.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002K_210_1050' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_200K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_002K_210_14__EN   :  # FS_15M00  = 210e6/14   # //  210	14	7500	2	15.000	131072	17.47626667	17	127500.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002K_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_002K_210_350__EN  :  # FS_600K0 = 210e6/350 # // 210	350	300	2	NA	0.600	131072	436.907	433	129900.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002K_210_350' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_600K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_002K_210_75__EN   :  # FS_2M800 = 210e6/ 75 # // 210	75	1400	2	NA	2.800	131072	93.623	89	124600.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002K_210_75' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_2M800  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_002K_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	7000	2	NA	14.000	131072	18.725	17	119000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002K_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_002K_210_700__EN  :  # FS_300K0 = 210e6/700  # //  210	700	150	2	0.300	131072	873.8133333	863	129450.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002K_210_700' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_300K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_002K_210_500__EN  :  # FS_420K0 = 210e6/ 500 # // 210	500		210	2	NA	0.420	131072	624.152	619	129990.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002K_210_500' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_420K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_002K_210_420__EN  :  # FS_500K0 = 210e6/420 # // 210	420	250	2	NA	0.500	131072	524.288	523	130750.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002K_210_420' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_500K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_002K_210_300__EN  :  # FS_700K0 = 210e6/300 # // 210	300	350	2	NA	0.700	131072	374.491	373	130550.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002K_210_300' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_700K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	##TODO: TEST_CASE N_005K
	if c_case_info.TEST_CASE__N_005K_210_420__EN  :  # FS_500K0 = 210e6/420  # // 210	420	100	5	0.500	131072	1310.72	1307	130700.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005K_210_420' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_500K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_005K_210_14__EN   :  # FS_15M00  = 210e6/14   # //  210	14	3000	5	15.000	131072	43.69066667	43	129000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005K_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_005K_210_140__EN  :  # FS_1M500 = 210e6/140 # // 210	140	300	5	NA	1.500	131072	436.907	433	129900.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005K_210_140' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1M500  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_005K_210_210__EN  :  # FS_1M000 = 210e6/210 # // 210	210	200	5	NA	1.000	131072	655.360	619	123800.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005K_210_210' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1M000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_005K_210_150__EN  :  # FS_1M400 = 210e6/150 # // 210	150	280	5	NA	1.400	131072	468.114	467	130760.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005K_210_150' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1M400  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_005K_210_30__EN   :  # FS_7M000 = 210e6/ 30 # // 210	30	1400	5	NA	7.000	131072	93.623	89	124600.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005K_210_30' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_7M000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_005K_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	2800	5	NA	14.000	131072	46.811	43	120400.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005K_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_005K_210_200__EN  :  # FS_1M050 = 210e6/ 200 # // 210	200		210	5	NA	1.050	131072	624.152	619	129990.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005K_210_200' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1M050  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_005K_210_280__EN  :  # FS_750K0 = 210e6/280  # //  210	280	150	5	0.750	131072	873.8133333	863	129450.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005K_210_280' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_750K0  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	##TODO: TEST_CASE N_010K
	if c_case_info.TEST_CASE__N_010K_210_210__EN  :  # FS_1M000 = 210e6/210  # // 210	210	100	10	1.000	131072	1310.72	1307	130700.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_010K_210_210' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1M000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_010K_210_14__EN   :  # FS_15M00  = 210e6/14   # //  210	14	1500	10	15.000	131072	87.38133333	83	124500.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_010K_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_010K_210_70__EN   :  # FS_3M000 = 210e6/ 70 # // 210	70	300	10	NA	3.000	131072	436.907	433	129900.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_010K_210_70' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_3M000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_010K_210_75__EN   :  # FS_2M800 = 210e6/ 75 # // 210	75	280	10	NA	2.800	131072	468.114	467	130760.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_010K_210_75' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_2M800  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_010K_210_15__EN   :  # FS_14M00 = 210e6/ 15 # // 210	15	1400	10	NA	14.000	131072	93.623	89	124600.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_010K_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_010K_210_100__EN  :  # FS_2M100 = 210e6/ 100 # // 210	100		210	10	NA	2.100	131072	624.152	619	129990.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_010K_210_100' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_2M100  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_010K_210_84__EN   :  # FS_2M500 = 210e6/ 84 # // 210	84	250	10	NA	2.500	131072	524.288	523	130750.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_010K_210_84' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_2M500  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_010K_210_140__EN  :  # FS_1M500  = 210e6/140  # //  210	140	150	10	1.500	131072	873.8133333	863	129450.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_010K_210_140' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1M500  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	##TODO: TEST_CASE N_020K
	if c_case_info.TEST_CASE__N_020K_210_105__EN  :  # FS_2M000 = 210e6/105  # // 210	105	100	20	2.000	131072	1310.72	1307	130700.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_020K_210_105' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_2M000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_020K_210_35__EN   :  # FS_6M000 = 210e6/ 35 # // 210	35	300	20	NA	6.000	131072	436.907	433	129900.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_020K_210_35' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_6M000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_020K_210_21__EN   :  # FS_10M00 = 210e6/21 # // 210	21	500		20	NA	10.000	131072	262.144	257	128500.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_020K_210_21' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_10M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_020K_210_70__EN   :  # FS_3M000   = 210e6/70   # //  210	70	150	20	3.000	131072	873.8133333	863	129450.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_020K_210_70' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_3M000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_020K_210_14__EN   :  # FS_15M00  = 210e6/14   # //  210	14	750		20	15.000	131072	174.7626667	173	129750.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_020K_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_020K_210_50__EN   :  # FS_4M200 = 210e6/  50 # // 210	50		210	20	NA	4.200	131072	624.152	619	129990.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_020K_210_50' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_4M200  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_020K_210_42__EN   :  # FS_5M000 = 210e6/ 42 # // 210	42	250	20	NA	5.000	131072	524.288	523	130750.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_020K_210_42' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_5M000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_020K_210_30__EN   :  # FS_7M000 = 210e6/ 30 # // 210	30	350	20	NA	7.000	131072	374.491	373	130550.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_020K_210_30' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_7M000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	
	##TODO: TEST_CASE N_050K
	if c_case_info.TEST_CASE__N_050K_210_42__EN   :  # FS_5M000 = 210e6/42   # // 210	42	100	50	5.000	131072	1310.72	1307	130700.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_050K_210_42' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_5M000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_050K_210_14__EN   :  # FS_15M00  = 210e6/14   # //  210	14	300	50	15.000	131072	436.9066667	433	129900.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_050K_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_050K_210_21__EN   :  # FS_10M00 = 210e6/ 21 # // 210	21	200	50	NA	10.000	131072	655.360	619	123800.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_050K_210_21' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_10M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_050K_210_15__EN   :  # FS_14M00 = 210e6/ 15 # // 210	15	280	50	NA	14.000	131072	468.114	467	130760.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_050K_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_050K_210_28__EN   :  # FS_7M500  = 210e6/28   # //  210	28	150	50	7.500	131072	873.8133333	863	129450.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_050K_210_28' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_7M500  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_050K_210_20__EN   :  # FS_10M50 = 210e6/  20 # // 210	20		210	50	NA	10.500	131072	624.152	619	129990.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_050K_210_20' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_10M50  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	
	##TODO: TEST_CASE N_100K
	if c_case_info.TEST_CASE__N_100K_210_14__EN   :  # FS_15M00  = 210e6/14   # // 210	14	150	100	15.000	131072	873.8133333	863	129450.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_100K_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 100e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_100K_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	140	100		NA	14.000	131072	936.229		929		130060.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_100K_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 100e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	##TODO: TEST_CASE N_200K
	if c_case_info.TEST_CASE__N_200K_210_14__EN   :  # FS_15M00  = 210e6/14   # // 210	14	75	200	15.000	131072	1747.626667	1747	131025.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_200K_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_200K_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	70	200		NA	14.000	131072	1872.457	1871	130970.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_200K_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	##TODO: TEST_CASE N_500K
	if c_case_info.TEST_CASE__N_500K_210_14__EN   :  # FS_15M00  = 210e6/14   # // 210	14	30	500	15.000	131072	4369.066667	4363	130890.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_500K_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 500e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_500K_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	28	500		NA	14.000	131072	4681.143	4679	131012.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_500K_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 500e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	##TODO: TEST_CASE N_001M
	if c_case_info.TEST_CASE__N_001M_210_14__EN   :  # FS_15M00  = 210e6/14   # // 210	14	15	1000	15.000	131072	8738.133333	8737	131055.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001M_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1000e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_001M_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	14	1000	NA	14.000	131072	9362.286	9349	130886.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001M_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1000e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	##TODO: TEST_CASE N_002M
	if c_case_info.TEST_CASE__N_002M_210_15__EN   :  # FS_14M00  = 210e6/15   # // 210	15	7	2000	14.000	131072	18724.57143	18719	131033.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002M_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2000e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_002M_210_14__EN   :  # FS_15M00 = 210e6/14 # // 210	14	7.5	2000	NA	15.000	131072	17476.267	17471	131032.50
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002M_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2000e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	##TODO: TEST_CASE N_005M
	if c_case_info.TEST_CASE__N_005M_210_14__EN   :  # FS_15M00  = 210e6/14   # // 210	14	3	5000	15.000	131072	43690.66667	43669	131007.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005M_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5000e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_005M_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	2.8	5000	NA	14.000	131072	46811.429	46811	131070.80
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005M_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5000e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	##TODO: TEST_CASE N_010M
	if c_case_info.TEST_CASE__N_010M_210_14__EN   :  # FS_15M00  = 210e6/14   # // 210	14	1.5	10000	15.000	131072	87381.33333	87380	131070.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_010M_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	if c_case_info.TEST_CASE__N_010M_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	1.4	10000	NA	14.000	131072	93622.857	93607	131049.80
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_010M_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	
	####
	
	##TODO: TEST_CASE U_001K
	if c_case_info.TEST_CASE__U_001K_210_212000__EN : # FS_0K991
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001K_210_212000' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		#FS_TARGET             = c_Fs_info.FS_4K999  ##
		FS_TARGET             = c_Fs_info.FS_9K991  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_001K_210_210100__EN : # FS_1K000 = 210e6/210100 # // 210	2100	0.001	210100	0.0004759638267	0.001000	131072	62.4152381	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001K_210_210100' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1K000  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_001K_210_210125__EN : # FS_0K99941 = 210e6/210125 =  999.41 # // 1		210	210125	1680	0.001	0.000595	0.000999	16384	9.752	7	11760.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001K_210_210125' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K99941  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_001K_210_210105__EN : # FS_0K99950 = 210e6/210105 =  999.50 # // 1		210	210105	2000	0.001	0.000500	0.001000	16384	8.192	7	14000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001K_210_210105' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K99950  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_001K_210_210075__EN : # FS_0K99964 = 210e6/210075 =  999.64 # // 1		210	210075	2800	0.001	0.000357	0.001000	16384	5.851	5	14000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001K_210_210075' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K99964  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_001K_210_210035__EN : # FS_0K99983 = 210e6/210035 =  999.83 # // 1		210	210035	6000	0.001	0.000167	0.001000	16384	2.731	2	12000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001K_210_210035' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K99983  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_001K_210_210025__EN : # FS_0K99988 = 210e6/210025 =  999.88 # // 1		210	210025	8400	0.001	0.000119	0.001000	16384	1.950	1	8400.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001K_210_210025' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K99988  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_001K_210_210015__EN : # FS_0K99993 = 210e6/210015 =  999.93 # // 1		210	210015	14000	0.001	0.000071	0.001000	16384	1.170	1	14000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001K_210_210015' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K99993  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	
	##TODO: TEST_CASE U_002K
	if c_case_info.TEST_CASE__U_002K_210_106000__EN : # FS_1K981
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_002K_210_106000' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1K981  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_002K_210_105050__EN : # FS_1K999 = 210e6/105050 # // 210	2100	0.002	105050	0.0009519276535	0.001999	131072	62.4152381	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_002K_210_105050' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1K999  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_002K_210_105075__EN:  # FS_1K99857 = 210e6/105075 = 1998.57 # // 2		210	105075	1400	0.002	0.001428	0.001999	16384	11.703	7	9800.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_002K_210_105075' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1K99857  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_002K_210_105070__EN:  # FS_1K99867 = 210e6/105070 = 1998.67 # // 2		210	105070	1500	0.002	0.001332	0.001999	16384	10.923	7	10500.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_002K_210_105070' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1K99867  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_002K_210_105035__EN:  # FS_1K99933 = 210e6/105035 = 1999.33 # // 2		210	105035	3000	0.002	0.000666	0.001999	16384	5.461	7	21000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_002K_210_105035' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1K99933  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 ##
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	
	##TODO: TEST_CASE__U_005K
	if c_case_info.TEST_CASE__U_005K_210_42400__EN: # FS_4K953
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_005K_210_42400' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_4K953 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 #
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_005K_210_42020__EN: # FS_4K998 = 210e6/ 42020 # // 210	2100	0.005	42020	0.002379819134	0.004998	131072	62.4152381	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_005K_210_42020' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_4K998 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 #
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_005K_210_42025__EN: # FS_4K9970  = 210e6/ 42025 =  4997.0 # // 5		210	42025	1680	0.005	0.002974	0.004997	16384	9.752	7	11760.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_005K_210_42025' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_4K9970 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 #
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_005K_210_42021__EN: # FS_4K9975  = 210e6/ 42021 =  4997.5 # // 5		210	42021	2000	0.005	0.002499	0.004998	16384	8.192	7	14000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_005K_210_42021' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_4K9975 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 #
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_005K_210_42015__EN: # FS_4K9982  = 210e6/ 42015 =  4998.2 # // 5		210	42015	2800	0.005	0.001785	0.004998	16384	5.851	7	19600.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_005K_210_42015' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_4K9982 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT2 #
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	
	##TODO: TEST_CASE__U_010K
	if c_case_info.TEST_CASE__U_010K_210_21004__EN: # FS_9K998 = 210e6/21004 # //  210	5250	0.01	21004	0.001904399162		0.009998	131072	24.96609524	23		120750.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010K_210_21004' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K998  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_010K_210_21010__EN: # FS_9K995 = 210e6/ 21010 # // 210	2100	0.01	21010	0.004759638267	0.009995	131072	62.4152381	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010K_210_21010' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K995  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_010K_210_21015__EN: # FS_9K9929  = 210e6/ 21015 =  9992.9 # // 10		210	21015	1400	0.01	0.007138	0.009993	131072	93.623	61	85400.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010K_210_21015' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K9929  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_010K_210_21014__EN: # FS_9K9933  = 210e6/ 21014 =  9993.3 # // 10		210	21014	1500	0.01	0.006662	0.009993	131072	87.381	61	91500.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010K_210_21014' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K9933  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_010K_210_21007__EN: # FS_9K9967  = 210e6/ 21007 =  9996.7 # // 10		210	21007	3000	0.01	0.003332	0.009997	131072	43.691	61	183000.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010K_210_21007' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K9967  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	
	##TODO: TEST_CASE__U_020K
	if c_case_info.TEST_CASE__U_020K_210_10510__EN: # FS_19K98 = 210e6/10510 # //  210	1050	0.02	10510	0.01902949572		0.019981	131072	124.8304762	113		118650.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_020K_210_10510' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_19K98  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_020K_210_10505__EN: # FS_19K99 = 210e6/ 10505 # // 210	2100	0.02	10505	0.009519276535	0.019990	131072	62.4152381	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_020K_210_10505' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_19K99  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_020K_210_10501__EN: # FS_19K998  = 210e6/ 10501 = 19998.1 # // 20		210	10501	10500	0.02	0.001905	0.019998	131072	12.483	61	640500.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_020K_210_10501' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_19K998  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_020K_210_10503__EN: # FS_19K994  = 210e6/ 10503 = 19994.3 # // 20		210	10503	3500	0.02	0.005713	0.019994	131072	37.449	61	213500.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_020K_210_10503' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_19K994  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_020K_210_10507__EN: # FS_19K987  = 210e6/ 10507 = 19986.7 # // 20		210	10507	1500	0.02	0.013324	0.019987	131072	87.381	61	91500.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_020K_210_10507' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_19K987  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	
	##TODO: TEST_CASE__U_050K
	if c_case_info.TEST_CASE__U_050K_210_4204__EN: # FS_49K95 = 210e6/4204
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_050K_210_4204' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_49K95  ## 210e6/4204
		#FS_TARGET             = c_Fs_info.FS_49K53  ## 210e6/4240
		#FS_TARGET             = c_Fs_info.FS_49K41  ## 210e6/4250
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_050K_210_4240__EN: # FS_49K53 = 210e6/4240
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_050K_210_4240' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		#FS_TARGET             = c_Fs_info.FS_49K95  ## 210e6/4204
		FS_TARGET             = c_Fs_info.FS_49K53  ## 210e6/4240
		#FS_TARGET             = c_Fs_info.FS_49K41  ## 210e6/4250
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_050K_210_4250__EN: # FS_49K41 = 210e6/  4250  # // 210	84	0.05	4250	0.5882352941	0.049412	131072	1560.380952	1559	130956.00	1559
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_050K_210_4250' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		#FS_TARGET             = c_Fs_info.FS_49K95  ## 210e6/4204
		#FS_TARGET             = c_Fs_info.FS_49K53  ## 210e6/4240
		FS_TARGET             = c_Fs_info.FS_49K41  ## 210e6/4250
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_050K_210_4228__EN: # FS_49K66 = 210e6/  4228  # // 210	150	0.05	4228	0.3311258278	0.049669	131072	873.8133333	863	129450.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_050K_210_4228' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_49K66  ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_050K_210_4201__EN: # FS_49K99 = 210e6/  4201 # // 210	4200	0.05	4201	0.01190192811	0.049988	131072	31.20761905	31	130200.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_050K_210_4201' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_49K99  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_050K_210_4202__EN: # FS_49K98 = 210e6/  4202 # // 210	2100	0.05	4202	0.02379819134	0.049976	131072	62.4152381	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_050K_210_4202' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_49K98  ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_050K_210_4203__EN: # FS_49K96   = 210e6/  4203           # // 50		210	4203	1400	0.05	0.035689	0.049964	131072	93.623	61	85400.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_050K_210_4203' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_49K96  ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	
	##TODO: TEST_CASE__U_100K
	if c_case_info.TEST_CASE__U_100K_210_2102__EN: # FS_99K91 = 210e6/   2102 # // 210	1050	0.1		2102	0.09514747859		0.099905	131072	124.8304762	113		118650.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_100K_210_2102' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 100e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_99K91 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_100K_210_2125__EN: # FS_98K82 = 210e6/  2125  # // 210	84	0.1		2125	1.176470588		0.098824	131072	1560.380952	1559	130956.00	1559
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_100K_210_2125' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 100e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_98K82 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_100K_210_2114__EN: # FS_99K33 = 210e6/  2114  # // 210	150	0.1		2114	0.6622516556	0.099338	131072	873.8133333	863	129450.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_100K_210_2114' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 100e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_99K33 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_100K_210_2101__EN: # FS_99K95 = 210e6/  2101 # // 210	2101	2100	0.1	0.047596	0.099952	131072	62.415	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_100K_210_2101' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '120K' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 100e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_99K95 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	
	##TODO: TEST_CASE__U_200K
	if c_case_info.TEST_CASE__U_200K_210_1060__EN: # FS_198K1 = 210e6/   1060 # // 210	105		0.2		1060	1.886792453			0.198113	131072	1248.304762	1237	129885.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1060' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_198K1 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_200K_210_1065__EN: # FS_197K1 = 210e6/  1065  # // 210	70	0.2		1065	2.816901408		0.197183	131072	1872.457143	1747	122290.00	1747
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1065' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_197K1 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_200K_210_1057__EN: # FS_198K6 = 210e6/  1057  # // 210	150	0.2		1057	1.324503311		0.198675	131072	873.8133333	863	129450.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1057' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_198K6 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_200K_210_1056__EN: # FS_198K9 = 210e6/  1056 # // 210	175	0.2	1056	1.136363636		0.198864	131072	748.9828571	743	130025.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1056' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_198K9 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_200K_210_1064__EN: # FS_197K4 = 210e6/  1064 # // 210	75	0.2	1064	2.631578947		0.197368	131072	1747.626667	1747	131025.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1064' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_197K4 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_200K_210_1075__EN: # FS_195K3 = 210e6/  1075 # // 210	42	0.2	1075	4.651162791		0.195349	131072	3120.761905	3119	130998.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1075' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_195K3 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_200K_210_1071__EN: # FS_196K1 = 210e6/  1071 # // 210	50	0.2	1071	3.921568627		0.196078	131072	2621.44	2621	131050.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1071' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_196K1 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_200K_210_1055__EN: # FS_199K1 = 210e6/  1055 # // 210	210	0.2	1055	0.9478672986	0.199052	131072	624.152381	619	129990.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1055' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_199K1 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_200K_210_1053__EN: # FS_199K4 = 210e6/  1053 # // 210	350	0.2	1053	0.5698005698	0.199430	131072	374.4914286	373	130550.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1053' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_199K4 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_200K_210_1051__EN: # FS_199K8 = 210e6/  1051 # // 210	1050	0.2	1051	0.1902949572	0.199810	131072	124.8304762	113	118650.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1051' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_199K8 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	
	
	##TODO: TEST_CASE__U_500K
	if c_case_info.TEST_CASE__U_500K_210_424__EN: # FS_495K3 = 210e6/    424 # // 210	105		0.5		424		4.716981132			0.495283	131072	1248.304762	1237	129885.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_500K_210_424' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 500e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_495K3 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_500K_210_421__EN: # FS_498K8 = 210e6/   421 # // 210	420	0.5	421	1.187648456	0.498812	131072	312.0761905	311	130620.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_500K_210_421' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 500e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_498K8 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	
	##TODO: TEST_CASE__U_001M
	if c_case_info.TEST_CASE__U_001M_210_212__EN: # FS_990K6 = 210e6/    212 # // 210	105		1		212		9.433962264			0.990566	131072	1248.304762	1237	129885.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001M_210_212' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET =   1e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_990K6 ##	
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	if c_case_info.TEST_CASE__U_001M_210_211__EN: # FS_995K3 = 210e6/   211 # // 210	210	1	211	4.739336493	0.995261	131072	624.152381	619	129990.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001M_210_211' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET =   1e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_995K3 ##	
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	##TODO: TEST_CASE__U_002M
	if c_case_info.TEST_CASE__U_002M_210_106__EN: # FS_1M981 = 210e6/    106 # // 210	105		2		106		18.86792453			1.981132	131072	1248.304762	1237	129885.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_002M_210_106' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ## 120K vs 1M2 vs 12M 
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 2000e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1M981  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) # down-sampling
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##	
	
	##TODO: TEST_CASE__U_005M
	if c_case_info.TEST_CASE__U_005M_210_43__EN: # FS_4M884 = 210e6/     43 # // 210	42		5		43		116.2790698			4.883721	131072	3120.761905	3119	130998.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_005M_210_43' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET = 5e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		#FS_TARGET             = c_Fs_info.FS_9  ##
		FS_TARGET             = c_Fs_info.FS_4M884  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) # down-sampling
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##		
	
	##TODO: TEST_CASE__U_010M
	if c_case_info.TEST_CASE__U_010M_210_22__EN: # FS_9M545 = 210e6/     22 # // 210	21		10		22		454.5454545			9.545455	131072	6241.52381	6229	130809.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010M_210_22' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET =  10e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9M545 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##		
	
	if c_case_info.TEST_CASE__U_010M_210_24__EN: # FS_8M750 = 210e6/    24 # // 10000	210	24	7	10	1250.000000	8.750000	131072	18724.571	18719	131033.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010M_210_24' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET =  10e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_8M750 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##		
	
	if c_case_info.TEST_CASE__U_010M_210_28__EN: # FS_7M500 = 210e6/    28 # // 10000	210	28	3	10	2500.000000	7.500000	131072	43690.667	43669	131007.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010M_210_28' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET =  10e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_7M500 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##		
	
	if c_case_info.TEST_CASE__U_010M_210_41__EN: # FS_5M122 = 210e6/    41 # // 10000	210	41	1.05	10	4878.048780	5.121951	131072	124830.476	124820	131061.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010M_210_41' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET =  10e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_5M122 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(2*FS_TARGET-DWAVE_FREQ_TARGET)*10*TEST_CONF['FS_TARGET']) ## for 2*Fs image
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##		
	
	if c_case_info.TEST_CASE__U_010M_210_43__EN: # FS_4M884 = 210e6/    43 # // 210	43		21		10		232.558140			4.883721	131072	6241.524	6229	130809.00
		## setup TEST_CONF ##
		TEST_CONF = {}
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010M_210_43' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
		TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
		#
		DWAVE_FREQ_TARGET =  10e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_4M884 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
		TEST_CONF['ADC_LIST_INT' ] = []
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-2*FS_TARGET)*10*TEST_CONF['FS_TARGET']) ## for 2*Fs image
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##		
	
	######################################################################
	
	
	
	####
	## cmu_adc_load_from_csv
	#ret = cmu.cmu_adc_load_from_csv(csv_filename)
	#adc_list_rb = ret[0]
	#fs_rb       = ret[1]
	#ns_rb       = ret[2]
	#print(adc_list_rb[0][0:5])
	#print(adc_list_rb[1][0:5])
	#print(fs_rb)
	#print(ns_rb)
	####
	
	####
	## adc_display_data_list_int
	#ret = cmu.cmu_adc_display_data_list_int(adc_list_rb,fs_rb)
	#print(ret)
	####
	
	
	####
	#report__test_sheet_data_from_csv_file(TEST_CONF)
	
	
	########
	
	
	
	###################################################
	
	########
	# TEST
	
	####
	## test counter off
	ret = cmu.cmu_test_counter('OFF')
	print(ret)
	####
	
	####
	## test counter reset
	ret = cmu.cmu_test_counter('RESET')
	print(ret)
	####
	
	
	####
	## close
	ret = cmu.ok_cmu_close()
	print(ret)
	####
	
	
#

