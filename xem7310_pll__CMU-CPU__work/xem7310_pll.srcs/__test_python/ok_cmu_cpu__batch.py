# ok_cmu_cpu__batch.py
#
#  batch
#

##
import time
#import numpy as np
import matplotlib.pyplot as plt

####
## filename to batch
#PY_FILENAME = 'ok_cmu_cpu__09_test_sheet_data.py'
#PY_FILENAME = 'ok_cmu_cpu__10_self_test.py'
PY_FILENAMES = [
	'tcp_echo\echo-client.py',
	'tcp_echo\scpi-client3.py',
	#'ok_cmu_cpu__01_open.py',
	'LAN_cmu_cpu__03_mon.py',
	#'ok_cmu_cpu__10_self_test.py',
	'LAN_cmu_cpu__10_self_test.py'
	]
#PY_FILENAME = 'LAN_cmu_cpu__10_self_test.py'
#
## batch interval
#INT_SEC = 1800 # 30min
#INT_SEC = 300 # 5min
#INT_SEC = 180 # 3min
#INT_SEC = 30 
INT_SEC = 10
#INT_SEC = 5
#INT_SEC = 3
#INT_SEC = 0.1 
## repeat number
#MAX_COUNT = 2*12
#MAX_COUNT = 12*8
MAX_COUNT = 2*60*5

def my_wait(tt):
	try:
		plt.pause(tt)
	except NameError:
		print("cannot call plt.pause()... so call time.sleep()")
		time.sleep(tt)
	except (KeyboardInterrupt, SystemExit):
		raise # exit
	except:
		raise

####
cnt=0
while True:
	import matplotlib.pyplot as plt
	plt.close('all')
	#
	#exec(open(PY_FILENAME).read())
	for ff in PY_FILENAMES:
		print('\n\n> TEST py.file: ' + ff)
		exec(open(ff).read())
		print('> TEST py.file: done! \n')
		
	#
	#plt.draw()
	#plt.show()
	#plt.pause(10)
	#
	print('> continue: {}'.format(cnt))
	#
	my_wait(INT_SEC)
	#plt.pause(INT_SEC)
	#time.sleep(INT_SEC)
	#
	cnt=cnt+1
	if cnt==MAX_COUNT:
		break
	#
#
print('> all done!')

####