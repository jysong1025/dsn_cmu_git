# ok_cmu_cpu__batch_b__adc_undersample.py
#
#  batch
#

##
import time
#import numpy as np
import matplotlib.pyplot as plt

# memory garbage clear 
import gc


####
## filename to batch
PY_FILENAMES = [
	#'tcp_echo\echo-client.py',
	#'tcp_echo\scpi-client3.py',
	#'ok_cmu_cpu__01_open.py',
	#'LAN_cmu_cpu__03_mon.py',
	#'ok_cmu_cpu__10_self_test.py',
	#'LAN_cmu_cpu__10_self_test.py',
	#'ok_cmu_cpu__10_self_test_a__adc_mismatch.py'
	'ok_cmu_cpu__10_self_test_b__adc_undersample.py'
	''
	]
#PY_FILENAME = 'LAN_cmu_cpu__10_self_test.py'
#
## batch interval
#INT_SEC = 1800 # 30min
#INT_SEC = 300 # 5min
#INT_SEC = 180 # 3min
#INT_SEC = 30 
INT_SEC = 10
#INT_SEC = 5
#INT_SEC = 3
#INT_SEC = 0.1 
## repeat number
#MAX_COUNT = 2*12
#MAX_COUNT = 12*8
#MAX_COUNT = 2*60*5
#
#SUM_INT_HOUR = 0.05
#SUM_INT_HOUR = 0.5
#SUM_INT_HOUR = 5
SUM_INT_HOUR = 36 # ... 48H
#SUM_INT_HOUR = 18 # ... 24H  16/21*24=18.29
#SUM_INT_HOUR = 16 # ... 21H
#SUM_INT_HOUR = 12 # ... 16H  16/21*16=12.2  21/16*12=15
#SUM_INT_HOUR = 10 # ... 14H  16/21*14=10
#
SUM_INT_MIN = SUM_INT_HOUR*60
SUM_INT_SEC = SUM_INT_MIN*60
#
#MAX_COUNT = int(SUM_INT_SEC/INT_SEC)
#MAX_COUNT = 2
#MAX_COUNT = 100 # 3H
#MAX_COUNT = 200 # 6H
MAX_COUNT = 600 # 12H
#MAX_COUNT = 1000 # 20H

##
print(INT_SEC)
print(MAX_COUNT)

def my_wait(tt):
	try:
		plt.pause(tt)
	except NameError:
		print("cannot call plt.pause()... so call time.sleep()")
		time.sleep(tt)
	except (KeyboardInterrupt, SystemExit):
		raise # exit
	except:
		raise

####
cnt=0
while True:
	import matplotlib.pyplot as plt
	plt.close('all')
	#
	#exec(open(PY_FILENAME).read())
	for ff in PY_FILENAMES:
		if not ff:
			continue
		print('\n\n> TEST py.file: ' + ff)
		exec(open(ff).read())
		print('> TEST py.file: done! \n')
		#  garbage collect
		gc.collect()
		#my_wait(0.001)
	#
	#plt.draw()
	#plt.show()
	#plt.pause(10)
	#
	print('> continue: {}'.format(cnt))
	#
	print(INT_SEC)
	print(MAX_COUNT)
        #
	my_wait(INT_SEC)
	#plt.pause(INT_SEC)
	#time.sleep(INT_SEC)
	#
	cnt=cnt+1
	if cnt==MAX_COUNT:
		break
	#
	#  garbage collect
	#gc.collect()
#
print('> all done!')

####