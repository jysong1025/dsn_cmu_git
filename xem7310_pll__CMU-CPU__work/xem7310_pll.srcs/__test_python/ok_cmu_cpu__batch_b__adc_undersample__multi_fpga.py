# ok_cmu_cpu__batch_b__adc_undersample.py
#
#  batch
#

##
import time
#import numpy as np
import matplotlib.pyplot as plt

# memory garbage clear 
import gc


####
## filename to batch
PY_FILENAMES = [
	#'tcp_echo\echo-client.py',
	#'tcp_echo\scpi-client3.py',
	#'ok_cmu_cpu__01_open.py',
	#'LAN_cmu_cpu__03_mon.py',
	#'ok_cmu_cpu__10_self_test.py',
	#'LAN_cmu_cpu__10_self_test.py',
	#'ok_cmu_cpu__10_self_test_a__adc_mismatch.py',
	#'ok_cmu_cpu__10_self_test_b__adc_undersample.py',
	'ok_cmu_cpu__10_self_test_b__adc_undersample__multi_fpga_1.py',
	'ok_cmu_cpu__10_self_test_b__adc_undersample__multi_fpga_2.py',
	'ok_cmu_cpu__10_self_test_b__adc_undersample__multi_fpga_3.py',
	'ok_cmu_cpu__10_self_test_b__adc_undersample__multi_fpga_4.py',
	'ok_cmu_cpu__10_self_test_b__adc_undersample__multi_fpga_5.py',
	''
	]
# remove empty
PY_FILENAMES = [x for x in PY_FILENAMES if x != []]
#
#PY_FILENAME = 'LAN_cmu_cpu__10_self_test.py'
#
## batch interval
#INT_SEC = 1800 # 30min
#INT_SEC = 300 # 5min
#INT_SEC = 180 # 3min
#INT_SEC = 30 
##INT_SEC = 10
#INT_SEC = 5
INT_SEC = 3
#INT_SEC = 0.1

## long interval
INT_LONG_EN  = 1
INT_LONG_SEC = 300
#INT_LONG_SEC = 600
## long interval freq count
MOD_CNT_INT_LONG = 50
#MOD_CNT_INT_LONG = 100

## repeat number
#MAX_COUNT = 2*12
#MAX_COUNT = 12*8
#MAX_COUNT = 2*60*5
#
#SUM_INT_HOUR = 0.05
#SUM_INT_HOUR = 0.5
#SUM_INT_HOUR = 5
SUM_INT_HOUR = 36 # ... 48H
#SUM_INT_HOUR = 18 # ... 24H  16/21*24=18.29
#SUM_INT_HOUR = 16 # ... 21H
#SUM_INT_HOUR = 12 # ... 16H  16/21*16=12.2  21/16*12=15
#SUM_INT_HOUR = 10 # ... 14H  16/21*14=10
#
SUM_INT_MIN = SUM_INT_HOUR*60
SUM_INT_SEC = SUM_INT_MIN*60
#
#MAX_COUNT = int(SUM_INT_SEC/INT_SEC)
MAX_COUNT = 1
##MAX_COUNT = 2
#MAX_COUNT = 20
##MAX_COUNT = 50
####MAX_COUNT = 100 # 3H
##MAX_COUNT = 200 # 6H ~ 13H
###MAX_COUNT = 300 
##MAX_COUNT = 400 # 6H ~ 24H
#MAX_COUNT = 600 # 12H
#MAX_COUNT = 1000 # 20H ~ 2.5D = 60H

##
print('{}={}'.format('INT_SEC',INT_SEC))
if INT_LONG_EN:
	print('{}={}'.format('INT_LONG_SEC',INT_LONG_SEC))
	print('{}={}'.format('MOD_CNT_INT_LONG',MOD_CNT_INT_LONG))
print('{}={}'.format('MAX_COUNT',MAX_COUNT))

def my_wait(tt):
	try:
		plt.pause(tt)
	except NameError:
		print("cannot call plt.pause()... so call time.sleep()")
		time.sleep(tt)
	except (KeyboardInterrupt, SystemExit):
		raise # exit
	except:
		raise

def my_collect():
    ret = gc.collect()
    while True:
        if ret == 0:
            break
        ret = gc.collect()
        #
    #

####
cnt=0
while True:
	cnt += 1
	#import matplotlib.pyplot as plt
	plt.close('all')
	my_collect()
	#
	#exec(open(PY_FILENAME).read())
	cnt_ff = 0
	for ff in PY_FILENAMES:
		cnt_ff += 1
		if not ff:
			continue
		#
		# clear
		plt.close('all')
		my_collect()
		#
		print('\n\n> TEST py.file: ' + ff)
		#
		exec(open(ff).read())
		#print('> TEST py.file: done! \n')
		print('[{}/{}]({}/{})> TEST py.file: done! \n'.format(cnt,MAX_COUNT,cnt_ff,len(PY_FILENAMES)))
		#
		#  garbage collect
		#my_collect()
		my_wait(INT_SEC)
	#
	#plt.draw()
	#plt.show()
	#plt.pause(10)
	#
	print('> continue: {}'.format(cnt))
	#
	print('{}={}'.format('INT_SEC',INT_SEC))
	if INT_LONG_EN:
		print('{}={}'.format('INT_LONG_SEC',INT_LONG_SEC))
		print('{}={}'.format('MOD_CNT_INT_LONG',MOD_CNT_INT_LONG))
	print('{}={}'.format('MAX_COUNT',MAX_COUNT))
	#
	#
	#cnt=cnt+1
	if cnt==MAX_COUNT:
		break
	#
	# check long interval
	if INT_LONG_EN and (cnt % MOD_CNT_INT_LONG == 0):
		print('long interval~~~')
		my_wait(INT_LONG_SEC)
	else:    
		my_wait(INT_SEC)
		#plt.pause(INT_SEC)
		#time.sleep(INT_SEC)
	#
	#  garbage collect
	#my_collect()
#
print('> all done!')

####