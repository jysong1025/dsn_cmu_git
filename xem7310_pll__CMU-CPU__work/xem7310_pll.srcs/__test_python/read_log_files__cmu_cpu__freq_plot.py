# read_log_files__cmu_cpu__freq_plot.py
#  
#  read log file and display 
#  read LOG_* files 
#  output ANAL_* file
#
#  rev0: draft
#
#  https://www.quora.com/How-does-one-extract-arrays-from-CSV-files-with-columns-in-Python
#
#  https://www.devdungeon.com/content/working-binary-data-python
#  https://pyformat.info/
#  https://matplotlib.org/api/_as_gen/matplotlib.pyplot.psd.html#matplotlib.pyplot.psd
#    https://matplotlib.org/gallery/lines_bars_and_markers/psd_demo.html#sphx-glr-gallery-lines-bars-and-markers-psd-demo-py
#
# for import numpy: https://scipy.org/install.html
#   python -m pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
# for import multiarray:
#   https://stackoverflow.com/questions/21324426/numpy-build-fails-with-cannot-import-multiarray
#   pip install numpy
#   pip install matplotlib
#
# https://matplotlib.org/users/tight_layout_guide.html
#
# https://pythonspot.com/tk-file-dialogs/
#
#



## load library/module
import csv
import numpy as np
import matplotlib.pyplot as plt
plt.ion() # matplotlib interactive mode 
#plt.close('all')
from sys import exit
import tkinter as tk
from tkinter import filedialog
import os
import datetime
#
import glob # for filenames

## controls

## parameters

## functions

def is_int(str):
	try:
		#tmp = int(str)
		int(str)
		return True
	except ValueError:
		return False

def is_float(str):
	try:
		#tmp = float(str)
		float(str)
		return True
	except ValueError:
		return False

def is_complex(str):
	try:
		#tmp = complex(str)
		complex(str)
		return True
	except ValueError:
		return False


## start codes below

## read measure from the report file 
def f_read_rpt_file(filename_rpt, MAX_ROW_COUNT=50, LAST_ROW_SIDE_EN=0):
	filename_list  = []
	Fs_list           = []
	TEST_FREQ_HZ_list = []
	SAMPLES_DFT_list  = []
	TEMP_list      = []
	mean_x_list    = []
	mean_y_list    = []
	std_x_list     = []
	std_y_list     = []
	mag_X_f_0_list = []
	mag_Y_f_0_list = []
	mag_X_f_t_list = []
	mag_Y_f_t_list = []
	mag_R_f_t_list = []
	ang_R_f_t_list = []
	with open(filename_rpt, 'r', newline='') as csvFile:
		read_data = csv.reader(csvFile, delimiter=',')
		line_count = 0
		for row in read_data:
			# count lines
			line_count += 1
			#
			# add list 
			#
			filename_list     +=[row[ 0]]
			Fs_list           +=[int(row[1])] ##
			TEMP_list         +=[float(row[ 4])]
			mean_x_list       +=[float(row[ 7])]
			mean_y_list       +=[float(row[ 8])]
			std_x_list        +=[float(row[ 9])]
			std_y_list        +=[float(row[10])]
			TEST_FREQ_HZ_list += [float(row[11])] ##
			#
			SAMPLES_DFT       = int(row[12])
			SAMPLES_DFT_list  += [SAMPLES_DFT] ##
			#
			mag_X_f_0_list    +=[np.real(complex(row[13]))/SAMPLES_DFT] # scale for 1/N*DFT
			mag_Y_f_0_list    +=[np.real(complex(row[14]))/SAMPLES_DFT] # scale for 1/N*DFT
			mag_X_f_t_list    +=[float(row[19])/SAMPLES_DFT] # scale for 1/N*DFT
			mag_Y_f_t_list    +=[float(row[20])/SAMPLES_DFT] # scale for 1/N*DFT
			mag_R_f_t_list    +=[float(row[21])]
			ang_R_f_t_list    +=[float(row[22])]
			#
			if LAST_ROW_SIDE_EN == 0 and line_count >= MAX_ROW_COUNT :
				break
			#
		#
		if LAST_ROW_SIDE_EN == 1 :
			filename_list     =  filename_list    [(-MAX_ROW_COUNT):]
			Fs_list           =  Fs_list          [(-MAX_ROW_COUNT):]
			TEMP_list         =  TEMP_list        [(-MAX_ROW_COUNT):]
			mean_x_list       =  mean_x_list      [(-MAX_ROW_COUNT):]
			mean_y_list       =  mean_y_list      [(-MAX_ROW_COUNT):]
			std_x_list        =  std_x_list       [(-MAX_ROW_COUNT):]
			std_y_list        =  std_y_list       [(-MAX_ROW_COUNT):]
			TEST_FREQ_HZ_list =  TEST_FREQ_HZ_list[(-MAX_ROW_COUNT):]
			SAMPLES_DFT_list  =  SAMPLES_DFT_list [(-MAX_ROW_COUNT):]
			mag_X_f_0_list    =  mag_X_f_0_list   [(-MAX_ROW_COUNT):]
			mag_Y_f_0_list    =  mag_Y_f_0_list   [(-MAX_ROW_COUNT):]
			mag_X_f_t_list    =  mag_X_f_t_list   [(-MAX_ROW_COUNT):]
			mag_Y_f_t_list    =  mag_Y_f_t_list   [(-MAX_ROW_COUNT):]
			mag_R_f_t_list    =  mag_R_f_t_list   [(-MAX_ROW_COUNT):]
			ang_R_f_t_list    =  ang_R_f_t_list   [(-MAX_ROW_COUNT):]
		#
	return  filename_list, Fs_list, TEST_FREQ_HZ_list, SAMPLES_DFT_list, TEMP_list, \
		mean_x_list, mean_y_list, std_x_list, std_y_list, mag_X_f_0_list, mag_Y_f_0_list, \
		mag_X_f_t_list, mag_Y_f_t_list, mag_R_f_t_list, ang_R_f_t_list

def f_read_log_file(filename_log):
	#
	# list
	test_freq_kHz_list    = []
	mean_mag_R_f_t_list   = []
	std_mag_R_f_t_list    = []
	cv_mag_R_f_t_ppm_list = []
	mean_ang_R_f_t_list   = []
	std_ang_R_f_t_list    = []
	cv_ang_R_f_t_ppm_list = []
	#
	with open(filename_log, 'r', newline='') as csvFile:
		read_data = csv.reader(csvFile, delimiter=',')
		line_count = 0
		for row in read_data:
			# count lines
			line_count += 1
			#
			print(len(row))
			#
			# check data location
			if len(row)==7 \
				and is_float(row[0]) \
				and is_float(row[1]) \
				and is_float(row[2]) \
				and is_float(row[3]) \
				and is_float(row[4]) \
				and is_float(row[5]) \
				and is_float(row[6]) :
				test_freq_kHz_list    += [float(row[0])]
				mean_mag_R_f_t_list   += [float(row[1])]
				std_mag_R_f_t_list    += [float(row[2])]
				cv_mag_R_f_t_ppm_list += [float(row[3])]
				mean_ang_R_f_t_list   += [float(row[4])]
				std_ang_R_f_t_list    += [float(row[5])]
				cv_ang_R_f_t_ppm_list += [float(row[6])]
		#
	#
	return test_freq_kHz_list,\
		mean_mag_R_f_t_list,std_mag_R_f_t_list,cv_mag_R_f_t_ppm_list,\
		mean_ang_R_f_t_list,std_ang_R_f_t_list,cv_ang_R_f_t_ppm_list
		
		
####
# ratio vs freq plot : mag-ratio(ch2/ch1) vs freq ... mean / std / cv 
print('> analyze log files')


## filename prefix 
#FILE_PREFIX = ''
#
#FILE_PREFIX = 'LOG_RPT_NP_DUMP_BD1__'
#FILE_PREFIX = 'LOG_RPT_NP_DUMP_BD2__'
#FILE_PREFIX = 'LOG_RPT_NP_DUMP_BD3__'
#FILE_PREFIX = 'LOG_RPT_NP_DUMP_BD4__'
FILE_PREFIX = 'LOG_RPT_NP_DUMP_BD5__'
#
#FILE_PREFIX = 'LOG_RPT_XP_DUMP_BD1__'
#FILE_PREFIX = 'LOG_RPT_XP_DUMP_BD2__'
#FILE_PREFIX = 'LOG_RPT_XP_DUMP_BD3__'
#FILE_PREFIX = 'LOG_RPT_XP_DUMP_BD4__'
#FILE_PREFIX = 'LOG_RPT_XP_DUMP_BD5__'

# filename postfix 
#FILE_POSTFIX = ''
#
#FILE_POSTFIX = '*_4K__TS_201907*.txt'
#FILE_POSTFIX = '*_16K__TS_201907*.txt'
#FILE_POSTFIX = '*_32K__TS_201907*.txt'
#
FILE_POSTFIX =  '*_4K__TS_2019071*.txt'
#FILE_POSTFIX = '*_16K__TS_2019071*.txt'
#FILE_POSTFIX = '*_32K__TS_2019071*.txt'

## filenames
filenames_to_read_condition = FILE_PREFIX + FILE_POSTFIX ###
filenames_to_read_list = glob.glob(filenames_to_read_condition)
print('>>> {} = {}'.format('filenames_to_read_list',filenames_to_read_list))

## read log files
cnt_files = 0
len_files = len(filenames_to_read_list)
ret_list = []
#
for ff_name in filenames_to_read_list:
	cnt_files += 1
	#
	print('>> file {:2}/{:2}: {}'.format(cnt_files,len_files,ff_name))
	ret = f_read_log_file(ff_name)
	#
	#print(ret)
	#
	ret_list += [ret]

# test 
print(len(ret_list))
print(len(ret_list[0]))    # ret
#print(ret_list)

## display  mean_mag_R_f_t and mean_ang_R_f_t  vs log files 

FIG_14_EN = 1
#
plt.ion() # matplotlib interactive mode 


# plot mean elements 
#
num_log_files        = len(ret_list)
log_trials           = list(range(num_log_files))
#
#
info_files = FILE_PREFIX +FILE_POSTFIX
#
if FIG_14_EN:
	#FIG_NUM = 14 
	FIG_NUM = None # for new figure windows
	fig = plt.figure(FIG_NUM,figsize=(15,10))
	#
	plt.suptitle('plot of mean data vs log files: '+info_files+' (dotted 200ppm lines, dash-dot 0.1% lines)')
	#
	plt.subplot(211) ### 
	#
	# plot mean data 
	for ii in log_trials:
		plt.plot([tt*1e3  for tt in ret_list[ii][0]],ret_list[ii][1], label=filenames_to_read_list[ii], alpha=0.8)
	#
	plt.ylabel('mag of ratio')
	plt.xlabel('test freq[Hz]')
	plt.xscale('log')
	plt.legend()
	#
	# add ref lines 0.1% and 200ppm
	for ii in log_trials:
		plt.plot([tt*1e3  for tt in ret_list[ii][0]],[[yy*1.001, yy*0.999]   for yy in ret_list[ii][1]], '-.', label=filenames_to_read_list[ii], alpha=0.8)
		plt.plot([tt*1e3  for tt in ret_list[ii][0]],[[yy*1.0002, yy*0.9998] for yy in ret_list[ii][1]],  ':', label=filenames_to_read_list[ii], alpha=0.8)
	#
	#plt.legend()
	#
	plt.subplot(212) ### 
	#
	# plot mean data 
	for ii in log_trials:
		plt.plot([tt*1e3  for tt in ret_list[ii][0]],ret_list[ii][4], label=filenames_to_read_list[ii], alpha=0.8)
	#
	plt.ylabel('angle of ratio [degree]')
	plt.xlabel('test freq[Hz]')
	plt.xscale('log')
	plt.legend()
	#
	# add ref lines 0.1% and 200ppm
	for ii in log_trials:
		plt.plot([tt*1e3  for tt in ret_list[ii][0]],[[yy*1.001, yy*0.999]   for yy in ret_list[ii][4]], '-.', label=filenames_to_read_list[ii], alpha=0.8)
		plt.plot([tt*1e3  for tt in ret_list[ii][0]],[[yy*1.0002, yy*0.9998] for yy in ret_list[ii][4]],  ':', label=filenames_to_read_list[ii], alpha=0.8)
	#
	#plt.legend()
	#
	# save figure 
	filename_fig = 'ANAL_'+FILE_PREFIX+FILE_POSTFIX[2:-5]+'.png'
	print(filename_fig)
	plt.savefig(filename_fig)


## print out analysis file for log files

## print out to log file 
def f_write_anal_file(filename_anal, filenames_to_read_list, ret_list):
	newFile = open(filename_anal, "wt")
	##
	num_log_files        = len(ret_list)
	log_trials           = list(range(num_log_files))
	#
	#num_freq             = len(ret_list)
	#xlist_trials         = list(range(num_freq))
	#xlist_test_freq      = [ff_dict['test_freq'] for ff_dict in file_info__freq_plot]
	#test_freq_labels     = ['{:3.0f}'.format(tt/1e3) for tt in xlist_test_freq]
	#
	xlist_test_freq_kHz = [tt  for tt in ret_list[0][0]] # freq at the first list element
	#
	num_freq            = len(xlist_test_freq_kHz)
	xlist_trials        = list(range(num_freq))
	#
	# mean over log_trials
	ylist_mean_mag_R_f_t = np.mean([ret_list[ii][1]  for ii in log_trials], axis=0)
	ylist_mean_ang_R_f_t = np.mean([ret_list[ii][4]  for ii in log_trials], axis=0)
	# std over log_trials
	ylist_std_mag_R_f_t  = np.std ([ret_list[ii][1]  for ii in log_trials], axis=0)
	ylist_std_ang_R_f_t  = np.std ([ret_list[ii][4]  for ii in log_trials], axis=0)
	# cv 
	ylist_cv_mag_R_f_t = [ylist_std_mag_R_f_t[ii]/abs(ylist_mean_mag_R_f_t[ii])  for ii in xlist_trials]
	ylist_cv_ang_R_f_t = [ylist_std_ang_R_f_t[ii]/abs(ylist_mean_ang_R_f_t[ii])  for ii in xlist_trials]
	#
	print(log_trials)
	print(xlist_trials)
	#
	print([ret_list[ii][1]  for ii in log_trials])
	print([ret_list[ii][4]  for ii in log_trials])
	#
	print(ylist_mean_mag_R_f_t)
	print(ylist_mean_ang_R_f_t)
	print(ylist_std_mag_R_f_t )
	print(ylist_std_ang_R_f_t )
	print(ylist_cv_mag_R_f_t  )
	print(ylist_cv_ang_R_f_t  )
	#
	newFile.write('{} = {}\n'.format('filename_anal        ', filename_anal ))
	#
	newFile.write('{} = {}\n'.format('xlist_test_freq_kHz     ', xlist_test_freq_kHz ))
	#
	newFile.write('{} = {}\n'.format('ylist_mean_mag_R_f_t', ylist_mean_mag_R_f_t))
	newFile.write('{} = {}\n'.format('ylist_mean_ang_R_f_t', ylist_mean_ang_R_f_t))
	newFile.write('{} = {}\n'.format('ylist_std_mag_R_f_t ', ylist_std_mag_R_f_t ))
	newFile.write('{} = {}\n'.format('ylist_std_ang_R_f_t ', ylist_std_ang_R_f_t ))
	newFile.write('{} = {}\n'.format('ylist_cv_mag_R_f_t  ', ylist_cv_mag_R_f_t  ))
	newFile.write('{} = {}\n'.format('ylist_cv_ang_R_f_t  ', ylist_cv_ang_R_f_t  ))
	#
	newFile.write('----\n')
	#
	newFile.write('{:22}, {:22}, {:22}, {:22}, {:22}, {:22}, {:22}\n'.format(
			'test freq[kHz]', 
			'mean_mag_R_f_t',
			'std_mag_R_f_t ',
			'cv_mag_R_f_t[ppm]',
			'mean_ang_R_f_t',
			'std_ang_R_f_t ',
			'cv_ang_R_f_t[ppm]'
			))
	for ii in xlist_trials:
		newFile.write('{:22}, {:22}, {:22}, {:22}, {:22}, {:22}, {:22}\n'.format(
			xlist_test_freq_kHz[ii], #float(test_freq_labels[ii]), 
			ylist_mean_mag_R_f_t  [ii] ,
			ylist_std_mag_R_f_t   [ii] ,
			ylist_cv_mag_R_f_t    [ii]*1e6,
			ylist_mean_ang_R_f_t  [ii] ,
			ylist_std_ang_R_f_t   [ii] ,
			ylist_cv_ang_R_f_t    [ii]*1e6
			))
	#
	newFile.close()

# log filename
filename_anal = 'ANAL_'+FILE_PREFIX+FILE_POSTFIX[2:-5]+'.txt'
print(filename_anal)

# call 
f_write_anal_file(filename_anal, filenames_to_read_list, ret_list)



# done 

