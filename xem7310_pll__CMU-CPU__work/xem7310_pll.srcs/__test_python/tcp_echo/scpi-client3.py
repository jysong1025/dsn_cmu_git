## scpi-client3.py
import socket
#import time
from time import sleep
import sys

# connect(), close() ... style

# https://docs.python.org/3/library/socket.html
# https://docs.python.org/2/library/socket.html

## control parameters 

HOST = '192.168.172.1'  # The server's hostname or IP address
HOST2 = '192.168.168.123'  # The server's hostname or IP address
PORT = 5025               # The port used by the server
ss = None # socket
#
cmd_str__IDN = b'*IDN?\n'
cmd_str__RST = b'*RST\n'
cmd_str__CMEP_EN = b':CMEP:EN'
#
cmd_str__CMEP_MKWI = b':CMEP:MKWI'
cmd_str__CMEP_MKWO = b':CMEP:MKWO'
cmd_str__CMEP_MKTI = b':CMEP:MKTI'
cmd_str__CMEP_MKTO = b':CMEP:MKTO'
#
cmd_str__CMEP_WI = b':CMEP:WI'
cmd_str__CMEP_WO = b':CMEP:WO'
cmd_str__CMEP_TI = b':CMEP:TI'
cmd_str__CMEP_TO = b':CMEP:TO'
cmd_str__CMEP_TAC = b':CMEP:TAC'
cmd_str__CMEP_TMO = b':CMEP:TMO'
cmd_str__CMEP_PI = b':CMEP:PI'
cmd_str__CMEP_PO = b':CMEP:PO'
#
cmd_str__CMEP_WMI = b':CMEP:WMI'
cmd_str__CMEP_WMO = b':CMEP:WMO'

## functions

#def sleep(tt):
#	time.sleep(tt)

def scpi_open (timeout=3.3):
	try:
		ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ss.settimeout(timeout)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 16384)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 16384) # 8192 16384 32768 65536
	except OSError as msg:
		ss = None
		print('error in socket: ', msg)
		raise
	return ss

def scpi_connect (ss, HOST, PORT):
	try:
		ss.connect((HOST, PORT))
	except OSError as msg:
		ss.close()
		ss = None
		print('error in connect: ', msg)
		raise


def scpi_close (ss):
	try:
		ss.close()
	except:
		if ss == None:
			print('error: ss==None')
		raise

def scpi_comm_resp_ss (ss, cmd_str, buf_size=2048, intval=0.01) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# recv data until finding the sentinel '\n'
	try:
		data = ss.recv(buf_size) # try 1024 131072 524288
		# try   
		while (1):
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
	except:
		print('error in recv')
		raise
	#
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# NG response check 
	if data[0:2]==b'NG':
		print('Received: NG as response')
		#input('Press Enter key!')
		sleep(3)
	#
	return data


# scpi command for numeric block response
def scpi_comm_resp_numb_ss (ss, cmd_str, buf_size=2048, intval=0.01) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# cmd: ":CMEP:PO#HBC 524288\n"
	# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
	#
	# recv data until finding the sentinel '\n' // TODO: numeric block
	# but check the sentinel after the data byte count is met.
	try:
		# find the numeric head : must 10 in data 
		data = ss.recv(buf_size)
		while True:
			if len(data)>=10:
				break
			data = data + ss.recv(buf_size)
		#
		#print('header: ', repr(data[0:10])) # header
		#
		# find byte count 
		byte_count = int(data[3:9])
		#print('byte_count=', repr(byte_count)) 
		#
		# collect all data by byte count
		count_to_recv = byte_count + 10 + 1# add header count #add /n
		while True:
			if len(data)>=count_to_recv:
				break
			data = data + ss.recv(buf_size)
		#
		# check the sentinel 
		while True:
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
		#
	except:
		print('error in recv')
		raise
	#
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	data = data[10:(10+byte_count)]
	print('data:', data[0:20].hex(),  ' (first 20 bytes)')
	#
	return [byte_count, data]
	
## test routines

## open socket and connect scpi server
try:
	ss = scpi_open()
	scpi_connect(ss, HOST, PORT)
except socket.timeout:
	ss = scpi_open()
	scpi_connect(ss, HOST2, PORT)
except:
	raise

#01 scpi : *IDN?
print('>>> {} : {}'.format('Test',cmd_str__IDN))
rsp = scpi_comm_resp_ss(ss, cmd_str__IDN)
print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))

# scpi : NG...
cmd_str__TEST = b'WH?\n'
print('>>> {} : {}'.format('Test',cmd_str__TEST))
scpi_comm_resp_ss(ss, cmd_str__TEST)

#02 scpi : *RST? // reset board vs network IC
print('>>> {} : {}'.format('Test',cmd_str__RST))
#scpi_comm(cmd_str__RST)
scpi_comm_resp_ss(ss, cmd_str__RST)

# scpi : NG...
cmd_str__TEST = b'test ??? test'
print('>>> {} : {}'.format('Test',cmd_str__TEST))
scpi_comm_resp_ss(ss, cmd_str__TEST)

# scpi : response 
#scpi_resp()

#03 :CMEP:EN
# :CMEP:EN ON|OFF <NL>			
# :CMEP:EN? <NL>			
#
# ":CMEP:EN ON\n"
# ":CMEP:EN OFF\n"
# ":CMEP:EN?"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_EN))
scpi_comm_resp_ss(ss, cmd_str__CMEP_EN+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_EN+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_EN+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_EN+b'?\n')

#04 :CMEP:MKWI
# :CMEP:MKWI  #Hnnnnnnnn <NL>
# :CMEP:MKWI? <NL>
#
# ":CMEP:MKWI #HFFCCAAFF\n"
# ":CMEP:MKWI?\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_MKWI))
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWI+b' #HFFCCAA33\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWI+b'?\n')

#05 :CMEP:MKWO
# :CMEP:MKWO  #Hnnnnnnnn <NL>
# :CMEP:MKWO? <NL>
#
# ":CMEP:MKWO #HFF3355FF\n"
# ":CMEP:MKWO?\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_MKWO))
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWO+b' #HFF3355FF\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWO+b'?\n')

#06 :CMEP:MKTI 
# :CMEP:MKTI  #Hnnnnnnnn <NL>
# :CMEP:MKTI? <NL>
#
# ":CMEP:MKTI #HFA3355FF\n"
# ":CMEP:MKTI?\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_MKTI))
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKTI+b' #HFA3355FF\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKTI+b'?\n')

#07 :CMEP:MKTO 
# :CMEP:MKTO  #Hnnnnnnnn <NL>
# :CMEP:MKTO? <NL>
#
# ":CMEP:MKTO #HFA3355AF\n"
# ":CMEP:MKTO?\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_MKTO))
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKTO+b' #HFA3355AF\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKTO+b'?\n')


#08 :CMEP:WI
# :CMEP:WI#Hnn  #Hnnnnnnnn <NL>
# :CMEP:WI#Hnn? <NL>
#
# ":CMEP:WI#H00 #H00001234\n"
# ":CMEP:WI#H00?\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_WI))
scpi_comm_resp_ss(ss, cmd_str__CMEP_WI+b' #H00 #H00001234\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WI+b'#H00?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWI+b' #HFFFFFFFF\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWI+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WI+b'#H00 #HABCD1234\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WI+b'#H00?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWI+b' #HFFFF0000\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWI+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WI+b'#H00 #H00000000\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WI+b'#H00?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWI+b' #H0000FFFF\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWI+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WI+b'#H00 #H00000000\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WI+b'#H00?\n')

#09 :CMEP:WO
# :CMEP:WO#Hnn? <NL>
#
# ":CMEP:WO#H20?\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_WO))
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWO+b' #HFFFFFFFF\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKWO+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#H20?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#H23?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#H39?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#H21?\n')

#10 :CMEP:TI
# :CMEP:TI#Hnn  #Hnnnnnnnn <NL>
# :CMEP:TI#Hnn? <NL>
#
# ":CMEP:TI#H40 #H00000002\n"
# ":CMEP:TI#H40?\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_TI))
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40 #H00000002\n') # count2 up
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#H21?\n') # {count2,count1}
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40 #H00000002\n') # count2 up
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40 #H00000002\n') # count2 up
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#H21?\n') # {count2,count1}

#11 :CMEP:TO
# :CMEP:TO#Hnn? <NL>
#
# ":CMEP:TO#H60?\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_TO))
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKTO+b' #HFFFFFFFF\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_MKTO+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TO+b'#H60?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40 #H00000001\n') # count2 reset
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40 #H00000004\n') # count2 down
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#H21?\n') # {count2,count1}
scpi_comm_resp_ss(ss, cmd_str__CMEP_TO+b'#H60?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TO+b'#H60?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TI+b'#H40 #H00000001\n') # count2 reset
scpi_comm_resp_ss(ss, cmd_str__CMEP_TO+b'#H60?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TO+b'#H60?\n')

#12 :CMEP:TAC
# // void activate_mcs_ep_ti(u32 offset, u32 bit_loc);
# :CMEP:TAC#Hnn  #Hnn<NL>
#
# ":CMEP:TAC#H40 #H01\n"  vs ":CMEP:TAC#H40 01\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_TAC))
scpi_comm_resp_ss(ss, cmd_str__CMEP_TAC+b'#H40 #H02\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#H21?\n') # {count2,count1}
scpi_comm_resp_ss(ss, cmd_str__CMEP_TO+b'#H60?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TAC+b'#H40 #H00\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#H21?\n') # {count2,count1}
scpi_comm_resp_ss(ss, cmd_str__CMEP_TO+b'#H60?\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TO+b'#H60?\n')

#13 :CMEP:TMO
# // u32 is_triggered_mcs_ep_to(u32 offset, u32 mask);
# :CMEP:TMO#Hnn  #Hnnnnnnnn<NL>
#
# cmd: ":CMEP:TMO#H60 #HFFFF0000\n" vs ":CMEP:TMO#H60? #HFFFF0000\n"
# rsp: "ON\n" or "OFF\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_TMO))
scpi_comm_resp_ss(ss, cmd_str__CMEP_TMO+b'#H60 #HFFFF0000\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TAC+b'#H40 #H02\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#H21?\n') # {count2,count1}
scpi_comm_resp_ss(ss, cmd_str__CMEP_TAC+b'#H40 #H00\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TMO+b'#H60 #HFFFF0000\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_TMO+b'#H60 #HFFFF0000\n')

#14 :CMEP:PI
# :CMEP:PI#Hnn  #m_nnnnnn_rrrrrrrrrrrrrrrrrrrr <NL>
# m : byte number in unit = 4
# nnnnnn :  data byte length  ~< 001024 
# rrr..rrr : byte data 
#
# ":CMEP:PI#Hnn #4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
# cmd: ":CMEP:PI#H8A #4_001024_rrrrrrrrrr...rrrrrrrrrr\n"  // fifo @ 0x8A ... max 4096 depth = 2^12
print('>>> {} : {}'.format('Test',cmd_str__CMEP_PI))
#
#(1024).to_bytes(4, byteorder='little') #Return an array of bytes representing an integer.
# >>> (1024).to_bytes(4, byteorder='little')
# b'\x00\x04\x00\x00'
#
bdata = b''
#for ii in [0,1,2,3,4,5,6,7,8,9]:
for ii in [	0xDCBA0DF0,
			0x12341DF1,
			0x87652DF2,
			0xDCBA3DF3,
			0x12344DF4,
			0x87655DF5,
			0xDCBA6DF6,
			0x12347DF7,
			0x87658DF8,
			0x12349DF9]:
	bdata = bdata + ii.to_bytes(4, byteorder='little')
print('bdata to send: ' + bdata.hex()) # we can see data in little-endian.
scpi_comm_resp_ss(ss, cmd_str__CMEP_PI+b'#H8A #4_000040_'+ bdata + b'\n')
#              8c                 20c
# issue buffer check... little .. big order in.. buffer / fifo... must re-order 
# MCS IO mem on buf8 - buf32 location constraint is must.... 4byte-->u32*
#
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')
#scpi_comm_resp_ss(ss, cmd_str__CMEP_WO+b'#HAA?\n')

#15 :CMEP:PO
# :CMEP:PO#Hnn nnnnnn<NL>
#
# cmd: ":CMEP:PO#HAA 000040\n"
# cmd: ":CMEP:PO#HAA 001024\n"
# cmd: ":CMEP:PO#HBC 524288\n"
# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_PO))
[count,rsp] = scpi_comm_resp_numb_ss (ss, cmd_str__CMEP_PO+b'#HAA 000040\n', 2048)
print('len rcvd: ' + str(len(rsp)))
print('hex code rcvd: ' + rsp.hex())
#
#scpi_comm_resp_ss(ss, cmd_str__CMEP_PO+b'#HAA 001024\n')
cmd_str = cmd_str__CMEP_PO+b'#HBC 524288\n'
print(cmd_str)
[count,rsp] = scpi_comm_resp_numb_ss (ss, cmd_str) # 
#rsp = scpi_comm_resp_ss(ss, cmd_str__CMEP_PO+b'#HBC 052428\n', 52428) #
#rsp = scpi_comm_resp_ss(ss, cmd_str__CMEP_PO+b'#HBC 005242\n', 5242) # 
#rsp = scpi_comm_resp_ss(ss, cmd_str__CMEP_PO+b'#HBC 000524\n', 524) # OK
print('len rcvd: ' + str(len(rsp)))
#
# recv size issue ...
#   Note that both the IP header and the tcp header can have 
#   a size greater than 20, when using options, and so you may 
#   still encounter fragmentation with message shorter than 1460 bytes.
#   fragmentation with message shorter than 1460 bytes
#   python socket 1460 bytes fragmentation
#   
#   MTU in w5500 // see https://cdn.sparkfun.com/datasheets/Dev/Arduino/Shields/W5500_datasheet_v1.0.2_1.pdf
#      ... 1460 bytes is max of w5500 
#
#   how to receive fragmented packet in a single recv() in socket? --> use multiple recv()
#   tcp socket multiple message to recv()
#       https://www.software7.com/blog/tcp-and-fragmented-messages/
#
#        int numLeft = lengthOfMessage;
#                int len = 0;
#                int numBytes;
#                do {
#                    numBytes = recv(socket, buf + len, numLeft, 0);
#                    len += numBytes;
#                    numLeft -= numBytes;
#                } while(numBytes > 0 && len < lengthOfMessage);
#        
#                //check result of recv for closed socket (numBytes == 0) and errors (numBytes < 0)
#                //if no errors, then...
#   message ... may split 1024 bytes ...
#           ... may have a head message and data messages....
#
# int.from_bytes(b'\xF1\x10', byteorder='big')
# >>> int.from_bytes(b'\x01\xFF', byteorder='big')
# 511
# >>> int.from_bytes(b'\xFF\x01', byteorder='little')
# 511
#read_back = []
#for ii in range(0,10):
#    dat = int.from_bytes(bdata[ii*4:ii*4+4], byteorder='little')
#    #print(dat)
#    read_back += [dat]
#print(read_back)
#

#16 :CMEP:WMI
# :CMEP:WMI#Hnn  #Hnnnnnnnn #Hmmmmmmmm<NL>
#
# ":CMEP:WMI#H00 #HABCD1234 #HFF00FF00\n"
print('>>> {} : {}'.format('Test',cmd_str__CMEP_WMI))
scpi_comm_resp_ss(ss, cmd_str__CMEP_WMI+b'#H00 #HABCD1234 #HFF00FF00\n')


#17 :CMEP:WMO
# :CMEP:WMO#Hnn  #Hmmmmmmmm<NL>
#
# ":CMEP:WMO#H20 #HFFFF0000\n"
scpi_comm_resp_ss(ss, cmd_str__CMEP_WMO+b'#H20 #HFFFF0000\n')


## CMEP disable
print('>>> {} : {}'.format('Test',cmd_str__CMEP_EN))
scpi_comm_resp_ss(ss, cmd_str__CMEP_EN+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__CMEP_EN+b'?\n')


## close socket
scpi_close(ss)


##



