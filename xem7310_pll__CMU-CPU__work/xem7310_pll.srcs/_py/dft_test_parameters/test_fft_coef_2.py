import numpy as np
import time

# test example:  
#NN=2**17
#loc_to_collect_in_xx = 3
#loc_to_collect_in_XX = 5
#
#xx = np.zeros(NN)
#xx[loc_to_collect_in_xx] = 1
#
#XX = np.fft.fft(xx)
#
#print('{}'.format(XX))
#print('{}'.format(XX[loc_to_collect_in_XX]))
#print('{}, {}'.format( np.real(XX[loc_to_collect_in_XX]), np.imag(XX[loc_to_collect_in_XX]) ))

tic = time.clock()

# setup
#NN=2**17
#NN=2**16
NN=2**10
loc_to_collect_in_XX = 5


# file open
filename_write = 'fft_coef_from__exp__{}_{}.csv'.format(NN,loc_to_collect_in_XX)
with open(filename_write, 'w', newline='') as f:
    for ii in range(NN):
        if (ii%100==0) : print(ii)
        #
        #xx = np.zeros(NN)
        #xx[ii] = 1
        #XX = np.fft.fft(xx)
        #WW = XX[loc_to_collect_in_XX]
        #
        WW_exp = np.exp(-2*np.pi/NN*(loc_to_collect_in_XX)*1j*ii)
        #
        #err = np.abs(WW-WW_exp)
        #
        f.write('{}, {} \n'.format( np.real(WW_exp), np.imag(WW_exp) ))
        #f.write('{}, {}, {}, {}, {} \n'.format( np.real(WW), np.imag(WW), np.real(WW_exp), np.imag(WW_exp), err ))

toc = time.clock()

print('{}'.format(toc-tic))
