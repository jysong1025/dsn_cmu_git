## cmu_cpu__20_lan_support_test.py : test code for cowork.

## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org

# power options: ON/OFF 
# DAC options: any ... 0x0000~0x7FFF ... 0V ~ 10V
# dwave freq options: 100kHz ~ 10MHz
# filter bw options: 120kHz, 1.2MHz, 12MHz, 120MHz
# ADC gain options 
#
# test case: no signal / noise level check @ 10.4Msps, 9.6Msps, 960ksps
# test case: normal sampling with 10.4Msps,  10kHz signal, BW 120kHz, some DAC // filter BW effect check
# test case: normal sampling with 10.4Msps, 100kHz signal, BW 120kHz, some DAC
# test case: normal sampling with 10.4Msps,   1MHz signal, BW 1.2MHz, some DAC
# test case:   down-sampling with  960ksps,   1MHz signal, BW 1.2MHz, some DAC -->  40kHz image
# test case:   down-sampling with  1.9Mpps,   2MHz signal, BW 1.2MHz, some DAC --> 100kHz image
# test case:   down-sampling with  3.9Msps,   4MHz signal, BW  12MHz, some DAC --> 100kHz image // filter poor a bit ... R-net small or BW 5MHz or pwm-4level needed...
# test case:   down-sampling with  9.6Msps,  10MHz signal, BW  12MHz, some DAC --> 400kHz image // 10MHz dwave poor
#  
# cf. down-converting = oversampling and decimation...
# note  down-sampling = analog decimation...
#  
#  
#  test case 0-0: no signal / noise level check @ 10.4Msps
#  test case 0-1: no signal / noise level check @ 960ksps
#  test case 1-0: normal sampling with 10.4Msps,  10kHz signal, BW 120kHz, some DAC
#  test case 1-1: normal sampling with 10.4Msps, 100kHz signal, BW 120kHz, some DAC
#  test case 1-2: normal sampling with 10.4Msps,   1MHz signal, BW 1.2MHz, some DAC
#  test case 2-0:   down-sampling with  960ksps,   1MHz signal, BW 1.2MHz, some DAC -->  40kHz image
#  test case 2-1:   down-sampling with  1.9Mpps,   2MHz signal, BW 1.2MHz, some DAC --> 100kHz image
#  test case 2-1:   down-sampling with  3.9Msps,   4MHz signal, BW  12MHz, some DAC --> 100kHz image
#  




###########################################################################
# check debug mode
if __debug__:
	print('>>> In debug mode ... ')


###########################################################################
###########################################################################


####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
BIT_FILENAME = ''; 
#
####


####
## common
#  
def _form_dict_idx_(var,idx):
	return '{:20} --> {}'.format(idx, var[idx])
def _form_dict_idx_hex_16b_(var,idx):
	return '{:20} --> {:#04x}'.format(idx, var[idx])
def _form_hex_32b_(val):
	return '0x{:08X}'.format(val)
#


####
## TODO : library call
##$$import ok_cmu_cpu__lib as cmu
##$$
import eps
import cmu_cpu__lib as cmu
import cmu_cpu__lib_conf as conf

#eps.eps_test()

# check configuration : EP_ADRS_CONFIG
##$$EP_ADRS = cmu.conf.EP_ADRS_CONFIG
EP_ADRS = conf.EP_ADRS_CONFIG
#  

#  display some configuration info
print(_form_dict_idx_(EP_ADRS,'board_name'))
print(_form_dict_idx_(EP_ADRS,'ver'))
print(_form_dict_idx_(EP_ADRS,'bit_filename'))
#
print(_form_dict_idx_hex_16b_(EP_ADRS,'FPGA_IMAGE_ID'))
print(_form_dict_idx_hex_16b_(EP_ADRS,'TEST_CON'))
print(_form_dict_idx_hex_16b_(EP_ADRS,'BRD_CON'))
#
####


####
## TODO: init : dev
##$$dev = cmu.ok_cmu_init()
##$$print(dev)

dev = eps.EPS_Dev()


####
## TODO: open
##$$ret = cmu.ok_cmu_open()
##$$print(ret)

## test ip 
_host_,_port_ = eps.set_host_ip_by_ping()
print(_host_)
print(_port_)

## open socket and connect
ss = dev.Open(_host_,_port_) 

## flag check max retry count
MAX_CNT = 50
##MAX_CNT = 500 # 20000 # 280 for 192000 samples
#MAX_CNT = 5000 # 1500 for 25600 samples @ 12500 count period	

## check open 
if dev.IsOpen():
	print('>>> device opened.')
else:
	print('>>> device is not open.')
	## raise
	input('')
	MAX_CNT = 3	

## get serial from board 
ret = dev.GetSerialNumber()
print(ret)


####
## FPGA_CONFIGURE
##$$if FPGA_CONFIGURE==1: 
##$$	ret = cmu.ok_cmu_conf(BIT_FILENAME)
##$$	print(ret)
  

####
## read fpga_image_id
##$$fpga_image_id = cmu.cmu_read_fpga_image_id()
dev.UpdateWireOuts()
fpga_image_id = dev.GetWireOutValue(EP_ADRS['FPGA_IMAGE_ID'])
#
print(_form_hex_32b_(fpga_image_id))

####
## read FPGA internal temp and volt
##$$ret = cmu.cmu_monitor_fpga()
dev.UpdateWireOuts()
fpga_image_id = dev.GetWireOutValue(EP_ADRS['XADC_TEMP'])  
print(fpga_image_id)


########
# TEST on

####
## test counter on 
ret = cmu.cmu_test_counter(dev, EP_ADRS, 'ON')
print(ret)



##############################################
# TODO: test board control

# BRD_CON[0]  = hw reset
# BRD_CON[16] = timestamp disp en
print('\n>> {}'.format('test hw reset'))
dev.SetWireInValue(EP_ADRS['BRD_CON'],0x00000001,0x00010001) # (ep,val,mask)
dev.UpdateWireIns()
#
input('ENTER key')

print('\n>> {}'.format('test timestamp disp en'))
dev.SetWireInValue(EP_ADRS['BRD_CON'],0x00010000,0x00010001) # (ep,val,mask)
dev.UpdateWireIns()
#
time_stamp = []
#
dev.UpdateWireOuts()
time_stamp += [dev.GetWireOutValue(EP_ADRS['TIME_STAMP'])]
print('{} = {}'.format('time_stamp',time_stamp))
#
dev.UpdateWireOuts()
time_stamp += [dev.GetWireOutValue(EP_ADRS['TIME_STAMP'])]
print('{} = {}'.format('time_stamp',time_stamp))
#
dev.UpdateWireOuts()
time_stamp += [dev.GetWireOutValue(EP_ADRS['TIME_STAMP'])]
print('{} = {}'.format('time_stamp',time_stamp))
#
time_stamp_diff = [zz[0]-zz[1] for zz in zip(time_stamp[1:], time_stamp[0:-1])]
print('{} = {}'.format('time_stamp_diff',time_stamp_diff))

# convert time based on 10MHz or 100ns or 0.1us
print('{} = {}'.format('time_stamp_diff_us',[xx*0.1 for xx in time_stamp_diff]))
print('{} = {}'.format('time_stamp_diff_ms',[xx*0.1*0.001 for xx in time_stamp_diff]))

#
input('ENTER key')

print('\n>> {}'.format('test clear all'))
dev.SetWireInValue(EP_ADRS['BRD_CON'],0x00000000,0x00010001) # (ep,val,mask)
dev.UpdateWireIns()
#
input('ENTER key')


########
# TODO: SPO 

####
ret = cmu.cmu_spo_enable(dev, EP_ADRS)
print(ret)
####

####
ret = cmu.cmu_spo_init(dev, EP_ADRS)
print(ret)
####

####
ret = cmu.cmu_spo_bit__leds(dev, EP_ADRS,0xFF)
print(ret)
####

####
ret = cmu.cmu_spo_bit__amp_pwr(dev, EP_ADRS, 'ON')
#ret = cmu.cmu_spo_bit__amp_pwr(dev, EP_ADRS,'OFF')
print(ret)
####

##$$  ####
##$$  # adc gain 
##$$  cmu.cmu_spo_bit__adc0_gain('1X')
##$$  #cmu.cmu_spo_bit__adc0_gain('10X')
##$$  #cmu.cmu_spo_bit__adc0_gain('100X')
##$$  cmu.cmu_spo_bit__adc1_gain('1X')
##$$  #cmu.cmu_spo_bit__adc1_gain('10X')
##$$  #cmu.cmu_spo_bit__adc1_gain('100X')
##$$  ####
##$$  
##$$  ####
##$$  # filter path 
##$$  ## BW 120MHz 
##$$  #cmu.cmu_spo_bit__vi_bw('120M')
##$$  #cmu.cmu_spo_bit__vq_bw('120M')
##$$  ## BW 12MHz 
##$$  #cmu.cmu_spo_bit__vi_bw('12M')
##$$  #cmu.cmu_spo_bit__vq_bw('12M')
##$$  ## BW 1.2MHz 
##$$  cmu.cmu_spo_bit__vi_bw('1M2')
##$$  cmu.cmu_spo_bit__vq_bw('1M2')
##$$  ## BW 120kHz 
##$$  #cmu.cmu_spo_bit__vi_bw('120K')
##$$  #cmu.cmu_spo_bit__vq_bw('120K')
##$$  
##$$  ####


########
# TODO: DAC 

##$$  ####
##$$  ret = cmu.cmu_dac_bias_enable()
##$$  print(ret)
##$$  ####
##$$  
##$$  ####
##$$  ret = cmu.cmu_dac_bias_init()
##$$  print(ret)
##$$  ####
##$$  
##$$  
##$$  #### setup internal bias DAC 
##$$  DAC1_CODE = 0x0000
##$$  DAC2_CODE = 0x0000
##$$  DAC3_CODE = 0x0000 
##$$  DAC4_CODE = 0x0000
##$$  #
##$$  ## DAC1_CODE = 0x1000
##$$  ## DAC2_CODE = 0x1000
##$$  ## ## for filter-out monitoring (making small)
##$$  ## #DAC3_CODE = 0x0800
##$$  ## #DAC4_CODE = 0x0800
##$$  ## DAC3_CODE = 0x1000 # normal 
##$$  ## DAC4_CODE = 0x1000
##$$  ## #DAC3_CODE = 0x2000
##$$  ## #DAC4_CODE = 0x2000
##$$  ## #DAC3_CODE = 0x2800
##$$  ## #DAC4_CODE = 0x2800
##$$  ## #DAC3_CODE = 0x4000
##$$  ## #DAC4_CODE = 0x4000
##$$  ## ## for 8f-4level monitoring
##$$  ## #DAC3_CODE = 0x2000 
##$$  ## #DAC4_CODE = 0x2000
##$$  ret = cmu.cmu_dac_bias_set_buffer(
##$$  		DAC1=DAC1_CODE,
##$$  		DAC2=DAC2_CODE,
##$$  		DAC3=DAC3_CODE,
##$$  		DAC4=DAC4_CODE)
##$$  print(ret)
##$$  ####
##$$  
##$$  
##$$  ####
##$$  ret = cmu.cmu_dac_bias_update()
##$$  print(ret)
##$$  ####
##$$  
##$$  ####
##$$  ret = cmu.cmu_dac_bias_readback()
##$$  print(ret)
##$$  ####


#######
# TODO: DWAVE

####
# dwave_enable 
ret = cmu.cmu_dwave_enable(dev, EP_ADRS)
print(ret)
####

####
# read dwave base freq
ret = cmu.cmu_dwave_read_base_freq(dev, EP_ADRS)
print(ret)
####

####
# dwave_wr_cnt_period
#   dwave_base  80MHz/40000=2000Hz=2kHz
#   dwave_base 160MHz/40000=4000Hz=4kHz
#
## 10MHz
#CNT_PERIOD = 8
#CNT_DIFF   = 6
## 5MHz
#CNT_PERIOD = 16 
#CNT_DIFF   = 12
## 4MHz
#CNT_PERIOD = 20 
#CNT_DIFF   = 15
## 3.3MHz
#CNT_PERIOD = 24
#CNT_DIFF   = 28
## 2MHz
#CNT_PERIOD = 40 
#CNT_DIFF   = 30
## 1MHz
#CNT_PERIOD = 80 
#CNT_DIFF   = 60
## 800kHz
#CNT_PERIOD = 100 
#CNT_DIFF   =  75
## 625kHz # 80000000Hz/625kHz=128
#CNT_PERIOD =  128
#CNT_DIFF   =   96
## 400kHz@80MHz or 800kHz@160MHz
#CNT_PERIOD = 200 
#CNT_DIFF   = 150
## 200kHz@80MHz or 400kHz@160MHz
CNT_PERIOD = 400 
CNT_DIFF   = 300
## 100kHz
#CNT_PERIOD = 800 
#CNT_DIFF   = 600
## 50kHz@80MHz or 100kHz@160MHz
#CNT_PERIOD = 1600
#CNT_DIFF   = 1200
## 10kHz
#CNT_PERIOD = 8000
#CNT_DIFF   = 6000
## 4kHz
#CNT_PERIOD = 20000
#CNT_DIFF   = 15000
#  
ret = cmu.cmu_dwave_wr_cnt_period(dev, EP_ADRS, CNT_PERIOD)
print(ret)
#
# dwave_wr_cnt_diff
ret = cmu.cmu_dwave_wr_cnt_diff(dev, EP_ADRS, CNT_DIFF)
print(ret)
####

####
ret = cmu.cmu_dwave_wr_output_dis__enable_all(dev, EP_ADRS)
#ret = cmu.cmu_dwave_wr_output_dis__enable_path_i_only()
#ret = cmu.cmu_dwave_wr_output_dis__enable_path_q_only()
print(ret)
####

####
# set dwave parameters
ret = cmu.cmu_dwave_set_para(dev, EP_ADRS)
print(ret)
####

####
# dwave_pulse_on
ret = cmu.cmu_dwave_pulse_on(dev, EP_ADRS)
print(ret)
####


####
# wait for pulse stability
cmu.sleep(1)


#############################################


########
# TODO: ADC

####
# cmu_adc_enable
ret = cmu.cmu_adc_enable(dev, EP_ADRS)
print(ret)
####

####
# cmu_adc_reset
ret = cmu.cmu_adc_reset(dev, EP_ADRS)
print(ret)
####

####
# cmu_adc_set_para
#
ADC_BASE_FREQ       =210000000
#ADC_BASE_FREQ       =125000000
#
FS_TARGET          = 15000000   # 
#FS_TARGET          =10416666   # 125000000/   12=10416666 // 10.4Msps ###
#FS_TARGET          = 9615384   # 125000000/   13= 9615384 //  9.6Msps
#FS_TARGET          = 3906250   # 125000000/   32= 3906250 //  3.9Msps ###
#FS_TARGET          = 1893939   # 125000000/  66= 1893939 //  1.9Msps
#FS_TARGET          =  961538   # 125000000/  130=  961538 //  960ksps ###
#FS_TARGET          =   96153   # 125000000/ 1300=   96153 //   96ksps
#FS_TARGET          =    9615   # 125000000/13000=    9615 //  9615sps
#
#FS_TARGET           =8333333
#FS_TARGET           =7400000
#FS_TARGET           = 992064    # 125000000/ 126= 992064
#FS_TARGET           =  98971    # 125000000/1263=  98971
ADC_NUM_SAMPLES     =131072
ADC_INPUT_DELAY_TAP =15
PIN_TEST_FRC_HIGH   =0
PIN_DLLN_FRC_LOW    =0
PTTN_CNT_UP_EN      =0
#
ret = cmu.cmu_adc_set_para (dev, EP_ADRS,
	ADC_BASE_FREQ       = ADC_BASE_FREQ      ,
	FS_TARGET           = FS_TARGET          ,
	ADC_NUM_SAMPLES     = ADC_NUM_SAMPLES    ,
	ADC_INPUT_DELAY_TAP = ADC_INPUT_DELAY_TAP,
	PIN_TEST_FRC_HIGH   = PIN_TEST_FRC_HIGH  ,
	PIN_DLLN_FRC_LOW    = PIN_DLLN_FRC_LOW   ,
	PTTN_CNT_UP_EN      = PTTN_CNT_UP_EN     )
print(ret)
####

####
# cmu_adc_init
ret = cmu.cmu_adc_init(dev, EP_ADRS)
print(ret)
####

####
# cmu_check_adc_test_pattern
ret = cmu.cmu_check_adc_test_pattern(dev, EP_ADRS)
print(ret)
####

####
# cmu_adc_is_fifo_empty
ret = cmu.cmu_adc_is_fifo_empty(dev, EP_ADRS)
print(ret)
####


####
# cmu_adc_update
ret = cmu.cmu_adc_update(dev, EP_ADRS)
print(ret)
####


####
# cmu_adc_load_from_fifo
adc_list = cmu.cmu_adc_load_from_fifo(dev, EP_ADRS)
####


####
# adc_display_data_list_int
#ret = cmu.cmu_adc_display_data_list_int (adc_list,FS_TARGET)
#
DUMP_FILENAME='DUMP'
num_smp_zoom = 500
ret = cmu.cmu_adc_display_data_list_int__zoom (adc_list,DUMP_FILENAME,FS_TARGET,num_smp_zoom)
print(ret)
####



########

####
input('> See DWAVE on. Enter...')
####


####
# work above!
####



###############################
## finish test
###############################



########
# ADC off

####
# cmu_adc_disable
ret = cmu.cmu_adc_disable(dev, EP_ADRS)
print(ret)
####

########
# DWAVE off

####
# dwave_pulse_off
ret = cmu.cmu_dwave_pulse_off(dev, EP_ADRS)
print(ret)
####

####
# dwave_disable
ret = cmu.cmu_dwave_disable(dev, EP_ADRS)
print(ret)
####



########
# DAC off

##  ####
##  ret = cmu.cmu_dac_bias_set_buffer() # set default values
##  print(ret)
##  ####
##  
##  ####
##  ret = cmu.cmu_dac_bias_update() 
##  print(ret)
##  ####
##  
##  ####
##  ret = cmu.cmu_dac_bias_disable() 
##  print(ret)
##  ####


########
# SPO off

####
ret = cmu.cmu_spo_bit__amp_pwr(dev, EP_ADRS,'OFF')
print(ret)
####


####
ret = cmu.cmu_spo_bit__leds(dev, EP_ADRS,0x00)
print(ret)
####


####
ret = cmu.cmu_spo_disable(dev, EP_ADRS)
print(ret)
####


########
# TEST off

####
## test counter off
##$$ret = cmu.cmu_test_counter('OFF')
ret = cmu.cmu_test_counter(dev, EP_ADRS,'OFF')
print(ret)
####

####
## test counter reset
##$$ret = cmu.cmu_test_counter('RESET')
ret = cmu.cmu_test_counter(dev, EP_ADRS,'RESET')
print(ret)
####


## close socket
dev.Close()
print('>>> device closed.')


##############################################







