## ok_cmu_cpu__08_cowork_dac_dwave_filter_adc.py : test code for cowork.

## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org

# power options: ON/OFF 
# DAC options: any ... 0x0000~0x7FFF ... 0V ~ 10V
# dwave freq options: 100kHz ~ 10MHz
# filter bw options: 120kHz, 1.2MHz, 12MHz, 120MHz
# ADC gain options 
#
# test case: no signal / noise level check @ 10.4Msps, 9.6Msps, 960ksps
# test case: normal sampling with 10.4Msps,  10kHz signal, BW 120kHz, some DAC // filter BW effect check
# test case: normal sampling with 10.4Msps, 100kHz signal, BW 120kHz, some DAC
# test case: normal sampling with 10.4Msps,   1MHz signal, BW 1.2MHz, some DAC
# test case:   down-sampling with  960ksps,   1MHz signal, BW 1.2MHz, some DAC -->  40kHz image
# test case:   down-sampling with  1.9Mpps,   2MHz signal, BW 1.2MHz, some DAC --> 100kHz image
# test case:   down-sampling with  3.9Msps,   4MHz signal, BW  12MHz, some DAC --> 100kHz image // filter poor a bit ... R-net small or BW 5MHz or pwm-4level needed...
# test case:   down-sampling with  9.6Msps,  10MHz signal, BW  12MHz, some DAC --> 400kHz image // 10MHz dwave poor
#  
# cf. down-converting = oversampling and decimation...
# note  down-sampling = analog decimation...
#  
#  
#  test case 0-0: no signal / noise level check @ 10.4Msps
#  test case 0-1: no signal / noise level check @ 960ksps
#  test case 1-0: normal sampling with 10.4Msps,  10kHz signal, BW 120kHz, some DAC
#  test case 1-1: normal sampling with 10.4Msps, 100kHz signal, BW 120kHz, some DAC
#  test case 1-2: normal sampling with 10.4Msps,   1MHz signal, BW 1.2MHz, some DAC
#  test case 2-0:   down-sampling with  960ksps,   1MHz signal, BW 1.2MHz, some DAC -->  40kHz image
#  test case 2-1:   down-sampling with  1.9Mpps,   2MHz signal, BW 1.2MHz, some DAC --> 100kHz image
#  test case 2-1:   down-sampling with  3.9Msps,   4MHz signal, BW  12MHz, some DAC --> 100kHz image
#  

####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
BIT_FILENAME = ''; 
#
####

####
## library call
import ok_cmu_cpu__lib as cmu
#
# check configuration : OK_EP_ADRS_CONFIG
#  
#EP_ADRS = cmu.conf.OK_EP_ADRS_CONFIG
EP_ADRS = cmu.conf.EP_ADRS_CONFIG
#  
def form_contents(var,idx):
	return '{} = {}'.format(idx, var[idx])
#
def form_address(var,idx):
	return '{} @ {:#04x}'.format(idx, var[idx])
#  
print(form_contents(EP_ADRS,'board_name'))
print(form_contents(EP_ADRS,'ver'))
print(form_contents(EP_ADRS,'bit_filename'))
#
print(form_address(EP_ADRS,'FPGA_IMAGE_ID'))
#
####

####
## TODO: init : dev
dev = cmu.ok_cmu_init()
print(dev)
####

####
ret = cmu.ok_cmu_caller_id()
print(ret)
####

####
## TODO: open
ret = cmu.ok_cmu_open()
print(ret)
####

####
## FPGA_CONFIGURE
if FPGA_CONFIGURE==1: 
	ret = cmu.ok_cmu_conf(BIT_FILENAME)
	print(ret)
####  

####
## read fpga_image_id
fpga_image_id = cmu.cmu_read_fpga_image_id()
#
def form_hex_32b(val):
	return '0x{:08X}'.format(val)
#
print(form_hex_32b(fpga_image_id))
####

####
## read FPGA internal temp and volt
ret = cmu.cmu_monitor_fpga()
print(ret)
####

####
## test counter on 
ret = cmu.cmu_test_counter('ON')
print(ret)
####

########
# TODO: SPO 

####
ret = cmu.cmu_spo_enable()
print(ret)
####

####
ret = cmu.cmu_spo_init()
print(ret)
####

####
ret = cmu.cmu_spo_bit__leds(0xFF)
print(ret)
####

####
ret = cmu.cmu_spo_bit__amp_pwr('ON')
#ret = cmu.cmu_spo_bit__amp_pwr('OFF')
print(ret)
####

####
# adc gain 
cmu.cmu_spo_bit__adc0_gain('1X')
#cmu.cmu_spo_bit__adc0_gain('10X')
#cmu.cmu_spo_bit__adc0_gain('100X')
cmu.cmu_spo_bit__adc1_gain('1X')
#cmu.cmu_spo_bit__adc1_gain('10X')
#cmu.cmu_spo_bit__adc1_gain('100X')
####

####
# filter path 
## BW 120MHz 
#cmu.cmu_spo_bit__vi_bw('120M')
#cmu.cmu_spo_bit__vq_bw('120M')
## BW 12MHz 
#cmu.cmu_spo_bit__vi_bw('12M')
#cmu.cmu_spo_bit__vq_bw('12M')
## BW 1.2MHz 
cmu.cmu_spo_bit__vi_bw('1M2')
cmu.cmu_spo_bit__vq_bw('1M2')
## BW 120kHz 
#cmu.cmu_spo_bit__vi_bw('120K')
#cmu.cmu_spo_bit__vq_bw('120K')

####


########
# TODO: DAC 

#//{


####
ret = cmu.cmu_dac_bias_enable()
print(ret)
####

####
ret = cmu.cmu_dac_bias_init()
print(ret)
####


#### setup internal bias DAC 
DAC1_CODE = 0x0000
DAC2_CODE = 0x0000
DAC3_CODE = 0x0000 
DAC4_CODE = 0x0000
#
## DAC1_CODE = 0x1000
## DAC2_CODE = 0x1000
## ## for filter-out monitoring (making small)
## #DAC3_CODE = 0x0800
## #DAC4_CODE = 0x0800
## DAC3_CODE = 0x1000 # normal 
## DAC4_CODE = 0x1000
## #DAC3_CODE = 0x2000
## #DAC4_CODE = 0x2000
## #DAC3_CODE = 0x2800
## #DAC4_CODE = 0x2800
## #DAC3_CODE = 0x4000
## #DAC4_CODE = 0x4000
## ## for 8f-4level monitoring
## #DAC3_CODE = 0x2000 
## #DAC4_CODE = 0x2000
ret = cmu.cmu_dac_bias_set_buffer(
		DAC1=DAC1_CODE,
		DAC2=DAC2_CODE,
		DAC3=DAC3_CODE,
		DAC4=DAC4_CODE)
print(ret)
####


####
ret = cmu.cmu_dac_bias_update()
print(ret)
####

####
ret = cmu.cmu_dac_bias_readback()
print(ret)
####

#//}


#######
# TODO: DWAVE

####
# dwave_enable 
ret = cmu.cmu_dwave_enable()
print(ret)
####

####
# read dwave base freq
ret = cmu.cmu_dwave_read_base_freq()
print(ret)
####

####
# dwave_wr_cnt_period
#   dwave_base  80MHz/40000=2000Hz=2kHz
#   dwave_base 160MHz/40000=4000Hz=4kHz
#
## 10MHz
#CNT_PERIOD = 8
#CNT_DIFF   = 6
## 5MHz
#CNT_PERIOD = 16 
#CNT_DIFF   = 12
## 4MHz
#CNT_PERIOD = 20 
#CNT_DIFF   = 15
## 3.3MHz
#CNT_PERIOD = 24
#CNT_DIFF   = 28
## 2MHz
#CNT_PERIOD = 40 
#CNT_DIFF   = 30
## 1MHz
#CNT_PERIOD = 80 
#CNT_DIFF   = 60
## 800kHz
#CNT_PERIOD = 100 
#CNT_DIFF   =  75
## 625kHz # 80000000Hz/625kHz=128
#CNT_PERIOD =  128
#CNT_DIFF   =   96
## 400kHz@80MHz or 800kHz@160MHz
#CNT_PERIOD = 200 
#CNT_DIFF   = 150
## 200kHz@80MHz or 400kHz@160MHz
CNT_PERIOD = 400 
CNT_DIFF   = 300
## 100kHz
#CNT_PERIOD = 800 
#CNT_DIFF   = 600
## 50kHz@80MHz or 100kHz@160MHz
#CNT_PERIOD = 1600
#CNT_DIFF   = 1200
## 10kHz
#CNT_PERIOD = 8000
#CNT_DIFF   = 6000
## 4kHz
#CNT_PERIOD = 20000
#CNT_DIFF   = 15000
#  
ret = cmu.cmu_dwave_wr_cnt_period(CNT_PERIOD)
print(ret)
#
# dwave_wr_cnt_diff
ret = cmu.cmu_dwave_wr_cnt_diff(CNT_DIFF)
print(ret)
####

####
ret = cmu.cmu_dwave_wr_output_dis__enable_all()
#ret = cmu.cmu_dwave_wr_output_dis__enable_path_i_only()
#ret = cmu.cmu_dwave_wr_output_dis__enable_path_q_only()
print(ret)
####

####
# set dwave parameters
ret = cmu.cmu_dwave_set_para()
print(ret)
####

####
# dwave_pulse_on
ret = cmu.cmu_dwave_pulse_on()
print(ret)
####


####
# wait for pulse stability
cmu.sleep(1)

########
# TODO: ADC


####
# cmu_adc_enable
ret = cmu.cmu_adc_enable()
print(ret)
####

####
# cmu_adc_reset
ret = cmu.cmu_adc_reset()
print(ret)
####

####
# cmu_adc_set_para
#
ADC_BASE_FREQ       =210000000
#ADC_BASE_FREQ       =125000000
#
FS_TARGET          = 15000000   # 
#FS_TARGET          =10416666   # 125000000/   12=10416666 // 10.4Msps ###
#FS_TARGET          = 9615384   # 125000000/   13= 9615384 //  9.6Msps
#FS_TARGET          = 3906250   # 125000000/   32= 3906250 //  3.9Msps ###
#FS_TARGET          = 1893939   # 125000000/  66= 1893939 //  1.9Msps
#FS_TARGET          =  961538   # 125000000/  130=  961538 //  960ksps ###
#FS_TARGET          =   96153   # 125000000/ 1300=   96153 //   96ksps
#FS_TARGET          =    9615   # 125000000/13000=    9615 //  9615sps
#
#FS_TARGET           =8333333
#FS_TARGET           =7400000
#FS_TARGET           = 992064    # 125000000/ 126= 992064
#FS_TARGET           =  98971    # 125000000/1263=  98971
ADC_NUM_SAMPLES     =131072
ADC_INPUT_DELAY_TAP =15
PIN_TEST_FRC_HIGH   =0
PIN_DLLN_FRC_LOW    =0
PTTN_CNT_UP_EN      =0
#
ret = cmu.cmu_adc_set_para (
	ADC_BASE_FREQ       = ADC_BASE_FREQ      ,
	FS_TARGET           = FS_TARGET          ,
	ADC_NUM_SAMPLES     = ADC_NUM_SAMPLES    ,
	ADC_INPUT_DELAY_TAP = ADC_INPUT_DELAY_TAP,
	PIN_TEST_FRC_HIGH   = PIN_TEST_FRC_HIGH  ,
	PIN_DLLN_FRC_LOW    = PIN_DLLN_FRC_LOW   ,
	PTTN_CNT_UP_EN      = PTTN_CNT_UP_EN     )
print(ret)
####

####
# cmu_adc_init
ret = cmu.cmu_adc_init()
print(ret)
####

####
# cmu_check_adc_test_pattern
ret = cmu.cmu_check_adc_test_pattern()
print(ret)
####

####
# cmu_adc_is_fifo_empty
ret = cmu.cmu_adc_is_fifo_empty()
print(ret)
####


####
# cmu_adc_update
ret = cmu.cmu_adc_update()
print(ret)
####


####
# cmu_adc_load_from_fifo
adc_list = cmu.cmu_adc_load_from_fifo()
####


####
# adc_display_data_list_int
#ret = cmu.cmu_adc_display_data_list_int (adc_list,FS_TARGET)
#
DUMP_FILENAME='DUMP'
num_smp_zoom = 500
ret = cmu.cmu_adc_display_data_list_int__zoom (adc_list,DUMP_FILENAME,FS_TARGET,num_smp_zoom)
print(ret)
####


########

####
input('> See DWAVE on. Enter...')
####


####
# work above!
####


####
#cmu.sleep(3)
####


####
## adc_save_data_list_int_to_csv
#csv_filename = cmu.cmu_adc_save_data_list_int_to_csv(adc_list,'DUMP',FS_TARGET)
#print(csv_filename)
####


####
## cmu_adc_load_from_csv
#ret = cmu.cmu_adc_load_from_csv(csv_filename)
#adc_list_rb = ret[0]
#fs_rb       = ret[1]
#ns_rb       = ret[2]
#print(adc_list_rb[0][0:5])
#print(adc_list_rb[1][0:5])
#print(fs_rb)
#print(ns_rb)
####

####
## adc_display_data_list_int
#ret = cmu.cmu_adc_display_data_list_int(adc_list_rb,fs_rb)
#print(ret)
####


########
# ADC off

####
# cmu_adc_disable
ret = cmu.cmu_adc_disable()
print(ret)
####

########
# DWAVE off

####
# dwave_pulse_off
ret = cmu.cmu_dwave_pulse_off()
print(ret)
####

####
# dwave_disable
ret = cmu.cmu_dwave_disable()
print(ret)
####


########
# DAC off

####
ret = cmu.cmu_dac_bias_set_buffer() # set default values
print(ret)
####

####
ret = cmu.cmu_dac_bias_update() 
print(ret)
####

####
ret = cmu.cmu_dac_bias_disable() 
print(ret)
####

########
# SPO off

####
ret = cmu.cmu_spo_bit__amp_pwr('OFF')
print(ret)
####


####
ret = cmu.cmu_spo_bit__leds(0x00)
print(ret)
####


####
ret = cmu.cmu_spo_disable()
print(ret)
####

########
# TEST off

####
## test counter off
ret = cmu.cmu_test_counter('OFF')
print(ret)
####

####
## test counter reset
ret = cmu.cmu_test_counter('RESET')
print(ret)
####


####
## close
ret = cmu.ok_cmu_close()
print(ret)
####