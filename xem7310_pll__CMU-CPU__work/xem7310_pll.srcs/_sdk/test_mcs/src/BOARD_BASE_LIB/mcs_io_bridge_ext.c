//#include "xparameters.h"
//#include "xil_cache.h"

#include "microblaze_sleep.h"
#include "xil_printf.h" // print() for pure string; xil_printf() for formatted string

#include "ioLibrary/Ethernet/W5500/w5500.h" // for w5500 io functions

#include "mcs_io_bridge_ext.h" //$$ board dependent


// common subfunctions //{
u32 hexchr2data_u32(u8 hexchr) {
	// '0' -->  0
	// 'A' --> 10
	u32 val;
	s32 val_L;
	s32 val_H;
	//
	val_L = (s32)hexchr - (s32)'0';
	//if ((val_L>=0)&&(val_L<10)) {
	if (val_L<10) {
		val = (u32)val_L;
	}
	else {
		val_H = (s32)hexchr - (s32)'A';
		//if ((val_H>=0)&&(val_H<6)) {
		//	val = (u32)val_H+10;
		// }
		//else {
		//	val = (u32)(-1); // no hex code.
		// }
		val = (u32)val_H+10;
		//
		if (val>15) {
			val += (s32)'A' - (s32)'a';
		}
	}
	//
	return val; 
}

u32 hexstr2data_u32(u8* hexstr, u32 len) {
	u32 val;
	u32 loc;
	u32 ii;
	loc = 0;
	val = 0;
	for (ii=0;ii<len;ii++) {
		val = (val<<4) + hexchr2data_u32(hexstr[loc++]);
	}
	return val;
}

u32 decchr2data_u32(u8 decchr) {
	// '0' -->  0
	u32 val;
	s32 val_t;
	//
	val_t = (s32)decchr - (s32)'0';
	if (val_t<10) {
		val = (u32)val_t;
	}
	else {
		val = (u32)(-1); // no valid code.
	}
	//
	return val; 
}

u32 decstr2data_u32(u8* decstr, u32 len) {
	u32 val;
	u32 loc;
	u32 ii;
	loc = 0;
	val = 0;
	for (ii=0;ii<len;ii++) {
		val = (val*10) + decchr2data_u32(decstr[loc++]);
	}
	return val;
}

u32 is_dec_char(u8 chr) { // 0 for dec; -1 for not
	u32 ret;
	ret = decchr2data_u32(chr);
	if (ret != -1)  ret = 0;
	return ret;
}

void *memcpy(void *dest, const void *src, size_t n) {
	u32 ii;
	for (ii=0;ii<n;ii++) {
		*((u8*)dest) = *((u8*)src);
		dest++;
		src++;
	}	
}

//}


// subfunctions for pipe //{

void dcopy_pipe8_to_buf8  (u32 adrs_p8, u8 *p_buf_u8, u32 len) {
	u32 ii;
	u8 val;
	//
	for (ii=0;ii<len;ii++) {
		val = (u8)XIomodule_In32 (adrs_p8);
		p_buf_u8[ii] = val;
	}
}
	
void dcopy_buf8_to_pipe8  (u8 *p_buf_u8, u32 adrs_p8, u32 len) {
	u32 ii;
	u32 val;
	//
	for (ii=0;ii<len;ii++) {
		val = p_buf_u8[ii]&0x000000FF;
		XIomodule_Out32 (adrs_p8, val);
	}
}

void dcopy_pipe32_to_buf32(u32 adrs_p32, u32 *p_buf_u32, u32 len_byte) { // (src,dst,len)
	u32 ii;
	u32 len;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		p_buf_u32[ii] = XIomodule_In32 (adrs_p32);
	}
}


void dcopy_buf32_to_pipe32(u32 *p_buf_u32, u32 adrs_p32, u32 len_byte) { // (src,dst,len)
	u32 ii;
	u32 val;
	u32 len;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		val = p_buf_u32[ii];
		XIomodule_Out32 (adrs_p32, val);
	}
}

void dcopy_buf8_to_pipe32(u8 *p_buf_u8, u32 adrs_p32, u32 len_byte) { // (src,dst,len_byte)
	u32 ii;
	u32 val;
	//u32 *p_val;
	u8  *p_val_u8;
	u32 len;
	//
	//p_val = (u32*)p_buf_u8; //$$ NG ...  MCS IO mem on buf8 -> buf32 location has some constraint.
	p_val_u8 = (u8*)&val;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		p_val_u8[0] = p_buf_u8[ii*4+0];
		p_val_u8[1] = p_buf_u8[ii*4+1];
		p_val_u8[2] = p_buf_u8[ii*4+2];
		p_val_u8[3] = p_buf_u8[ii*4+3];
		//
		//xil_printf("check: 0x%08X\r\n",(unsigned int)val); //$$ test
		//
		XIomodule_Out32 (adrs_p32, val);
	}
}

void dcopy_pipe32_to_pipe32(u32 src_adrs_p32, u32 dst_adrs_p32, u32 len_byte) { // (src,dst,len)
	u32 ii;
	u32 len;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		XIomodule_Out32 (dst_adrs_p32, XIomodule_In32 (src_adrs_p32));
	}
}

void dcopy_pipe8_to_pipe32 (u32 src_adrs_p8,  u32 dst_adrs_p32, u32 len_byte) { // (src,dst,len)
	// src_adrs_p8          dst_adrs_p32
	// {AA},{BB},{CC},{DD}  {DD,CC,BB,AA}   
	u32 ii;
	u32 len;
	u32 val;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		val = 0   +  ((XIomodule_In32 (src_adrs_p8))&0x000000FF)     ;
		val = val + (((XIomodule_In32 (src_adrs_p8))&0x000000FF)<< 8);
		val = val + (((XIomodule_In32 (src_adrs_p8))&0x000000FF)<<16);
		val = val + (((XIomodule_In32 (src_adrs_p8))&0x000000FF)<<24);
		XIomodule_Out32 (dst_adrs_p32, val);
	}
}

void dcopy_pipe32_to_pipe8 (u32 src_adrs_p32, u32 dst_adrs_p8,  u32 len_byte) { // (src,dst,len)
	// src_adrs_p32    dst_adrs_p8
	// {DD,CC,BB,AA}   {AA},{BB},{CC},{DD}
	u32 ii;
	u32 len;
	u32 val;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		val = XIomodule_In32 (src_adrs_p32);
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
		val = val >> 8;
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
		val = val >> 8;
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
		val = val >> 8;
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
	}
}

//}



// === MCS io access functions === //{

//// for low-level driver test
u32 _test_read_mcs (char* txt, u32 adrs) {
	u32 value;
	xil_printf(txt);
	value = XIomodule_In32 (adrs);
	xil_printf("mcs rd: 0x%08X @ 0x%08X \r\n", value, adrs);
	return value;
}

u32 _test_write_mcs(char* txt, u32 adrs, u32 value) {
	xil_printf(txt);
	XIomodule_Out32 (adrs, value);
	xil_printf("mcs wr: 0x%08X @ 0x%08X \r\n", value, adrs);
	return value;
}


u32 read_mcs_io (u32 adrs) {
	u32 value = XIomodule_In32 (adrs);
	return value;
}

u32 write_mcs_io(u32 adrs, u32 value) {
	XIomodule_Out32 (adrs, value);
	return value;
}


//  u32   read_mcs_fpga_img_id(u32 adrs_base) {
//  	return XIomodule_In32 (adrs_base + ADRS_FPGA_IMAGE_OFST);
//  }
//  
//  u32   read_mcs_test_reg(u32 adrs_base) {
//  	return XIomodule_In32 (adrs_base + ADRS_TEST_REG___OFST);
//  }
//  
//  void write_mcs_test_reg(u32 adrs_base, u32 data) {
//  	      XIomodule_Out32 (adrs_base + ADRS_TEST_REG___OFST, data);
//  }

//}



// === dedicated LAN interface (wiznet 850io) functions === //{

// note : dedicated LAN port masks fixed to full mask in mcs_io_bridge_ext.v

u8 hw_reset__wz850() {
	//u32 adrs;
	u32 value;

	//// trig LAN RESET @  master_spi_wz850
	//xil_printf(">>> trig LAN RESET @  master_spi_wz850: \n\r");
	
	//_test_write_mcs(">>> ADRS_MASK_WO____MHVSU: \r\n", ADRS_MASK_WO____MHVSU, 0xFFFFFFFF);
	//_test_write_mcs(">>> ADRS_MASK_WO____MHVSU: \r\n", ADRS_MASK_WI____MHVSU, 0xFFFFFFFF);
	
	// TODO: initialize mask
	// write_mcs_io (ADRS_MASK_WO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_WI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_ALL___MHVSU, 0xFFFFFFFF);
	
	// read out trig ouu 
	read_mcs_io (ADRS_LAN_TO_60);		

	// {...,w_LAN_INTn,w_LAN_SSNn,w_LAN_RSTn,w_LAN_valid}
	//_test_read_mcs (">>> ADRS_LAN_WO_20_MHVSU : \r\n", ADRS_LAN_WO_20_MHVSU); 
	//_test_write_mcs(">>> ADRS_LAN_WI_00_MHVSU : \r\n", ADRS_LAN_WI_00_MHVSU, 0x00000001);
	//_test_read_mcs (">>> ADRS_LAN_WO_20_MHVSU : \r\n", ADRS_LAN_WO_20_MHVSU);
	//_test_write_mcs(">>> ADRS_LAN_WI_00_MHVSU : \r\n", ADRS_LAN_WI_00_MHVSU, 0x00000000);
	//_test_read_mcs (">>> ADRS_LAN_WO_20_MHVSU : \r\n", ADRS_LAN_WO_20_MHVSU);
	
	write_mcs_io(ADRS_LAN_WI_00, 0x00000001);
	write_mcs_io(ADRS_LAN_WI_00, 0x00000000);
	
	while (1) {
		//value = _test_read_mcs (">>> ADRS_LAN_WO_20_MHVSU : \r\n", ADRS_LAN_WO_20_MHVSU);
		value = read_mcs_io (ADRS_LAN_WO_20);
		if ((value & 0x03) == 0x03) break;
		usleep(100); // 1us*100 sleep
		}
	//
	return 0;
}

// for  uint8_t  WIZCHIP_READ(uint32_t AddrSel)
u8    read_data__wz850 (u32 AddrSel) {
	u8 ret;
	//u32 adrs;
	u32 value;

	// set up mode
	AddrSel |= (_W5500_SPI_READ_ | _W5500_SPI_VDM_OP_);

	// TODO: initialize mask
	// write_mcs_io (ADRS_MASK_WO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_WI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_ALL___MHVSU, 0xFFFFFFFF);

	//// set up frame
	//   ADRS_LAN_WI_00_MHVSU
	//   ADRS_LAN_WI_01_MHVSU
	//   ADRS_LAN_WO_20_MHVSU
	
	
	//  adrs = ADRS_PORT_WI_01;
	//  value = AddrSel&0x00FFFFFF;
	//  XIomodule_Out32 (adrs, value);
	
	value = AddrSel&0x00FFFFFF;
	write_mcs_io (ADRS_LAN_WI_01, value);

	
	//  //
	//  adrs = ADRS_PORT_WI_02; // move to ADRS_LAN_WI_00_MHVSU [31:16]
	//  value = 1;
	//  XIomodule_Out32 (adrs, value);

	value = 1 << 16; // set length to 1 at high 16b

	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0002; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	//  //

	//  value = value | 0x00000002; // ... , trig_frame, trig_reset
	//  write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);
	//  
	//  
	//  //  adrs = ADRS_PORT_WI_00;
	//  //  value = 0x0000; // ... , trig_frame, trig_reset
	//  //  XIomodule_Out32 (adrs, value);
	//  
	//  value = value & 0xFFFFFFFD; // ... , trig_frame, trig_reset
	//  write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);
	
	// TODO: send frame // OK 
	write_mcs_io (ADRS_LAN_WI_00, value); // set data count at high 16b
	write_mcs_io (ADRS_LAN_TI_40, 0x00000002); // trig frame
	
	
	
	//
	while (1) {
		//
		// adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		// value = XIomodule_In32 (adrs);
		
		// TODO: frame done 
		// value = read_mcs_io (ADRS_LAN_WO_20);
		// if ((value & 0x10) == 0x10) break;
		value = read_mcs_io (ADRS_LAN_TO_60);		
		if ((value & 0x00000002) == 0x00000002) break;
		
		//usleep(100); // 1us*100 sleep
		//
		}
	//

	//// read fifo data
	//   ADRS_LAN_PO_A0_MHVSU
	
	// adrs = ADRS_PORT_PO_A0;
	// value = XIomodule_In32 (adrs);
	// //
	// ret = value & (0x000000ff);
	
	value = read_mcs_io (ADRS_LAN_PO_A0);
	ret   = value & (0x000000FF);
	
	return ret;
}

// for  void WIZCHIP_WRITE(uint32_t AddrSel, uint8_t wb )
void write_data__wz850 (u32 AddrSel, u8 wb) {
	//u32 adrs;
	u32 value;

	// set up mode
	AddrSel |= (_W5500_SPI_WRITE_ | _W5500_SPI_VDM_OP_);

	// TODO: initialize mask
	// write_mcs_io (ADRS_MASK_WO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_WI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_ALL___MHVSU, 0xFFFFFFFF);


	//// write fifo data
	//   ADRS_LAN_PI_80_MHVSU
	
	//  adrs = ADRS_PORT_PI_80;
	//  value = 0x000000FF & wb;
	//  XIomodule_Out32 (adrs, value);
	
	value = 0x000000FF & wb;
	write_mcs_io (ADRS_LAN_PI_80, value);

	//// set up frame
	//   ADRS_LAN_WI_00_MHVSU
	//   ADRS_LAN_WI_01_MHVSU
	//   ADRS_LAN_WO_20_MHVSU

	//  adrs = ADRS_PORT_WI_01;
	//  value = AddrSel&0x00FFFFFF;
	//  XIomodule_Out32 (adrs, value);

	value = AddrSel&0x00FFFFFF;
	write_mcs_io (ADRS_LAN_WI_01, value);

	//  //
	//  adrs = ADRS_PORT_WI_02;
	//  value = 1;
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0002; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0000; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	//  
	
	value = 1 << 16; // set length to 1 at high 16b
	//  value = value | 0x00000002; // ... , trig_frame, trig_reset
	//  write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);
	//  value = value & 0xFFFFFFFD; // ... , trig_frame, trig_reset
	//  write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);
	
	// TODO: send frame 
	write_mcs_io (ADRS_LAN_WI_00, value); // set data count at high 16b
	write_mcs_io (ADRS_LAN_TI_40, 0x00000002); // trig frame
	
	//
	while (1) {
		//
		//  adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		//  value = XIomodule_In32 (adrs);

		// TODO: frame done 
		// value = read_mcs_io (ADRS_LAN_WO_20);
		// if ((value & 0x10) == 0x10) break;
		value = read_mcs_io (ADRS_LAN_TO_60);		
		if ((value & 0x00000002) == 0x00000002) break;

		//usleep(100); // 1us*100 sleep
		//
		}
	//
}

// for  void WIZCHIP_READ_BUF (uint32_t AddrSel, uint8_t* pBuf, uint16_t len)
// read data from LAN-fifo to buffer  // recv
void  read_data_buf__wz850 (u32 AddrSel, u8* pBuf, u16 len) {
	//u32 adrs;
	u32 value;

	// set up mode
	AddrSel |= (_W5500_SPI_READ_ | _W5500_SPI_VDM_OP_);

	// TODO: initialize mask
	// write_mcs_io (ADRS_MASK_WO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_WI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_ALL___MHVSU, 0xFFFFFFFF);

	//// set up frame
	//   ADRS_LAN_WI_00_MHVSU
	//   ADRS_LAN_WI_01_MHVSU
	//   ADRS_LAN_WO_20_MHVSU
	
	//  adrs = ADRS_PORT_WI_01;
	//  value = AddrSel&0x00FFFFFF;
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_02;
	//  value = len;
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0002; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0000; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	
	value = AddrSel&0x00FFFFFF;
	write_mcs_io (ADRS_LAN_WI_01, value);
	
	value = len << 16; // set length to len at high 16b
	//  value = value | 0x00000002; // ... , trig_frame, trig_reset
	//  write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);
	//  value = value & 0xFFFFFFFD; // ... , trig_frame, trig_reset
	//  write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);

	// TODO: send frame 
	write_mcs_io (ADRS_LAN_WI_00, value); // set data count at high 16b
	write_mcs_io (ADRS_LAN_TI_40, 0x00000002); // trig frame
	
	//
	while (1) {
		//
		//adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		//value = XIomodule_In32 (adrs);
		//

		// TODO: frame done 
		// value = read_mcs_io (ADRS_LAN_WO_20);
		// if ((value & 0x10) == 0x10) break;
		value = read_mcs_io (ADRS_LAN_TO_60);		
		if ((value & 0x00000002) == 0x00000002) break;
		
		//usleep(100); // 1us*100 sleep
		//
		}
	//

	//// read fifo data : dcopy pipe_8b to buf_8b
	//  dcopy_pipe8_to_buf8  (ADRS_PORT_PO_A0, pBuf, len);
	dcopy_pipe8_to_buf8  (ADRS_LAN_PO_A0, pBuf, len);
	
	//
}

// for  void WIZCHIP_WRITE_BUF(uint32_t AddrSel, uint8_t* pBuf, uint16_t len)
// write data from buffer to LAN-fifo // send
void write_data_buf__wz850 (u32 AddrSel, u8* pBuf, u16 len) {
	//u32 adrs;
	u32 value;

	// set up mode
	AddrSel |= (_W5500_SPI_WRITE_ | _W5500_SPI_VDM_OP_);

	// TODO: initialize mask
	// write_mcs_io (ADRS_MASK_WO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_WI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_ALL___MHVSU, 0xFFFFFFFF);

	// write buffer data to LAN-fifo : dcopy buf_8b to pipe_8b
	//dcopy_buf8_to_pipe8(pBuf, ADRS_PORT_PI_80, len);
	dcopy_buf8_to_pipe8(pBuf, ADRS_LAN_PI_80, len);

	//// set up frame
	//   ADRS_LAN_WI_00_MHVSU
	//   ADRS_LAN_WI_01_MHVSU
	//   ADRS_LAN_WO_20_MHVSU

	//  adrs = ADRS_PORT_WI_01;
	//  value = AddrSel&0x00FFFFFF;
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_02;
	//  value = len;
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0002; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0000; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	//

	value = AddrSel&0x00FFFFFF;
	write_mcs_io (ADRS_LAN_WI_01, value);
	
	value = len << 16; // set length to len at high 16b
	//  value = value | 0x00000002; // ... , trig_frame, trig_reset
	//  write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);
	//  value = value & 0xFFFFFFFD; // ... , trig_frame, trig_reset
	//  write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);

	// TODO: send frame 
	write_mcs_io (ADRS_LAN_WI_00, value); // set data count at high 16b
	write_mcs_io (ADRS_LAN_TI_40, 0x00000002); // trig frame
	
	//
	while (1) {
		//
		//  adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		//  value = XIomodule_In32 (adrs);
		//
		
		// TODO: frame done 
		// value = read_mcs_io (ADRS_LAN_WO_20);
		// if ((value & 0x10) == 0x10) break;
		value = read_mcs_io (ADRS_LAN_TO_60);		
		if ((value & 0x00000002) == 0x00000002) break;
		
		//usleep(100); // 1us*100 sleep
		//
		}
	//
}

// FROM w5500.c : WIZCHIP_READ_BUF
// FROM w5500.c : void wiz_recv_data(uint8_t sn, uint8_t *wizdata, uint16_t len)
// FROM socket.c: int32_t recv(uint8_t sn, uint8_t * buf, uint16_t len)
// FROM socket.c: int32_t recvfrom(uint8_t sn, uint8_t * buf, uint16_t len, uint8_t * addr, uint16_t *port)
// ...
// TO    WIZCHIP_READ_PIPE // test
// TO    void wiz_recv_data_pipe(uint8_t sn, uint8_t *wizdata, uint32_t len_u32)
// TO    int32_t recv_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32)
// TO    int32_t recvfrom_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32, uint8_t * addr, uint16_t *port)
//
// read data from LAN-fifo to pipe // recv
//void  read_data_pipe__wz850 (u32 AddrSel, u32 ep_offset, u32 len_u32) { // test
void  read_data_pipe__wz850 (u32 AddrSel, u32 dst_adrs_p32, u32 len_u32) { // test
	//u32 adrs;
	u32 value;
	u32 ii;
	//
	u32 len_4byte;
	u32 data_u32;
	u8  *p_data_u8;

	// set up mode
	AddrSel |= (_W5500_SPI_READ_ | _W5500_SPI_VDM_OP_);

	// TODO: initialize mask
	// write_mcs_io (ADRS_MASK_WO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_WI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_ALL___MHVSU, 0xFFFFFFFF);

 	// length based on 4 bytes
	len_4byte = len_u32>>2; // len_u32 must be muliple of 4.

	//// set up frame
	//   ADRS_LAN_WI_00_MHVSU
	//   ADRS_LAN_WI_01_MHVSU
	//   ADRS_LAN_WO_20_MHVSU
	
	//  adrs = ADRS_PORT_WI_01;
	//  value = AddrSel&0x00FFFFFF;
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_02;
	//  value = len_u32; //$$
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0002; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0000; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	//  //
	
	value = AddrSel&0x00FFFFFF;
	write_mcs_io (ADRS_LAN_WI_01, value);
	
	value = len_u32 << 16; // set length to len at high 16b //$$
	//  value = value | 0x00000002; // ... , trig_frame, trig_reset
	//  write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);
	//  value = value & 0xFFFFFFFD; // ... , trig_frame, trig_reset
	//  write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);

	// TODO: send frame 
	write_mcs_io (ADRS_LAN_WI_00, value); // set data count at high 16b
	write_mcs_io (ADRS_LAN_TI_40, 0x00000002); // trig frame
	
	while (1) {
		//
		//adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		//value = XIomodule_In32 (adrs);
		//
		
		// TODO: frame done 
		// value = read_mcs_io (ADRS_LAN_WO_20);
		// if ((value & 0x10) == 0x10) break;
		value = read_mcs_io (ADRS_LAN_TO_60);		
		if ((value & 0x00000002) == 0x00000002) break;
		
		//usleep(100); // 1us*100 sleep
		//
		}
	//

	//// read fifo data from LAN to pipe
	//
	// address of pipe end-point //$$ ep_offset --> dst_adrs_p32 
	//adrs = ADRS_BASE_CMU + (ep_offset<<4); // MCS1 //$$
	//
	p_data_u8 = (u8*)&data_u32;
	for (ii=0;ii<len_4byte;ii++) {
		value = XIomodule_In32 (ADRS_LAN_PO_A0); // read data0 from LAN-fifo
		p_data_u8[0] = (u8) (value & 0x00FF);
		//
		value = XIomodule_In32 (ADRS_LAN_PO_A0); // read data1 from LAN-fifo
		p_data_u8[1] = (u8) (value & 0x00FF);
		//
		value = XIomodule_In32 (ADRS_LAN_PO_A0); // read data2 from LAN-fifo
		p_data_u8[2] = (u8) (value & 0x00FF);
		//
		value = XIomodule_In32 (ADRS_LAN_PO_A0); // read data3 from LAN-fifo
		p_data_u8[3] = (u8) (value & 0x00FF);
		//
		//XIomodule_Out32 (adrs, data_u32);// write a 32-bit value to pipe end-point
		XIomodule_Out32 (dst_adrs_p32, data_u32);// write a 32-bit value to pipe end-point
	}
	//
}

// FROM w5500.c : WIZCHIP_WRITE_BUF
// FROM w5500.c : void wiz_send_data(uint8_t sn, uint8_t *wizdata, uint16_t len)
// FROM socket.c: int32_t send(uint8_t sn, uint8_t * buf, uint16_t len)
// FROM socket.c: int32_t sendto(uint8_t sn, uint8_t * buf, uint16_t len, uint8_t * addr, uint16_t port)
// ...
// TO    WIZCHIP_WRITE_PIPE // test
// TO    void wiz_send_data_pipe(uint8_t sn, uint8_t *wizdata, uint32_t len_u32)
// TO    int32_t send_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32)
// TO    int32_t sendto_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32, uint8_t * addr, uint16_t port)
//
// write data from pipe to LAN-fifo // send
void write_data_pipe__wz850 (u32 AddrSel, u32 src_adrs_p32, u32 len_u32) {
	//u32 adrs;
	u32 value;
	u32 ii;
	//
	u32 len_4byte;
	u32 data_u32;
	u8  *p_data_u8;

	// set up LAN spi mode
	AddrSel |= (_W5500_SPI_WRITE_ | _W5500_SPI_VDM_OP_);

	// TODO: initialize mask
	// write_mcs_io (ADRS_MASK_WO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_WI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TI____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_TO____MHVSU, 0xFFFFFFFF);
	// write_mcs_io (ADRS_MASK_ALL___MHVSU, 0xFFFFFFFF);

	// address of pipe end-point //$$ ep_offset --> src_adrs_p32
	//adrs = ADRS_BASE_CMU + (ep_offset<<4); // MCS1

 	// length based on 4 bytes
	len_4byte = len_u32>>2; // len_u32 must be muliple of 4.

	// note that 32 bits from pipe-out and 8 bits into LAN-fifo
	// need to send 4 bytes byte-by-byte ...
	p_data_u8 = (u8*)&data_u32;
	for (ii=0;ii<len_4byte;ii++) {
		//// read from data pipe such as adc pipe
		data_u32 = XIomodule_In32 (src_adrs_p32); // read a 32-bit value from pipe end-point
		//...
		//// write the value to LAN pipe end-point
		// send data in the order of ... p_data_u8[0], p_data_u8[1], p_data_u8[2], p_data_u8[3]
		value = 0x000000FF & p_data_u8[0];
		XIomodule_Out32 (ADRS_LAN_PI_80, value); // write data0 to LAN-fifo
		//
		value = 0x000000FF & p_data_u8[1];
		XIomodule_Out32 (ADRS_LAN_PI_80, value); // write data1 to LAN-fifo
		//
		value = 0x000000FF & p_data_u8[2];
		XIomodule_Out32 (ADRS_LAN_PI_80, value); // write data2 to LAN-fifo
		//
		value = 0x000000FF & p_data_u8[3];
		XIomodule_Out32 (ADRS_LAN_PI_80, value); // write data3 to LAN-fifo

	}

	//// set up frame
	//   ADRS_LAN_WI_00_MHVSU
	//   ADRS_LAN_WI_01_MHVSU
	//   ADRS_LAN_WO_20_MHVSU

	//  adrs = ADRS_PORT_WI_01;
	//  value = AddrSel&0x00FFFFFF;
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_02;
	//  value = len_u32; //$$
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0002; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	//  //
	//  adrs = ADRS_PORT_WI_00;
	//  value = 0x0000; // ... , trig_frame, trig_reset
	//  XIomodule_Out32 (adrs, value);
	//  //
	
	value = AddrSel&0x00FFFFFF;
	write_mcs_io (ADRS_LAN_WI_01, value);
	
	value = len_u32 << 16; // set length to len at high 16b //$$
	// value = value | 0x00000002; // ... , trig_frame, trig_reset
	// write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);
	// value = value & 0xFFFFFFFD; // ... , trig_frame, trig_reset
	// write_mcs_io (ADRS_LAN_WI_00_MHVSU, value);	

	// TODO: send frame 
	write_mcs_io (ADRS_LAN_WI_00, value); // set data count at high 16b
	write_mcs_io (ADRS_LAN_TI_40, 0x00000002); // trig frame
	
	while (1) {
		//  //
		//  adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		//  value = XIomodule_In32 (adrs);
		//  //
		
		// TODO: frame done 
		// value = read_mcs_io (ADRS_LAN_WO_20);
		// if ((value & 0x10) == 0x10) break;
		value = read_mcs_io (ADRS_LAN_TO_60);		
		if ((value & 0x00000002) == 0x00000002) break;
		
		//usleep(100); // 1us*100 sleep
		//
		}
	//
}

//}



// === MCS-EP access functions === //{

// * EP common subfunctions : //{
// note that mask should be careful... MCS and LAN as well...

// note for EXT-CMU
//   BRD_CON
//   wi03 // ADRS_PORT_WI_03
//
//   bit[0]=HW_reset
//   bit[1]=rst_adc  
//   bit[2]=rst_dwave
//   bit[3]=rst_bias 
//   bit[4]=rst_spo  
//   ...
//   bit[8]=mcs_ep_po_en
//   bit[9]=mcs_ep_pi_en
//   bit[10]=mcs_ep_to_en
//   bit[11]=mcs_ep_ti_en
//   bit[12]=mcs_ep_wo_en
//   bit[13]=mcs_ep_wi_en
//   ...
//   bit[16]=time_stamp_disp_en


void  enable_mcs_ep() {
	//adrs = ADRS_PORT_WI_10;
	//value = 0x0000003F;
	//XIomodule_Out32 (adrs, value);
	//XIomodule_Out32 (ADRS_MASK_WI,    0x0000003F); // write mask
	//XIomodule_Out32 (ADRS_PORT_WI_10, 0x0000003F); // write MCS0 
	//XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask
	
	// for EXT-CMU
	XIomodule_Out32 (ADRS_MASK_WI___, (0x3F<<8) ); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_03, (0x3F<<8) ); // 
	XIomodule_Out32 (ADRS_MASK_WI___,   MASK_ALL); // reset mask
}

void disable_mcs_ep() {
	//adrs = ADRS_PORT_WI_10;
	//value = 0x00000000;
	//XIomodule_Out32 (adrs, value);
	//XIomodule_Out32 (ADRS_MASK_WI,    0x0000003F); // write mask
	//XIomodule_Out32 (ADRS_PORT_WI_10, 0x00000000); // write MCS0 // TODO: remove
	//XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask

	// for EXT-CMU
	XIomodule_Out32 (ADRS_MASK_WI___, (0x3F<<8) ); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_03, 0x00000000); // 
	XIomodule_Out32 (ADRS_MASK_WI___,   MASK_ALL); // reset mask
}

void  reset_mcs_ep() {
	//XIomodule_Out32 (ADRS_MASK_WI,    0x00010000); // write mask
	//XIomodule_Out32 (ADRS_PORT_WI_10, 0x00010000); // write MCS0
	//usleep(100);
	//XIomodule_Out32 (ADRS_PORT_WI_10, 0x00000000); // write MCS0
	//XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask

	// for EXT-CMU
	//  HW_reset
	XIomodule_Out32 (ADRS_MASK_WI___, (0x01<<0) ); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_03, (0x01<<0) ); // 
	usleep(100);
	XIomodule_Out32 (ADRS_PORT_WI_03, 0x00000000); // 
	XIomodule_Out32 (ADRS_MASK_WI___,   MASK_ALL); // reset mask
}

void  reset_io_dev() {
	// adc / dwave / bias / spo
	//XIomodule_Out32 (ADRS_MASK_WI,    0x001E0000); // write mask
	//XIomodule_Out32 (ADRS_PORT_WI_10, 0x001E0000); // write MCS0
	//usleep(100);
	//XIomodule_Out32 (ADRS_PORT_WI_10, 0x00000000); // write MCS0
	//XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask

	// for EXT-CMU
	//  rst_adc rst_dwave rst_bias rst_spo
	XIomodule_Out32 (ADRS_MASK_WI___, (0x0F<<1) ); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_03, (0x0F<<1) ); // 
	usleep(100);
	XIomodule_Out32 (ADRS_PORT_WI_03, 0x00000000); // 
	XIomodule_Out32 (ADRS_MASK_WI___,   MASK_ALL); // reset mask
}


u32 is_enabled_mcs_ep() {
	// for EXT-CMU
	u32 val;
	u32 ret;
	XIomodule_Out32 (ADRS_MASK_ALL__,   MASK_ALL); // reset mask
	val = XIomodule_In32 (ADRS_PORT_WI_03); // assume MASK_ALL
	if ((val&(0x3F<<8))==(0x3F<<8)) {
		ret = 1;
	}
	else ret = 0;
	return ret;

}

//}

// * EP io-mask subfunctions : //{

u32  read_mcs_ep_wi_mask(u32 adrs_base) {
	//return XIomodule_In32 (ADRS_MASK_WI____CMU);
	return XIomodule_In32 (adrs_base + ADRS_MASK_WI____OFST);
}

u32 write_mcs_ep_wi_mask(u32 adrs_base, u32 mask) {
	//XIomodule_Out32 (ADRS_MASK_WI____CMU, mask); // mask 
	XIomodule_Out32 (adrs_base + ADRS_MASK_WI____OFST, mask); // mask 
	return mask;
}

u32  read_mcs_ep_wo_mask(u32 adrs_base) {
	//return XIomodule_In32 (ADRS_MASK_WO____CMU);
	return XIomodule_In32 (adrs_base + ADRS_MASK_WO____OFST);
}

u32 write_mcs_ep_wo_mask(u32 adrs_base, u32 mask) {
	//XIomodule_Out32 (ADRS_MASK_WO____CMU, mask); // mask 
	XIomodule_Out32 (adrs_base + ADRS_MASK_WO____OFST, mask); // mask 
	return mask;
}

u32  read_mcs_ep_ti_mask(u32 adrs_base) {
	//return XIomodule_In32 (ADRS_MASK_TI____CMU);
	return XIomodule_In32 (adrs_base + ADRS_MASK_TI____OFST);
}

u32 write_mcs_ep_ti_mask(u32 adrs_base, u32 mask) {
	//XIomodule_Out32 (ADRS_MASK_TI____CMU, mask); // mask 
	XIomodule_Out32 (adrs_base + ADRS_MASK_TI____OFST, mask); // mask 
	return mask;
}

u32  read_mcs_ep_to_mask(u32 adrs_base) {
	//return XIomodule_In32 (ADRS_MASK_TO____CMU);
	return XIomodule_In32 (adrs_base + ADRS_MASK_TO____OFST);
}

u32 write_mcs_ep_to_mask(u32 adrs_base, u32 mask) {
	//XIomodule_Out32 (ADRS_MASK_TO____CMU, mask); // mask 
	XIomodule_Out32 (adrs_base + ADRS_MASK_TO____OFST, mask); // mask 
	return mask;
}


u32  read_mcs_ep_xx_data(u32 adrs_base, u32 offset) {
	u32 adrs;
	u32 value;
	//adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	adrs = adrs_base + (offset<<4); // MCS1
	value = XIomodule_In32 (adrs);
	return value;	
}

u32 write_mcs_ep_xx_data(u32 adrs_base, u32 offset, u32 data) {
	u32 adrs;
	//adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	adrs = adrs_base + (offset<<4); // MCS1
	XIomodule_Out32 (adrs, data);
	return data;
}


u32  read_mcs_ep_wi_data(u32 adrs_base, u32 offset) {
	return read_mcs_ep_xx_data(adrs_base, offset);
}

u32 write_mcs_ep_wi_data(u32 adrs_base, u32 offset, u32 data) {
	return write_mcs_ep_xx_data(adrs_base, offset, data);
}

u32  read_mcs_ep_wo_data(u32 adrs_base, u32 offset) {
	return read_mcs_ep_xx_data(adrs_base, offset);
}

// u32 write_mcs_ep_wo_data(u32 offset, u32 data) // NA

u32  read_mcs_ep_ti_data(u32 adrs_base, u32 offset) {
	return read_mcs_ep_xx_data(adrs_base, offset);
}

u32 write_mcs_ep_ti_data(u32 adrs_base, u32 offset, u32 data) {
	return write_mcs_ep_xx_data(adrs_base, offset, data);
}

u32  read_mcs_ep_to_data(u32 adrs_base, u32 offset) {
	return read_mcs_ep_xx_data(adrs_base, offset);
}

// u32 write_mcs_ep_to_data(u32 offset, u32 data) // NA


//// PIPE-DATA direct

// u32  read_mcs_ep_pi_data() // NA

u32 write_mcs_ep_pi_data(u32 adrs_base, u32 offset, u32 data) { // OK
	return write_mcs_ep_xx_data(adrs_base, offset, data);
}

u32  read_mcs_ep_po_data(u32 adrs_base, u32 offset) { // OK
	return read_mcs_ep_xx_data(adrs_base, offset);
}

// u32 write_mcs_ep_po_data() // NA

//}

// * EP functions : //{

u32   read_mcs_ep_wi(u32 adrs_base, u32 offset) {
	return read_mcs_ep_wi_data(adrs_base, offset);
}

void write_mcs_ep_wi(u32 adrs_base, u32 offset, u32 data, u32 mask) {
	write_mcs_ep_wi_mask(adrs_base, mask);
	write_mcs_ep_wi_data(adrs_base, offset, data);
}

u32   read_mcs_ep_wo(u32 adrs_base, u32 offset, u32 mask) {
	write_mcs_ep_wo_mask(adrs_base, mask);
	return read_mcs_ep_wo_data(adrs_base, offset);
}

u32   read_mcs_ep_ti(u32 adrs_base, u32 offset) {
	return read_mcs_ep_ti_data(adrs_base, offset);
}

void write_mcs_ep_ti(u32 adrs_base, u32 offset, u32 data, u32 mask) {
	write_mcs_ep_ti_mask(adrs_base, mask);
	write_mcs_ep_ti_data(adrs_base, offset, data);
}

void activate_mcs_ep_ti(u32 adrs_base, u32 offset, u32 bit_loc) { 
	u32 value;
	value = 0x00000001 << bit_loc;
	write_mcs_ep_ti(adrs_base, offset, value, value);
}

u32   read_mcs_ep_to(u32 adrs_base, u32 offset, u32 mask) {
	write_mcs_ep_to_mask(adrs_base, mask);
	return read_mcs_ep_to_data(adrs_base, offset);
}

u32 is_triggered_mcs_ep_to(u32 adrs_base, u32 offset, u32 mask) {
	u32 ret;
	u32 value;
	//
	value = read_mcs_ep_to(adrs_base, offset, mask);
	if (value==0) ret = 0;
	else ret = 1;
	return ret;
}


//// PIPE-DATA in buffer 

// write data from buffer to pipe-in 
u32 write_mcs_ep_pi_buf(u32 adrs_base, u32 offset, u32 len_byte, u8 *p_data) {
	u32 adrs;
	//
	//adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	adrs = adrs_base + (offset<<4); // MCS1
	dcopy_buf32_to_pipe32((u32*)p_data, adrs, len_byte);
	//
	return (len_byte&0xFFFFFFFC);
}

// read data from pipe-out to buffer 
u32  read_mcs_ep_po_buf(u32 adrs_base, u32 offset, u32 len_byte, u8 *p_data) { 
	u32 adrs;
	//
	//adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	adrs = adrs_base + (offset<<4); // MCS1
	dcopy_pipe32_to_buf32(adrs, (u32*)p_data, len_byte);
	//
	return (len_byte&0xFFFFFFFC);
}


//// PIPE-DATA in fifo 

// write data from fifo to pipe-in 
// u32 write_mcs_ep_pi_fifo(u32 offset, u32 len_byte, u8 *p_data) { // test
// 	return 0;
// }

// read data from pipe-out to fifo 
// u32  read_mcs_ep_po_fifo(u32 offset, u32 len_byte, u8 *p_data) { // test
// 	return 0;
// }

//}

//}


// NOTE: legacy compatible functions ... CPP class to come
//
//  GetWireOutValue
//  UpdateWireOuts 
//  SetWireInValue
//  UpdateWireIns // dummy
//  ActivateTriggerIn // ActivateTriggerIn(int epAddr, int bit)
//  UpdateTriggerOuts // not much // dummy
//  IsTriggered // not much // IsTriggered(int epAddr, UINT32 mask)
//  ReadFromPipeOut
//  WriteToPipeIn // not much
//  // https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel-members.html


// === TODO: EEPROM access functions === //{
	

u32  eeprom_send_frame_ep (u32 MEM_WI_b32, u32 MEM_FDAT_WI_b32) {
//  def eeprom_send_frame_ep (MEM_WI, MEM_FDAT_WI):
//  	## //// end-point map :
//  	## // wire [31:0] w_MEM_WI      = ep13wire;
//  	## // wire [31:0] w_MEM_FDAT_WI = ep12wire;
//  	## // wire [31:0] w_MEM_TI = ep53trig; assign ep53ck = sys_clk;
//  	## // wire [31:0] w_MEM_TO; assign ep73trig = w_MEM_TO; assign ep73ck = sys_clk;
//  	## // wire [31:0] w_MEM_PI = ep93pipe; wire w_MEM_PI_wr = ep93wr; 
//  	## // wire [31:0] w_MEM_PO; assign epB3pipe = w_MEM_PO; wire w_MEM_PO_rd = epB3rd; 	
//  //$$ same endpoints for EXT-CMU
//  

	xil_printf(">>>>>> eeprom_send_frame_ep  \r\n");
	u32 ret;
	u32 cnt_loop;
	
//  	print('{} = 0x{:08X}'.format('MEM_WI', MEM_WI))
//  	dev.SetWireInValue(0x13,MEM_WI,0xFFFFFFFF) # (ep,val,mask)
	write_mcs_ep_wi(MCS_EP_BASE, 0x13, MEM_WI_b32, 0xFFFFFFFF); // adrs_base, EP_offset_EP, data, mask
//  	#dev.UpdateWireIns()	
	xil_printf("write_mcs_ep_wi: 0x%08X @ 0x%02X \r\n", MEM_WI_b32, 0x13);
//  	
//  	print('{} = 0x{:08X}'.format('MEM_FDAT_WI', MEM_FDAT_WI))
//  	dev.SetWireInValue(0x12,MEM_FDAT_WI,0xFFFFFFFF) # (ep,val,mask)
	write_mcs_ep_wi(MCS_EP_BASE, 0x12, MEM_FDAT_WI_b32, 0xFFFFFFFF); // adrs_base, EP_offset_EP, data, mask
//  	dev.UpdateWireIns()	
	xil_printf("write_mcs_ep_wi: 0x%08X @ 0x%02X \r\n", MEM_FDAT_WI_b32, 0x12);
//  	
//  	# clear TO 
//  	dev.UpdateTriggerOuts()
//  	ret=dev.GetTriggerOutVector(0x73)
	ret = read_mcs_ep_to(MCS_EP_BASE, 0x73, 0xFFFFFFFF);
//  	print('{} = 0x{:08X}'.format('ret', ret))
	xil_printf("read_mcs_ep_to: 0x%08X @ 0x%02X \r\n", ret, 0x73);
//  
//  
//  	# act TI 
//  	dev.ActivateTriggerIn(0x53, 2)	## (ep, loc)
	activate_mcs_ep_ti(MCS_EP_BASE, 0x53, 2);
	xil_printf("activate_mcs_ep_ti: loc %d @ 0x%02X \r\n", 2, 0x53);

//  	
//  	# check frame done
//  	cnt_loop = 0;
	cnt_loop = 0;
//  	while 1:
	while (1) {
//  		# First, query all XEM Trigger Outs.
//  		dev.UpdateTriggerOuts()
//  		# check trigger out //$$  0x01 w_MEM_TO[0]  or  0x04 w_MEM_TO[2] 
//  		if dev.IsTriggered(0x73, 0x04) == True: # // (ep, mask)
		ret = is_triggered_mcs_ep_to(MCS_EP_BASE, 0x73, 0x04);
		if (ret==1) {
			xil_printf("is_triggered_mcs_ep_to: 0x%08X @ 0x%02X \r\n", ret, 0x73);
			break;
		}
//  			break
//  		cnt_loop += 1;
		cnt_loop += 1;
//  		print('{} = {}'.format('cnt_loop', cnt_loop))
//  		if (cnt_loop>MAX_count):
//  			break
	}
//  	print('{} = {}'.format('cnt_loop', cnt_loop))
	xil_printf("cnt_loop = %d \r\n", cnt_loop);
//  
//  	# # read again TO 
//  	# dev.UpdateTriggerOuts()
//  	# ret=dev.GetTriggerOutVector(0x73)
//  	# print('{} = 0x{:08X}'.format('ret', ret))
//  
//  	#
//  	return ret	

	return 0;
}

// global variables for eeprom
static u8 g_EEPROM__LAN_access = 1;
static u8 g_EEPROM__on_TP      = 1;
static u8 g_EEPROM__buf_2KB[2048];

u32 eeprom_set_g_var (u8 EEPROM__LAN_access, u8 EEPROM__on_TP) {
//  def eeprom_set_g_var (EEPROM__LAN_access=1, EEPROM__on_TP=1):
//  	print('\n>>>>>> eeprom_set_g_var')
//  	global g_EEPROM__LAN_access
//  	global g_EEPROM__on_TP
//  	#
//  	g_EEPROM__LAN_access = EEPROM__LAN_access
//  	g_EEPROM__on_TP      = EEPROM__on_TP
//  	#
//  	return
	u32 ret;
	//
	g_EEPROM__LAN_access = EEPROM__LAN_access;
	g_EEPROM__on_TP      = EEPROM__on_TP     ;
	//
	
	// update wire wi11 //$$ MCS_SETUP_WI wi11 --> wi19
	// bit [9] = w_con_port__L_MEM_SIO__H_TP    // set from MCS
	// bit [8] = w_con_fifo_path__L_sspi_H_lan  // set from MCS
	ret = (g_EEPROM__LAN_access<<8) + (g_EEPROM__on_TP<<9);
	//write_mcs_ep_wi(MCS_EP_BASE, 0x11, ret, 0x00000300); // adrs_base, EP_offset, data, mask
	write_mcs_ep_wi(MCS_EP_BASE, 0x19, ret, 0x00000300); // adrs_base, EP_offset, data, mask
	
	//
	return ret;
}

u32  eeprom_send_frame (u8 CMD_b8, u8 STA_in_b8, u8 ADL_b8, u8 ADH_b8, u16 num_bytes_DAT_b16, u8 con_disable_SBP_b8) {
//  def eeprom_send_frame (CMD=0x05, STA_in=0, ADL=0, ADH=0, num_bytes_DAT=1, con_disable_SBP=0):

	xil_printf(">>>>>> eeprom_send_frame  \r\n");
	u32 ret;

//  	## 
//  	#num_bytes_DAT               = 1
//  	#con_disable_SBP             = 0
//  	
//  	con_fifo_path__L_sspi_H_lan = 1 # LAN access
//  	#con_fifo_path__L_sspi_H_lan = 0 # slave spi access
	u32 con_fifo_path__L_sspi_H_lan = g_EEPROM__LAN_access; //
//  
//  	con_port__L_MEM_SIO__H_TP   = 1 # test TP	
//  	#con_port__L_MEM_SIO__H_TP   = 0 # test MEM_SIO
	u32 con_port__L_MEM_SIO__H_TP = g_EEPROM__on_TP; //
//  	
//  	#
//  	set_data_WI = (con_port__L_MEM_SIO__H_TP<<17) + (con_fifo_path__L_sspi_H_lan<<16) + (con_disable_SBP<<15) + num_bytes_DAT
	u32 set_data_WI = (con_port__L_MEM_SIO__H_TP<<17) + (con_fifo_path__L_sspi_H_lan<<16) + ((u32)con_disable_SBP_b8<<15) + ((u32)num_bytes_DAT_b16);
//  	
//  	frame_data_CMD     = CMD    ## 0x05
//  	frame_data_STA_in  = STA_in ## 0x00
//  	frame_data_ADL     = ADL    ## 0x00
//  	frame_data_ADH     = ADH    ## 0x00
//  	#
//  	set_data_FDAT_WI = (frame_data_ADH<<24) + (frame_data_ADL<<16) + (frame_data_STA_in<<8) + frame_data_CMD
	u32 set_data_FDAT_WI = ((u32)ADH_b8<<24) + ((u32)ADL_b8<<16) + ((u32)STA_in_b8<<8) + (u32)CMD_b8;
//  	
//  	ret = eeprom_send_frame_ep (MEM_WI=set_data_WI, MEM_FDAT_WI=set_data_FDAT_WI)
	ret = eeprom_send_frame_ep (set_data_WI, set_data_FDAT_WI);
//  	#
//  	return ret

	return ret;
}

void eeprom_write_enable() {
//  def eeprom_write_enable():
//  	print('\n>>>>>> eeprom_write_enable')
	xil_printf(">>>>>> eeprom_write_enable  \r\n");
//  	#
//  	## // CMD_WREN__96 
//  	print('\n>>> CMD_WREN__96')
//  	eeprom_send_frame (CMD=0x96, con_disable_SBP=1)
	eeprom_send_frame (0x96, 0, 0, 0, 1, 1); // (CMD=0x96, con_disable_SBP=1)
}

void eeprom_write_disable() {
//  def eeprom_write_disable():
//  	print('\n>>>>>> eeprom_write_disable')
	xil_printf(">>>>>> eeprom_write_disable  \r\n");
//  	#
//  	## // CMD_WRDI__91 
//  	print('\n>>> CMD_WRDI__91')
//  	eeprom_send_frame (CMD=0x91)
	eeprom_send_frame (0x91, 0, 0, 0, 1, 0); // (CMD=0x91)
}

u32 eeprom_read_status() {
//  def eeprom_read_status():
//  	print('\n>>>>>> eeprom_read_status')
	xil_printf(">>>>>> eeprom_read_status  \r\n");
	u32 ret;
	
//  	## // CMD_RDSR__05 
//  	print('\n>>> CMD_RDSR__05')
//  	eeprom_send_frame (CMD=0x05) 
	eeprom_send_frame (0x05, 0, 0, 0, 1, 0); //
//  
//  	# clear TO 
//  	dev.UpdateTriggerOuts()
//  	ret=dev.GetTriggerOutVector(0x73)
	ret = read_mcs_ep_to(MCS_EP_BASE, 0x73, 0xFFFFFFFF);
//  	print('{} = 0x{:08X}'.format('ret', ret))
//  
//  	# read again TO 
//  	dev.UpdateTriggerOuts()
//  	ret=dev.GetTriggerOutVector(0x73)
	ret = read_mcs_ep_to(MCS_EP_BASE, 0x73, 0xFFFFFFFF);
//  	print('{} = 0x{:08X}'.format('ret', ret))
//  	
//  	MUST_ZEROS = (ret>>12)&0x0F
//  	
//  	BP1 = (ret>>11)&0x01
//  	BP0 = (ret>>10)&0x01
//  	WEL = (ret>> 9)&0x01
//  	WIP = (ret>> 8)&0x01
	ret = (ret>> 8)&0xFF;
//  	
//  	#
//  	return [BP1, BP0, WEL, WIP, MUST_ZEROS]
	return ret;
}

void eeprom_write_status (u8 BP1_b8, u8 BP0_b8) {
//  def eeprom_write_status(BP1, BP0):
//  	print('\n>>>>>> eeprom_write_status')
	xil_printf(">>>>>> eeprom_write_status  \r\n");

//  	## // CMD_WREN__96 
//  	#print('\n>>> CMD_WREN__96')
//  	#eeprom_send_frame (CMD=0x96)
//  	eeprom_write_enable()
	eeprom_write_enable();
//  	
//  	##
//  	STA_in = (BP1<<3) + (BP0<<2)
	u8 STA_in_b8 = (BP1_b8<<3) + (BP0_b8<<2);
//  	
//  	## // CMD_WRSR__6E
//  	print('\n>>> CMD_WRSR__6E')
//  	eeprom_send_frame (CMD=0x6E, STA_in=STA_in)
	eeprom_send_frame (0x6E, STA_in_b8, 0, 0, 1, 0);
//  	
//  	#
//  	return None
}

u32 is_eeprom_available() {
//  def is_eeprom_available():
//  	print('\n>>>>>> is_eeprom_available')
	xil_printf(">>>>>> is_eeprom_available  \r\n");
	//  	ret = 1
	u32 ret = 1;
	u32 val;
//  	
//  	## initialize by sending stand-by pulse 
//  	eeprom_write_disable() # SBP
	eeprom_write_disable();
//  	#
//  	[BP1, BP0, WEL, WIP, _] = eeprom_read_status()
	val = eeprom_read_status();
//  	print('{}={}'.format('WEL',WEL))
//  	#
//  	if WEL==0:
//  		ret = ret*1
//  	else:
//  		ret = ret*0
	if ((val&0x02)==0x00) {
		ret = ret*1;
	}
	else {
		ret = ret*0;
	}
//  	
//  	## 
//  	eeprom_write_enable() ## No SBP
	eeprom_write_enable();
//  	#
//  	[BP1, BP0, WEL, WIP, _] = eeprom_read_status()
	val = eeprom_read_status();
//  	print('{}={}'.format('WEL',WEL))
//  	#
//  	if WEL==1:
//  		ret = ret*1
//  	else:
//  		ret = ret*0
	if ((val&0x02)==0x02) {
		ret = ret*1;
	}
	else {
		ret = ret*0;
	}
//  		
//  	##
//  	if (ret==1):
//  		return True
//  	else:
//  		return False
	return ret;
}

void eeprom_erase_all() {
//  def eeprom_erase_all():
//  	print('\n>>>>>> eeprom_erase_all')
	xil_printf(">>>>>> eeprom_erase_all  \r\n");
//  	#
//  	
//  	eeprom_write_enable()
	eeprom_write_enable();
//  	
//  	## // CMD_ERAL__6D
//  	print('\n>>> CMD_ERAL__6D')
//  	eeprom_send_frame (CMD=0x6D)
	eeprom_send_frame (0x6D, 0, 0, 0, 1, 0);
//  
//  	pass
}

void eeprom_set_all() {
//  def eeprom_set_all():
//  	print('\n>>>>>> eeprom_set_all')
	xil_printf(">>>>>> eeprom_set_all  \r\n");
///  	#
//  	
//  	eeprom_write_enable()
	eeprom_write_enable();
//  	
//  	## // CMD_SETAL_67
//  	print('\n>>> CMD_SETAL_67')
//  	eeprom_send_frame (CMD=0x67)
	eeprom_send_frame (0x67, 0, 0, 0, 1, 0);
//  
//  	pass
}


void eeprom_reset_fifo() {
//  def eeprom_reset_fifo():
//  	print('\n>>>>>> eeprom_reset_fifo')
	xil_printf(">>>>>> eeprom_reset_fifo  \r\n");
//  	
//  	#  // w_MEM_TI
//  	#  assign w_MEM_rst      = w_MEM_TI[0];
//  	#  assign w_MEM_fifo_rst = w_MEM_TI[1];
//  	#  assign w_trig_frame   = w_MEM_TI[2];	
//  	
//  	# act TI 
//  	dev.ActivateTriggerIn(0x53, 1)	## (ep, loc)
	activate_mcs_ep_ti(MCS_EP_BASE, 0x53, 1);
//  	
//  	pass
}

u16 eeprom_read_fifo (u16 num_bytes_DAT_b16, u8 *buf_dataout) {
//  def eeprom_read_fifo(num_data=1):
//  	print('\n>>>>>> eeprom_read_fifo')
	xil_printf(">>>>>> eeprom_read_fifo  \r\n");
//  	#
//  	
//  	bytes_in_one_sample = 4 # for 32-bit end-point
//  	num_bytes_from_fifo = num_data * bytes_in_one_sample
//  	print('{} = {}'.format('num_bytes_from_fifo', num_bytes_from_fifo))
//  	
//  	## setup data buffer for fifo data
//  	dataout = bytearray([0] * num_bytes_from_fifo)
//  	
//  	## call api function to read pipeout data
//  	data_count = dev.ReadFromPipeOut(0xB3, dataout)

	// memory copy from 32-bit width pipe to 8-bit width buffer // ADRS_BASE_MHVSU or MCS_EP_BASE
	dcopy_pipe8_to_buf8 (MCS_EP_BASE + (0xB3<<4), buf_dataout, num_bytes_DAT_b16); // (u32 adrs_p8, u8 *p_buf_u8, u32 len)

//  	print('{} : {}'.format('data_count [byte]',data_count))
//  	
//  	##  if data_count<0:
//  	##  	#return
//  	##  	# set test data 
//  	##  	num_data = 40
//  	##  	data_count = num_data * bytes_in_one_sample
//  	##  	data_int_list = [1,2,3, 2, 1, -1 ]
//  	##  	data_bytes_list = [x.to_bytes(bytes_in_one_sample,byteorder='little',signed=True) for x in data_int_list]
//  	##  	print('{} = {}'.format('data_bytes_list', data_bytes_list))
//  	##  	#dataout = b'\x01\x00\x00\x00\x02\x00\x00\x00'
//  	##  	dataout = b''.join(data_bytes_list)
//  	
//  	## convert bytearray to 32-bit data : high 24 bits to be ignored due to 8-bit fifo
//  	data_fifo_int_list = []
//  	for ii in range(0,num_data):
//  		temp_data = int.from_bytes(dataout[ii*bytes_in_one_sample:(ii+1)*bytes_in_one_sample], byteorder='little', signed=True)
//  		data_fifo_int_list += [temp_data&0x000000FF] # mask low 8-bit
//  	
//  	
//  	## print out 
//  	#if __debug__:print('{} = {}'.format('data_fifo_int_list', data_fifo_int_list))
//  	
//  	return data_fifo_int_list

	return num_bytes_DAT_b16;
}

u16 eeprom_write_fifo (u16 num_bytes_DAT_b16, u8 *buf_datain) {
//  def eeprom_write_fifo(datain__int_list=[0]):
//  	print('\n>>>>>> eeprom_write_fifo')
	xil_printf(">>>>>> eeprom_write_fifo  \r\n");
//  	#
//  
//  	## convert 32-bit data to bytearray
//  	bytes_in_one_sample = 4 # for 32-bit end-point
//  	num_data = len(datain__int_list)
//  	num_bytes_to_fifo = num_data * bytes_in_one_sample
//  	datain = bytearray([0] * num_bytes_to_fifo)
//  	
//  	# convert bytes list : high 24 bits to be ignored due to 8-bit fifo
//  	datain__bytes_list = [x.to_bytes(bytes_in_one_sample,byteorder='little',signed=True) for x in datain__int_list]
//  	if __debug__:print('{} = {}'.format('datain__bytes_list', datain__bytes_list[:20]))
//  	
//  	# take out bytearray from list
//  	datain = b''.join(datain__bytes_list) 
//  	if __debug__:print('{} = {}'.format('datain', datain[:20]))
//  	
//  	## call api for pipein
//  	data_count = dev.WriteToPipeIn(0x93, datain)

	// memory copy from 8-bit width buffer to 32-bit width pipe // ADRS_BASE_MHVSU or MCS_EP_BASE
	dcopy_buf8_to_pipe8  (buf_datain, MCS_EP_BASE + (0x93<<4), num_bytes_DAT_b16); //  (u8 *p_buf_u8, u32 adrs_p8, u32 len)
	
//  	
//  	return 
	return num_bytes_DAT_b16;
}

u16 eeprom_read_data (u16 ADRS_b16, u16 num_bytes_DAT_b16, u8 *buf_dataout) {
//  def eeprom_read_data(ADRS_b16=0x0000, num_bytes_DAT=1):
//  	print('\n>>>>>> eeprom_read_data')
	xil_printf(">>>>>> eeprom_read_data  \r\n");
	u16 ret;
//  	#
//  	
//  	## reset fifo test 
//  	eeprom_reset_fifo()
	eeprom_reset_fifo();
//  
//  	## convert address
//  	ADL = (ADRS_b16>>0)&0x00FF 
//  	ADH = (ADRS_b16>>8)&0x00FF
	u8 ADL = (ADRS_b16>>0)&0x00FF;
	u8 ADH = (ADRS_b16>>8)&0x00FF;
//  	print('{} = 0x{:08X}'.format('ADRS_b16', ADRS_b16))
//  	print('{} = 0x{:04X}'.format('ADH', ADH))
//  	print('{} = 0x{:04X}'.format('ADL', ADL))
//  	
//  	## // CMD_READ__03 
//  	print('\n>>> CMD_READ__03')
//  	eeprom_send_frame (CMD=0x03, ADL=ADL, ADH=ADH, num_bytes_DAT=num_bytes_DAT)
	eeprom_send_frame (0x03, 0, ADL, ADH, num_bytes_DAT_b16, 0);
//  
//  	## call fifo
//  	ret = eeprom_read_fifo(num_data=num_bytes_DAT)
	ret = eeprom_read_fifo (num_bytes_DAT_b16, buf_dataout);
//  
//  	#
//  	return ret
	return ret;
}

u16 eeprom_read_data_current (u16 num_bytes_DAT_b16, u8 *buf_dataout) {
//  def eeprom_read_data_current(num_bytes_DAT=1):
//  	print('\n>>>>>> eeprom_read_data_current')
	xil_printf(">>>>>> eeprom_read_data_current  \r\n");
	u16 ret;
//  	#
//  	
//  	## reset fifo test 
//  	eeprom_reset_fifo()
	eeprom_reset_fifo();
//  
//  	## // CMD_CRRD__06 
//  	print('\n>>> CMD_CRRD__06')
//  	eeprom_send_frame (CMD=0x06, num_bytes_DAT=num_bytes_DAT)
	eeprom_send_frame (0x06, 0, 0, 0, num_bytes_DAT_b16, 0);
//  
//  	## call fifo
//  	ret = eeprom_read_fifo(num_data=num_bytes_DAT)
	ret = eeprom_read_fifo (num_bytes_DAT_b16, buf_dataout);
//  	#
//  	return ret
	return ret;
}

u16 eeprom_write_data_16B (u16 ADRS_b16, u16 num_bytes_DAT_b16) {
//  #def eeprom_write_data_16B(ADRS_b16=0x0000, num_bytes_DAT=16, data8b_in=[0]*16) :
//  def eeprom_write_data_16B(ADRS_b16=0x0000, num_bytes_DAT=16) :
//  	print('\n>>>>>> eeprom_write_data_16B')
	xil_printf(">>>>>> eeprom_write_data_16B  \r\n");
//  	
//  	## call fifo 
//  	#eeprom_write_fifo(datain__int_list=data8b_in)
//  	
//  	## write enble
//  	eeprom_write_enable()
	eeprom_write_enable();
//  	
//  	## convert address
//  	ADL = (ADRS_b16>>0)&0x00FF 
//  	ADH = (ADRS_b16>>8)&0x00FF
	u8 ADL = (ADRS_b16>>0)&0x00FF;
	u8 ADH = (ADRS_b16>>8)&0x00FF;
//  	print('{} = 0x{:08X}'.format('ADRS_b16', ADRS_b16))
//  	print('{} = 0x{:04X}'.format('ADH', ADH))
//  	print('{} = 0x{:04X}'.format('ADL', ADL))
//  	
//  	## // CMD_WRITE_6C 
//  	print('\n>>> CMD_WRITE_6C')
//  	eeprom_send_frame (CMD=0x6C, ADL=ADL, ADH=ADH, num_bytes_DAT=num_bytes_DAT, con_disable_SBP=1)
	eeprom_send_frame (0x6C, 0, ADL, ADH, num_bytes_DAT_b16, 1);
//  	pass
	return num_bytes_DAT_b16;
}

u16 eeprom_write_data (u16 ADRS_b16, u16 num_bytes_DAT_b16, u8 *buf_datain) {
//  def eeprom_write_data(ADRS_b16=0x0000, num_bytes_DAT=1, data8b_in=[0]):
//  	print('\n>>>>>> eeprom_write_data')
	xil_printf(">>>>>> eeprom_write_data  \r\n");
//  
//  	##  The 11XX features a 16-byte page buffer, meaning that
//  	##  up to 16 bytes can be written at one time. To utilize this
//  	##  feature, the master can transmit up to 16 data bytes to
//  	##  the 11XX, which are temporarily stored in the page buffer.
//  	##  After each data byte, the master sends a MAK, indicating
//  	##  whether or not another data byte is to follow. A
//  	##  NoMAK indicates that no more data is to follow, and as
//  	##  such will initiate the internal write cycle.
//  	
//  	## reset fifo test 
//  	eeprom_reset_fifo()
	eeprom_reset_fifo();
//  
//  	if num_bytes_DAT <= 16:
	if (num_bytes_DAT_b16 <= 16) {
//  		eeprom_write_fifo(datain__int_list=data8b_in)
		eeprom_write_fifo (num_bytes_DAT_b16, buf_datain); // (u16 num_bytes_DAT_b16, u8 *buf_datain)
//  		#eeprom_write_data_16B(ADRS_b16=ADRS_b16, num_bytes_DAT=num_bytes_DAT, data8b_in=data8b_in)
//  		eeprom_write_data_16B(ADRS_b16=ADRS_b16, num_bytes_DAT=num_bytes_DAT)
		eeprom_write_data_16B (ADRS_b16, num_bytes_DAT_b16); // (u16 ADRS_b16, u16 num_bytes_DAT_b16)
		num_bytes_DAT_b16 = 0; // sent all
	}
	else {
//  	else:
//  		## call fifo : 8-bit width, depth 2048
//  		## note buf size 2048 
//  		## note 8-bit --> 32-bit conversion ... 4x loss
//  		## fifo size will be 2048/4=512
//  		#fifo_size = 512; # OK
//  		#fifo_size = 1024; # OK with 2048+56 buf fpga-side
//  		#fifo_size = 1024+512; # OK with 4096+512 buf fpga-side
//  		fifo_size = 2048; # OK with 4096*4+128 buf fpga-side
//  		#eeprom_write_fifo(datain__int_list=data8b_in)
//  		for ii in range(0,num_bytes_DAT,fifo_size):
//  			if __debug__: print('{} = {}'.format('ii', ii))
//  			eeprom_write_fifo(datain__int_list=data8b_in[(ii):(ii+fifo_size)])

		// eeprom fifo depth 2048 ready
		eeprom_write_fifo (num_bytes_DAT_b16, buf_datain); // (u16 num_bytes_DAT_b16, u8 *buf_datain)

//  			
//  		##  16-byte page buffer operation support
//  		for ii in range(0,num_bytes_DAT,16):
//  			#eeprom_write_data_16B(ADRS_b16=ADRS_b16+ii, data8b_in=data8b_in[(ii):(ii+16)])
//  			eeprom_write_data_16B(ADRS_b16=ADRS_b16+ii)
		// split address by 16
		while (1) {
			eeprom_write_data_16B (ADRS_b16, 16); // (u16 ADRS_b16, u16 num_bytes_DAT_b16)
			//
			ADRS_b16          += 16;
			num_bytes_DAT_b16 -= 16;
			//
			if (num_bytes_DAT_b16 <= 16) {
				eeprom_write_data_16B (ADRS_b16, num_bytes_DAT_b16); // (u16 ADRS_b16, u16 num_bytes_DAT_b16)
				num_bytes_DAT_b16 = 0;
				break;
			}
		}

	}
//  	pass
	return num_bytes_DAT_b16;
}

u8* get_adrs__g_EEPROM__buf_2KB() {
	// xil_printf(">>>>>> get_adrs__g_EEPROM__buf_2KB  \r\n");
	// xil_printf("     g_EEPROM__buf_2KB   : 0x%08X \r\n", g_EEPROM__buf_2KB);
	// xil_printf("    &g_EEPROM__buf_2KB[0]: 0x%08X \r\n", &g_EEPROM__buf_2KB[0]);
	// xil_printf("(u8*)g_EEPROM__buf_2KB   : 0x%08X \r\n", (u8*)g_EEPROM__buf_2KB);
	return (u8*)g_EEPROM__buf_2KB;
}

u16 eeprom_read_all() { // copy eeprom to g_EEPROM__buf_2KB
//  def eeprom_read_all():
	xil_printf(">>>>>> eeprom_read_all  \r\n");
//  	global g_EEPROM__buf_2KB
//  	
//  	g_EEPROM__buf_2KB = eeprom_read_data(ADRS_b16=0x0000, num_bytes_DAT=2048)
	eeprom_read_data (0x0000, 2048, g_EEPROM__buf_2KB); // (u16 ADRS_b16, u16 num_bytes_DAT_b16, u8 *buf_datain)
//  	
//  	return g_EEPROM__buf_2KB
	return 2048;
}

u16 eeprom_write_all() { // copy eeprom to g_EEPROM__buf_2KB
	xil_printf(">>>>>> eeprom_write_all  \r\n");
	eeprom_write_data (0x0000, 2048, g_EEPROM__buf_2KB); // (u16 ADRS_b16, u16 num_bytes_DAT_b16, u8 *buf_datain)
	return 2048;
}


u8 ignore_nonprint_code(u8 ch) {
	if (ch< 0x20 || ch>0x7E)
		ch = '.';
	return ch;
}

void hex_txt_display (s16 len_b16, u8 *p_mem_data, u32 adrs_offset) {
//void hex_txt_display (u8 *p_mem_data, u32 adrs_offset, u8 *buf_txtout) {
//  def hex_txt_display(mem_data__list, offset=0x0000):
//  	# display : every 16 bytes
//  	#print(mem_data_2KB__list)
//  	# 014  0x00E0  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
//  	# 015  0x00F0  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
//  	# 016  0x0100  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
//  	# 017  0x0110  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
//  	# 018  0x0120  FF FF FF FE DC BA FF FF  FF FF FF FF FF FF FF FF  ................	
//  	
//  	mem_data_2KB__list = mem_data__list
//  	adrs_ofs = offset
//  	
//  	num_bytes_in_MEM = len(mem_data_2KB__list)
//  	num_bytes_in_a_display_line = 16
//  	
//  	output_display = ''
//  	for ii in range(0,int(num_bytes_in_MEM/num_bytes_in_a_display_line)):
//  		xx              = mem_data_2KB__list[(ii*16):(ii*16+16)]                                 # load line data
//  		output_display += '{:03d}  '  .format(ii)                                                # line number 
//  		output_display += '0x{:04X}  '.format(ii*16+adrs_ofs)                                             # start address each line
//  		output_display += ''.join([ '{:02X} '.format(jj) for jj in xx[0:8 ] ])                   # hex code 
//  		output_display += ' '
//  		output_display += ''.join([ '{:02X} '.format(jj) for jj in xx[8:16] ])                   # hex code 
//  		output_display += ' '
//  		output_display += ''.join([ chr(jj) if (jj>= 0x20 and jj<=0x7E) else '.' for jj in xx ]) # printable code
//  		output_display += '\n'                                                                   # line feed
//  	#
//  	return output_display
	u32 ii=0;
	while (1) {
		xil_printf("%03d  ",ii);
		xil_printf("0x%04X  ",adrs_offset);
		xil_printf("%02X %02X %02X %02X %02X %02X %02X %02X  ",
			p_mem_data[0],p_mem_data[1],p_mem_data[2],p_mem_data[3],
			p_mem_data[4],p_mem_data[5],p_mem_data[6],p_mem_data[7]);
		xil_printf("%02X %02X %02X %02X %02X %02X %02X %02X  ",
			p_mem_data[8+0],p_mem_data[8+1],p_mem_data[8+2],p_mem_data[8+3],
			p_mem_data[8+4],p_mem_data[8+5],p_mem_data[8+6],p_mem_data[8+7]);
		xil_printf("%c%c%c%c%c%c%c%c %c%c%c%c%c%c%c%c",
			ignore_nonprint_code(p_mem_data[0]  ),ignore_nonprint_code(p_mem_data[1]  ),ignore_nonprint_code(p_mem_data[2]  ),ignore_nonprint_code(p_mem_data[3]  ),
			ignore_nonprint_code(p_mem_data[4]  ),ignore_nonprint_code(p_mem_data[5]  ),ignore_nonprint_code(p_mem_data[6]  ),ignore_nonprint_code(p_mem_data[7]  ),
			ignore_nonprint_code(p_mem_data[8+0]),ignore_nonprint_code(p_mem_data[8+1]),ignore_nonprint_code(p_mem_data[8+2]),ignore_nonprint_code(p_mem_data[8+3]),
			ignore_nonprint_code(p_mem_data[8+4]),ignore_nonprint_code(p_mem_data[8+5]),ignore_nonprint_code(p_mem_data[8+6]),ignore_nonprint_code(p_mem_data[8+7]));
		xil_printf("\r\n");
		//
		ii += 1;
		adrs_offset += 16;
		p_mem_data  += 16;
		len_b16 -= 16;
		//
		if (len_b16<=0) break;
	}

}

u8 cal_checksum (u16 len_b16, u8 *p_data_b8) {
//  def cal_checksum (data_b8_list):
//  	ret = sum(data_b8_list) & 0xFF
//  	return ret
	u8 ret;
	//
	ret = 0;
	while (1) {
		ret     += (*p_data_b8);
		len_b16   -= 1;
		p_data_b8 += 1;
		if (len_b16==0) break;
	}
	//
	return ret;
}

u8 gen_checksum (u16 len_b16, u8 *p_data_b8) {
//  def gen_checksum (data_b8_list):
//  	ret = 0x100 - sum(data_b8_list) & 0xFF
//  	return ret
	u8 ret;
	ret = - cal_checksum (len_b16, p_data_b8);
	return ret; 
}

u8 chk_all_zeros (u16 len_b16, u8 *p_data_b8) {
	u8 ret;
	//
	ret = 1;
	while (1) {
		if ((*p_data_b8)==0x00) 
			ret = ret * 1;
		else 
			ret = ret * 0;
		len_b16   -= 1;
		p_data_b8 += 1;
		if (len_b16==0) break;
	}
	//
	return ret;	// 1 for all zero.
}

//}










