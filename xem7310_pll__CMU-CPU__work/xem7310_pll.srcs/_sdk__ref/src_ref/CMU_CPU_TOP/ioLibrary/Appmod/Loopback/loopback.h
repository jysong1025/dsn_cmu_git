
#ifndef  _WIZCHIP_LOOPBACK_H_
#define  _WIZCHIP_LOOPBACK_H_

//#define _LOOPBACK_DEBUG_
//#define _LOOPBACK_DEBUG_WCMSG_ //$$ welcome message control

////#define _SCPI_DEBUG_ //$$ SCPI server debug
//#define _SCPI_DEBUG_WCMSG_

#define DATA_BUF_SIZE   2048 //$$ init
//#define DATA_BUF_SIZE   512
//#define DATA_BUF_SIZE   8192

int32_t loopback_tcps(uint8_t, uint8_t*, uint16_t);
int32_t loopback_udps(uint8_t, uint8_t*, uint16_t);

// scpi
#define MAX_CNT_STAY_SOCK_ESTABLISHED 0x00100000
int32_t scpi_tcps(uint8_t sn, uint8_t* buf, uint16_t port); //$$ scpi server

#endif   // _WIZCHIP_LOOPBACK_H_
