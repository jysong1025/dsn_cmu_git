/*
 * test_iobus_lowlevel.c : test MB MCS IO bus with low level driver
 * note: stdio --> MDM ... see system.mss in bsp
 */

// note:
//   low level IO access driver : xiomodule_l.h
//   logic-design under test  : mcs_io_bridge.v


#include <stdio.h>
#include "xil_printf.h"

#include "xiomodule_l.h" // low-level driver
//#include "xstatus.h"
//#include "mb_interface.h"

#include "../CMU_CPU_TOP/platform.h" // default bsp setting
#include "../CMU_CPU_TOP/cmu_cpu_config.h" // CMU-CPU board config


int main()
{
    init_platform();

    // test for print on jtag-terminal
    print("Hello World!!\n\r");

    // test low-level driver for IO bus
    //
    // XIomodule_In32 (XPAR_IOMODULE_0_IO_BASEADDR + byte_offset);
    // XIomodule_Out32(XPAR_IOMODULE_0_IO_BASEADDR + byte_offset, value);
    //
    xil_printf(">> test low-level driver for IO bus \n\r");
    //
    u32 adrs;
    u32 value;
    //
    // ADRS_FPGA_IMAGE_ID
    adrs = ADRS_FPGA_IMAGE;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    // ADRS_TEST_REG
    adrs = ADRS_TEST_REG;
    value = 0x00001234;
    u32 cnt = 0;
    //
    while (1) {
    //
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: test XIomodule_Out32: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    value++;
    //
    cnt++;
	if (cnt==10) break;
    }
    //
    ////

    // test IO bus range scope
	//#define XPAR_IOMODULE_0_IO_BASEADDR 0xC0000000U
	//#define XPAR_IOMODULE_0_IO_HIGHADDR 0xFFFFFFFFU
    //
    xil_printf(">> test IO bus range scope \n\r");
    //
    adrs = 0x0FFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0x1FFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0x2FFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0x3FFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0x4FFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0x5FFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0x6FFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0x7FFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0x8FFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0x9FFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0xAFFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0xBFFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0xCFFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0xDFFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0xEFFFFFFFU;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = 0xFFFFFFFFU;
        value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    ////

    // test IO ports
    //
    //// test PORT_WI
    xil_printf(">> test PORT_WI (out) \n\r");
    //
    // ADRS_PORT_WI_00
    adrs = ADRS_PORT_WI_00;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    value = value + 1;
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: test XIomodule_Out32: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    // ADRS_PORT_WI_01
    adrs = ADRS_PORT_WI_01;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    value = value + 1;
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: test XIomodule_Out32: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    //
    //// test PORT_WO
    xil_printf(">> test PORT_WO (in) \n\r");
    //
    // ADRS_PORT_WO_00
    adrs = ADRS_PORT_WO_20;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    // ADRS_PORT_WO_01
    adrs = ADRS_PORT_WO_21;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: test XIomodule_In32 : 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    ////

    ////
    xil_printf(">>> Finished \r\n");

    //
    cleanup_platform();
    return 0;
}
