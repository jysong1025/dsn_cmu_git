#ifndef __MCS_IO_BRIDGE_H_
#define __MCS_IO_BRIDGE_H_

#include "xil_types.h"


#include "mhvsu_base_config.h" //$$ board dependent

#define _MCS_DEBUG_


//// common //{
u32 hexchr2data_u32(u8 hexchr);
u32 hexstr2data_u32(u8* hexstr, u32 len);
u32 decchr2data_u32(u8 chr);
u32 decstr2data_u32(u8* str, u32 len);
u32 is_dec_char(u8 chr); // 0 for dec; -1 for not
//}


//// MCS io access functions  //{
u32 _test_read_mcs (char* txt, u32 adrs);
u32 _test_write_mcs(char* txt, u32 adrs, u32 value);
//
u32 read_mcs_io (u32 adrs);
u32 write_mcs_io(u32 adrs, u32 value);
//
u32   read_mcs_fpga_img_id(u32 adrs_base);
u32   read_mcs_test_reg(u32 adrs_base);
void write_mcs_test_reg(u32 adrs_base, u32 data);
//}


//// wiznet 850io functions //{
u8     hw_reset__wz850();
u8    read_data__wz850 (u32 AddrSel);
void write_data__wz850 (u32 AddrSel, u8 value);
void  read_data_buf__wz850 (u32 AddrSel, u8* p_val, u16 len);
void write_data_buf__wz850 (u32 AddrSel, u8* p_val, u16 len);
//
//void  read_data_pipe__wz850 (u32 AddrSel, u32 ep_offset, u32 len_u32); // not used yet
void  read_data_pipe__wz850 (u32 AddrSel, u32 dst_adrs_p32, u32 len_u32); // not used yet
void write_data_pipe__wz850 (u32 AddrSel, u32 src_adrs_p32, u32 len);
//}



//// MCS-EP access subfunctions //{
void  enable_mcs_ep(); // enable MCS end-point control over CMU-CPU
void disable_mcs_ep(); // disable MCS end-point control over CMU-CPU
void reset_mcs_ep(); // reset MCS end-point control

//$$ check to use
void reset_io_dev(); // reset ...  adc / dwave / bias / spo //$$ to see for PGU 

u32 is_enabled_mcs_ep(); 
//
u32  read_mcs_ep_wi_mask(u32 adrs_base);
u32 write_mcs_ep_wi_mask(u32 adrs_base, u32 mask);
u32  read_mcs_ep_wo_mask(u32 adrs_base);
u32 write_mcs_ep_wo_mask(u32 adrs_base, u32 mask);
u32  read_mcs_ep_ti_mask(u32 adrs_base);
u32 write_mcs_ep_ti_mask(u32 adrs_base, u32 mask);
u32  read_mcs_ep_to_mask(u32 adrs_base);
u32 write_mcs_ep_to_mask(u32 adrs_base, u32 mask);
u32  read_mcs_ep_wi_data(u32 adrs_base, u32 offset);
u32 write_mcs_ep_wi_data(u32 adrs_base, u32 offset, u32 data);
u32  read_mcs_ep_wo_data(u32 adrs_base, u32 offset);
u32  read_mcs_ep_ti_data(u32 adrs_base, u32 offset);
u32 write_mcs_ep_ti_data(u32 adrs_base, u32 offset, u32 data);
u32  read_mcs_ep_to_data(u32 adrs_base, u32 offset);
u32 write_mcs_ep_pi_data(u32 adrs_base, u32 offset, u32 data); // write data from a 32b-value to pipe-in(32b)
u32  read_mcs_ep_po_data(u32 adrs_base, u32 offset);           // read data from pipe-out(32b) to a 32b-value
//
u32   read_mcs_ep_wi(u32 adrs_base, u32 offset);
void write_mcs_ep_wi(u32 adrs_base, u32 offset, u32 data, u32 mask);
u32   read_mcs_ep_wo(u32 adrs_base, u32 offset, u32 mask);
u32   read_mcs_ep_ti(u32 adrs_base, u32 offset);
void write_mcs_ep_ti(u32 adrs_base, u32 offset, u32 data, u32 mask); //$$
void activate_mcs_ep_ti(u32 adrs_base, u32 offset, u32 bit_loc);
u32   read_mcs_ep_to(u32 adrs_base, u32 offset, u32 mask); //$$
u32 is_triggered_mcs_ep_to(u32 adrs_base, u32 offset, u32 mask);
//
u32 write_mcs_ep_pi_buf (u32 adrs_base, u32 offset, u32 len_byte, u8 *p_data); // write data from buffer(32b) to pipe-in(8b) // not used
u32  read_mcs_ep_po_buf (u32 adrs_base, u32 offset, u32 len_byte, u8 *p_data); // read data from pipe-out(8b) to buffer(32b) // not used
//u32 write_mcs_ep_pi_fifo(u32 offset, u32 len_byte, u8 *p_data); // write data from fifo(32b) to pipe-in(8b) 
//u32  read_mcs_ep_po_fifo(u32 offset, u32 len_byte, u8 *p_data); // read data from pipe-out(8b) to fifo(32b) 

//}

//// subfunctions for pipe //{

// note pipe end-points in CMU or PGU 
// pipe-in  address: 
//    ADRS_PORT_PI_80 ... LAN  fifo  8-bit in MCS0
//    pi8A            ... TEST fifo 32-bit in MCS1
// pipe-out address: 
//    ADRS_PORT_PO_A0 ... LAN  fifo  8-bit in MCS0
//    poAA            ... TEST fifo 32-bit in MCS1
//    poBC            ... ADC  fifo 32-bit in MCS1
//    poBD            ... ADC  fifo 32-bit in MCS1


// PIPE-DATA in buffer (pointer)
//     buffer ... u32 buf[], u8 buf[]
//     pipe  ... 32-bit pipe, 8-bit pipe
//        32-bit pipe --> 32-bit buf
//        32-bit buf  --> 32-bit pipe
//         8-bit pipe -->  8-bit buf
//         8-bit buf  -->  8-bit pipe
//         8-bit pipe --> 32-bit buf
//        32-bit buf  -->  8-bit pipe
void dcopy_pipe8_to_buf8  (u32 adrs_p8, u8 *p_buf_u8, u32 len); // (src,dst,len)
void dcopy_buf8_to_pipe8  (u8 *p_buf_u8, u32 adrs_p8, u32 len); // (src,dst,len)
void dcopy_pipe32_to_buf32(u32 adrs_p32, u32 *p_buf_u32, u32 len_byte); // (src,dst,len_byte) 
void dcopy_buf32_to_pipe32(u32 *p_buf_u32, u32 adrs_p32, u32 len_byte); // (src,dst,len_byte)
void dcopy_buf8_to_pipe32(u8 *p_buf_u8, u32 adrs_p32, u32 len_byte); // (src,dst,len_byte) //$$ used in cmd_str__PGEP_PI

// PIPE-DATA in fifo/pipe (IO address)
//     pipe  ... 32-bit pipe, 8-bit pipe
//        32-bit pipe --> 32-bit pipe
//        32-bit pipe -->  8-bit pipe
//         8-bit pipe --> 32-bit pipe
void dcopy_pipe32_to_pipe32(u32 src_adrs_p32, u32 dst_adrs_p32, u32 len_byte);
void dcopy_pipe8_to_pipe32 (u32 src_adrs_p8,  u32 dst_adrs_p32, u32 len_byte);
void dcopy_pipe32_to_pipe8 (u32 src_adrs_p32, u32 dst_adrs_p8,  u32 len_byte);


//}



//// eeprom access over WTP-EP //{

//...
//  
//  ## TODO: EEPROM functions 
//  #  // w_MEM_WI   
//  #  assign w_num_bytes_DAT               = w_MEM_WI[12:0]; // 12-bit 
//  #  assign w_con_disable_SBP             = w_MEM_WI[15]; // 1-bit
//  #  assign w_con_fifo_path__L_sspi_H_lan = w_MEM_WI[16]; // 1-bit
//  #  assign w_con_port__L_MEM_SIO__H_TP   = w_MEM_WI[17]; // 1-bit
//  #  
//  #  // w_MEM_FDAT_WI
//  #  assign w_frame_data_CMD              = w_MEM_FDAT_WI[ 7: 0]; // 8-bit
//  #  assign w_frame_data_STA_in           = w_MEM_FDAT_WI[15: 8]; // 8-bit
//  #  assign w_frame_data_ADL              = w_MEM_FDAT_WI[23:16]; // 8-bit
//  #  assign w_frame_data_ADH              = w_MEM_FDAT_WI[31:24]; // 8-bit
//  #  
//  #  // w_MEM_TI
//  #  assign w_MEM_rst      = w_MEM_TI[0];
//  #  assign w_MEM_fifo_rst = w_MEM_TI[1];
//  #  assign w_trig_frame   = w_MEM_TI[2];
//  #  
//  #  // w_MEM_TO
//  #  assign w_MEM_TO[0]     = w_MEM_valid    ;
//  #  assign w_MEM_TO[1]     = w_done_frame   ;
//  #  assign w_MEM_TO[2]     = w_done_frame_TO; //$$ rev
//  #  assign w_MEM_TO[15: 8] = w_frame_data_STA_out; 
//  #  
//  #  // w_MEM_PI
//  #  assign w_frame_data_DAT_wr    = w_MEM_PI[7:0]; // 8-bit
//  #  assign w_frame_data_DAT_wr_en = w_MEM_PI_wr;
//  #  
//  #  // w_MEM_PO
//  #  assign w_MEM_PO[7:0]          = w_frame_data_DAT_rd; // 8-bit
//  #  assign w_MEM_PO[31:8]         = 24'b0; // unused
//  #  assign w_frame_data_DAT_rd_en = w_MEM_PO_rd;
//  

u32  eeprom_send_frame_ep (u32 MEM_WI, u32 MEM_FDAT_WI);
u32  eeprom_set_g_var (u8 EEPROM__LAN_access, u8 EEPROM__on_TP);
u32  eeprom_send_frame (u8 CMD_b8, u8 STA_in_b8, u8 ADL_b8, u8 ADH_b8, u16 num_bytes_DAT_b16, u8 con_disable_SBP_b8);
void eeprom_write_enable();
void eeprom_write_disable();
u32 eeprom_read_status();
void eeprom_write_status (u8 BP1_b8, u8 BP0_b8);
u32 is_eeprom_available();
void eeprom_erase_all();
void eeprom_set_all();
void eeprom_reset_fifo();

u16 eeprom_read_fifo (u16 num_bytes_DAT_b16, u8 *buf_dataout);
u16 eeprom_write_fifo (u16 num_bytes_DAT_b16, u8 *buf_datain);
u16 eeprom_read_data (u16 ADRS_b16, u16 num_bytes_DAT_b16, u8 *buf_dataout);
u16 eeprom_read_data_current (u16 num_bytes_DAT_b16, u8 *buf_dataout);
u16 eeprom_write_data_16B (u16 ADRS_b16, u16 num_bytes_DAT_b16);
u16 eeprom_write_data (u16 ADRS_b16, u16 num_bytes_DAT_b16, u8 *buf_datain);
u8* get_adrs__g_EEPROM__buf_2KB();
u16 eeprom_read_all();
u16 eeprom_write_all();

void hex_txt_display (s16 len_b16, u8 *p_mem_data, u32 adrs_offset);
u8 cal_checksum (u16 len_b16, u8 *p_data_b8);
u8 gen_checksum (u16 len_b16, u8 *p_data_b8);
u8 chk_all_zeros (u16 len_b16, u8 *p_data_b8);

//}

#endif
