#ifndef __MHVSU_BASE_CONFIG_H_		/* prevent circular inclusions */
#define __MHVSU_BASE_CONFIG_H_		/* by using protection macros */

#ifdef __cplusplus
extern "C" {
#endif

////

#include "xiomodule_l.h" // low-level driver
// for XPAR_IOMODULE_0_IO_BASEADDR
// for XIomodule_In32() and XIomodule_Out32()

#include "xil_printf.h"

//  // macro for CMU-CPU board support
//  //#define _CMU_CPU_
//  
//  // macro for PGU-CPU board support
//  #define _PGU_CPU_

// macro for FPGA LAN end point access
//   for w5500.c
// _PGU_CPU_ --> _MCS_IO_BRDG_
#define _MCS_IO_BRDG_


// offset definition for mcs_io_bridge.v //{
#define MCS_IO_INST_OFFSET              0x00000000 // for LAN
#define MCS_IO_INST_OFFSET_CMU          0x00010000 // for CMU
#define MCS_IO_INST_OFFSET_PGU          0x00020000 // for PGU
//#define MCS_IO_INST_OFFSET_EXT          0x00030000 // for MHVSU_BASE (port end-points + lan end-points)
#define MCS_IO_INST_OFFSET_EXT          0x00040000 // for NEW CMU (port end-points + lan end-points)
//}

// BASE //{
#define ADRS_BASE           XPAR_IOMODULE_0_IO_BASEADDR + MCS_IO_INST_OFFSET     // not used
#define ADRS_BASE_CMU       XPAR_IOMODULE_0_IO_BASEADDR + MCS_IO_INST_OFFSET_CMU // not used
#define ADRS_BASE_PGU       XPAR_IOMODULE_0_IO_BASEADDR + MCS_IO_INST_OFFSET_PGU // not used
#define ADRS_BASE_MHVSU     XPAR_IOMODULE_0_IO_BASEADDR + MCS_IO_INST_OFFSET_EXT
//}

// TODO: remove // base address for LAN //{
#define ADRS_PORT_WI_00         ADRS_BASE     + 0x00000000 // output wire [31:0]
#define ADRS_PORT_WI_01         ADRS_BASE     + 0x00000010 // output wire [31:0]
#define ADRS_PORT_WI_02         ADRS_BASE     + 0x00000020 // output wire [31:0]
#define ADRS_PORT_WI_03         ADRS_BASE     + 0x00000030 // output wire [31:0]
#define ADRS_PORT_WI_04         ADRS_BASE     + 0x00000040 // output wire [31:0]
#define ADRS_PORT_WI_05         ADRS_BASE     + 0x00000050 // output wire [31:0]
#define ADRS_PORT_WI_06         ADRS_BASE     + 0x00000060 // output wire [31:0]
#define ADRS_PORT_WI_07         ADRS_BASE     + 0x00000070 // output wire [31:0]
#define ADRS_PORT_WI_08         ADRS_BASE     + 0x00000080 // output wire [31:0]
#define ADRS_PORT_WI_09         ADRS_BASE     + 0x00000090 // output wire [31:0]
#define ADRS_PORT_WI_0A         ADRS_BASE     + 0x000000A0 // output wire [31:0]
#define ADRS_PORT_WI_0B         ADRS_BASE     + 0x000000B0 // output wire [31:0]
#define ADRS_PORT_WI_0C         ADRS_BASE     + 0x000000C0 // output wire [31:0]
#define ADRS_PORT_WI_0D         ADRS_BASE     + 0x000000D0 // output wire [31:0]
#define ADRS_PORT_WI_0E         ADRS_BASE     + 0x000000E0 // output wire [31:0]
#define ADRS_PORT_WI_0F         ADRS_BASE     + 0x000000F0 // output wire [31:0]
#define ADRS_PORT_WI_10         ADRS_BASE     + 0x00000100 // output wire [31:0]
#define ADRS_PORT_WI_11         ADRS_BASE     + 0x00000110 // output wire [31:0]
#define ADRS_PORT_WI_12         ADRS_BASE     + 0x00000120 // output wire [31:0]
#define ADRS_PORT_WI_13         ADRS_BASE     + 0x00000130 // output wire [31:0]
#define ADRS_PORT_WI_14         ADRS_BASE     + 0x00000140 // output wire [31:0]
#define ADRS_PORT_WI_15         ADRS_BASE     + 0x00000150 // output wire [31:0]
#define ADRS_PORT_WI_16         ADRS_BASE     + 0x00000160 // output wire [31:0]
#define ADRS_PORT_WI_17         ADRS_BASE     + 0x00000170 // output wire [31:0]
#define ADRS_PORT_WI_18         ADRS_BASE     + 0x00000180 // output wire [31:0]
#define ADRS_PORT_WI_19         ADRS_BASE     + 0x00000190 // output wire [31:0]
#define ADRS_PORT_WI_1A         ADRS_BASE     + 0x000001A0 // output wire [31:0]
#define ADRS_PORT_WI_1B         ADRS_BASE     + 0x000001B0 // output wire [31:0]
#define ADRS_PORT_WI_1C         ADRS_BASE     + 0x000001C0 // output wire [31:0]
#define ADRS_PORT_WI_1D         ADRS_BASE     + 0x000001D0 // output wire [31:0]
#define ADRS_PORT_WI_1E         ADRS_BASE     + 0x000001E0 // output wire [31:0]
#define ADRS_PORT_WI_1F         ADRS_BASE     + 0x000001F0 // output wire [31:0]
//
#define ADRS_PORT_WO_20         ADRS_BASE     + 0x00000200 // input wire [31:0]
#define ADRS_PORT_WO_21         ADRS_BASE     + 0x00000210 // input wire [31:0]
//
#define ADRS_PORT_TI_40         ADRS_BASE     + 0x00000400 // input wire, output wire [31:0],
#define ADRS_PORT_TI_41         ADRS_BASE     + 0x00000410 // input wire, output wire [31:0],
//
#define ADRS_PORT_TO_60         ADRS_BASE     + 0x00000600 // input wire, input wire [31:0],
#define ADRS_PORT_TO_61         ADRS_BASE     + 0x00000610 // input wire, input wire [31:0],
//
#define ADRS_PORT_PI_80         ADRS_BASE     + 0x00000800 // output wire, output wire [31:0],
#define ADRS_PORT_PI_81         ADRS_BASE     + 0x00000810 // output wire, output wire [31:0],
//
#define ADRS_PORT_PO_A0         ADRS_BASE     + 0x00000A00 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A1         ADRS_BASE     + 0x00000A10 // output wire, input wire [31:0],
//
#define ADRS_FPGA_IMAGE         ADRS_BASE     + 0x00000F00 // image id
#define ADRS_TEST_REG           ADRS_BASE     + 0x00000F04 // test reg 
#define ADRS_MASK_WI            ADRS_BASE     + 0x00000F10 // mask
#define ADRS_MASK_WO            ADRS_BASE     + 0x00000F14 // mask
#define ADRS_MASK_TI            ADRS_BASE     + 0x00000F18 // mask
#define ADRS_MASK_TO            ADRS_BASE     + 0x00000F1C // mask
//}

// common offset for end point address //{
#define ADRS_PORT_WI_00_OFST     0x00000000 // output wire [31:0] 
#define ADRS_PORT_WI_01_OFST     0x00000010 // output wire [31:0] 
#define ADRS_PORT_WI_02_OFST     0x00000020 // output wire [31:0]
#define ADRS_PORT_WI_03_OFST     0x00000030 // output wire [31:0] 
#define ADRS_PORT_WI_04_OFST     0x00000040 // output wire [31:0] 
#define ADRS_PORT_WI_05_OFST     0x00000050 // output wire [31:0] 
#define ADRS_PORT_WI_06_OFST     0x00000060 // output wire [31:0] 
#define ADRS_PORT_WI_07_OFST     0x00000070 // output wire [31:0] 
#define ADRS_PORT_WI_08_OFST     0x00000080 // output wire [31:0]
#define ADRS_PORT_WI_09_OFST     0x00000090 // output wire [31:0]
#define ADRS_PORT_WI_0A_OFST     0x000000A0 // output wire [31:0]
#define ADRS_PORT_WI_0B_OFST     0x000000B0 // output wire [31:0]
#define ADRS_PORT_WI_0C_OFST     0x000000C0 // output wire [31:0]
#define ADRS_PORT_WI_0D_OFST     0x000000D0 // output wire [31:0]
#define ADRS_PORT_WI_0E_OFST     0x000000E0 // output wire [31:0]
#define ADRS_PORT_WI_0F_OFST     0x000000F0 // output wire [31:0]
#define ADRS_PORT_WI_10_OFST     0x00000100 // output wire [31:0]
#define ADRS_PORT_WI_11_OFST     0x00000110 // output wire [31:0]
#define ADRS_PORT_WI_12_OFST     0x00000120 // output wire [31:0]
#define ADRS_PORT_WI_13_OFST     0x00000130 // output wire [31:0]
#define ADRS_PORT_WI_14_OFST     0x00000140 // output wire [31:0]
#define ADRS_PORT_WI_15_OFST     0x00000150 // output wire [31:0]
#define ADRS_PORT_WI_16_OFST     0x00000160 // output wire [31:0]
#define ADRS_PORT_WI_17_OFST     0x00000170 // output wire [31:0]
#define ADRS_PORT_WI_18_OFST     0x00000180 // output wire [31:0]
#define ADRS_PORT_WI_19_OFST     0x00000190 // output wire [31:0]
#define ADRS_PORT_WI_1A_OFST     0x000001A0 // output wire [31:0]
#define ADRS_PORT_WI_1B_OFST     0x000001B0 // output wire [31:0]
#define ADRS_PORT_WI_1C_OFST     0x000001C0 // output wire [31:0]
#define ADRS_PORT_WI_1D_OFST     0x000001D0 // output wire [31:0]
#define ADRS_PORT_WI_1E_OFST     0x000001E0 // output wire [31:0]
#define ADRS_PORT_WI_1F_OFST     0x000001F0 // output wire [31:0]
//
#define ADRS_PORT_WO_20_OFST     0x00000200 // input wire [31:0]
#define ADRS_PORT_WO_21_OFST     0x00000210 // input wire [31:0]
#define ADRS_PORT_WO_22_OFST     0x00000220 // input wire [31:0]
#define ADRS_PORT_WO_23_OFST     0x00000230 // input wire [31:0]
#define ADRS_PORT_WO_24_OFST     0x00000240 // input wire [31:0]
#define ADRS_PORT_WO_25_OFST     0x00000250 // input wire [31:0]
#define ADRS_PORT_WO_26_OFST     0x00000260 // input wire [31:0]
#define ADRS_PORT_WO_27_OFST     0x00000270 // input wire [31:0]
#define ADRS_PORT_WO_28_OFST     0x00000280 // input wire [31:0]
#define ADRS_PORT_WO_29_OFST     0x00000290 // input wire [31:0]
#define ADRS_PORT_WO_2A_OFST     0x000002A0 // input wire [31:0]
#define ADRS_PORT_WO_2B_OFST     0x000002B0 // input wire [31:0]
#define ADRS_PORT_WO_2C_OFST     0x000002C0 // input wire [31:0]
#define ADRS_PORT_WO_2D_OFST     0x000002D0 // input wire [31:0]
#define ADRS_PORT_WO_2E_OFST     0x000002E0 // input wire [31:0]
#define ADRS_PORT_WO_2F_OFST     0x000002F0 // input wire [31:0]
#define ADRS_PORT_WO_30_OFST     0x00000300 // input wire [31:0]
#define ADRS_PORT_WO_31_OFST     0x00000310 // input wire [31:0]
#define ADRS_PORT_WO_32_OFST     0x00000320 // input wire [31:0]
#define ADRS_PORT_WO_33_OFST     0x00000330 // input wire [31:0]
#define ADRS_PORT_WO_34_OFST     0x00000340 // input wire [31:0]
#define ADRS_PORT_WO_35_OFST     0x00000350 // input wire [31:0]
#define ADRS_PORT_WO_36_OFST     0x00000360 // input wire [31:0]
#define ADRS_PORT_WO_37_OFST     0x00000370 // input wire [31:0]
#define ADRS_PORT_WO_38_OFST     0x00000380 // input wire [31:0]
#define ADRS_PORT_WO_39_OFST     0x00000390 // input wire [31:0]
#define ADRS_PORT_WO_3A_OFST     0x000003A0 // input wire [31:0]
#define ADRS_PORT_WO_3B_OFST     0x000003B0 // input wire [31:0]
#define ADRS_PORT_WO_3C_OFST     0x000003C0 // input wire [31:0]
#define ADRS_PORT_WO_3D_OFST     0x000003D0 // input wire [31:0]
#define ADRS_PORT_WO_3E_OFST     0x000003E0 // input wire [31:0]
#define ADRS_PORT_WO_3F_OFST     0x000003F0 // input wire [31:0]
//
#define ADRS_PORT_TI_40_OFST     0x00000400 // input wire, output wire [31:0]
#define ADRS_PORT_TI_41_OFST     0x00000410 // input wire, output wire [31:0]
#define ADRS_PORT_TI_42_OFST     0x00000420 // input wire, output wire [31:0]
#define ADRS_PORT_TI_43_OFST     0x00000430 // input wire, output wire [31:0]
#define ADRS_PORT_TI_44_OFST     0x00000440 // input wire, output wire [31:0]
#define ADRS_PORT_TI_45_OFST     0x00000450 // input wire, output wire [31:0]
#define ADRS_PORT_TI_46_OFST     0x00000460 // input wire, output wire [31:0]
#define ADRS_PORT_TI_47_OFST     0x00000470 // input wire, output wire [31:0]
#define ADRS_PORT_TI_48_OFST     0x00000480 // input wire, output wire [31:0]
#define ADRS_PORT_TI_49_OFST     0x00000490 // input wire, output wire [31:0]
#define ADRS_PORT_TI_4A_OFST     0x000004A0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_4B_OFST     0x000004B0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_4C_OFST     0x000004C0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_4D_OFST     0x000004D0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_4E_OFST     0x000004E0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_4F_OFST     0x000004F0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_50_OFST     0x00000500 // input wire, output wire [31:0]
#define ADRS_PORT_TI_51_OFST     0x00000510 // input wire, output wire [31:0]
#define ADRS_PORT_TI_52_OFST     0x00000520 // input wire, output wire [31:0]
#define ADRS_PORT_TI_53_OFST     0x00000530 // input wire, output wire [31:0]
#define ADRS_PORT_TI_54_OFST     0x00000540 // input wire, output wire [31:0]
#define ADRS_PORT_TI_55_OFST     0x00000550 // input wire, output wire [31:0]
#define ADRS_PORT_TI_56_OFST     0x00000560 // input wire, output wire [31:0]
#define ADRS_PORT_TI_57_OFST     0x00000570 // input wire, output wire [31:0]
#define ADRS_PORT_TI_58_OFST     0x00000580 // input wire, output wire [31:0]
#define ADRS_PORT_TI_59_OFST     0x00000590 // input wire, output wire [31:0]
#define ADRS_PORT_TI_5A_OFST     0x000005A0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_5B_OFST     0x000005B0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_5C_OFST     0x000005C0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_5D_OFST     0x000005D0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_5E_OFST     0x000005E0 // input wire, output wire [31:0]
#define ADRS_PORT_TI_5F_OFST     0x000005F0 // input wire, output wire [31:0]
//
#define ADRS_PORT_TO_60_OFST     0x00000600 // input wire, input wire [31:0]
#define ADRS_PORT_TO_61_OFST     0x00000610 // input wire, input wire [31:0]
#define ADRS_PORT_TO_62_OFST     0x00000620 // input wire, input wire [31:0]
#define ADRS_PORT_TO_63_OFST     0x00000630 // input wire, input wire [31:0]
#define ADRS_PORT_TO_64_OFST     0x00000640 // input wire, input wire [31:0]
#define ADRS_PORT_TO_65_OFST     0x00000650 // input wire, input wire [31:0]
#define ADRS_PORT_TO_66_OFST     0x00000660 // input wire, input wire [31:0]
#define ADRS_PORT_TO_67_OFST     0x00000670 // input wire, input wire [31:0]
#define ADRS_PORT_TO_68_OFST     0x00000680 // input wire, input wire [31:0]
#define ADRS_PORT_TO_69_OFST     0x00000690 // input wire, input wire [31:0]
#define ADRS_PORT_TO_6A_OFST     0x000006A0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_6B_OFST     0x000006B0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_6C_OFST     0x000006C0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_6D_OFST     0x000006D0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_6E_OFST     0x000006E0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_6F_OFST     0x000006F0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_70_OFST     0x00000700 // input wire, input wire [31:0]
#define ADRS_PORT_TO_71_OFST     0x00000710 // input wire, input wire [31:0]
#define ADRS_PORT_TO_72_OFST     0x00000720 // input wire, input wire [31:0]
#define ADRS_PORT_TO_73_OFST     0x00000730 // input wire, input wire [31:0]
#define ADRS_PORT_TO_74_OFST     0x00000740 // input wire, input wire [31:0]
#define ADRS_PORT_TO_75_OFST     0x00000750 // input wire, input wire [31:0]
#define ADRS_PORT_TO_76_OFST     0x00000760 // input wire, input wire [31:0]
#define ADRS_PORT_TO_77_OFST     0x00000770 // input wire, input wire [31:0]
#define ADRS_PORT_TO_78_OFST     0x00000780 // input wire, input wire [31:0]
#define ADRS_PORT_TO_79_OFST     0x00000790 // input wire, input wire [31:0]
#define ADRS_PORT_TO_7A_OFST     0x000007A0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_7B_OFST     0x000007B0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_7C_OFST     0x000007C0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_7D_OFST     0x000007D0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_7E_OFST     0x000007E0 // input wire, input wire [31:0]
#define ADRS_PORT_TO_7F_OFST     0x000007F0 // input wire, input wire [31:0]
//
#define ADRS_PORT_PI_80_OFST     0x00000800 // output wire, output wire [31:0]
#define ADRS_PORT_PI_81_OFST     0x00000810 // output wire, output wire [31:0]
#define ADRS_PORT_PI_82_OFST     0x00000820 // output wire, output wire [31:0]
#define ADRS_PORT_PI_83_OFST     0x00000830 // output wire, output wire [31:0]
#define ADRS_PORT_PI_84_OFST     0x00000840 // output wire, output wire [31:0]
#define ADRS_PORT_PI_85_OFST     0x00000850 // output wire, output wire [31:0]
#define ADRS_PORT_PI_86_OFST     0x00000860 // output wire, output wire [31:0]
#define ADRS_PORT_PI_87_OFST     0x00000870 // output wire, output wire [31:0]
#define ADRS_PORT_PI_88_OFST     0x00000880 // output wire, output wire [31:0]
#define ADRS_PORT_PI_89_OFST     0x00000890 // output wire, output wire [31:0]
#define ADRS_PORT_PI_8A_OFST     0x000008A0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_8B_OFST     0x000008B0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_8C_OFST     0x000008C0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_8D_OFST     0x000008D0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_8E_OFST     0x000008E0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_8F_OFST     0x000008F0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_90_OFST     0x00000900 // output wire, output wire [31:0]
#define ADRS_PORT_PI_91_OFST     0x00000910 // output wire, output wire [31:0]
#define ADRS_PORT_PI_92_OFST     0x00000920 // output wire, output wire [31:0]
#define ADRS_PORT_PI_93_OFST     0x00000930 // output wire, output wire [31:0]
#define ADRS_PORT_PI_94_OFST     0x00000940 // output wire, output wire [31:0]
#define ADRS_PORT_PI_95_OFST     0x00000950 // output wire, output wire [31:0]
#define ADRS_PORT_PI_96_OFST     0x00000960 // output wire, output wire [31:0]
#define ADRS_PORT_PI_97_OFST     0x00000970 // output wire, output wire [31:0]
#define ADRS_PORT_PI_98_OFST     0x00000980 // output wire, output wire [31:0]
#define ADRS_PORT_PI_99_OFST     0x00000990 // output wire, output wire [31:0]
#define ADRS_PORT_PI_9A_OFST     0x000009A0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_9B_OFST     0x000009B0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_9C_OFST     0x000009C0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_9D_OFST     0x000009D0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_9E_OFST     0x000009E0 // output wire, output wire [31:0]
#define ADRS_PORT_PI_9F_OFST     0x000009F0 // output wire, output wire [31:0]
//
#define ADRS_PORT_PO_A0_OFST     0x00000A00 // output wire, input wire [31:0]
#define ADRS_PORT_PO_A1_OFST     0x00000A10 // output wire, input wire [31:0]
#define ADRS_PORT_PO_A2_OFST     0x00000A20 // output wire, input wire [31:0]
#define ADRS_PORT_PO_A3_OFST     0x00000A30 // output wire, input wire [31:0]
#define ADRS_PORT_PO_A4_OFST     0x00000A40 // output wire, input wire [31:0]
#define ADRS_PORT_PO_A5_OFST     0x00000A50 // output wire, input wire [31:0]
#define ADRS_PORT_PO_A6_OFST     0x00000A60 // output wire, input wire [31:0]
#define ADRS_PORT_PO_A7_OFST     0x00000A70 // output wire, input wire [31:0]
#define ADRS_PORT_PO_A8_OFST     0x00000A80 // output wire, input wire [31:0]
#define ADRS_PORT_PO_A9_OFST     0x00000A90 // output wire, input wire [31:0]
#define ADRS_PORT_PO_AA_OFST     0x00000AA0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_AB_OFST     0x00000AB0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_AC_OFST     0x00000AC0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_AD_OFST     0x00000AD0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_AE_OFST     0x00000AE0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_AF_OFST     0x00000AF0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_B0_OFST     0x00000B00 // output wire, input wire [31:0]
#define ADRS_PORT_PO_B1_OFST     0x00000B10 // output wire, input wire [31:0]
#define ADRS_PORT_PO_B2_OFST     0x00000B20 // output wire, input wire [31:0]
#define ADRS_PORT_PO_B3_OFST     0x00000B30 // output wire, input wire [31:0]
#define ADRS_PORT_PO_B4_OFST     0x00000B40 // output wire, input wire [31:0]
#define ADRS_PORT_PO_B5_OFST     0x00000B50 // output wire, input wire [31:0]
#define ADRS_PORT_PO_B6_OFST     0x00000B60 // output wire, input wire [31:0]
#define ADRS_PORT_PO_B7_OFST     0x00000B70 // output wire, input wire [31:0]
#define ADRS_PORT_PO_B8_OFST     0x00000B80 // output wire, input wire [31:0]
#define ADRS_PORT_PO_B9_OFST     0x00000B90 // output wire, input wire [31:0]
#define ADRS_PORT_PO_BA_OFST     0x00000BA0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_BB_OFST     0x00000BB0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_BC_OFST     0x00000BC0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_BD_OFST     0x00000BD0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_BE_OFST     0x00000BE0 // output wire, input wire [31:0]
#define ADRS_PORT_PO_BF_OFST     0x00000BF0 // output wire, input wire [31:0]
//
// dedicated lan interface
#define ADRS_LAN_WI_00_OFST      0x00000C00 // output wire [31:0]
#define ADRS_LAN_WI_01_OFST      0x00000C04 // output wire [31:0]
#define ADRS_LAN_WO_20_OFST      0x00000C80 // input wire [31:0]
#define ADRS_LAN_WO_21_OFST      0x00000C84 // input wire [31:0]
#define ADRS_LAN_TI_40_OFST      0x00000D00 // input wire, output wire [31:0],
#define ADRS_LAN_TI_41_OFST      0x00000D04 // input wire, output wire [31:0],
#define ADRS_LAN_TO_60_OFST      0x00000D80 // input wire, input wire [31:0],
#define ADRS_LAN_TO_61_OFST      0x00000D84 // input wire, input wire [31:0],
#define ADRS_LAN_PI_80_OFST      0x00000E00 // output wire, output wire [31:0],
#define ADRS_LAN_PI_81_OFST      0x00000E04 // output wire, output wire [31:0],
#define ADRS_LAN_PO_A0_OFST      0x00000E80 // output wire, input wire [31:0],
#define ADRS_LAN_PO_A1_OFST      0x00000E84 // output wire, input wire [31:0],
//
#define ADRS_FPGA_IMAGE_OFST     0x00000F00 // image id
#define ADRS_TEST_REG___OFST     0x00000F04 // test reg 
#define ADRS_MASK_WI____OFST     0x00000F10 // mask
#define ADRS_MASK_WO____OFST     0x00000F14 // mask
#define ADRS_MASK_TI____OFST     0x00000F18 // mask
#define ADRS_MASK_TO____OFST     0x00000F1C // mask	
//
#define ADRS_LAN_CONF_00_OFST    0x00000FC0 //input  wire [31:0]   i_lan_conf_00  // (BASE_ADRS_IP_32B  + i_adrs_offset_ip_32b )
#define ADRS_LAN_CONF_01_OFST    0x00000FC4 //input  wire [31:0]   i_lan_conf_01  // (BASE_ADRS_MAC_48B[31: 0] + i_adrs_offset_mac_48b[31: 0] )
#define ADRS_LAN_CONF_02_OFST    0x00000FC8 //input  wire [31:0]   i_lan_conf_02  // ( {16'b0,BASE_ADRS_MAC_48B[47:32]} + {16'b0,i_adrs_offset_mac_48b[47:32]} )
#define ADRS_LAN_CONF_03_OFST    0x00000FCC //input  wire [31:0]   i_lan_conf_03  // ( i_lan_timeout_rtr_16b , i_lan_timeout_rcr_16b )
//

//}


// TODO: remove// base address for CMU //{
#define ADRS_PORT_WI_00_CMU     ADRS_BASE_CMU + 0x00000000 // output wire [31:0]
#define ADRS_PORT_WI_01_CMU     ADRS_BASE_CMU + 0x00000010 // output wire [31:0]
#define ADRS_PORT_WI_02_CMU     ADRS_BASE_CMU + 0x00000020 // output wire [31:0]
#define ADRS_PORT_WI_03_CMU     ADRS_BASE_CMU + 0x00000030 // output wire [31:0]
#define ADRS_PORT_WI_04_CMU     ADRS_BASE_CMU + 0x00000040 // output wire [31:0]
#define ADRS_PORT_WI_05_CMU     ADRS_BASE_CMU + 0x00000050 // output wire [31:0]
#define ADRS_PORT_WI_06_CMU     ADRS_BASE_CMU + 0x00000060 // output wire [31:0]
#define ADRS_PORT_WI_07_CMU     ADRS_BASE_CMU + 0x00000070 // output wire [31:0]
#define ADRS_PORT_WI_08_CMU     ADRS_BASE_CMU + 0x00000080 // output wire [31:0]
#define ADRS_PORT_WI_09_CMU     ADRS_BASE_CMU + 0x00000090 // output wire [31:0]
#define ADRS_PORT_WI_0A_CMU     ADRS_BASE_CMU + 0x000000A0 // output wire [31:0]
#define ADRS_PORT_WI_0B_CMU     ADRS_BASE_CMU + 0x000000B0 // output wire [31:0]
#define ADRS_PORT_WI_0C_CMU     ADRS_BASE_CMU + 0x000000C0 // output wire [31:0]
#define ADRS_PORT_WI_0D_CMU     ADRS_BASE_CMU + 0x000000D0 // output wire [31:0]
#define ADRS_PORT_WI_0E_CMU     ADRS_BASE_CMU + 0x000000E0 // output wire [31:0]
#define ADRS_PORT_WI_0F_CMU     ADRS_BASE_CMU + 0x000000F0 // output wire [31:0]

#define ADRS_PORT_WI_10_CMU     ADRS_BASE_CMU + 0x00000100 // output wire [31:0]
#define ADRS_PORT_WI_11_CMU     ADRS_BASE_CMU + 0x00000110 // output wire [31:0]
#define ADRS_PORT_WI_12_CMU     ADRS_BASE_CMU + 0x00000120 // output wire [31:0]
#define ADRS_PORT_WI_13_CMU     ADRS_BASE_CMU + 0x00000130 // output wire [31:0]
#define ADRS_PORT_WI_14_CMU     ADRS_BASE_CMU + 0x00000140 // output wire [31:0]
#define ADRS_PORT_WI_15_CMU     ADRS_BASE_CMU + 0x00000150 // output wire [31:0]
#define ADRS_PORT_WI_16_CMU     ADRS_BASE_CMU + 0x00000160 // output wire [31:0]
#define ADRS_PORT_WI_17_CMU     ADRS_BASE_CMU + 0x00000170 // output wire [31:0]
#define ADRS_PORT_WI_18_CMU     ADRS_BASE_CMU + 0x00000180 // output wire [31:0]
#define ADRS_PORT_WI_19_CMU     ADRS_BASE_CMU + 0x00000190 // output wire [31:0]
#define ADRS_PORT_WI_1A_CMU     ADRS_BASE_CMU + 0x000001A0 // output wire [31:0]
#define ADRS_PORT_WI_1B_CMU     ADRS_BASE_CMU + 0x000001B0 // output wire [31:0]
#define ADRS_PORT_WI_1C_CMU     ADRS_BASE_CMU + 0x000001C0 // output wire [31:0]
#define ADRS_PORT_WI_1D_CMU     ADRS_BASE_CMU + 0x000001D0 // output wire [31:0]
#define ADRS_PORT_WI_1E_CMU     ADRS_BASE_CMU + 0x000001E0 // output wire [31:0]
#define ADRS_PORT_WI_1F_CMU     ADRS_BASE_CMU + 0x000001F0 // output wire [31:0]
//
#define ADRS_PORT_WO_20_CMU     ADRS_BASE_CMU + 0x00000200 // input wire [31:0]
#define ADRS_PORT_WO_21_CMU     ADRS_BASE_CMU + 0x00000210 // input wire [31:0]
#define ADRS_PORT_TI_40_CMU     ADRS_BASE_CMU + 0x00000400 // input wire, output wire [31:0],
#define ADRS_PORT_TI_41_CMU     ADRS_BASE_CMU + 0x00000410 // input wire, output wire [31:0],
#define ADRS_PORT_TO_60_CMU     ADRS_BASE_CMU + 0x00000600 // input wire, input wire [31:0],
#define ADRS_PORT_TO_61_CMU     ADRS_BASE_CMU + 0x00000610 // input wire, input wire [31:0],
#define ADRS_PORT_PI_80_CMU     ADRS_BASE_CMU + 0x00000800 // output wire, output wire [31:0],
#define ADRS_PORT_PI_81_CMU     ADRS_BASE_CMU + 0x00000810 // output wire, output wire [31:0],
#define ADRS_PORT_PI_8A_CMU     ADRS_BASE_CMU + 0x000008A0 // output wire, output wire [31:0],
#define ADRS_PORT_PO_A0_CMU     ADRS_BASE_CMU + 0x00000A00 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A1_CMU     ADRS_BASE_CMU + 0x00000A10 // output wire, input wire [31:0],
#define ADRS_PORT_PO_AA_CMU     ADRS_BASE_CMU + 0x00000AA0 // output wire, input wire [31:0],
//
#define ADRS_FPGA_IMAGE_CMU     ADRS_BASE_CMU + 0x00000F00 // image id
#define ADRS_TEST_REG___CMU     ADRS_BASE_CMU + 0x00000F04 // test reg 
#define ADRS_MASK_WI____CMU     ADRS_BASE_CMU + 0x00000F10 // mask
#define ADRS_MASK_WO____CMU     ADRS_BASE_CMU + 0x00000F14 // mask
#define ADRS_MASK_TI____CMU     ADRS_BASE_CMU + 0x00000F18 // mask
#define ADRS_MASK_TO____CMU     ADRS_BASE_CMU + 0x00000F1C // mask
//}

// TODO: remove// base address for PGU //{
#define ADRS_PORT_WI_00_PGU     ADRS_BASE_PGU + 0x00000000 // output wire [31:0]
#define ADRS_PORT_WI_01_PGU     ADRS_BASE_PGU + 0x00000010 // output wire [31:0]
#define ADRS_PORT_WI_02_PGU     ADRS_BASE_PGU + 0x00000020 // output wire [31:0]
#define ADRS_PORT_WI_03_PGU     ADRS_BASE_PGU + 0x00000030 // output wire [31:0]
#define ADRS_PORT_WI_04_PGU     ADRS_BASE_PGU + 0x00000040 // output wire [31:0]
#define ADRS_PORT_WI_05_PGU     ADRS_BASE_PGU + 0x00000050 // output wire [31:0]
#define ADRS_PORT_WI_06_PGU     ADRS_BASE_PGU + 0x00000060 // output wire [31:0]
#define ADRS_PORT_WI_07_PGU     ADRS_BASE_PGU + 0x00000070 // output wire [31:0]
#define ADRS_PORT_WI_08_PGU     ADRS_BASE_PGU + 0x00000080 // output wire [31:0]
#define ADRS_PORT_WI_09_PGU     ADRS_BASE_PGU + 0x00000090 // output wire [31:0]
#define ADRS_PORT_WI_0A_PGU     ADRS_BASE_PGU + 0x000000A0 // output wire [31:0]
#define ADRS_PORT_WI_0B_PGU     ADRS_BASE_PGU + 0x000000B0 // output wire [31:0]
#define ADRS_PORT_WI_0C_PGU     ADRS_BASE_PGU + 0x000000C0 // output wire [31:0]
#define ADRS_PORT_WI_0D_PGU     ADRS_BASE_PGU + 0x000000D0 // output wire [31:0]
#define ADRS_PORT_WI_0E_PGU     ADRS_BASE_PGU + 0x000000E0 // output wire [31:0]
#define ADRS_PORT_WI_0F_PGU     ADRS_BASE_PGU + 0x000000F0 // output wire [31:0]
//
#define ADRS_PORT_WI_10_PGU     ADRS_BASE_PGU + 0x00000100 // output wire [31:0]
#define ADRS_PORT_WI_11_PGU     ADRS_BASE_PGU + 0x00000110 // output wire [31:0]
#define ADRS_PORT_WI_12_PGU     ADRS_BASE_PGU + 0x00000120 // output wire [31:0]
#define ADRS_PORT_WI_13_PGU     ADRS_BASE_PGU + 0x00000130 // output wire [31:0]
#define ADRS_PORT_WI_14_PGU     ADRS_BASE_PGU + 0x00000140 // output wire [31:0]
#define ADRS_PORT_WI_15_PGU     ADRS_BASE_PGU + 0x00000150 // output wire [31:0]
#define ADRS_PORT_WI_16_PGU     ADRS_BASE_PGU + 0x00000160 // output wire [31:0]
#define ADRS_PORT_WI_17_PGU     ADRS_BASE_PGU + 0x00000170 // output wire [31:0]
#define ADRS_PORT_WI_18_PGU     ADRS_BASE_PGU + 0x00000180 // output wire [31:0]
#define ADRS_PORT_WI_19_PGU     ADRS_BASE_PGU + 0x00000190 // output wire [31:0]
#define ADRS_PORT_WI_1A_PGU     ADRS_BASE_PGU + 0x000001A0 // output wire [31:0]
#define ADRS_PORT_WI_1B_PGU     ADRS_BASE_PGU + 0x000001B0 // output wire [31:0]
#define ADRS_PORT_WI_1C_PGU     ADRS_BASE_PGU + 0x000001C0 // output wire [31:0]
#define ADRS_PORT_WI_1D_PGU     ADRS_BASE_PGU + 0x000001D0 // output wire [31:0]
#define ADRS_PORT_WI_1E_PGU     ADRS_BASE_PGU + 0x000001E0 // output wire [31:0]
#define ADRS_PORT_WI_1F_PGU     ADRS_BASE_PGU + 0x000001F0 // output wire [31:0]
//
#define ADRS_PORT_WO_20_PGU     ADRS_BASE_PGU + 0x00000200 // input wire [31:0]
#define ADRS_PORT_WO_21_PGU     ADRS_BASE_PGU + 0x00000210 // input wire [31:0]
#define ADRS_PORT_TI_40_PGU     ADRS_BASE_PGU + 0x00000400 // input wire, output wire [31:0],
#define ADRS_PORT_TI_41_PGU     ADRS_BASE_PGU + 0x00000410 // input wire, output wire [31:0],
#define ADRS_PORT_TO_60_PGU     ADRS_BASE_PGU + 0x00000600 // input wire, input wire [31:0],
#define ADRS_PORT_TO_61_PGU     ADRS_BASE_PGU + 0x00000610 // input wire, input wire [31:0],
#define ADRS_PORT_PI_80_PGU     ADRS_BASE_PGU + 0x00000800 // output wire, output wire [31:0],
#define ADRS_PORT_PI_81_PGU     ADRS_BASE_PGU + 0x00000810 // output wire, output wire [31:0],
#define ADRS_PORT_PI_8A_PGU     ADRS_BASE_PGU + 0x000008A0 // output wire, output wire [31:0],
#define ADRS_PORT_PO_A0_PGU     ADRS_BASE_PGU + 0x00000A00 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A1_PGU     ADRS_BASE_PGU + 0x00000A10 // output wire, input wire [31:0],
#define ADRS_PORT_PO_AA_PGU     ADRS_BASE_PGU + 0x00000AA0 // output wire, input wire [31:0],
//
#define ADRS_FPGA_IMAGE_PGU     ADRS_BASE_PGU + 0x00000F00 // image id
#define ADRS_TEST_REG___PGU     ADRS_BASE_PGU + 0x00000F04 // test reg 
#define ADRS_MASK_WI____PGU     ADRS_BASE_PGU + 0x00000F10 // mask
#define ADRS_MASK_WO____PGU     ADRS_BASE_PGU + 0x00000F14 // mask
#define ADRS_MASK_TI____PGU     ADRS_BASE_PGU + 0x00000F18 // mask
#define ADRS_MASK_TO____PGU     ADRS_BASE_PGU + 0x00000F1C // mask
//}

// base for  ADRS_BASE_MHVSU //{
#define ADRS_PORT_WI_00_MHVSU     ADRS_BASE_MHVSU + 0x00000000 // output wire [31:0]
#define ADRS_PORT_WI_01_MHVSU     ADRS_BASE_MHVSU + 0x00000010 // output wire [31:0]
#define ADRS_PORT_WI_02_MHVSU     ADRS_BASE_MHVSU + 0x00000020 // output wire [31:0]
#define ADRS_PORT_WI_03_MHVSU     ADRS_BASE_MHVSU + 0x00000030 // output wire [31:0]
#define ADRS_PORT_WI_04_MHVSU     ADRS_BASE_MHVSU + 0x00000040 // output wire [31:0]
#define ADRS_PORT_WI_05_MHVSU     ADRS_BASE_MHVSU + 0x00000050 // output wire [31:0]
#define ADRS_PORT_WI_06_MHVSU     ADRS_BASE_MHVSU + 0x00000060 // output wire [31:0]
#define ADRS_PORT_WI_07_MHVSU     ADRS_BASE_MHVSU + 0x00000070 // output wire [31:0]
#define ADRS_PORT_WI_08_MHVSU     ADRS_BASE_MHVSU + 0x00000080 // output wire [31:0]
#define ADRS_PORT_WI_09_MHVSU     ADRS_BASE_MHVSU + 0x00000090 // output wire [31:0]
#define ADRS_PORT_WI_0A_MHVSU     ADRS_BASE_MHVSU + 0x000000A0 // output wire [31:0]
#define ADRS_PORT_WI_0B_MHVSU     ADRS_BASE_MHVSU + 0x000000B0 // output wire [31:0]
#define ADRS_PORT_WI_0C_MHVSU     ADRS_BASE_MHVSU + 0x000000C0 // output wire [31:0]
#define ADRS_PORT_WI_0D_MHVSU     ADRS_BASE_MHVSU + 0x000000D0 // output wire [31:0]
#define ADRS_PORT_WI_0E_MHVSU     ADRS_BASE_MHVSU + 0x000000E0 // output wire [31:0]
#define ADRS_PORT_WI_0F_MHVSU     ADRS_BASE_MHVSU + 0x000000F0 // output wire [31:0]
#define ADRS_PORT_WI_10_MHVSU     ADRS_BASE_MHVSU + 0x00000100 // output wire [31:0]
#define ADRS_PORT_WI_11_MHVSU     ADRS_BASE_MHVSU + 0x00000110 // output wire [31:0]
#define ADRS_PORT_WI_12_MHVSU     ADRS_BASE_MHVSU + 0x00000120 // output wire [31:0]
#define ADRS_PORT_WI_13_MHVSU     ADRS_BASE_MHVSU + 0x00000130 // output wire [31:0]
#define ADRS_PORT_WI_14_MHVSU     ADRS_BASE_MHVSU + 0x00000140 // output wire [31:0]
#define ADRS_PORT_WI_15_MHVSU     ADRS_BASE_MHVSU + 0x00000150 // output wire [31:0]
#define ADRS_PORT_WI_16_MHVSU     ADRS_BASE_MHVSU + 0x00000160 // output wire [31:0]
#define ADRS_PORT_WI_17_MHVSU     ADRS_BASE_MHVSU + 0x00000170 // output wire [31:0]
#define ADRS_PORT_WI_18_MHVSU     ADRS_BASE_MHVSU + 0x00000180 // output wire [31:0]
#define ADRS_PORT_WI_19_MHVSU     ADRS_BASE_MHVSU + 0x00000190 // output wire [31:0]
#define ADRS_PORT_WI_1A_MHVSU     ADRS_BASE_MHVSU + 0x000001A0 // output wire [31:0]
#define ADRS_PORT_WI_1B_MHVSU     ADRS_BASE_MHVSU + 0x000001B0 // output wire [31:0]
#define ADRS_PORT_WI_1C_MHVSU     ADRS_BASE_MHVSU + 0x000001C0 // output wire [31:0]
#define ADRS_PORT_WI_1D_MHVSU     ADRS_BASE_MHVSU + 0x000001D0 // output wire [31:0]
#define ADRS_PORT_WI_1E_MHVSU     ADRS_BASE_MHVSU + 0x000001E0 // output wire [31:0]
#define ADRS_PORT_WI_1F_MHVSU     ADRS_BASE_MHVSU + 0x000001F0 // output wire [31:0]
//
#define ADRS_PORT_WO_20_MHVSU     ADRS_BASE_MHVSU + 0x00000200 // input wire [31:0]
#define ADRS_PORT_WO_21_MHVSU     ADRS_BASE_MHVSU + 0x00000210 // input wire [31:0]
#define ADRS_PORT_WO_22_MHVSU     ADRS_BASE_MHVSU + 0x00000220 // input wire [31:0]
#define ADRS_PORT_WO_23_MHVSU     ADRS_BASE_MHVSU + 0x00000230 // input wire [31:0]
#define ADRS_PORT_WO_24_MHVSU     ADRS_BASE_MHVSU + 0x00000240 // input wire [31:0]
#define ADRS_PORT_WO_25_MHVSU     ADRS_BASE_MHVSU + 0x00000250 // input wire [31:0]
#define ADRS_PORT_WO_26_MHVSU     ADRS_BASE_MHVSU + 0x00000260 // input wire [31:0]
#define ADRS_PORT_WO_27_MHVSU     ADRS_BASE_MHVSU + 0x00000270 // input wire [31:0]
#define ADRS_PORT_WO_28_MHVSU     ADRS_BASE_MHVSU + 0x00000280 // input wire [31:0]
#define ADRS_PORT_WO_29_MHVSU     ADRS_BASE_MHVSU + 0x00000290 // input wire [31:0]
#define ADRS_PORT_WO_2A_MHVSU     ADRS_BASE_MHVSU + 0x000002A0 // input wire [31:0]
#define ADRS_PORT_WO_2B_MHVSU     ADRS_BASE_MHVSU + 0x000002B0 // input wire [31:0]
#define ADRS_PORT_WO_2C_MHVSU     ADRS_BASE_MHVSU + 0x000002C0 // input wire [31:0]
#define ADRS_PORT_WO_2D_MHVSU     ADRS_BASE_MHVSU + 0x000002D0 // input wire [31:0]
#define ADRS_PORT_WO_2E_MHVSU     ADRS_BASE_MHVSU + 0x000002E0 // input wire [31:0]
#define ADRS_PORT_WO_2F_MHVSU     ADRS_BASE_MHVSU + 0x000002F0 // input wire [31:0]
#define ADRS_PORT_WO_30_MHVSU     ADRS_BASE_MHVSU + 0x00000300 // input wire [31:0]
#define ADRS_PORT_WO_31_MHVSU     ADRS_BASE_MHVSU + 0x00000310 // input wire [31:0]
#define ADRS_PORT_WO_32_MHVSU     ADRS_BASE_MHVSU + 0x00000320 // input wire [31:0]
#define ADRS_PORT_WO_33_MHVSU     ADRS_BASE_MHVSU + 0x00000330 // input wire [31:0]
#define ADRS_PORT_WO_34_MHVSU     ADRS_BASE_MHVSU + 0x00000340 // input wire [31:0]
#define ADRS_PORT_WO_35_MHVSU     ADRS_BASE_MHVSU + 0x00000350 // input wire [31:0]
#define ADRS_PORT_WO_36_MHVSU     ADRS_BASE_MHVSU + 0x00000360 // input wire [31:0]
#define ADRS_PORT_WO_37_MHVSU     ADRS_BASE_MHVSU + 0x00000370 // input wire [31:0]
#define ADRS_PORT_WO_38_MHVSU     ADRS_BASE_MHVSU + 0x00000380 // input wire [31:0]
#define ADRS_PORT_WO_39_MHVSU     ADRS_BASE_MHVSU + 0x00000390 // input wire [31:0]
#define ADRS_PORT_WO_3A_MHVSU     ADRS_BASE_MHVSU + 0x000003A0 // input wire [31:0]
#define ADRS_PORT_WO_3B_MHVSU     ADRS_BASE_MHVSU + 0x000003B0 // input wire [31:0]
#define ADRS_PORT_WO_3C_MHVSU     ADRS_BASE_MHVSU + 0x000003C0 // input wire [31:0]
#define ADRS_PORT_WO_3D_MHVSU     ADRS_BASE_MHVSU + 0x000003D0 // input wire [31:0]
#define ADRS_PORT_WO_3E_MHVSU     ADRS_BASE_MHVSU + 0x000003E0 // input wire [31:0]
#define ADRS_PORT_WO_3F_MHVSU     ADRS_BASE_MHVSU + 0x000003F0 // input wire [31:0]
//
#define ADRS_PORT_TI_40_MHVSU     ADRS_BASE_MHVSU + 0x00000400 // input wire, output wire [31:0],
#define ADRS_PORT_TI_41_MHVSU     ADRS_BASE_MHVSU + 0x00000410 // input wire, output wire [31:0],
#define ADRS_PORT_TI_42_MHVSU     ADRS_BASE_MHVSU + 0x00000420 // input wire, output wire [31:0],
#define ADRS_PORT_TI_43_MHVSU     ADRS_BASE_MHVSU + 0x00000430 // input wire, output wire [31:0],
#define ADRS_PORT_TI_44_MHVSU     ADRS_BASE_MHVSU + 0x00000440 // input wire, output wire [31:0],
#define ADRS_PORT_TI_45_MHVSU     ADRS_BASE_MHVSU + 0x00000450 // input wire, output wire [31:0],
#define ADRS_PORT_TI_46_MHVSU     ADRS_BASE_MHVSU + 0x00000460 // input wire, output wire [31:0],
#define ADRS_PORT_TI_47_MHVSU     ADRS_BASE_MHVSU + 0x00000470 // input wire, output wire [31:0],
#define ADRS_PORT_TI_48_MHVSU     ADRS_BASE_MHVSU + 0x00000480 // input wire, output wire [31:0],
#define ADRS_PORT_TI_49_MHVSU     ADRS_BASE_MHVSU + 0x00000490 // input wire, output wire [31:0],
#define ADRS_PORT_TI_4A_MHVSU     ADRS_BASE_MHVSU + 0x000004A0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_4B_MHVSU     ADRS_BASE_MHVSU + 0x000004B0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_4C_MHVSU     ADRS_BASE_MHVSU + 0x000004C0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_4D_MHVSU     ADRS_BASE_MHVSU + 0x000004D0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_4E_MHVSU     ADRS_BASE_MHVSU + 0x000004E0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_4F_MHVSU     ADRS_BASE_MHVSU + 0x000004F0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_50_MHVSU     ADRS_BASE_MHVSU + 0x00000500 // input wire, output wire [31:0],
#define ADRS_PORT_TI_51_MHVSU     ADRS_BASE_MHVSU + 0x00000510 // input wire, output wire [31:0],
#define ADRS_PORT_TI_52_MHVSU     ADRS_BASE_MHVSU + 0x00000520 // input wire, output wire [31:0],
#define ADRS_PORT_TI_53_MHVSU     ADRS_BASE_MHVSU + 0x00000530 // input wire, output wire [31:0],
#define ADRS_PORT_TI_54_MHVSU     ADRS_BASE_MHVSU + 0x00000540 // input wire, output wire [31:0],
#define ADRS_PORT_TI_55_MHVSU     ADRS_BASE_MHVSU + 0x00000550 // input wire, output wire [31:0],
#define ADRS_PORT_TI_56_MHVSU     ADRS_BASE_MHVSU + 0x00000560 // input wire, output wire [31:0],
#define ADRS_PORT_TI_57_MHVSU     ADRS_BASE_MHVSU + 0x00000570 // input wire, output wire [31:0],
#define ADRS_PORT_TI_58_MHVSU     ADRS_BASE_MHVSU + 0x00000580 // input wire, output wire [31:0],
#define ADRS_PORT_TI_59_MHVSU     ADRS_BASE_MHVSU + 0x00000590 // input wire, output wire [31:0],
#define ADRS_PORT_TI_5A_MHVSU     ADRS_BASE_MHVSU + 0x000005A0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_5B_MHVSU     ADRS_BASE_MHVSU + 0x000005B0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_5C_MHVSU     ADRS_BASE_MHVSU + 0x000005C0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_5D_MHVSU     ADRS_BASE_MHVSU + 0x000005D0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_5E_MHVSU     ADRS_BASE_MHVSU + 0x000005E0 // input wire, output wire [31:0],
#define ADRS_PORT_TI_5F_MHVSU     ADRS_BASE_MHVSU + 0x000005F0 // input wire, output wire [31:0],
//
#define ADRS_PORT_TO_60_MHVSU     ADRS_BASE_MHVSU + 0x00000600 // input wire, input wire [31:0],
#define ADRS_PORT_TO_61_MHVSU     ADRS_BASE_MHVSU + 0x00000610 // input wire, input wire [31:0],
#define ADRS_PORT_TO_62_MHVSU     ADRS_BASE_MHVSU + 0x00000620 // input wire, input wire [31:0],
#define ADRS_PORT_TO_63_MHVSU     ADRS_BASE_MHVSU + 0x00000630 // input wire, input wire [31:0],
#define ADRS_PORT_TO_64_MHVSU     ADRS_BASE_MHVSU + 0x00000640 // input wire, input wire [31:0],
#define ADRS_PORT_TO_65_MHVSU     ADRS_BASE_MHVSU + 0x00000650 // input wire, input wire [31:0],
#define ADRS_PORT_TO_66_MHVSU     ADRS_BASE_MHVSU + 0x00000660 // input wire, input wire [31:0],
#define ADRS_PORT_TO_67_MHVSU     ADRS_BASE_MHVSU + 0x00000670 // input wire, input wire [31:0],
#define ADRS_PORT_TO_68_MHVSU     ADRS_BASE_MHVSU + 0x00000680 // input wire, input wire [31:0],
#define ADRS_PORT_TO_69_MHVSU     ADRS_BASE_MHVSU + 0x00000690 // input wire, input wire [31:0],
#define ADRS_PORT_TO_6A_MHVSU     ADRS_BASE_MHVSU + 0x000006A0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_6B_MHVSU     ADRS_BASE_MHVSU + 0x000006B0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_6C_MHVSU     ADRS_BASE_MHVSU + 0x000006C0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_6D_MHVSU     ADRS_BASE_MHVSU + 0x000006D0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_6E_MHVSU     ADRS_BASE_MHVSU + 0x000006E0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_6F_MHVSU     ADRS_BASE_MHVSU + 0x000006F0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_70_MHVSU     ADRS_BASE_MHVSU + 0x00000700 // input wire, input wire [31:0],
#define ADRS_PORT_TO_71_MHVSU     ADRS_BASE_MHVSU + 0x00000710 // input wire, input wire [31:0],
#define ADRS_PORT_TO_72_MHVSU     ADRS_BASE_MHVSU + 0x00000720 // input wire, input wire [31:0],
#define ADRS_PORT_TO_73_MHVSU     ADRS_BASE_MHVSU + 0x00000730 // input wire, input wire [31:0],
#define ADRS_PORT_TO_74_MHVSU     ADRS_BASE_MHVSU + 0x00000740 // input wire, input wire [31:0],
#define ADRS_PORT_TO_75_MHVSU     ADRS_BASE_MHVSU + 0x00000750 // input wire, input wire [31:0],
#define ADRS_PORT_TO_76_MHVSU     ADRS_BASE_MHVSU + 0x00000760 // input wire, input wire [31:0],
#define ADRS_PORT_TO_77_MHVSU     ADRS_BASE_MHVSU + 0x00000770 // input wire, input wire [31:0],
#define ADRS_PORT_TO_78_MHVSU     ADRS_BASE_MHVSU + 0x00000780 // input wire, input wire [31:0],
#define ADRS_PORT_TO_79_MHVSU     ADRS_BASE_MHVSU + 0x00000790 // input wire, input wire [31:0],
#define ADRS_PORT_TO_7A_MHVSU     ADRS_BASE_MHVSU + 0x000007A0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_7B_MHVSU     ADRS_BASE_MHVSU + 0x000007B0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_7C_MHVSU     ADRS_BASE_MHVSU + 0x000007C0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_7D_MHVSU     ADRS_BASE_MHVSU + 0x000007D0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_7E_MHVSU     ADRS_BASE_MHVSU + 0x000007E0 // input wire, input wire [31:0],
#define ADRS_PORT_TO_7F_MHVSU     ADRS_BASE_MHVSU + 0x000007F0 // input wire, input wire [31:0],
//
#define ADRS_PORT_PI_80_MHVSU     ADRS_BASE_MHVSU + 0x00000800 // output wire, output wire [31:0],
#define ADRS_PORT_PI_81_MHVSU     ADRS_BASE_MHVSU + 0x00000810 // output wire, output wire [31:0],
#define ADRS_PORT_PI_82_MHVSU     ADRS_BASE_MHVSU + 0x00000820 // output wire, output wire [31:0],
#define ADRS_PORT_PI_83_MHVSU     ADRS_BASE_MHVSU + 0x00000830 // output wire, output wire [31:0],
#define ADRS_PORT_PI_84_MHVSU     ADRS_BASE_MHVSU + 0x00000840 // output wire, output wire [31:0],
#define ADRS_PORT_PI_85_MHVSU     ADRS_BASE_MHVSU + 0x00000850 // output wire, output wire [31:0],
#define ADRS_PORT_PI_86_MHVSU     ADRS_BASE_MHVSU + 0x00000860 // output wire, output wire [31:0],
#define ADRS_PORT_PI_87_MHVSU     ADRS_BASE_MHVSU + 0x00000870 // output wire, output wire [31:0],
#define ADRS_PORT_PI_88_MHVSU     ADRS_BASE_MHVSU + 0x00000880 // output wire, output wire [31:0],
#define ADRS_PORT_PI_89_MHVSU     ADRS_BASE_MHVSU + 0x00000890 // output wire, output wire [31:0],
#define ADRS_PORT_PI_8A_MHVSU     ADRS_BASE_MHVSU + 0x000008A0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_8B_MHVSU     ADRS_BASE_MHVSU + 0x000008B0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_8C_MHVSU     ADRS_BASE_MHVSU + 0x000008C0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_8D_MHVSU     ADRS_BASE_MHVSU + 0x000008D0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_8E_MHVSU     ADRS_BASE_MHVSU + 0x000008E0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_8F_MHVSU     ADRS_BASE_MHVSU + 0x000008F0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_90_MHVSU     ADRS_BASE_MHVSU + 0x00000900 // output wire, output wire [31:0],
#define ADRS_PORT_PI_91_MHVSU     ADRS_BASE_MHVSU + 0x00000910 // output wire, output wire [31:0],
#define ADRS_PORT_PI_92_MHVSU     ADRS_BASE_MHVSU + 0x00000920 // output wire, output wire [31:0],
#define ADRS_PORT_PI_93_MHVSU     ADRS_BASE_MHVSU + 0x00000930 // output wire, output wire [31:0],
#define ADRS_PORT_PI_94_MHVSU     ADRS_BASE_MHVSU + 0x00000940 // output wire, output wire [31:0],
#define ADRS_PORT_PI_95_MHVSU     ADRS_BASE_MHVSU + 0x00000950 // output wire, output wire [31:0],
#define ADRS_PORT_PI_96_MHVSU     ADRS_BASE_MHVSU + 0x00000960 // output wire, output wire [31:0],
#define ADRS_PORT_PI_97_MHVSU     ADRS_BASE_MHVSU + 0x00000970 // output wire, output wire [31:0],
#define ADRS_PORT_PI_98_MHVSU     ADRS_BASE_MHVSU + 0x00000980 // output wire, output wire [31:0],
#define ADRS_PORT_PI_99_MHVSU     ADRS_BASE_MHVSU + 0x00000990 // output wire, output wire [31:0],
#define ADRS_PORT_PI_9A_MHVSU     ADRS_BASE_MHVSU + 0x000009A0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_9B_MHVSU     ADRS_BASE_MHVSU + 0x000009B0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_9C_MHVSU     ADRS_BASE_MHVSU + 0x000009C0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_9D_MHVSU     ADRS_BASE_MHVSU + 0x000009D0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_9E_MHVSU     ADRS_BASE_MHVSU + 0x000009E0 // output wire, output wire [31:0],
#define ADRS_PORT_PI_9F_MHVSU     ADRS_BASE_MHVSU + 0x000009F0 // output wire, output wire [31:0],
//
#define ADRS_PORT_PO_A0_MHVSU     ADRS_BASE_MHVSU + 0x00000A00 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A1_MHVSU     ADRS_BASE_MHVSU + 0x00000A10 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A2_MHVSU     ADRS_BASE_MHVSU + 0x00000A20 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A3_MHVSU     ADRS_BASE_MHVSU + 0x00000A30 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A4_MHVSU     ADRS_BASE_MHVSU + 0x00000A40 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A5_MHVSU     ADRS_BASE_MHVSU + 0x00000A50 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A6_MHVSU     ADRS_BASE_MHVSU + 0x00000A60 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A7_MHVSU     ADRS_BASE_MHVSU + 0x00000A70 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A8_MHVSU     ADRS_BASE_MHVSU + 0x00000A80 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A9_MHVSU     ADRS_BASE_MHVSU + 0x00000A90 // output wire, input wire [31:0],
#define ADRS_PORT_PO_AA_MHVSU     ADRS_BASE_MHVSU + 0x00000AA0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_AB_MHVSU     ADRS_BASE_MHVSU + 0x00000AB0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_AC_MHVSU     ADRS_BASE_MHVSU + 0x00000AC0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_AD_MHVSU     ADRS_BASE_MHVSU + 0x00000AD0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_AE_MHVSU     ADRS_BASE_MHVSU + 0x00000AE0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_AF_MHVSU     ADRS_BASE_MHVSU + 0x00000AF0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_B0_MHVSU     ADRS_BASE_MHVSU + 0x00000B00 // output wire, input wire [31:0],
#define ADRS_PORT_PO_B1_MHVSU     ADRS_BASE_MHVSU + 0x00000B10 // output wire, input wire [31:0],
#define ADRS_PORT_PO_B2_MHVSU     ADRS_BASE_MHVSU + 0x00000B20 // output wire, input wire [31:0],
#define ADRS_PORT_PO_B3_MHVSU     ADRS_BASE_MHVSU + 0x00000B30 // output wire, input wire [31:0],
#define ADRS_PORT_PO_B4_MHVSU     ADRS_BASE_MHVSU + 0x00000B40 // output wire, input wire [31:0],
#define ADRS_PORT_PO_B5_MHVSU     ADRS_BASE_MHVSU + 0x00000B50 // output wire, input wire [31:0],
#define ADRS_PORT_PO_B6_MHVSU     ADRS_BASE_MHVSU + 0x00000B60 // output wire, input wire [31:0],
#define ADRS_PORT_PO_B7_MHVSU     ADRS_BASE_MHVSU + 0x00000B70 // output wire, input wire [31:0],
#define ADRS_PORT_PO_B8_MHVSU     ADRS_BASE_MHVSU + 0x00000B80 // output wire, input wire [31:0],
#define ADRS_PORT_PO_B9_MHVSU     ADRS_BASE_MHVSU + 0x00000B90 // output wire, input wire [31:0],
#define ADRS_PORT_PO_BA_MHVSU     ADRS_BASE_MHVSU + 0x00000BA0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_BB_MHVSU     ADRS_BASE_MHVSU + 0x00000BB0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_BC_MHVSU     ADRS_BASE_MHVSU + 0x00000BC0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_BD_MHVSU     ADRS_BASE_MHVSU + 0x00000BD0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_BE_MHVSU     ADRS_BASE_MHVSU + 0x00000BE0 // output wire, input wire [31:0],
#define ADRS_PORT_PO_BF_MHVSU     ADRS_BASE_MHVSU + 0x00000BF0 // output wire, input wire [31:0],
//
// dedicated lan interface:
//
// 'b_0000_XXHH_hhxx_xx00
//
//         1100 (C)
//         1101 (D)
//         1110 (E)
//         
// 'b_0000_1100_00xx_xx00 -- LAN_WI_00
// 'b_0000_1100_01xx_xx00 -- LAN_WI_10
// 'b_0000_1100_10xx_xx00 -- LAN_WO_20
// 'b_0000_1100_11xx_xx00 -- LAN_WO_30
// 'b_0000_1101_00xx_xx00 -- LAN_TI_40
// 'b_0000_1101_01xx_xx00 -- LAN_TI_50
// 'b_0000_1101_10xx_xx00 -- LAN_TO_60
// 'b_0000_1101_11xx_xx00 -- LAN_TO_70
// 'b_0000_1110_00xx_xx00 -- LAN_PI_80
// 'b_0000_1110_01xx_xx00 -- LAN_PI_90
// 'b_0000_1110_10xx_xx00 -- LAN_PO_A0
// 'b_0000_1110_11xx_xx00 -- LAN_PO_B0
//
#define ADRS_LAN_WI_00_MHVSU      ADRS_BASE_MHVSU + 0x00000C00 // output wire [31:0]
#define ADRS_LAN_WI_01_MHVSU      ADRS_BASE_MHVSU + 0x00000C04 // output wire [31:0]
#define ADRS_LAN_WO_20_MHVSU      ADRS_BASE_MHVSU + 0x00000C80 // input wire [31:0]
#define ADRS_LAN_WO_21_MHVSU      ADRS_BASE_MHVSU + 0x00000C84 // input wire [31:0]
#define ADRS_LAN_TI_40_MHVSU      ADRS_BASE_MHVSU + 0x00000D00 // input wire, output wire [31:0],
#define ADRS_LAN_TI_41_MHVSU      ADRS_BASE_MHVSU + 0x00000D04 // input wire, output wire [31:0],
#define ADRS_LAN_TO_60_MHVSU      ADRS_BASE_MHVSU + 0x00000D80 // input wire, input wire [31:0],
#define ADRS_LAN_TO_61_MHVSU      ADRS_BASE_MHVSU + 0x00000D84 // input wire, input wire [31:0],
#define ADRS_LAN_PI_80_MHVSU      ADRS_BASE_MHVSU + 0x00000E00 // output wire, output wire [31:0],
#define ADRS_LAN_PI_81_MHVSU      ADRS_BASE_MHVSU + 0x00000E04 // output wire, output wire [31:0],
#define ADRS_LAN_PO_A0_MHVSU      ADRS_BASE_MHVSU + 0x00000E80 // output wire, input wire [31:0],
#define ADRS_LAN_PO_A1_MHVSU      ADRS_BASE_MHVSU + 0x00000E84 // output wire, input wire [31:0],
//
#define ADRS_FPGA_IMAGE_MHVSU     ADRS_BASE_MHVSU + 0x00000F00 // image id
#define ADRS_TEST_REG___MHVSU     ADRS_BASE_MHVSU + 0x00000F04 // test reg 
#define ADRS_MASK_ALL___MHVSU     ADRS_BASE_MHVSU + 0x00000F08 // mask all //$$
#define ADRS_MASK_WI____MHVSU     ADRS_BASE_MHVSU + 0x00000F10 // mask for port IO
#define ADRS_MASK_WO____MHVSU     ADRS_BASE_MHVSU + 0x00000F14 // mask for port IO
#define ADRS_MASK_TI____MHVSU     ADRS_BASE_MHVSU + 0x00000F18 // mask for port IO
#define ADRS_MASK_TO____MHVSU     ADRS_BASE_MHVSU + 0x00000F1C // mask for port IO
//
#define ADRS_LAN_CONF_00_MHVSU    ADRS_BASE_MHVSU + 0x00000FC0 //input  wire [31:0]   i_lan_conf_00  // (BASE_ADRS_IP_32B  + i_adrs_offset_ip_32b )
#define ADRS_LAN_CONF_01_MHVSU    ADRS_BASE_MHVSU + 0x00000FC4 //input  wire [31:0]   i_lan_conf_01  // (BASE_ADRS_MAC_48B[31: 0] + i_adrs_offset_mac_48b[31: 0] )
#define ADRS_LAN_CONF_02_MHVSU    ADRS_BASE_MHVSU + 0x00000FC8 //input  wire [31:0]   i_lan_conf_02  // ( {16'b0,BASE_ADRS_MAC_48B[47:32]} + {16'b0,i_adrs_offset_mac_48b[47:32]} )
#define ADRS_LAN_CONF_03_MHVSU    ADRS_BASE_MHVSU + 0x00000FCC //input  wire [31:0]   i_lan_conf_03  // ( i_lan_timeout_rtr_16b , i_lan_timeout_rcr_16b )
	
//}


// parameters common //{
#define MASK_ALL                      0xFFFFFFFF
//}

// TODO: remove// CMU parameters //{

#define CMU_PAR__OFF                  0x00000000
#define CMU_PAR__ON                   0x00000001
#define CMU_PAR__RESET                0x00000002
#define CMU_PAR__GN_OFF               0x00000010
#define CMU_PAR__GN_1X                0x00000011
#define CMU_PAR__GN_10X               0x00000012
#define CMU_PAR__GN_100X              0x00000013
#define CMU_PAR__BW_OFF               0x00000020
#define CMU_PAR__BW_120K              0x00000021
#define CMU_PAR__BW_1M2               0x00000022
#define CMU_PAR__BW_12M               0x00000023
#define CMU_PAR__BW_120M              0x00000024

//}

// TODO: remove// PGU parameters //{

#define PGU_PAR__OFF                  0x00000000
#define PGU_PAR__ON                   0x00000001
	
//}


// TODO: remove// CMU end points adrs //{
//
#define EP_ADRS__board_name           "CMU-CPU-F5500"
#define EP_ADRS__ver                  0xF3190306
#define EP_ADRS__bit_filename         "xem7310__cmu_cpu__top__F3_19_0306.bit"
//
//wire-in
#define EP_ADRS__SW_BUILD_ID          0x00
#define EP_ADRS__TEST_CON             0x01
#define EP_ADRS__TEST_CC_DIN          0x02
#define EP_ADRS__wi03                 0x03
#define EP_ADRS__DAC_TEST_IN          0x04
#define EP_ADRS__DWAVE_DIN_BY_TRIG    0x05
#define EP_ADRS__DWAVE_CON            0x06
#define EP_ADRS__SPO_CON              0x07
#define EP_ADRS__SPO_DIN_B0_L         0x08
#define EP_ADRS__SPO_DIN_B0_H         0x09
#define EP_ADRS__SPO_DIN_B1_L         0x0A
#define EP_ADRS__SPO_DIN_B1_H         0x0B
#define EP_ADRS__SPO_DIN_B2_L         0x0C
#define EP_ADRS__SPO_DIN_B2_H         0x0D
#define EP_ADRS__SPO_DIN_B3_L         0x0E
#define EP_ADRS__SPO_DIN_B3_H         0x0F
#define EP_ADRS__DAC_A2A3_CON         0x10
#define EP_ADRS__DAC_BIAS_CON         0x11
#define EP_ADRS__wi12                 0x12
#define EP_ADRS__wi13                 0x13
#define EP_ADRS__DAC_A2A3_DIN21       0x14
#define EP_ADRS__DAC_A2A3_DIN43       0x15
#define EP_ADRS__DAC_BIAS_DIN21       0x16
#define EP_ADRS__DAC_BIAS_DIN43       0x17
#define EP_ADRS__ADC_HS_WI            0x18
#define EP_ADRS__wi19                 0x19
#define EP_ADRS__wi1A                 0x1A
#define EP_ADRS__wi1B                 0x1B
#define EP_ADRS__wi1C                 0x1C
#define EP_ADRS__ADC_HS_UPD_SMP       0x1D
#define EP_ADRS__ADC_HS_SMP_PRD       0x1E
#define EP_ADRS__ADC_HS_DLY_TAP_OPT   0x1F
//wire-out           
#define EP_ADRS__FPGA_IMAGE_ID        0x20
#define EP_ADRS__TEST_OUT             0x21
#define EP_ADRS__TEST_CC_MON          0x22
#define EP_ADRS__DWAVE_BASE_FREQ      0x23
#define EP_ADRS__DAC_TEST_OUT         0x24
#define EP_ADRS__DWAVE_DOUT_BY_TRIG   0x25
#define EP_ADRS__DWAVE_FLAG           0x26
#define EP_ADRS__SPO_FLAG             0x27
#define EP_ADRS__SPO_MON_B0_L         0x28
#define EP_ADRS__SPO_MON_B0_H         0x29
#define EP_ADRS__SPO_MON_B1_L         0x2A
#define EP_ADRS__SPO_MON_B1_H         0x2B
#define EP_ADRS__SPO_MON_B2_L         0x2C
#define EP_ADRS__SPO_MON_B2_H         0x2D
#define EP_ADRS__SPO_MON_B3_L         0x2E
#define EP_ADRS__SPO_MON_B3_H         0x2F
#define EP_ADRS__DAC_A2A3_FLAG        0x30
#define EP_ADRS__DAC_BIAS_FLAG        0x31
#define EP_ADRS__DAC_TEST_RB1         0x32
#define EP_ADRS__DAC_TEST_RB2         0x33
#define EP_ADRS__DAC_A2A3_RB21        0x34
#define EP_ADRS__DAC_A2A3_RB43        0x35
#define EP_ADRS__DAC_BIAS_RB21        0x36
#define EP_ADRS__DAC_BIAS_RB43        0x37
#define EP_ADRS__ADC_HS_WO            0x38
#define EP_ADRS__ADC_BASE_FREQ        0x39
#define EP_ADRS__XADC_TEMP            0x3A
#define EP_ADRS__XADC_VOLT            0x3B
#define EP_ADRS__ADC_HS_DOUT0         0x3C
#define EP_ADRS__ADC_HS_DOUT1         0x3D
#define EP_ADRS__ADC_HS_DOUT2         0x3E
#define EP_ADRS__ADC_HS_DOUT3         0x3F
//trig-in            
#define EP_ADRS__TEST_TI              0x40
#define EP_ADRS__TEST_TI_HS           0x41 //##$$ 2019/3/5, 2018/12/8 
#define EP_ADRS__DWAVE_TI             0x46
#define EP_ADRS__DAC_BIAS_TI          0x50
#define EP_ADRS__DAC_A2A3_TI          0x51
#define EP_ADRS__ADC_HS_TI            0x58
//trig-out                           
#define EP_ADRS__TEST_TO              0x60
#define EP_ADRS__DAC_BIAS_TO          0x70
#define EP_ADRS__DAC_A2A3_TO          0x71
#define EP_ADRS__ADC_HS_TO            0x78
//pipe-out                           
#define EP_ADRS__ADC_HS_DOUT0_PO      0xBC
#define EP_ADRS__ADC_HS_DOUT1_PO      0xBD
#define EP_ADRS__ADC_HS_DOUT2_PO      0xBE
#define EP_ADRS__ADC_HS_DOUT3_PO      0xBF
//}

// TODO: remove// PGU end points adrs //{

#define EP_ADRS__SW_BUILD_ID__PGU      0x00
#define EP_ADRS__TEST_CON__PGU         0x01
#define EP_ADRS__TEST_IO_CON__PGU      0x03
#define EP_ADRS__DACX_DAT_WI__PGU      0x04
#define EP_ADRS__DACX_WI__PGU          0x05
#define EP_ADRS__CLKD_WI__PGU          0x06
#define EP_ADRS__SPIO_WI__PGU          0x07
#define EP_ADRS__FPGA_IMAGE_ID__PGU    0x20
#define EP_ADRS__TEST_OUT__PGU         0x21
#define EP_ADRS__TIMESTAMP_WO__PGU     0x22
#define EP_ADRS__TEST_IO_MON__PGU      0x23
#define EP_ADRS__DACX_DAT_WO__PGU      0x24
#define EP_ADRS__DACX_WO__PGU          0x25
#define EP_ADRS__CLKD_WO__PGU          0x26
#define EP_ADRS__SPIO_WO__PGU          0x27
#define EP_ADRS__XADC_TEMP__PGU        0x3A
#define EP_ADRS__XADC_VOLT__PGU        0x3B
#define EP_ADRS__TEST_TI__PGU          0x40
#define EP_ADRS__TEST_IO_TI__PGU       0x43
#define EP_ADRS__DACX_DAT_TI__PGU      0x44
#define EP_ADRS__DACX_TI__PGU          0x45
#define EP_ADRS__CLKD_TI__PGU          0x46
#define EP_ADRS__SPIO_TI__PGU          0x47
#define EP_ADRS__DAC0_DAT_PI__PGU      0x84
#define EP_ADRS__DAC1_DAT_PI__PGU      0x85

//}

#ifdef __cplusplus
}
#endif

#endif /* end of protection macro */
