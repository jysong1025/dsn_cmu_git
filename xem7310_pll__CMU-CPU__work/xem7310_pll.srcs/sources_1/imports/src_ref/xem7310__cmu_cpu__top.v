// # this           : xem7310__cmu_cpu__top.v
// # xdc related    : xem7310__cmu_cpu__top.xdc
//                  
// # FPGA board     : XEM7310-A200
// # FPGA board sch : NA
//
// # base on socket : CMU-CPU-F5500-REVA 19-05-13
// # base sch       : CMU_CPU_F5500_R190510__DEBUG_0709.pdf

// unused
//`default_nettype none

/* top module integration */
module xem7310__cmu_cpu__top ( 
	
	// OK interface //{
	input  wire [4:0]   okUH,
	output wire [2:0]   okHU,
	inout  wire [31:0]  okUHU,
	inout  wire         okAA,
	//}
	
	// external clock ports //{
	input  wire         sys_clkp, 
	input  wire         sys_clkn,
	//}
	
	// TXEM7310 interface //{
	
	// ## LAN for END-POINTS 
	output wire  o_B15_L6P , // # H17    EP_LAN_PWDN 
	output wire  o_B15_L7P , // # J22    EP_LAN_MOSI
	output wire  o_B15_L7N , // # H22    EP_LAN_SCLK
	output wire  o_B15_L8P , // # H20    EP_LAN_CS_B
	input  wire  i_B15_L8N , // # G20    EP_LAN_INT_B
	output wire  o_B15_L9P , // # K21    EP_LAN_RST_B
	input  wire  i_B15_L9N , // # K22    EP_LAN_MISO
	
	// ## TP
	inout  wire  io_B15_L1P , // # H13    TP0 // test for eeprom : VCC_3.3V
	inout  wire  io_B15_L1N , // # G13    TP1 // test for eeprom : VSS_GND
	inout  wire  io_B15_L2P , // # G15    TP2 // test for eeprom : SCIO
	inout  wire  io_B15_L2N , // # G16    TP3 // test for        : NA
	inout  wire  io_B15_L3P , // # J14    TP4 // test for        : NA
	inout  wire  io_B15_L3N , // # H14    TP5 // test for        : NA
	inout  wire  io_B15_L5P , // # J15    TP6 // test for        : NA
	inout  wire  io_B15_L5N , // # H15    TP7 // test for        : NA
	//  
	//  // ## ADC
	//  input  wire  i_B15_L10P, // # H20    AUX_AD11P
	//  input  wire  i_B15_L10N, // # G20    AUX_AD11N

	//}	
	
	//// BANK 13 34 35 signals in connectors
	
	// MC1 - odd //{
	output wire			o_B34_L24P,        // # MC1-15  # ADC_XX_TEST_B 
	output wire			o_B34_L24N,        // # MC1-17  # ADC_XX_DUAL_LANE_B
	input  wire			i_B34D_L17P,       // # MC1-19  # ADC_01_DB_P  // ADC_01 PN swap in logic
	input  wire			i_B34D_L17N,       // # MC1-21  # ADC_01_DB_N  // ADC_01 PN swap in logic
	input  wire			i_B34D_L16P,       // # MC1-23  # ADC_01_DA_P  // ADC_01 PN swap in logic
	input  wire			i_B34D_L16N,       // # MC1-25  # ADC_01_DA_N  // ADC_01 PN swap in logic
	input  wire			c_B34D_L14P_SRCC,  // # MC1-27  # ADC_01_DCO_P // ADC_01 PN swap in logic
	input  wire			c_B34D_L14N_SRCC,  // # MC1-29  # ADC_01_DCO_N // ADC_01 PN swap in logic
	output wire			o_B34_L10P,        // # MC1-31  # BIAS_LOAD_B  // DAC_BIAS internal
	output wire			o_B34_L10N,        // # MC1-33  # BIAS_CLR_B   // DAC_BIAS internal
	//                                   , // # MC1-35  # DGND               
	output wire			o_B34_L20P,        // # MC1-37  # DWAVE_F01 // inverted in logic
	output wire			o_B34_L20N,        // # MC1-39  # DWAVE_F02 // inverted in logic
	output wire			o_B34_L3P,         // # MC1-41  # DWAVE_F03 // inverted in logic
	output wire			o_B34_L3N,         // # MC1-43  # DWAVE_F11 // inverted in logic
	output wire			o_B34_L9P,         // # MC1-45  # DWAVE_F12 // inverted in logic
	output wire			o_B34_L9N,         // # MC1-47  # DWAVE_F13 // inverted in logic
	output wire			o_B34_L2P,         // # MC1-49  # BIAS_SYNCn  // DAC_BIAS internal
	output wire			o_B34_L2N,         // # MC1-51  # BIAS_SCLK   // DAC_BIAS internal
	output wire			o_B34_L4P,         // # MC1-53  # BIAS_SDI    // DAC_BIAS internal
	//                                   , // # MC1-55  # DGND               
	input  wire			i_B34_L4N,         // # MC1-57  # BIAS_SDO    // DAC_BIAS internal
	input  wire			i_B34D_L1P,        // # MC1-59  # ADC_10_DB_P  
	input  wire			i_B34D_L1N,        // # MC1-61  # ADC_10_DB_N  
	input  wire			i_B34D_L7P,        // # MC1-63  # ADC_10_DA_P  
	input  wire			i_B34D_L7N,        // # MC1-65  # ADC_10_DA_N  
	input  wire			c_B34D_L12P_MRCC,  // # MC1-67  # ADC_10_DCO_P 
	input  wire			c_B34D_L12N_MRCC,  // # MC1-69  # ADC_10_DCO_N 
	input  wire			i_B13_L2P,         // # MC1-71  # CC12 // MISO     // DAC_A2A3_MISO   //$$
	inout  wire			o_B13_L2N,         // # MC1-73  # CC11 // TEST-OUT // reserved output //$$
	output wire			o_B13_L4P,         // # MC1-75  # CC10 // SCSB     // DAC_A2A3_SYNB   //$$
	output wire			o_B13_L4N,         // # MC1-77  # CC9  // SCLK     // DAC_A2A3_SCLK   //$$
	output wire			o_B13_L1P,         // # MC1-79  # CC8  // MOSI     // DAC_A2A3_MOSI   //$$
	//}
	
	// MC1 - even //{
	inout  wire			io_B13_SYS_CLK_MC1,// # MC1-8   # FPGA_IO_C
	input  wire			i_XADC_VN,         // # MC1-10  # XADC_VN // external ADC ports 
	input  wire			i_XADC_VP,         // # MC1-12  # XADC_VP // external ADC ports 
	//                                   , // # MC1-14  # GND             
	output wire			o_B34D_L21P,       // # MC1-16  # ADC_XX_CNV_P
	output wire			o_B34D_L21N,       // # MC1-18  # ADC_XX_CNV_N
	output wire			o_B34D_L19P,       // # MC1-20  # ADC_XX_CLK_P
	output wire			o_B34D_L19N,       // # MC1-22  # ADC_XX_CLK_N   
	input  wire			i_B34D_L23P,       // # MC1-24  # ADC_11_DB_P  // reserved
	input  wire			i_B34D_L23N,       // # MC1-26  # ADC_11_DB_N  // reserved
	input  wire			i_B34D_L15P,       // # MC1-28  # ADC_11_DA_P  // reserved
	input  wire			i_B34D_L15N,       // # MC1-30  # ADC_11_DA_N  // reserved
	input  wire			c_B34D_L13P_MRCC,  // # MC1-32  # ADC_11_DCO_P // reserved
	input  wire			c_B34D_L13N_MRCC,  // # MC1-34  # ADC_11_DCO_N // reserved
	//                                   , // # MC1-36  # MC1_VCCO      
	input  wire			c_B34D_L11P_SRCC,  // # MC1-38  # ADC_00_DCO_P
	input  wire			c_B34D_L11N_SRCC,  // # MC1-40  # ADC_00_DCO_N
	input  wire			i_B34D_L18P,       // # MC1-42  # ADC_00_DA_P
	input  wire			i_B34D_L18N,       // # MC1-44  # ADC_00_DA_N
	input  wire			i_B34D_L22P,       // # MC1-46  # ADC_00_DB_P
	input  wire			i_B34D_L22N,       // # MC1-48  # ADC_00_DB_N
	input  wire			i_B34_L6P,         // # MC1-50  # CC20 // D_D     //$$
	input  wire			i_B34_L6N,         // # MC1-52  # CC21 // C_D     //$$
	output wire			o_B34_L5P,         // # MC1-54  # CC6  // SPO_CD3 //$$
	//                                   , // # MC1-56  # MC1_VCCO  
	output wire			o_B34_L5N,         // # MC1-58  # CC7  // SPO_EN3 //$$
	input  wire			i_B34_L8P,         // # MC1-60  # CC22 // B_D     //$$
	input  wire			i_B34_L8N,         // # MC1-62  # CC23 // A_D     //$$
	output wire			o_B13_L5P,         // # MC1-64  # CC0 // SPO_CD0  //$$
	output wire			o_B13_L5N,         // # MC1-66  # CC1 // SPO_EN0  //$$
	output wire			o_B13_L3P,         // # MC1-68  # CC2 // SPO_CD1  //$$
	output wire			o_B13_L3N,         // # MC1-70  # CC3 // SPO_EN1  //$$
	output wire			o_B13_L16P,        // # MC1-72  # CC4 // SPO_CD2  //$$
	output wire			o_B13_L16N,        // # MC1-74  # CC5 // SPO_EN2  //$$
	inout  wire			io_B13_L1N,        // # MC1-76  # FPGA_IO_A
	//}
	
	// MC2 - odd //{
	input  wire			i_B13_SYS_CLK_MC2, // # MC2-11  # LAN_PWDN --> FPGA_IO_D //$$ from U98 MAX6576ZUT+T
	//                                   , // # MC2-13  # DGND            
	input  wire			i_B35_L21P,        // # MC2-15  # LAN_MISO
	output wire			o_B35_L21N,        // # MC2-17  # LAN_RSTn
	input  wire			i_B35_L19P,        // # MC2-19  # CC14      // RBCK1
	input  wire			i_B35_L19N,        // # MC2-21  # CC15      // UNBAL
	inout  wire			o_B35_L18P,        // # MC2-23  # DAC_D4P   // reserved output
	inout  wire			o_B35_L18N,        // # MC2-25  # DAC_D4N   // reserved output
	inout  wire			o_B35_L23P,        // # MC2-27  # DAC_D3P   // reserved output
	inout  wire			o_B35_L23N,        // # MC2-29  # DAC_D3N   // reserved output
	inout  wire			o_B35_L15P,        // # MC2-31  # DAC_D2P   // reserved output
	inout  wire			o_B35_L15N,        // # MC2-33  # DAC_D2N   // reserved output
	//                                   , // # MC2-35  # MC2_VCCO        
	inout  wire			o_B35_L9P,         // # MC2-37  # DAC_D1P   // reserved output
	inout  wire			o_B35_L9N,         // # MC2-39  # DAC_D1N   // reserved output
	inout  wire			o_B35_L7P,         // # MC2-41  # DAC_D0P   // reserved output
	inout  wire			o_B35_L7N,         // # MC2-43  # DAC_D0N   // reserved output
	inout  wire			o_B35_L11P_SRCC,   // # MC2-45  # DAC_DCI_P // reserved output
	inout  wire			o_B35_L11N_SRCC,   // # MC2-47  # DAC_DCI_N // reserved output
	inout  wire			o_B35_L4P,         // # MC2-49  # DAC_D5P   // reserved output
	inout  wire			o_B35_L4N,         // # MC2-51  # DAC_D5N   // reserved output
	input  wire			i_B35_L6P,         // # MC2-53  # FPGA_IO_B // from U62 MAX6576ZUT+T
	//                                   , // # MC2-55  # MC2_VCCO        
	input  wire			i_B35_L6N,         // # MC2-57  # CC13      // RBCK2
	inout  wire			o_B35_L1P,         // # MC2-59  # DAC_D6P   // reserved output
	inout  wire			o_B35_L1N,         // # MC2-61  # DAC_D6N   // reserved output
	inout  wire			o_B35_L13P_MRCC,   // # MC2-63  # DAC_D7P   // reserved output
	inout  wire			o_B35_L13N_MRCC,   // # MC2-65  # DAC_D7N   // reserved output
	inout  wire			io_B13_L17P,       // # MC2-67  # CD0+      // reserved
	inout  wire			io_B13_L17N,       // # MC2-69  # CD0-      // reserved
	inout  wire			io_B13_L13P_MRCC,  // # MC2-71  # CD2+      // reserved
	inout  wire			io_B13_L13N_MRCC,  // # MC2-73  # CD2-      // reserved
	input  wire			i_B13_L11P_SRCC,   // # MC2-75  # CC19      // A_R
	input  wire			i_B35D_L12P_MRCC,  // # MC2-77  # OSC_IN_P  // DAC_DCO_P //$$ loopback conv clock
	input  wire			i_B35D_L12N_MRCC,  // # MC2-79  # OSC_IN_N  // DAC_DCO_N //$$ loopback conv clock
	//}
	
	// MC2 - even //{
	output wire			o_B35_IO0,         // # MC2-10  # LAN_SCSn
	output wire			o_B35_IO25,        // # MC2-12  # LAN_SCLK
	//                                   , // # MC2-14  # DGND             
	output wire			o_B35_L24P,        // # MC2-16  # LAN_MOSI
	input  wire			i_B35_L24N,        // # MC2-18  # LAN_INTn // o --> i 
	inout  wire			o_B35_L22P,        // # MC2-20  # DAC_D8P    // reserved output
	inout  wire			o_B35_L22N,        // # MC2-22  # DAC_D8N    // reserved output
	inout  wire			o_B35_L20P,        // # MC2-24  # DAC_D9P    // reserved output
	inout  wire			o_B35_L20N,        // # MC2-26  # DAC_D9N    // reserved output
	inout  wire			o_B35_L16P,        // # MC2-28  # DAC_D10P   // reserved output
	inout  wire			o_B35_L16N,        // # MC2-30  # DAC_D10N   // reserved output
	inout  wire			o_B35_L17P,        // # MC2-32  # DAC_D11P   // reserved output
	inout  wire			o_B35_L17N,        // # MC2-34  # DAC_D11N   // reserved output
	//                                   , // # MC2-36  # DGND            
	inout  wire			o_B35_L14P_SRCC,   // # MC2-38  # DAC_D12P   // reserved output
	inout  wire			o_B35_L14N_SRCC,   // # MC2-40  # DAC_D12N   // reserved output
	inout  wire			o_B35_L10P,        // # MC2-42  # DAC_D13P   // reserved output
	inout  wire			o_B35_L10N,        // # MC2-44  # DAC_D13N   // reserved output
	inout  wire			o_B35_L8P,         // # MC2-46  # DAC_D14P   // reserved output
	inout  wire			o_B35_L8N,         // # MC2-48  # DAC_D14N   // reserved output
	inout  wire			o_B35_L5P,         // # MC2-50  # DAC_D15P   // reserved output
	inout  wire			o_B35_L5N,         // # MC2-52  # DAC_D15N   // reserved output
	input  wire			i_B35_L3P,         // # MC2-54  # DAC_SDO    // reserved
	//                                     // # MC2-56  # DGND             
	inout  wire			io_B35_L3N,        // # MC2-58  # DAC_SDIO   // reserved
	inout  wire			o_B35_L2P,         // # MC2-60  # DAC_SCLK   // reserved output
	inout  wire			o_B35_L2N,         // # MC2-62  # DAC_CSB    // reserved output
	inout  wire			io_B13_L14P_SRCC,  // # MC2-64  # CD3+       // reserved
	inout  wire			io_B13_L14N_SRCC,  // # MC2-66  # CD3-       // reserved
	inout  wire			io_B13_L15P,       // # MC2-68  # CD1+       // reserved
	inout  wire			io_B13_L15N,       // # MC2-70  # CD1-       // reserved
	input  wire			i_B13_L6P,         // # MC2-72  # CC16 // D_R
	input  wire			i_B13_L6N,         // # MC2-74  # CC17 // C_R
	input  wire			i_B13_L11N_SRCC,   // # MC2-76  # CC18 // B_R
	//}


	// LED on XEM7310 //{
	output wire [7:0]   led
	//}
	
	);


/*parameter common */  //{

// TODO: FPGA_IMAGE_ID: h_ED_20_1218
//parameter FPGA_IMAGE_ID = 32'h2018_0611; // CMU-PLAN-A
//parameter FPGA_IMAGE_ID = 32'h_CA_18_0716; // CMU-1MHz-SUB
//parameter FPGA_IMAGE_ID = 32'h_FF_18_0801; // CMU-SIO-TEST
//parameter FPGA_IMAGE_ID = 32'h_FD_18_0820; // CMU-HSADC-TEST
//parameter FPGA_IMAGE_ID = 32'h_FD_18_0904; // CMU-HSADC-TEST // dwave rev. added dis control
//parameter FPGA_IMAGE_ID = 32'h_FB_18_1105; // CMU-CPU-TEST // dual ADC, 8f-4level DAC.
//parameter FPGA_IMAGE_ID = 32'h_FB_18_11DD; //// serial bus control only 
//parameter FPGA_IMAGE_ID = 32'h_FA_18_1127; //// DAC control 
//parameter FPGA_IMAGE_ID = 32'h_F9_18_1128; //// DWAVE control 
//parameter FPGA_IMAGE_ID = 32'h_F8_18_1207; //// ADC control ... clock loopback test 
//parameter FPGA_IMAGE_ID = 32'h_F8_19_0103; //// DAC_A2A3 pinmap revision
//parameter FPGA_IMAGE_ID = 32'h_F7_18_1222; //// DWAVE clock up to 200MHz : 160MHz also OK
//parameter FPGA_IMAGE_ID = 32'h_F6_19_0107; //// ADC base clock up to 210MHz ... from 125MHz // DWAVE 160MHz
//parameter FPGA_IMAGE_ID = 32'h_F6_19_0108; //// ADC base 210MHz // ADC fifo 70MHz // DWAVE 160MHz // PerfOption in vivado
//parameter FPGA_IMAGE_ID = 32'h_F5_19_0115; //// MCS 72MHz + XSDK(hello)
//parameter FPGA_IMAGE_ID = 32'h_F4_19_0217; //// MCS 72MHz + XSDK(debug net) + LAN-SPIO 144MHz 
//parameter FPGA_IMAGE_ID = 32'h_F4_19_0219; //// MCS 72MHz + XSDK(debug net) + LAN-SPIO 144MHz (end-point rev) 
//parameter FPGA_IMAGE_ID = 32'h_F4_19_0222; //// MCS 72MHz + XSDK(debug net) + LAN-SPIO 144MHz (end-point rev) + ADC input timing xdc rev
//parameter FPGA_IMAGE_ID = 32'h_F4_19_0223; //// MCS 72MHz + XSDK + LAN-SPIO 144MHz + ADC input-tap rev
//parameter FPGA_IMAGE_ID = 32'h_F4_19_0225; //// MCS 72MHz + XSDK + LAN-SPIO 144MHz + ADC input serdes rev
//parameter FPGA_IMAGE_ID = 32'h_F3_19_0306; //// MCS 72MHz + XSDK + LAN-SPIO 144MHz + LAN-Endpoint
//parameter FPGA_IMAGE_ID = 32'h_C8_19_0320; // CMU-CPU-TEST-F5500 
//parameter FPGA_IMAGE_ID = 32'h_C8_19_0329; // CMU-CPU-TEST-F5500  + DWAVE 135MHz 
//parameter FPGA_IMAGE_ID = 32'h_EF_19_0516; // CMU-CPU-TEST-F5500 // DWAVE 135MHz + temp sensor
//parameter FPGA_IMAGE_ID = 32'h_C8_19_0430; // CMU-CPU-TEST-F5500 + rev_a // SPO high 32b bug fix 
//parameter FPGA_IMAGE_ID = 32'h_C8_19_0513; // CMU-CPU-TEST-F5500 + rev_b // DWAVE pulse en sync bug fix 
//parameter FPGA_IMAGE_ID = 32'h_C8_19_05DD; //// full function release : temp sensor, eeprom, TCP setting. // pending
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0517; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0607; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor + dwave pin swap for out2 (DWAVE_F1*)
//parameter FPGA_IMAGE_ID = 32'h_EE_19_06C7; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor + dwave pin swap for out2 (DWAVE_F1*) + sw warm-up control
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0618; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor + dwave pin swap for out2 (DWAVE_F1*) + sw warm-up control + adc period count 22bit
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0717; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor + dwave pin NON-swap for out2 (DWAVE_F1*) + sw warm-up control + adc period count 22bit
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0718; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor + dwave pin NON-swap for out2 (DWAVE_F1*) + sw warm-up control + adc period count 22bit + dwaveout timing
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0905; // CMU-CPU-TEST-F5500 // SPO bus1 DAC 12bit pin swap 
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0909; // CMU-CPU-TEST-F5500 // SPO bus1 DAC 12bit pin swap // debug
//parameter FPGA_IMAGE_ID = 32'h_EE_20_1006; // CMU-CPU-TEST-F5500 // ADC 210M --> 180M
//parameter FPGA_IMAGE_ID = 32'h_EE_20_1007; // CMU-CPU-TEST-F5500 // ADC 210M --> 195M
//parameter FPGA_IMAGE_ID = 32'h_EE_20_1008; // CMU-CPU-TEST-F5500 // ADC 210M --> 196M
//parameter FPGA_IMAGE_ID = 32'h_EE_20_1105; // CMU-CPU-TEST-F5500 // ADC 210M back // clock domain rename
//parameter FPGA_IMAGE_ID = 32'h_ED_20_1120; // CMU-CPU-TEST-F5500 // logic wrappers merge1 (stamp, test)
//parameter FPGA_IMAGE_ID = 32'h_ED_20_1124; // CMU-CPU-TEST-F5500 // logic wrappers merge2 (lan) 
//parameter FPGA_IMAGE_ID = 32'h_ED_20_1125; // CMU-CPU-TEST-F5500 // logic wrappers merge3 (eeprom) // release__cmu_cpu__r1
//parameter FPGA_IMAGE_ID = 32'h_ED_20_1203; // CMU-CPU-TEST-F5500 // SPIO support (MCP23S17)
parameter FPGA_IMAGE_ID = 32'h_ED_20_1218; // CMU-CPU-TEST-F5500 // adc input delay re-timing
////

// check SW_BUILD_ID
parameter REQ_SW_BUILD_ID = 32'h_ACAC_C8C8; // 0 for bypass 
////


// ADC_BASE_FREQ
// 250MHz : 32'd250_000_000; //            // 4ns 
// 200MHz : 32'd200_000_000; //            // 5ns 
// 210MHz : 32'd210_000_000; //            // 4.76190476ns 
// 180MHz : 32'd180_000_000; //            // 5.556ns 
// 195MHz : 32'd180_000_000; //            // 5.12820513ns 
// 125MHz : 32'd125_000_000; // 0x07735940 // 8ns 
// 120MHz : 32'd120_000_000; // 0x07270E00 // 8.333ns
//parameter ADC_BASE_FREQ = 32'd125_000_000; 
//parameter ADC_BASE_FREQ = 32'd200_000_000; //
parameter ADC_BASE_FREQ = 32'd210_000_000; //
//parameter ADC_BASE_FREQ = 32'd180_000_000; //
//parameter ADC_BASE_FREQ = 32'd195_000_000; //
//parameter ADC_BASE_FREQ = 32'd250_000_000;
////

// DWAVE_BASE_FREQ
//  80MHz :  32'd80_000_000; // 0x4C4B400 // 12.5ns 
// 160MHz : 32'd160_000_000; // 0x9896800 // 6.25ns
//parameter DWAVE_BASE_FREQ = 32'd80_000_000; 
//parameter DWAVE_BASE_FREQ = 32'd135_000_000;
parameter DWAVE_BASE_FREQ = 32'd160_000_000;
//parameter DWAVE_BASE_FREQ = 32'd200_000_000;
////

//}

//-------------------------------------------------------//

/* TODO: clock/pll and reset */ //{

// clock pll0 //{
wire clk_out1_200M ; // REFCLK 200MHz for IDELAYCTRL // for pll1
wire clk_out2_140M ; // for pll2 ... 140M
//
wire clk_out3_10M  ; // for slow logic 
//wire clk_out4_125M ; // unused
//wire clk_out5_62p5M; // unused
//
wire clk_locked_pre;
clk_wiz_0  clk_wiz_0_inst (
	// Clock out ports  
	.clk_out1_200M (clk_out1_200M ), 
	.clk_out2_140M (clk_out2_140M ),
	//.clk_out3_10M  (clk_out3_10M  ), // not exact due to 140MHz
	//.clk_out4_125M (clk_out4_125M ), 
	//.clk_out5_62p5M(clk_out5_62p5M), 
	// Status and control signals               
	//.locked(clk_locked),
	.locked(clk_locked_pre),
	// Clock in ports
	.clk_in1_p(sys_clkp),
	.clk_in1_n(sys_clkn)
);
////
clk_wiz_0_0  clk_wiz_0_0_inst (
	// Clock out ports  
	.clk_out1_10M (clk_out3_10M),  
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(), // not used
	// Clock in ports
	.clk_in1(clk_out1_200M)
);


//}

// clock pll1 //{
wire clk1_out1_160M; // for DWAVE 
wire clk1_out2_120M; // unused
wire clk1_out3_80M ; // unused
wire clk1_out4_60M ; // unused // 10M exact...
wire clk1_locked;
// clk_wiz_0_1_135M  clk_wiz_0_1_inst ( // 135MHz test
clk_wiz_0_1  clk_wiz_0_1_inst (
	// Clock out ports  
	.clk_out1_160M(clk1_out1_160M),  
	.clk_out2_120M(clk1_out2_120M), 
	.clk_out3_80M (clk1_out3_80M ), 
	.clk_out4_60M (clk1_out4_60M ), 
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk1_locked),
	// Clock in ports
	.clk_in1(clk_out1_200M)
);
//}

// clock pll2 //{
wire clk2_out1_210M; // for HR-ADC and test_clk
wire clk2_out1_180M; // for HR-ADC and test_clk
wire clk2_out1_195M; // for HR-ADC and test_clk
wire clk2_out1_196M; // for HR-ADC and test_clk
//
wire clk2_out2_105M; // unused
wire clk2_out3_60M ; // for ADC fifo/serdes
wire clk2_out4_30M ; // unused             
wire clk2_locked;
wire clk2_1_locked;
wire clk2_2_locked;
wire clk2_3_locked;
wire clk2_4_locked;
//
clk_wiz_0_2  clk_wiz_0_2_inst (
	// Clock out ports  
	.clk_out1_210M(),  
	.clk_out2_105M(), 
	.clk_out3_60M (clk2_out3_60M ), 
	.clk_out4_30M (clk2_out4_30M ), 
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk2_locked),
	// Clock in ports
	.clk_in1(clk_out2_140M) // 200M --> 140M for better jitter.
);
//
clk_wiz_0_2_1  clk_wiz_0_2_1_inst (
	// Clock out ports  
	.clk_out1_180M(clk2_out1_180M),  
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk2_1_locked),
	// Clock in ports
	.clk_in1(clk_out2_140M) // 200M --> 140M for better jitter.
);
//
clk_wiz_0_2_2  clk_wiz_0_2_2_inst (
	// Clock out ports  
	.clk_out1_195M(clk2_out1_195M),  
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk2_2_locked),
	// Clock in ports
	.clk_in1(clk_out2_140M) // 200M --> 140M for better jitter.
);
//
clk_wiz_0_2_3  clk_wiz_0_2_3_inst (
	// Clock out ports  
	.clk_out1_210M(clk2_out1_210M),  
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk2_3_locked),
	// Clock in ports
	.clk_in1(clk_out2_140M) // 200M --> 140M for better jitter.
);
//
clk_wiz_0_2_4  clk_wiz_0_2_4_inst (
	// Clock out ports  
	.clk_out1_196M(clk2_out1_196M),  
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk2_4_locked),
	// Clock in ports
	.clk_in1(clk_out2_140M) // 200M --> 140M for better jitter.
);

//}

// clock pll3 //{
wire clk3_out1_72M ; // MCS core & UART ref 
wire clk3_out2_144M; // LAN-SPI control 144MHz 
wire clk3_out3_12M ; // IO bridge 12MHz 
// ... DDR3-CON 400MHz (pending)
wire clk3_locked;
clk_wiz_0_3  clk_wiz_0_3_inst (
	// Clock out ports  
	.clk_out1_72M (clk3_out1_72M ),  
	.clk_out2_144M(clk3_out2_144M), 
	.clk_out3_12M (clk3_out3_12M ), 
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk3_locked),
	// Clock in ports
	.clk_in1(clk_out1_200M)
);
//}

// clock locked 
wire clk_locked = clk1_locked & clk2_locked & clk3_locked;

// system clock
wire sys_clk	= clk_out3_10M; // for slow logic

// dwave clock
wire base_dwave_clk = clk1_out1_160M;

// adc clock 
wire base_adc_clk = clk2_out1_210M;
//wire base_adc_clk = clk2_out1_180M;
//wire base_adc_clk = clk2_out1_195M;
//wire base_adc_clk = clk2_out1_196M;

// ref clock for adc buffer 
wire ref_200M_clk = clk_out1_200M;

// adc fifo clock 
wire adc_fifo_clk = clk2_out3_60M;

// mcs clock 
wire mcs_clk = clk3_out1_72M;  // base clock for MCS block // 72MHz
							   
// lan clock                   
wire lan_clk = clk3_out2_144M; // base clock for LAN block // 144MHz

// lan io clock 
wire lan_io_clk = clk3_out3_12M; // io clock for LAN block // 12MHz


// system reset 
wire reset_n	= clk_locked;
wire reset		= ~reset_n;
//}
	

///TODO: //-------------------------------------------------------//


/* TODO: end-point wires */ //{

// end-points : USB vs LAN 

// wrapper modules : ok_endpoint_wrapper for USB  vs  lan_endpoint_wrapper for LAN
// ok_endpoint_wrapper  : usb host interface <--> end-points
//    okHost okHI
//    ok...
// lan_endpoint_wrapper : lan spi  interface <--> end-points
//    microblaze_mcs_1  soft_cpu_mcs_inst
//    mcs_io_bridge_ext  mcs_io_bridge_inst2
//    master_spi_wz850_ext  master_spi_wz850_inst
//    fifo_generator_3  LAN_fifo_wr_inst
//    fifo_generator_3  LAN_fifo_rd_inst


//// TODO: USB end-point wires: //{

// Wire In 		0x00 - 0x1F //{
wire [31:0] ep00wire; //$$ [TEST] SW_BUILD_ID 
wire [31:0] ep01wire; //$$ [TEST] TEST_CON 
wire [31:0] ep02wire; //$$ [TEST] TEST_CC_DIN 
wire [31:0] ep03wire; //$$ [TEST] BRD_CON //$$ new control for board
wire [31:0] ep04wire; //$$ [DAC] DAC_TEST_IN
wire [31:0] ep05wire; //$$ [DWAVE] DWAVE_DIN_BY_TRIG
wire [31:0] ep06wire; //$$ [DWAVE] DWAVE_CON
wire [31:0] ep07wire; //$$ [SPO] SPO_CON 
wire [31:0] ep08wire; //$$ [SPO] SPO_DIN_B0[31: 0] // SPO_CD0/SPO_EN0, CC0,CC1
wire [31:0] ep09wire; //$$ [SPO] SPO_DIN_B0[63:32] // SPO_CD0/SPO_EN0, CC0,CC1
wire [31:0] ep0Awire; //$$ [SPO] SPO_DIN_B1[31: 0] // SPO_CD1/SPO_EN0, CC2,CC3
wire [31:0] ep0Bwire; //$$ [SPO] SPO_DIN_B1[63:32] // SPO_CD1/SPO_EN0, CC2,CC3
wire [31:0] ep0Cwire; //$$ [SPO] SPO_DIN_B2[31: 0] // SPO_CD2/SPO_EN0, CC4,CC5
wire [31:0] ep0Dwire; //$$ [SPO] SPO_DIN_B2[63:32] // SPO_CD2/SPO_EN0, CC4,CC5
wire [31:0] ep0Ewire; //$$ [SPO] SPO_DIN_B3[31: 0] // SPO_CD3/SPO_EN0, CC6,CC7
wire [31:0] ep0Fwire; //$$ [SPO] SPO_DIN_B3[63:32] // SPO_CD3/SPO_EN0, CC6,CC7
wire [31:0] ep10wire; //$$ [DAC_A2A3] DAC_A2A3_CON
wire [31:0] ep11wire; //$$ [DAC_BIAS] DAC_BIAS_CON
wire [31:0] ep12wire; //$$ [MEM] MEM_FDAT_WI
wire [31:0] ep13wire; //$$ [MEM] MEM_WI
wire [31:0] ep14wire; //$$ [DAC_A2A3] DAC_A2A3_DIN21
wire [31:0] ep15wire; //$$ [DAC_A2A3] DAC_A2A3_DIN43
wire [31:0] ep16wire; //$$ [DAC_BIAS] DAC_BIAS_DIN21
wire [31:0] ep17wire; //$$ [DAC_BIAS] DAC_BIAS_DIN43
wire [31:0] ep18wire; //$$ [ADC_HS] ADC_HS_WI 
wire [31:0] ep19wire; //$$ [MCS] MCS_SETUP_WI // by MCS only
wire [31:0] ep1Awire; //$$ [SPIO] SPIO_FDAT_WI
wire [31:0] ep1Bwire; //$$ [SPIO] SPIO_WI
wire [31:0] ep1Cwire; // not assigned // [ADC_HS] ADC_FREQ_WI // to come
wire [31:0] ep1Dwire; //$$ [ADC_HS] ADC_HS_UPD_SMP    
wire [31:0] ep1Ewire; //$$ [ADC_HS] ADC_HS_SMP_PRD    
wire [31:0] ep1Fwire; //$$ [ADC_HS] ADC_HS_DLY_TAP_OPT
//}

// Wire Out 	0x20 - 0x3F //{
wire [31:0] ep20wire; //$$ [TEST] FPGA_IMAGE_ID
wire [31:0] ep21wire; //$$ [TEST] TEST_OUT
wire [31:0] ep22wire; //$$ [TEST] TEST_CC_MON
wire [31:0] ep23wire; //$$ [DWAVE] DWAVE_BASE_FREQ
wire [31:0] ep24wire; //$$ [DAC] DAC_TEST_OUT
wire [31:0] ep25wire; //$$ [DWAVE] DWAVE_DOUT_BY_TRIG
wire [31:0] ep26wire; //$$ [DWAVE] DWAVE_FLAG
wire [31:0] ep27wire; //$$ [SPO] SPO_FLAG
wire [31:0] ep28wire; //$$ [SPO] SPO_MON_B0[31: 0]
wire [31:0] ep29wire; //$$ [SPO] SPO_MON_B0[63:32]
wire [31:0] ep2Awire; //$$ [SPO] SPO_MON_B1[31: 0]
wire [31:0] ep2Bwire; //$$ [SPO] SPO_MON_B1[63:32]
wire [31:0] ep2Cwire; //$$ [SPO] SPO_MON_B2[31: 0]
wire [31:0] ep2Dwire; //$$ [SPO] SPO_MON_B2[63:32]
wire [31:0] ep2Ewire; //$$ [SPO] SPO_MON_B3[31: 0]
wire [31:0] ep2Fwire; //$$ [SPO] SPO_MON_B3[63:32]
wire [31:0] ep30wire; //$$ [DAC_A2A3] DAC_A2A3_FLAG
wire [31:0] ep31wire; //$$ [DAC_BIAS] DAC_BIAS_FLAG
wire [31:0] ep32wire; //$$ [DAC] DAC_TEST_RB1
wire [31:0] ep33wire; //$$ [DAC] DAC_TEST_RB2
wire [31:0] ep34wire; //$$ [DAC_A2A3] DAC_A2A3_RB21 
wire [31:0] ep35wire; //$$ [DAC_A2A3] DAC_A2A3_RB43
wire [31:0] ep36wire; //$$ [DAC_BIAS] DAC_BIAS_RB21
wire [31:0] ep37wire; //$$ [DAC_BIAS] DAC_BIAS_RB43
wire [31:0] ep38wire; //$$ [ADC_HS] ADC_HS_WO
wire [31:0] ep39wire; //$$ [ADC_HS] ADC_BASE_FREQ
wire [31:0] ep3Awire; //$$ [XADC] XADC_TEMP
wire [31:0] ep3Bwire; //$$ [XADC] XADC_VOLT  //$$ shared with [SPIO] SPIO_WO
wire [31:0] ep3Cwire; //$$ [ADC_HS] ADC_HS_DOUT0
wire [31:0] ep3Dwire; //$$ [ADC_HS] ADC_HS_DOUT1  
wire [31:0] ep3Ewire; //$$ [ADC_HS] ADC_HS_DOUT2
wire [31:0] ep3Fwire; //$$ [ADC_HS] ADC_HS_DOUT3
//}

// Trigger In 	0x40 - 0x5F //{
wire ep40ck = sys_clk;      wire [31:0] ep40trig; //$$ [TEST] TEST_TI
wire ep41ck = base_adc_clk; wire [31:0] ep41trig; //$$ [TEST] TEST_TI_HS
wire ep42ck = 1'b0;         wire [31:0] ep42trig;
wire ep43ck = 1'b0;         wire [31:0] ep43trig;
wire ep44ck = 1'b0;         wire [31:0] ep44trig;
wire ep45ck = 1'b0;         wire [31:0] ep45trig;
wire ep46ck = sys_clk;      wire [31:0] ep46trig; //$$ [DWAVE] DWAVE_TI
wire ep47ck = 1'b0;         wire [31:0] ep47trig;
wire ep48ck = 1'b0;         wire [31:0] ep48trig;
wire ep49ck = 1'b0;         wire [31:0] ep49trig;
wire ep4Ack = 1'b0;         wire [31:0] ep4Atrig; //$$ reserved // TEST_MCS for network command/response
wire ep4Bck = 1'b0;         wire [31:0] ep4Btrig;
wire ep4Cck = 1'b0;         wire [31:0] ep4Ctrig;
wire ep4Dck = 1'b0;         wire [31:0] ep4Dtrig;
wire ep4Eck = 1'b0;         wire [31:0] ep4Etrig;
wire ep4Fck = 1'b0;         wire [31:0] ep4Ftrig;
wire ep50ck = sys_clk;      wire [31:0] ep50trig; //$$ [DAC_BIAS] DAC_BIAS_TI
wire ep51ck = sys_clk;      wire [31:0] ep51trig; //$$ [DAC_A2A3] DAC_A2A3_TI
wire ep52ck = 1'b0;         wire [31:0] ep52trig;
wire ep53ck = sys_clk;      wire [31:0] ep53trig; //$$ [MEM] MEM_TI
wire ep54ck = 1'b0;         wire [31:0] ep54trig;
wire ep55ck = 1'b0;         wire [31:0] ep55trig;
wire ep56ck = 1'b0;         wire [31:0] ep56trig;
wire ep57ck = 1'b0;         wire [31:0] ep57trig;
wire ep58ck = sys_clk;      wire [31:0] ep58trig; //$$ [ADC_HS] ADC_HS_TI
wire ep59ck = 1'b0;         wire [31:0] ep59trig;
wire ep5Ack = 1'b0;         wire [31:0] ep5Atrig;
wire ep5Bck = sys_clk;      wire [31:0] ep5Btrig; //$$ [SPIO] SPIO_TI
wire ep5Cck = 1'b0;         wire [31:0] ep5Ctrig;
wire ep5Dck = 1'b0;         wire [31:0] ep5Dtrig;
wire ep5Eck = 1'b0;         wire [31:0] ep5Etrig;
wire ep5Fck = 1'b0;         wire [31:0] ep5Ftrig;
//}

// Trigger Out 	0x60 - 0x7F //{
wire ep60ck = sys_clk;      wire [31:0] ep60trig; //$$ [TEST] TEST_TO
wire ep61ck = 1'b0;         wire [31:0] ep61trig = 32'b0;
wire ep62ck = 1'b0;         wire [31:0] ep62trig = 32'b0;
wire ep63ck = 1'b0;         wire [31:0] ep63trig = 32'b0;
wire ep64ck = 1'b0;         wire [31:0] ep64trig = 32'b0;
wire ep65ck = 1'b0;         wire [31:0] ep65trig = 32'b0;
wire ep66ck = 1'b0;         wire [31:0] ep66trig = 32'b0;
wire ep67ck = 1'b0;         wire [31:0] ep67trig = 32'b0;
wire ep68ck = 1'b0;         wire [31:0] ep68trig = 32'b0;
wire ep69ck = 1'b0;         wire [31:0] ep69trig = 32'b0;
wire ep6Ack = 1'b0;         wire [31:0] ep6Atrig = 32'b0; //$$ reserved // TEST_MCS for network command/response
wire ep6Bck = 1'b0;         wire [31:0] ep6Btrig = 32'b0;
wire ep6Cck = 1'b0;         wire [31:0] ep6Ctrig = 32'b0;
wire ep6Dck = 1'b0;         wire [31:0] ep6Dtrig = 32'b0;
wire ep6Eck = 1'b0;         wire [31:0] ep6Etrig = 32'b0;
wire ep6Fck = 1'b0;         wire [31:0] ep6Ftrig = 32'b0;
wire ep70ck = sys_clk;      wire [31:0] ep70trig; //$$ [DAC_BIAS] DAC_BIAS_TO
wire ep71ck = sys_clk;      wire [31:0] ep71trig; //$$ [DAC_A2A3] DAC_A2A3_TO
wire ep72ck = 1'b0;         wire [31:0] ep72trig = 32'b0;
wire ep73ck = sys_clk;      wire [31:0] ep73trig; //$$ [MEM] MEM_TO
wire ep74ck = 1'b0;         wire [31:0] ep74trig = 32'b0;
wire ep75ck = 1'b0;         wire [31:0] ep75trig = 32'b0;
wire ep76ck = 1'b0;         wire [31:0] ep76trig = 32'b0;
wire ep77ck = 1'b0;         wire [31:0] ep77trig = 32'b0;
wire ep78ck = sys_clk;      wire [31:0] ep78trig; //$$ [ADC_HS] ADC_HS_TO
wire ep79ck = 1'b0;         wire [31:0] ep79trig = 32'b0;
wire ep7Ack = 1'b0;         wire [31:0] ep7Atrig = 32'b0;
wire ep7Bck = sys_clk;      wire [31:0] ep7Btrig; //$$ [SPIO] SPIO_TO
wire ep7Cck = 1'b0;         wire [31:0] ep7Ctrig = 32'b0;
wire ep7Dck = 1'b0;         wire [31:0] ep7Dtrig = 32'b0;
wire ep7Eck = 1'b0;         wire [31:0] ep7Etrig = 32'b0;
wire ep7Fck = 1'b0;         wire [31:0] ep7Ftrig = 32'b0; 
//}

// Pipe In 		0x80 - 0x9F // clock is assumed to use okClk //{
wire ep80wr; wire [31:0] ep80pipe;
wire ep81wr; wire [31:0] ep81pipe;
wire ep82wr; wire [31:0] ep82pipe;
wire ep83wr; wire [31:0] ep83pipe;
wire ep84wr; wire [31:0] ep84pipe;
wire ep85wr; wire [31:0] ep85pipe;
wire ep86wr; wire [31:0] ep86pipe;
wire ep87wr; wire [31:0] ep87pipe;
wire ep88wr; wire [31:0] ep88pipe;
wire ep89wr; wire [31:0] ep89pipe;
wire ep8Awr; wire [31:0] ep8Apipe; //$$ TEST_MCS for test fifo // MCS only
wire ep8Bwr; wire [31:0] ep8Bpipe;
wire ep8Cwr; wire [31:0] ep8Cpipe;
wire ep8Dwr; wire [31:0] ep8Dpipe;
wire ep8Ewr; wire [31:0] ep8Epipe;
wire ep8Fwr; wire [31:0] ep8Fpipe;
wire ep90wr; wire [31:0] ep90pipe;
wire ep91wr; wire [31:0] ep91pipe;
wire ep92wr; wire [31:0] ep92pipe;
wire ep93wr; wire [31:0] ep93pipe; //$$ [MEM] MEM_PI
wire ep94wr; wire [31:0] ep94pipe;
wire ep95wr; wire [31:0] ep95pipe;
wire ep96wr; wire [31:0] ep96pipe;
wire ep97wr; wire [31:0] ep97pipe;
wire ep98wr; wire [31:0] ep98pipe;
wire ep99wr; wire [31:0] ep99pipe;
wire ep9Awr; wire [31:0] ep9Apipe;
wire ep9Bwr; wire [31:0] ep9Bpipe;
wire ep9Cwr; wire [31:0] ep9Cpipe;
wire ep9Dwr; wire [31:0] ep9Dpipe;
wire ep9Ewr; wire [31:0] ep9Epipe;
wire ep9Fwr; wire [31:0] ep9Fpipe;
//}

// Pipe Out 	0xA0 - 0xBF //{
wire epA0rd; wire [31:0] epA0pipe = 32'b0;
wire epA1rd; wire [31:0] epA1pipe = 32'b0;
wire epA2rd; wire [31:0] epA2pipe = 32'b0;
wire epA3rd; wire [31:0] epA3pipe = 32'b0;
wire epA4rd; wire [31:0] epA4pipe = 32'b0;
wire epA5rd; wire [31:0] epA5pipe = 32'b0;
wire epA6rd; wire [31:0] epA6pipe = 32'b0;
wire epA7rd; wire [31:0] epA7pipe = 32'b0;
wire epA8rd; wire [31:0] epA8pipe = 32'b0;
wire epA9rd; wire [31:0] epA9pipe = 32'b0;
wire epAArd; wire [31:0] epAApipe = 32'b0; //$$ TEST_MCS for test fifo // MCS only
wire epABrd; wire [31:0] epABpipe = 32'b0;
wire epACrd; wire [31:0] epACpipe = 32'b0;
wire epADrd; wire [31:0] epADpipe = 32'b0;
wire epAErd; wire [31:0] epAEpipe = 32'b0;
wire epAFrd; wire [31:0] epAFpipe = 32'b0;
wire epB0rd; wire [31:0] epB0pipe = 32'b0;
wire epB1rd; wire [31:0] epB1pipe = 32'b0;
wire epB2rd; wire [31:0] epB2pipe = 32'b0;
wire epB3rd; wire [31:0] epB3pipe; //$$ [MEM] MEM_PO
wire epB4rd; wire [31:0] epB4pipe = 32'b0;
wire epB5rd; wire [31:0] epB5pipe = 32'b0;
wire epB6rd; wire [31:0] epB6pipe = 32'b0;
wire epB7rd; wire [31:0] epB7pipe = 32'b0;
wire epB8rd; wire [31:0] epB8pipe = 32'b0;
wire epB9rd; wire [31:0] epB9pipe = 32'b0;
wire epBArd; wire [31:0] epBApipe = 32'b0;
wire epBBrd; wire [31:0] epBBpipe = 32'b0;
wire epBCrd; wire [31:0] epBCpipe; //$$ [ADC_HS] ADC_HS_DOUT0_PO
wire epBDrd; wire [31:0] epBDpipe; //$$ [ADC_HS] ADC_HS_DOUT1_PO
wire epBErd; wire [31:0] epBEpipe; //$$ [ADC_HS] ADC_HS_DOUT2_PO
wire epBFrd; wire [31:0] epBFpipe; //$$ [ADC_HS] ADC_HS_DOUT3_PO
//}

// pipe interface clk: //{
//$$wire pipeClk; // rename: okClk --> pipeClk

// OK Target interface clk:
//(* keep = "true" *) 
wire okClk;

//}

//}

//// TODO: LAN end-point wires: //{

// wire in //{
wire [31:0] w_port_wi_00_1; //$$ [TEST] SW_BUILD_ID 
wire [31:0] w_port_wi_01_1; //$$ [TEST] TEST_CON 
wire [31:0] w_port_wi_02_1; //$$ [TEST] TEST_CC_DIN 
wire [31:0] w_port_wi_03_1; //$$ [TEST] BRD_CON //$$ new control for board
wire [31:0] w_port_wi_04_1; //$$ [DAC] DAC_TEST_IN
wire [31:0] w_port_wi_05_1; //$$ [DWAVE] DWAVE_DIN_BY_TRIG
wire [31:0] w_port_wi_06_1; //$$ [DWAVE] DWAVE_CON
wire [31:0] w_port_wi_07_1; //$$ [SPO] SPO_CON 
wire [31:0] w_port_wi_08_1; //$$ [SPO] SPO_DIN_B0[31: 0] // SPO_CD0/SPO_EN0, CC0,CC1
wire [31:0] w_port_wi_09_1; //$$ [SPO] SPO_DIN_B0[63:32] // SPO_CD0/SPO_EN0, CC0,CC1
wire [31:0] w_port_wi_0A_1; //$$ [SPO] SPO_DIN_B1[31: 0] // SPO_CD1/SPO_EN0, CC2,CC3
wire [31:0] w_port_wi_0B_1; //$$ [SPO] SPO_DIN_B1[63:32] // SPO_CD1/SPO_EN0, CC2,CC3
wire [31:0] w_port_wi_0C_1; //$$ [SPO] SPO_DIN_B2[31: 0] // SPO_CD2/SPO_EN0, CC4,CC5
wire [31:0] w_port_wi_0D_1; //$$ [SPO] SPO_DIN_B2[63:32] // SPO_CD2/SPO_EN0, CC4,CC5
wire [31:0] w_port_wi_0E_1; //$$ [SPO] SPO_DIN_B3[31: 0] // SPO_CD3/SPO_EN0, CC6,CC7
wire [31:0] w_port_wi_0F_1; //$$ [SPO] SPO_DIN_B3[63:32] // SPO_CD3/SPO_EN0, CC6,CC7
wire [31:0] w_port_wi_10_1; //$$ [DAC_A2A3] DAC_A2A3_CON
wire [31:0] w_port_wi_11_1; //$$ [DAC_BIAS] DAC_BIAS_CON
wire [31:0] w_port_wi_12_1; //$$ [MEM] MEM_FDAT_WI
wire [31:0] w_port_wi_13_1; //$$ [MEM] MEM_WI
wire [31:0] w_port_wi_14_1; //$$ [DAC_A2A3] DAC_A2A3_DIN21
wire [31:0] w_port_wi_15_1; //$$ [DAC_A2A3] DAC_A2A3_DIN43
wire [31:0] w_port_wi_16_1; //$$ [DAC_BIAS] DAC_BIAS_DIN21
wire [31:0] w_port_wi_17_1; //$$ [DAC_BIAS] DAC_BIAS_DIN43
wire [31:0] w_port_wi_18_1; //$$ [ADC_HS] ADC_HS_WI 
wire [31:0] w_port_wi_19_1; //$$ [MCS] MCS_SETUP_WI // by MCS only
wire [31:0] w_port_wi_1A_1; //$$ [SPIO] SPIO_FDAT_WI
wire [31:0] w_port_wi_1B_1; //$$ [SPIO] SPIO_WI
wire [31:0] w_port_wi_1C_1; // not assigned // [ADC_HS] ADC_FREQ_WI // to come
wire [31:0] w_port_wi_1D_1; //$$ [ADC_HS] ADC_HS_UPD_SMP    
wire [31:0] w_port_wi_1E_1; //$$ [ADC_HS] ADC_HS_SMP_PRD    
wire [31:0] w_port_wi_1F_1; //$$ [ADC_HS] ADC_HS_DLY_TAP_OPT
//}

// wire out //{
wire [31:0] w_port_wo_20_1; //$$ [TEST] FPGA_IMAGE_ID
wire [31:0] w_port_wo_21_1; //$$ [TEST] TEST_OUT
wire [31:0] w_port_wo_22_1; //$$ [TEST] TEST_CC_MON
wire [31:0] w_port_wo_23_1; //$$ [DWAVE] DWAVE_BASE_FREQ
wire [31:0] w_port_wo_24_1; //$$ [DAC] DAC_TEST_OUT
wire [31:0] w_port_wo_25_1; //$$ [DWAVE] DWAVE_DOUT_BY_TRIG
wire [31:0] w_port_wo_26_1; //$$ [DWAVE] DWAVE_FLAG
wire [31:0] w_port_wo_27_1; //$$ [SPO] SPO_FLAG
wire [31:0] w_port_wo_28_1; //$$ [SPO] SPO_MON_B0[31: 0]
wire [31:0] w_port_wo_29_1; //$$ [SPO] SPO_MON_B0[63:32]
wire [31:0] w_port_wo_2A_1; //$$ [SPO] SPO_MON_B1[31: 0]
wire [31:0] w_port_wo_2B_1; //$$ [SPO] SPO_MON_B1[63:32]
wire [31:0] w_port_wo_2C_1; //$$ [SPO] SPO_MON_B2[31: 0]
wire [31:0] w_port_wo_2D_1; //$$ [SPO] SPO_MON_B2[63:32]
wire [31:0] w_port_wo_2E_1; //$$ [SPO] SPO_MON_B3[31: 0]
wire [31:0] w_port_wo_2F_1; //$$ [SPO] SPO_MON_B3[63:32]
wire [31:0] w_port_wo_30_1; //$$ [DAC_A2A3] DAC_A2A3_FLAG
wire [31:0] w_port_wo_31_1; //$$ [DAC_BIAS] DAC_BIAS_FLAG
wire [31:0] w_port_wo_32_1; //$$ [DAC] DAC_TEST_RB1
wire [31:0] w_port_wo_33_1; //$$ [DAC] DAC_TEST_RB2
wire [31:0] w_port_wo_34_1; //$$ [DAC_A2A3] DAC_A2A3_RB21 
wire [31:0] w_port_wo_35_1; //$$ [DAC_A2A3] DAC_A2A3_RB43
wire [31:0] w_port_wo_36_1; //$$ [DAC_BIAS] DAC_BIAS_RB21
wire [31:0] w_port_wo_37_1; //$$ [DAC_BIAS] DAC_BIAS_RB43
wire [31:0] w_port_wo_38_1; //$$ [ADC_HS] ADC_HS_WO
wire [31:0] w_port_wo_39_1; //$$ [ADC_HS] ADC_BASE_FREQ
wire [31:0] w_port_wo_3A_1; //$$ [XADC] XADC_TEMP
wire [31:0] w_port_wo_3B_1; //$$ [XADC] XADC_VOLT  //$$ shared with [SPIO] SPIO_WO
wire [31:0] w_port_wo_3C_1; //$$ [ADC_HS] ADC_HS_DOUT0
wire [31:0] w_port_wo_3D_1; //$$ [ADC_HS] ADC_HS_DOUT1  
wire [31:0] w_port_wo_3E_1; //$$ [ADC_HS] ADC_HS_DOUT2
wire [31:0] w_port_wo_3F_1; //$$ [ADC_HS] ADC_HS_DOUT3
//}

// trig in //{
wire w_ck_40_1 = sys_clk     ; wire [31:0] w_port_ti_40_1; //$$ [TEST] TEST_TI
wire w_ck_41_1 = base_adc_clk; wire [31:0] w_port_ti_41_1; //$$ [TEST] TEST_TI_HS
//...
wire w_ck_46_1 = sys_clk     ; wire [31:0] w_port_ti_46_1; //$$ [DWAVE] DWAVE_TI 
//...
wire w_ck_4A_1 = mcs_clk     ; wire [31:0] w_port_ti_4A_1; //$$ reserved // TEST_MCS for network command/response 
//...
wire w_ck_50_1 = sys_clk     ; wire [31:0] w_port_ti_50_1; //$$ [DAC_BIAS] DAC_BIAS_TI
wire w_ck_51_1 = sys_clk     ; wire [31:0] w_port_ti_51_1; //$$ [DAC_A2A3] DAC_A2A3_TI
//...
wire w_ck_53_1 = sys_clk     ; wire [31:0] w_port_ti_53_1; //$$ [MEM] MEM_TI
//...
wire w_ck_58_1 = sys_clk     ; wire [31:0] w_port_ti_58_1; //$$ [ADC_HS] ADC_HS_TI 

wire w_ck_5B_1 = sys_clk     ; wire [31:0] w_port_ti_5B_1; //$$ [SPIO] SPIO_TI

//}

// trig out //{
wire w_ck_60_1 = sys_clk     ; wire [31:0] w_port_to_60_1; //$$ [TEST] TEST_TO 
wire w_ck_70_1 = sys_clk     ; wire [31:0] w_port_to_70_1; //$$ [DAC_BIAS] DAC_BIAS_TO
wire w_ck_71_1 = sys_clk     ; wire [31:0] w_port_to_71_1; //$$ [DAC_A2A3] DAC_A2A3_TO
wire w_ck_73_1 = sys_clk     ; wire [31:0] w_port_to_73_1; //$$ [MEM] MEM_TO
wire w_ck_78_1 = sys_clk     ; wire [31:0] w_port_to_78_1; //$$ [ADC_HS] ADC_HS_TO 
wire w_ck_7B_1 = sys_clk     ; wire [31:0] w_port_to_7B_1; //$$ [SPIO] SPIO_TO

//}

// pipe in //{
wire w_wr_8A_1; wire [31:0] w_port_pi_8A_1; //$$ TEST_MCS for test fifo 
wire w_wr_93_1; wire [31:0] w_port_pi_93_1; //$$ [MEM] MEM_PI
//}

// pipe out //{
wire w_rd_AA_1; wire [31:0] w_port_po_AA_1; //$$ TEST_MCS for test fifo // MCS only
wire w_rd_B3_1; wire [31:0] w_port_po_B3_1; //$$ [MEM] MEM_PO
wire w_rd_BC_1; wire [31:0] w_port_po_BC_1; // 
wire w_rd_BD_1; wire [31:0] w_port_po_BD_1; // 
wire w_rd_BE_1; wire [31:0] w_port_po_BE_1; // 
wire w_rd_BF_1; wire [31:0] w_port_po_BF_1; // 
//}

// pipe clock //{
wire w_ck_pipe; //$$ new 
//}

//}

//}


///TODO: //-------------------------------------------------------//


/* TODO: END-POINTS wrapper for EP_LAN */ //{

// lan_endpoint_wrapper   //{

wire [47:0] w_adrs_offset_mac_48b; // BASE = {8'h00,8'h08,8'hDC,8'h00,8'hAB,8'hCD}; // 00:08:DC:00:xx:yy ??48 bits
wire [31:0] w_adrs_offset_ip_32b ; // BASE = {8'd192,8'd168,8'd168,8'd112}; // 192.168.168.112 or C0:A8:A8:70 ??32 bits
wire [15:0] w_offset_lan_timeout_rtr_16b; //$$ = ep00wire[31:16]; // assign later 
wire [15:0] w_offset_lan_timeout_rcr_16b; //$$ = ep00wire[15: 0]; // assign later 

lan_endpoint_wrapper #(
	.MCS_IO_INST_OFFSET			(32'h_0004_0000), //$$ for CMU2020
	.FPGA_IMAGE_ID              (FPGA_IMAGE_ID)  
) lan_endpoint_wrapper_inst (
	
	//// pins and config //{
	
	// EP_LAN pins 
	.EP_LAN_MOSI   (EP_LAN_MOSI ), // output wire     EP_LAN_MOSI  ,
	.EP_LAN_SCLK   (EP_LAN_SCLK ), // output wire     EP_LAN_SCLK  ,
	.EP_LAN_CS_B   (EP_LAN_CS_B ), // output wire     EP_LAN_CS_B  ,
	.EP_LAN_INT_B  (EP_LAN_INT_B), // input  wire     EP_LAN_INT_B ,
	.EP_LAN_RST_B  (EP_LAN_RST_B), // output wire     EP_LAN_RST_B ,
	.EP_LAN_MISO   (EP_LAN_MISO ), // input  wire     EP_LAN_MISO  ,
	
	// for common 
	.clk           (sys_clk), // 10MHz
	.reset_n       (reset_n),
	
	// soft CPU clock
	.mcs_clk       (mcs_clk), // 72MHz
	
	// dedicated LAN clock 
	.lan_clk       (lan_clk), // 144MHz
	
	// MAC/IP address offsets
	.i_adrs_offset_mac_48b  (w_adrs_offset_mac_48b), // input  wire [47:0]  
	.i_adrs_offset_ip_32b   (w_adrs_offset_ip_32b ), // input  wire [31:0]  
	// LAN timeout setup
	.i_offset_lan_timeout_rtr_16b  (w_offset_lan_timeout_rtr_16b), // input  wire [15:0]
	.i_offset_lan_timeout_rcr_16b  (w_offset_lan_timeout_rcr_16b), // input  wire [15:0]
	
	//}

	// Wire In 		0x00 - 0x1F  //{
	.ep00wire (w_port_wi_00_1), // output wire [31:0]
	.ep01wire (w_port_wi_01_1), // output wire [31:0]
	.ep02wire (w_port_wi_02_1), // output wire [31:0]
	.ep03wire (w_port_wi_03_1), // output wire [31:0]
	.ep04wire (w_port_wi_04_1), // output wire [31:0]
	.ep05wire (w_port_wi_05_1), // output wire [31:0]
	.ep06wire (w_port_wi_06_1), // output wire [31:0]
	.ep07wire (w_port_wi_07_1), // output wire [31:0]
	.ep08wire (w_port_wi_08_1), // output wire [31:0]
	.ep09wire (w_port_wi_09_1), // output wire [31:0]
	.ep0Awire (w_port_wi_0A_1), // output wire [31:0]
	.ep0Bwire (w_port_wi_0B_1), // output wire [31:0]
	.ep0Cwire (w_port_wi_0C_1), // output wire [31:0]
	.ep0Dwire (w_port_wi_0D_1), // output wire [31:0]
	.ep0Ewire (w_port_wi_0E_1), // output wire [31:0]
	.ep0Fwire (w_port_wi_0F_1), // output wire [31:0]
	.ep10wire (w_port_wi_10_1), // output wire [31:0]
	.ep11wire (w_port_wi_11_1), // output wire [31:0]
	.ep12wire (w_port_wi_12_1), // output wire [31:0]
	.ep13wire (w_port_wi_13_1), // output wire [31:0]
	.ep14wire (w_port_wi_14_1), // output wire [31:0]
	.ep15wire (w_port_wi_15_1), // output wire [31:0]
	.ep16wire (w_port_wi_16_1), // output wire [31:0]
	.ep17wire (w_port_wi_17_1), // output wire [31:0]
	.ep18wire (w_port_wi_18_1), // output wire [31:0]
	.ep19wire (w_port_wi_19_1), // output wire [31:0]
	.ep1Awire (w_port_wi_1A_1), // output wire [31:0]
	.ep1Bwire (w_port_wi_1B_1), // output wire [31:0]
	.ep1Cwire (w_port_wi_1C_1), // output wire [31:0]
	.ep1Dwire (w_port_wi_1D_1), // output wire [31:0]
	.ep1Ewire (w_port_wi_1E_1), // output wire [31:0]
	.ep1Fwire (w_port_wi_1F_1), // output wire [31:0]
	//}
	
	// Wire Out 	0x20 - 0x3F //{
	.ep20wire (w_port_wo_20_1), // input wire [31:0]
	.ep21wire (w_port_wo_21_1), // input wire [31:0]
	.ep22wire (w_port_wo_22_1), // input wire [31:0]
	.ep23wire (w_port_wo_23_1), // input wire [31:0]
	.ep24wire (w_port_wo_24_1), // input wire [31:0]
	.ep25wire (w_port_wo_25_1), // input wire [31:0]
	.ep26wire (w_port_wo_26_1), // input wire [31:0]
	.ep27wire (w_port_wo_27_1), // input wire [31:0]
	.ep28wire (w_port_wo_28_1), // input wire [31:0]
	.ep29wire (w_port_wo_29_1), // input wire [31:0]
	.ep2Awire (w_port_wo_2A_1), // input wire [31:0]
	.ep2Bwire (w_port_wo_2B_1), // input wire [31:0]
	.ep2Cwire (w_port_wo_2C_1), // input wire [31:0]
	.ep2Dwire (w_port_wo_2D_1), // input wire [31:0]
	.ep2Ewire (w_port_wo_2E_1), // input wire [31:0]
	.ep2Fwire (w_port_wo_2F_1), // input wire [31:0]
	.ep30wire (w_port_wo_30_1), // input wire [31:0]
	.ep31wire (w_port_wo_31_1), // input wire [31:0]
	.ep32wire (w_port_wo_32_1), // input wire [31:0]
	.ep33wire (w_port_wo_33_1), // input wire [31:0]
	.ep34wire (w_port_wo_34_1), // input wire [31:0]
	.ep35wire (w_port_wo_35_1), // input wire [31:0]
	.ep36wire (w_port_wo_36_1), // input wire [31:0]
	.ep37wire (w_port_wo_37_1), // input wire [31:0]
	.ep38wire (w_port_wo_38_1), // input wire [31:0]
	.ep39wire (w_port_wo_39_1), // input wire [31:0]
	.ep3Awire (w_port_wo_3A_1), // input wire [31:0]
	.ep3Bwire (w_port_wo_3B_1), // input wire [31:0]
	.ep3Cwire (w_port_wo_3C_1), // input wire [31:0]
	.ep3Dwire (w_port_wo_3D_1), // input wire [31:0]
	.ep3Ewire (w_port_wo_3E_1), // input wire [31:0]
	.ep3Fwire (w_port_wo_3F_1), // input wire [31:0]
	//}
	
	// Trigger In 	0x40 - 0x5F //{
	.ep40ck (w_ck_40_1), .ep40trig (w_port_ti_40_1), // input wire, output wire [31:0],
	.ep41ck (w_ck_41_1), .ep41trig (w_port_ti_41_1), // input wire, output wire [31:0],
	.ep42ck (1'b0),      .ep42trig (), // input wire, output wire [31:0],
	.ep43ck (1'b0),      .ep43trig (), // input wire, output wire [31:0],
	.ep44ck (1'b0),      .ep44trig (), // input wire, output wire [31:0],
	.ep45ck (1'b0),      .ep45trig (), // input wire, output wire [31:0],
	.ep46ck (w_ck_46_1), .ep46trig (w_port_ti_46_1), // input wire, output wire [31:0],
	.ep47ck (1'b0),      .ep47trig (), // input wire, output wire [31:0],
	.ep48ck (1'b0),      .ep48trig (), // input wire, output wire [31:0],
	.ep49ck (1'b0),      .ep49trig (), // input wire, output wire [31:0],
	.ep4Ack (w_ck_4A_1), .ep4Atrig (w_port_ti_4A_1), // input wire, output wire [31:0],
	.ep4Bck (1'b0),      .ep4Btrig (), // input wire, output wire [31:0],
	.ep4Cck (1'b0),      .ep4Ctrig (), // input wire, output wire [31:0],
	.ep4Dck (1'b0),      .ep4Dtrig (), // input wire, output wire [31:0],
	.ep4Eck (1'b0),      .ep4Etrig (), // input wire, output wire [31:0],
	.ep4Fck (1'b0),      .ep4Ftrig (), // input wire, output wire [31:0],
	.ep50ck (w_ck_50_1), .ep50trig (w_port_ti_50_1), // input wire, output wire [31:0],
	.ep51ck (w_ck_51_1), .ep51trig (w_port_ti_51_1), // input wire, output wire [31:0],
	.ep52ck (1'b0),      .ep52trig (), // input wire, output wire [31:0],
	.ep53ck (w_ck_53_1), .ep53trig (w_port_ti_53_1), // input wire, output wire [31:0],
	.ep54ck (1'b0),      .ep54trig (), // input wire, output wire [31:0],
	.ep55ck (1'b0),      .ep55trig (), // input wire, output wire [31:0],
	.ep56ck (1'b0),      .ep56trig (), // input wire, output wire [31:0],
	.ep57ck (1'b0),      .ep57trig (), // input wire, output wire [31:0],
	.ep58ck (w_ck_58_1), .ep58trig (w_port_ti_58_1), // input wire, output wire [31:0],
	.ep59ck (1'b0),      .ep59trig (), // input wire, output wire [31:0],
	.ep5Ack (1'b0),      .ep5Atrig (), // input wire, output wire [31:0],
	.ep5Bck (w_ck_5B_1), .ep5Btrig (w_port_ti_5B_1), // input wire, output wire [31:0],
	.ep5Cck (1'b0),      .ep5Ctrig (), // input wire, output wire [31:0],
	.ep5Dck (1'b0),      .ep5Dtrig (), // input wire, output wire [31:0],
	.ep5Eck (1'b0),      .ep5Etrig (), // input wire, output wire [31:0],
	.ep5Fck (1'b0),      .ep5Ftrig (), // input wire, output wire [31:0],
	//}
	
	// Trigger Out 	0x60 - 0x7F //{
	.ep60ck (w_ck_60_1), .ep60trig (w_port_to_60_1), // input wire, input wire [31:0],
	.ep61ck (1'b0),      .ep61trig (32'b0), // input wire, input wire [31:0],
	.ep62ck (1'b0),      .ep62trig (32'b0), // input wire, input wire [31:0],
	.ep63ck (1'b0),      .ep63trig (32'b0), // input wire, input wire [31:0],
	.ep64ck (1'b0),      .ep64trig (32'b0), // input wire, input wire [31:0],
	.ep65ck (1'b0),      .ep65trig (32'b0), // input wire, input wire [31:0],
	.ep66ck (1'b0),      .ep66trig (32'b0), // input wire, input wire [31:0],
	.ep67ck (1'b0),      .ep67trig (32'b0), // input wire, input wire [31:0],
	.ep68ck (1'b0),      .ep68trig (32'b0), // input wire, input wire [31:0],
	.ep69ck (1'b0),      .ep69trig (32'b0), // input wire, input wire [31:0],
	.ep6Ack (1'b0),      .ep6Atrig (32'b0), // input wire, input wire [31:0],
	.ep6Bck (1'b0),      .ep6Btrig (32'b0), // input wire, input wire [31:0],
	.ep6Cck (1'b0),      .ep6Ctrig (32'b0), // input wire, input wire [31:0],
	.ep6Dck (1'b0),      .ep6Dtrig (32'b0), // input wire, input wire [31:0],
	.ep6Eck (1'b0),      .ep6Etrig (32'b0), // input wire, input wire [31:0],
	.ep6Fck (1'b0),      .ep6Ftrig (32'b0), // input wire, input wire [31:0],
	.ep70ck (w_ck_70_1), .ep70trig (w_port_to_70_1), // input wire, input wire [31:0],
	.ep71ck (w_ck_71_1), .ep71trig (w_port_to_71_1), // input wire, input wire [31:0],
	.ep72ck (1'b0),      .ep72trig (32'b0), // input wire, input wire [31:0],
	.ep73ck (w_ck_73_1), .ep73trig (w_port_to_73_1), // input wire, input wire [31:0],
	.ep74ck (1'b0),      .ep74trig (32'b0), // input wire, input wire [31:0],
	.ep75ck (1'b0),      .ep75trig (32'b0), // input wire, input wire [31:0],
	.ep76ck (1'b0),      .ep76trig (32'b0), // input wire, input wire [31:0],
	.ep77ck (1'b0),      .ep77trig (32'b0), // input wire, input wire [31:0],
	.ep78ck (w_ck_78_1), .ep78trig (w_port_to_78_1), // input wire, input wire [31:0],
	.ep79ck (1'b0),      .ep79trig (32'b0), // input wire, input wire [31:0],
	.ep7Ack (1'b0),      .ep7Atrig (32'b0), // input wire, input wire [31:0],
	.ep7Bck (w_ck_7B_1), .ep7Btrig (w_port_to_7B_1), // input wire, input wire [31:0],
	.ep7Cck (1'b0),      .ep7Ctrig (32'b0), // input wire, input wire [31:0],
	.ep7Dck (1'b0),      .ep7Dtrig (32'b0), // input wire, input wire [31:0],
	.ep7Eck (1'b0),      .ep7Etrig (32'b0), // input wire, input wire [31:0],
	.ep7Fck (1'b0),      .ep7Ftrig (32'b0), // input wire, input wire [31:0],
	//}
	
	// Pipe In 		0x80 - 0x9F //{
	.ep80wr (),          .ep80pipe (), // output wire, output wire [31:0],
	.ep81wr (),          .ep81pipe (), // output wire, output wire [31:0],
	.ep82wr (),          .ep82pipe (), // output wire, output wire [31:0],
	.ep83wr (),          .ep83pipe (), // output wire, output wire [31:0],
	.ep84wr (),          .ep84pipe (), // output wire, output wire [31:0],
	.ep85wr (),          .ep85pipe (), // output wire, output wire [31:0],
	.ep86wr (),          .ep86pipe (), // output wire, output wire [31:0],
	.ep87wr (),          .ep87pipe (), // output wire, output wire [31:0],
	.ep88wr (),          .ep88pipe (), // output wire, output wire [31:0],
	.ep89wr (),          .ep89pipe (), // output wire, output wire [31:0],
	.ep8Awr (w_wr_8A_1), .ep8Apipe (w_port_pi_8A_1), // output wire, output wire [31:0],
	.ep8Bwr (),          .ep8Bpipe (), // output wire, output wire [31:0],
	.ep8Cwr (),          .ep8Cpipe (), // output wire, output wire [31:0],
	.ep8Dwr (),          .ep8Dpipe (), // output wire, output wire [31:0],
	.ep8Ewr (),          .ep8Epipe (), // output wire, output wire [31:0],
	.ep8Fwr (),          .ep8Fpipe (), // output wire, output wire [31:0],
	.ep90wr (),          .ep90pipe (), // output wire, output wire [31:0],
	.ep91wr (),          .ep91pipe (), // output wire, output wire [31:0],
	.ep92wr (),          .ep92pipe (), // output wire, output wire [31:0],
	.ep93wr (w_wr_93_1), .ep93pipe (w_port_pi_93_1), // output wire, output wire [31:0],
	.ep94wr (),          .ep94pipe (), // output wire, output wire [31:0],
	.ep95wr (),          .ep95pipe (), // output wire, output wire [31:0],
	.ep96wr (),          .ep96pipe (), // output wire, output wire [31:0],
	.ep97wr (),          .ep97pipe (), // output wire, output wire [31:0],
	.ep98wr (),          .ep98pipe (), // output wire, output wire [31:0],
	.ep99wr (),          .ep99pipe (), // output wire, output wire [31:0],
	.ep9Awr (),          .ep9Apipe (), // output wire, output wire [31:0],
	.ep9Bwr (),          .ep9Bpipe (), // output wire, output wire [31:0],
	.ep9Cwr (),          .ep9Cpipe (), // output wire, output wire [31:0],
	.ep9Dwr (),          .ep9Dpipe (), // output wire, output wire [31:0],
	.ep9Ewr (),          .ep9Epipe (), // output wire, output wire [31:0],
	.ep9Fwr (),          .ep9Fpipe (), // output wire, output wire [31:0],
	//}
	
	// Pipe Out 	0xA0 - 0xBF //{
	.epA0rd (), .epA0pipe (32'b0), // output wire, input wire [31:0],
	.epA1rd (), .epA1pipe (32'b0), // output wire, input wire [31:0],
	.epA2rd (), .epA2pipe (32'b0), // output wire, input wire [31:0],
	.epA3rd (), .epA3pipe (32'b0), // output wire, input wire [31:0],
	.epA4rd (), .epA4pipe (32'b0), // output wire, input wire [31:0],
	.epA5rd (), .epA5pipe (32'b0), // output wire, input wire [31:0],
	.epA6rd (), .epA6pipe (32'b0), // output wire, input wire [31:0],
	.epA7rd (), .epA7pipe (32'b0), // output wire, input wire [31:0],
	.epA8rd (), .epA8pipe (32'b0), // output wire, input wire [31:0],
	.epA9rd (), .epA9pipe (32'b0), // output wire, input wire [31:0],
	.epAArd (w_rd_AA_1), .epAApipe(w_port_po_AA_1), // output wire, input wire [31:0],
	.epABrd (), .epABpipe (32'b0), // output wire, input wire [31:0],
	.epACrd (), .epACpipe (32'b0), // output wire, input wire [31:0],
	.epADrd (), .epADpipe (32'b0), // output wire, input wire [31:0],
	.epAErd (), .epAEpipe (32'b0), // output wire, input wire [31:0],
	.epAFrd (), .epAFpipe (32'b0), // output wire, input wire [31:0],
	.epB0rd (), .epB0pipe (32'b0), // output wire, input wire [31:0],
	.epB1rd (), .epB1pipe (32'b0), // output wire, input wire [31:0],
	.epB2rd (), .epB2pipe (32'b0), // output wire, input wire [31:0],
	.epB3rd (w_rd_B3_1), .epB3pipe (w_port_po_B3_1), // output wire, input wire [31:0],
	.epB4rd (), .epB4pipe (32'b0), // output wire, input wire [31:0],
	.epB5rd (), .epB5pipe (32'b0), // output wire, input wire [31:0],
	.epB6rd (), .epB6pipe (32'b0), // output wire, input wire [31:0],
	.epB7rd (), .epB7pipe (32'b0), // output wire, input wire [31:0],
	.epB8rd (), .epB8pipe (32'b0), // output wire, input wire [31:0],
	.epB9rd (), .epB9pipe (32'b0), // output wire, input wire [31:0],
	.epBArd (), .epBApipe (32'b0), // output wire, input wire [31:0],
	.epBBrd (), .epBBpipe (32'b0), // output wire, input wire [31:0],
	.epBCrd (w_rd_BC_1), .epBCpipe (w_port_po_BC_1), // output wire, input wire [31:0],
	.epBDrd (w_rd_BD_1), .epBDpipe (w_port_po_BD_1), // output wire, input wire [31:0],
	.epBErd (w_rd_BE_1), .epBEpipe (w_port_po_BE_1), // output wire, input wire [31:0],
	.epBFrd (w_rd_BF_1), .epBFpipe (w_port_po_BF_1), // output wire, input wire [31:0],
	//}
	
	// Pipe clock output //{
	.epPPck (w_ck_pipe) //output wire // sync with write/read of pipe // 72MHz
	//}
	);

//}

// assign //{

assign w_adrs_offset_mac_48b[47:16] = {8'h00,8'h00,8'h00,8'h00}; // assign high 32b
assign w_adrs_offset_ip_32b [31:16] = {8'h00,8'h00}            ; // assign high 16b
assign w_offset_lan_timeout_rtr_16b = 16'd0;
assign w_offset_lan_timeout_rcr_16b = 16'd0;

//}

////

//}


/* TODO: TEST FIFO */ //{
// emulate LAN-fifo from/to ADC-fifo
// test-place ADC-fifo with TEST-fifo, which can be read and written.
//
// fifo_generator_4 : test
// 32 bits
// 4096 depth = 2^12
// 2^12 * 4 byte = 16KB
		
fifo_generator_4 TEST_fifo_inst (
  //.rst       (~reset_n | ~w_LAN_RSTn | w_FIFO_reset), // input wire rst
  .rst       (~reset_n), // input wire rst
  .wr_clk    (mcs_clk),  // input wire wr_clk
  .wr_en     (w_wr_8A_1),      // input wire wr_en
  .din       (w_port_pi_8A_1), // input wire [31 : 0] din
  .rd_clk    (mcs_clk),  // input wire rd_clk
  .rd_en     (w_rd_AA_1),      // input wire rd_en
  .dout      (w_port_po_AA_1), // output wire [31 : 0] dout
  .full      (),  // output wire full
  .wr_ack    (),  // output wire wr_ack
  .empty     (),  // output wire empty
  .valid     ()   // output wire valid
);

//}


///TODO: //-------------------------------------------------------//



/* TODO: mapping endpoints to signals for CMU-CPU board */ //{


//// BRD_CON //{

// board control master
wire [31:0] w_BRD_CON = ep03wire | w_port_wi_03_1; //$$
// bit[0]=HW_reset
// bit[1]=rst_adc  
// bit[2]=rst_dwave
// bit[3]=rst_bias 
// bit[4]=rst_spo  
// ...
// bit[8]=mcs_ep_po_en
// bit[9]=mcs_ep_pi_en
// bit[10]=mcs_ep_to_en
// bit[11]=mcs_ep_ti_en
// bit[12]=mcs_ep_wo_en
// bit[13]=mcs_ep_wi_en
// ...
// bit[16]=time_stamp_disp_en

// sub wires 
wire w_HW_reset              = w_BRD_CON[0];
wire w_time_stamp_disp_en    = w_BRD_CON[16];
// new LAN selection : w_select__L_LAN_on_FPGA_MD__H_LAN_on_BASE_BD
//$$ wire w_sel__H_LAN_on_BASE_BD = w_BRD_CON[24]; 



//  //// CMU endpoint switch  
//  
//  // enable MCS control over CMU
//  	//"={11'b0, rst_adc, rst_dwave, rst_bias, rst_spo, rst_mcs_ep, 
//  	//	 10'b0, po_en, pi_en, to_en, ti_en , wo_en, wi_en}"
//  
//  //$$ w_BRD_CON or ep03wire // vs wi03 from LAN 

// reset wires 
wire w_rst_adc      = w_BRD_CON[1]; 
wire w_rst_dwave    = w_BRD_CON[2]; 
wire w_rst_bias     = w_BRD_CON[3]; 
wire w_rst_spo      = w_BRD_CON[4]; 
//  wire w_rst_mcs_ep   = w_BRD_CON[]; 

// endpoint mux enable : LAN(MCS) vs USB
wire w_mcs_ep_po_en = w_BRD_CON[ 8]; 
wire w_mcs_ep_pi_en = w_BRD_CON[ 9]; 
wire w_mcs_ep_to_en = w_BRD_CON[10]; 
wire w_mcs_ep_ti_en = w_BRD_CON[11];  
wire w_mcs_ep_wo_en = w_BRD_CON[12]; 
wire w_mcs_ep_wi_en = w_BRD_CON[13]; 

//  //
//  
//  // MCS1 sw reset 
//  assign reset_sw_mcs1_n = ~w_rst_mcs_ep;
//  
//  

//}


//// MCS_SETUP_WI //{

// MCS control 
wire [31:0] w_MCS_SETUP_WI = w_port_wi_19_1; //$$ dedicated to MCS. updated by MCS boot-up.
// bit[3:0]=slot_id, range of 00~99, set from EEPROM via MCS
// ...
// bit[8]=sel__H_LAN_for_EEPROM_fifo (or USB)
// bit[9]=sel__H_EEPROM_on_TP (or on Base)
// bit[10]=sel__H_LAN_on_BASE_BD (or on module)
// ...
// bit[31:16]=board_id, range of 0000~9999, set from EEPROM via MCS


wire [3:0]  w_slot_id             = w_MCS_SETUP_WI[3:0];
wire w_sel__H_LAN_for_EEPROM_fifo = w_MCS_SETUP_WI[8];
wire w_sel__H_EEPROM_on_TP        = w_MCS_SETUP_WI[9];
wire w_sel__H_LAN_on_BASE_BD      = w_MCS_SETUP_WI[10];
wire [15:0] w_board_id            = w_MCS_SETUP_WI[31:16];

// for dedicated LAN setup from MCS
assign w_adrs_offset_mac_48b[15:0] = {8'h00, 4'h0, w_slot_id}; // assign low 16b
assign w_adrs_offset_ip_32b [15:0] = {8'h00, 4'h0, w_slot_id}; // assign low 16b

//}


//// TEST wires //{

// count and test control
wire [31:0] w_TEST_CON = (w_mcs_ep_wi_en)? w_port_wi_01_1 : ep01wire;;
// bit[0] = count reset1  
// bit[1] = count disable1
// bit[2] = count autocount2
// bit[3] = disable__SPO_DAC12B_PINSWAP
// bit[5] = disable__dwave_warmup
// (bit[6]|~bit[4]) = disable__dwave2_pin_swap
// bit[7] = enable__pulse_loopback
// bit[15:8] = set__pulse_num[7:0]
// bit[23:16] = set__pulse_width[7:0]
// bit[31:24] = set__pulse_period[7:0]

wire [31:0] w_TEST_OUT;  assign ep21wire = w_TEST_OUT;
// bit[7:0] = count1[7:0]
// bit[15:8] = count2[7:0]
// bit[31:16] = r_cnt__pulse_loopback[15:0]
assign w_port_wo_21_1 = (w_mcs_ep_wo_en)? w_TEST_OUT : 32'hACAC_ACAC; //$$

wire [31:0] w_TEST_TI = (w_mcs_ep_ti_en)? w_port_ti_40_1 : ep40trig; 

wire [31:0] w_TEST_TO;  assign ep60trig = w_TEST_TO;
// ={15'b0, count2eqFF, 14'b0, count1eq80, count1eq00}
assign w_port_to_60_1 = (w_mcs_ep_to_en)? w_TEST_TO : 32'h0; //$$


// pulse loopback test
wire [31:0] w_TEST_TI_HS = (w_mcs_ep_ti_en)? w_port_ti_41_1 : ep41trig;

//}


//// SPIO wires //{

// SPIO_WI
wire [31:0] w_SPIO_CON_WI  = (w_mcs_ep_wi_en)? w_port_wi_1B_1 : ep1Bwire;
// bit[0]=SPIO_enable
// bit[4]=pin_swap_CSB_MOSI
// bit[8]=mode_forced_pin_enable //$$ for PGU interface
// bit[9]=forced_sig_MOSI
// bit[10]=forced_sig_SCLK
// bit[11]=forced_sig_CS
// bit[18:16]=CS_en[2:0]
// bit[31:24]=socket_en[7:0]

// SPIO_FDAT_WI
wire [31:0] w_SPIO_FDAT_WI = (w_mcs_ep_wi_en)? w_port_wi_1A_1 : ep1Awire;
// bit[27:25]=pin_address[2:0]
// bit[24]=read/write_bar
// bit[23:16]=reg_address[7:0]
// bit[15: 8]=GPA[7:0]
// bit[ 7: 0]=GPB[7:0]

// SPIO_WO
wire [31:0] w_SPIO_FLAG_WO; //$$ share with w_XADC_VOLT
// bit[26]  =frame_busy
// bit[25]  =frame_done
// bit[15:8]=readback_DA
// bit[7:0] =readback_DB

// SPIO_TI
wire [31:0] w_SPIO_TRIG_TI = (w_mcs_ep_ti_en)? w_port_ti_5B_1 : ep5Btrig;
// bit[0]=reset_trig
// bit[1]=spi_frame_trig

// SPIO_TO
wire [31:0] w_SPIO_TRIG_TO;
// bit[1]=spi_frame_done
assign ep7Btrig       = (~w_mcs_ep_to_en)? w_SPIO_TRIG_TO : 32'h0;
assign w_port_to_7B_1 = ( w_mcs_ep_to_en)? w_SPIO_TRIG_TO : 32'h0; //$$

// SPIO controls
wire w_SPIO_en = w_SPIO_CON_WI[0] & (~w_SPIO_TRIG_TI[0]); //$$
wire w_pin_swap_CSB_MOSI = w_SPIO_CON_WI[4];


//}

////

//}



//-------------------------------------------------------//

/* TODO: check IDs */ //{
//wire [31:0] w_SW_BUILD_ID = ep00wire; // switch with w_port_wi_00_1
wire [31:0] w_SW_BUILD_ID = (w_mcs_ep_wi_en)? w_port_wi_00_1 : ep00wire;
//
wire [31:0] w_FPGA_IMAGE_ID = 
				(w_SW_BUILD_ID==REQ_SW_BUILD_ID)? FPGA_IMAGE_ID : 
				(w_SW_BUILD_ID==32'b0          )? FPGA_IMAGE_ID : 
				32'b0 ;
//
assign ep20wire = (!w_mcs_ep_wo_en)? w_FPGA_IMAGE_ID : 32'hACAC_ACAC;
//
assign w_port_wo_20_1 = (w_mcs_ep_wo_en)? w_FPGA_IMAGE_ID : 32'hACAC_ACAC;

//}


/* TODO: TEST_CC */ //{
//
//wire [31:0] w_TEST_CC_DIN = ep02wire;
wire [31:0] w_TEST_CC_DIN = (w_mcs_ep_wi_en)? w_port_wi_02_1 : ep02wire;
wire [31:0] w_TEST_CC_MON;
assign ep22wire = w_TEST_CC_MON;
assign w_port_wo_22_1 = (w_mcs_ep_wo_en)? w_TEST_CC_MON : 32'hACAC_ACAC;
// wire names for monitoring
wire CTL_SPO_CDAT0; // CC0
wire CTL_SPO_EN0  ; // CC1
wire CTL_SPO_CDAT1; // CC2
wire CTL_SPO_EN1  ; // CC3
wire CTL_SPO_CDAT2; // CC4
wire CTL_SPO_EN2  ; // CC5
wire CTL_SPO_CDAT3; // CC6
wire CTL_SPO_EN3  ; // CC7
//
wire RB3  ; //$$ i_B13_L1P;       // CC12
wire RB2  ; //$$ i_B35_L6N;       // CC13
wire RB1  ; //$$ i_B35_L19P;      // CC14
wire UNBAL; //$$ i_B35_L19N;      // CC15
wire D_R  ; //$$ i_B13_L6P;       // CC16
wire C_R  ; //$$ i_B13_L6N;       // CC17
wire B_R  ; //$$ i_B13_L11N_SRCC; // CC18
wire A_R  ; //$$ i_B13_L11P_SRCC; // CC19
wire D_D  ; //$$ i_B34_L6P;       // CC20
wire C_D  ; //$$ i_B34_L6N;       // CC21
wire B_D  ; //$$ i_B34_L8P;       // CC22
wire A_D  ; //$$ i_B34_L8N;       // CC23
//
assign w_TEST_CC_MON[0] = CTL_SPO_CDAT0; // CC0
assign w_TEST_CC_MON[1] = CTL_SPO_EN0  ; // CC1
assign w_TEST_CC_MON[2] = CTL_SPO_CDAT1; // CC2
assign w_TEST_CC_MON[3] = CTL_SPO_EN1  ; // CC3
assign w_TEST_CC_MON[4] = CTL_SPO_CDAT2; // CC4
assign w_TEST_CC_MON[5] = CTL_SPO_EN2  ; // CC5
assign w_TEST_CC_MON[6] = CTL_SPO_CDAT3; // CC6
assign w_TEST_CC_MON[7] = CTL_SPO_EN3  ; // CC7
//
assign w_TEST_CC_MON[11:8] = 4'b0;
//
assign w_TEST_CC_MON[12] = RB3  ; //$$ i_B13_L1P;       // CC12
assign w_TEST_CC_MON[13] = RB2  ; //$$ i_B35_L6N;       // CC13
assign w_TEST_CC_MON[14] = RB1  ; //$$ i_B35_L19P;      // CC14
assign w_TEST_CC_MON[15] = UNBAL; //$$ i_B35_L19N;      // CC15
assign w_TEST_CC_MON[16] = D_R  ; //$$ i_B13_L6P;       // CC16
assign w_TEST_CC_MON[17] = C_R  ; //$$ i_B13_L6N;       // CC17
assign w_TEST_CC_MON[18] = B_R  ; //$$ i_B13_L11N_SRCC; // CC18
assign w_TEST_CC_MON[19] = A_R  ; //$$ i_B13_L11P_SRCC; // CC19
assign w_TEST_CC_MON[20] = D_D  ; //$$ i_B34_L6P;       // CC20
assign w_TEST_CC_MON[21] = C_D  ; //$$ i_B34_L6N;       // CC21
assign w_TEST_CC_MON[22] = B_D  ; //$$ i_B34_L8P;       // CC22
assign w_TEST_CC_MON[23] = A_D  ; //$$ i_B34_L8N;       // CC23
//
assign w_TEST_CC_MON[31:24] = 8'b0;
////

//}


/* TODO: DWAVE */ //{

//wire [31:0] w_DWAVE_DIN_BY_TRIG  = ep05wire;
wire [31:0] w_DWAVE_DIN_BY_TRIG  = (w_mcs_ep_wi_en)? w_port_wi_05_1 : ep05wire;
//wire [31:0] w_DWAVE_CON        = ep06wire;
wire [31:0] w_DWAVE_CON          = (w_mcs_ep_wi_en)? w_port_wi_06_1 : ep06wire;
//
wire [31:0] w_DWAVE_DOUT_BY_TRIG;
assign ep25wire = w_DWAVE_DOUT_BY_TRIG;
assign w_port_wo_25_1 = (w_mcs_ep_wo_en)? w_DWAVE_DOUT_BY_TRIG : 32'hACAC_ACAC;
//
wire [31:0] w_DWAVE_FLAG;
assign ep26wire = w_DWAVE_FLAG;
assign w_port_wo_26_1 = (w_mcs_ep_wo_en)? w_DWAVE_FLAG : 32'hACAC_ACAC;
//
wire [31:0] w_DWAVE_BASE_FREQ = DWAVE_BASE_FREQ;
assign ep23wire = w_DWAVE_BASE_FREQ;
assign w_port_wo_23_1 = (w_mcs_ep_wo_en)? w_DWAVE_BASE_FREQ : 32'hACAC_ACAC;
//
//wire [31:0] w_DWAVE_TI         = ep46trig;
wire [31:0] w_DWAVE_TI = (w_mcs_ep_ti_en)? w_port_ti_46_1 : ep46trig;
//
wire dwave_en 						= w_DWAVE_CON[0];
wire dwave_init 					= w_DWAVE_CON[1];
wire dwave_update 					= w_DWAVE_CON[2];
wire dwave_test 					= w_DWAVE_CON[3];
wire [1:0] dwave_test_datain_type	= w_DWAVE_CON[5:4];
wire [1:0] dwave_test_datain_port	= w_DWAVE_CON[7:6];
wire [31:0] dwave_test_datain		= {16'b0, w_DWAVE_CON[31:16]};
//
wire dwave_init_done;
wire dwave_update_done;
wire dwave_test_done;
//
assign w_DWAVE_FLAG = {28'b0,dwave_test_done,dwave_update_done,dwave_init_done,dwave_en};
// pulse outputs 
(* keep = "true" *) wire dwave_w1f1;
(* keep = "true" *) wire dwave_w1f2;
(* keep = "true" *) wire dwave_w1f3;
(* keep = "true" *) wire dwave_w2f1;
(* keep = "true" *) wire dwave_w2f2;
(* keep = "true" *) wire dwave_w2f3;
// 
dwave_control dwave_control_inst(
	.reset_n(reset_n & ~w_rst_dwave),
	.clk(sys_clk), // assume 10MHz or 100ns
	.en			(dwave_en),
	//
	.clk_dwave(base_dwave_clk), // assume 160MHz 
	//
	.init		(dwave_init),
	.update		(dwave_update),
	.test		(dwave_test),
	//
	.test_datain		(dwave_test_datain), 
	.test_datain_type	(dwave_test_datain_type),
	.test_datain_port	(dwave_test_datain_port),
	// trig interface 
	.i_trig_pulse_off      (w_DWAVE_TI[0]),
	.i_trig_pulse_on_cont  (w_DWAVE_TI[1]),
	.i_trig_pulse_on_num   (w_DWAVE_TI[2]),
	.i_trig_set_parameters (w_DWAVE_TI[3]),
	.i_wire_datain_by_trig   (w_DWAVE_DIN_BY_TRIG), // 32b
	.o_wire_dataout_by_trig  (w_DWAVE_DOUT_BY_TRIG), // 32b
	.i_trig_wr_cnt_period  (w_DWAVE_TI[16]),
	.i_trig_rd_cnt_period  (w_DWAVE_TI[24]),
	.i_trig_wr_cnt_diff    (w_DWAVE_TI[17]),
	.i_trig_rd_cnt_diff    (w_DWAVE_TI[25]),
	.i_trig_wr_num_pulses  (w_DWAVE_TI[18]),
	.i_trig_rd_num_pulses  (w_DWAVE_TI[26]),
	// i_phase disable // q_phase disable  
	// {f4,f3,f2,f1}
	//   f1, f2 ... i_phase ... for disable ... 0x3 ..3
	//   f3, f4 ... q_phase ... for disable ... 0xC ..12
	.i_trig_wr_output_dis  (w_DWAVE_TI[19]), 
	.i_trig_rd_output_dis  (w_DWAVE_TI[27]),
	// pulse out
	.pulse_out_f1_ref	(dwave_w1f2), //
	.pulse_out_f1_ref_n	(),
	.pulse_out_f1		(dwave_w1f1), //
	.pulse_out_f1_n		(),
	.pulse_out_f2_ref	(),
	.pulse_out_f2_ref_n	(),
	.pulse_out_f2		(dwave_w1f3), //
	.pulse_out_f2_n		(),
	.pulse_out_f3_ref	(dwave_w2f2), //
	.pulse_out_f3_ref_n	(),
	.pulse_out_f3		(dwave_w2f1), //
	.pulse_out_f3_n		(),
	.pulse_out_f4_ref	(),
	.pulse_out_f4_ref_n	(),
	.pulse_out_f4		(dwave_w2f3), //
	.pulse_out_f4_n		(),
	//
	.init_done			(dwave_init_done),
	.update_done		(dwave_update_done),
	.test_done			(dwave_test_done),
	.error(),
	.debug_out()
);
////

//}


/* TODO: TIMESTAMP */ //{

// global time index in debugger based on 10MHz 

// module //{
(* keep = "true" *) wire [31:0] w_timestamp;
//
sub_timestamp sub_timestamp_inst(
	.clk         (sys_clk),
	.reset_n     (reset_n & (~w_HW_reset)),
	.o_timestamp (w_timestamp),
	.valid       ()
);
//}

//}


/* TODO: XADC */  //{
//
wire [31:0] w_XADC_TEMP;
assign ep3Awire = w_XADC_TEMP; //$$ shared with time-stamp
assign w_port_wo_3A_1 = (w_mcs_ep_wo_en)? w_XADC_TEMP : 32'hACAC_ACAC;
//
wire [31:0] w_XADC_VOLT;
assign ep3Bwire = w_XADC_VOLT; //$$ shared with [SPIO] SPIO_WO
assign w_port_wo_3B_1 = (w_mcs_ep_wo_en)? w_XADC_VOLT : 32'hACAC_ACAC;
// XADC_DRP
wire [31:0] MEASURED_TEMP_MC;
wire [31:0] MEASURED_VCCINT_MV;
wire [31:0] MEASURED_VCCAUX_MV;
wire [31:0] MEASURED_VCCBRAM_MV;
//
(* keep = "true" *) wire [7:0] dbg_drp;

//
master_drp_ug480 master_drp_ug480_inst(
	.DCLK				(sys_clk), // input DCLK, // Clock input for DRP
	.RESET				(~reset_n), // input RESET,
	.VP					(i_XADC_VP), // input VP, VN,// Dedicated and Hardwired Analog Input Pair
	.VN					(i_XADC_VN),
	.MEASURED_TEMP		(), // output reg [15:0] MEASURED_TEMP, MEASURED_VCCINT,
	.MEASURED_VCCINT	(),
	.MEASURED_VCCAUX	(), // output reg [15:0] MEASURED_VCCAUX, MEASURED_VCCBRAM,
	.MEASURED_VCCBRAM	(),
	// converted to decimal
	.MEASURED_TEMP_MC		(MEASURED_TEMP_MC), 
	.MEASURED_VCCINT_MV		(MEASURED_VCCINT_MV),
	.MEASURED_VCCAUX_MV		(MEASURED_VCCAUX_MV), 
	.MEASURED_VCCBRAM_MV	(MEASURED_VCCBRAM_MV),
	//
	.ALM_OUT	(), // output wire ALM_OUT,
	.CHANNEL	(), // output wire [4:0] CHANNEL,
	.OT			(), // output wire OT,
	.XADC_EOC	(), // output wire XADC_EOC,
	.XADC_EOS	(), // output wire XADC_EOS
	.debug_out	(dbg_drp)
);

//
assign w_XADC_TEMP	= (w_time_stamp_disp_en)? w_timestamp : MEASURED_TEMP_MC;
//
assign w_XADC_VOLT = 
	(w_SPIO_en)? w_SPIO_FLAG_WO               :
	(count2[7:6]==2'b00)? MEASURED_VCCINT_MV  :
	(count2[7:6]==2'b01)? MEASURED_VCCAUX_MV  :
	(count2[7:6]==2'b10)? MEASURED_VCCBRAM_MV :					
	32'b0;
////

//}


/* TODO: SPO control  */ //{
//   master_spi_SN74LV8153_pio.v   
//   SN74LV8153_pio quad
//
//wire [31:0] w_SPO_CON    = ep07wire;
wire [31:0] w_SPO_CON = (w_mcs_ep_wi_en)? w_port_wi_07_1 : ep07wire;
//
//wire [63:0] w_SPO_DIN_B0 = {ep09wire, ep08wire};
//wire [63:0] w_SPO_DIN_B1 = {ep0Bwire, ep0Awire};
//wire [63:0] w_SPO_DIN_B2 = {ep0Dwire, ep0Cwire};
//wire [63:0] w_SPO_DIN_B3 = {ep0Fwire, ep0Ewire};
wire [63:0] w_SPO_DIN_B0 = (w_mcs_ep_wi_en)? {w_port_wi_09_1, w_port_wi_08_1} : {ep09wire, ep08wire};
wire [63:0] w_SPO_DIN_B1 = (w_mcs_ep_wi_en)? {w_port_wi_0B_1, w_port_wi_0A_1} : {ep0Bwire, ep0Awire};
wire [63:0] w_SPO_DIN_B2 = (w_mcs_ep_wi_en)? {w_port_wi_0D_1, w_port_wi_0C_1} : {ep0Dwire, ep0Cwire};
wire [63:0] w_SPO_DIN_B3 = (w_mcs_ep_wi_en)? {w_port_wi_0F_1, w_port_wi_0E_1} : {ep0Fwire, ep0Ewire};
//
wire [31:0] w_SPO_FLAG;
assign ep27wire = w_SPO_FLAG;
assign w_port_wo_27_1 = (w_mcs_ep_wo_en)? w_SPO_FLAG : 32'hACAC_ACAC;
//
wire [63:0] w_SPO_MON_B0;
wire [63:0] w_SPO_MON_B1;
wire [63:0] w_SPO_MON_B2;
wire [63:0] w_SPO_MON_B3;
//
assign ep28wire = w_SPO_MON_B0[31: 0];
assign ep29wire = w_SPO_MON_B0[63:32];
assign ep2Awire = w_SPO_MON_B1[31: 0];
assign ep2Bwire = w_SPO_MON_B1[63:32];
assign ep2Cwire = w_SPO_MON_B2[31: 0];
assign ep2Dwire = w_SPO_MON_B2[63:32];
assign ep2Ewire = w_SPO_MON_B3[31: 0];
assign ep2Fwire = w_SPO_MON_B3[63:32];
assign w_port_wo_28_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B0[31: 0] : 32'hACAC_ACAC;
assign w_port_wo_29_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B0[63:32] : 32'hACAC_ACAC;
assign w_port_wo_2A_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B1[31: 0] : 32'hACAC_ACAC;
assign w_port_wo_2B_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B1[63:32] : 32'hACAC_ACAC;
assign w_port_wo_2C_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B2[31: 0] : 32'hACAC_ACAC;
assign w_port_wo_2D_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B2[63:32] : 32'hACAC_ACAC;
assign w_port_wo_2E_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B3[31: 0] : 32'hACAC_ACAC;
assign w_port_wo_2F_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B3[63:32] : 32'hACAC_ACAC;
//
wire [63:0] CTL_IO_SPO_0_ctr_in = w_SPO_DIN_B0;
wire [63:0] CTL_IO_SPO_0_mon_out;
	assign w_SPO_MON_B0 = CTL_IO_SPO_0_mon_out;
//
// TODO: DAC 12 bit on SPO, pin swap test, glitch removal.
wire disable__SPO_DAC12B_PINSWAP = w_TEST_CON[3]; //$$
// 
//wire [63:0] CTL_IO_SPO_1_ctr_in = w_SPO_DIN_B1; 
wire [63:0] CTL_IO_SPO_1_ctr_in;
	assign CTL_IO_SPO_1_ctr_in[63:16] = (disable__SPO_DAC12B_PINSWAP)? w_SPO_DIN_B1[63:16] : w_SPO_DIN_B1[63:16];
	assign CTL_IO_SPO_1_ctr_in[ 7: 4] = (disable__SPO_DAC12B_PINSWAP)? w_SPO_DIN_B1[ 7: 4] : w_SPO_DIN_B1[15:12];
	assign CTL_IO_SPO_1_ctr_in[15: 8] = (disable__SPO_DAC12B_PINSWAP)? w_SPO_DIN_B1[15: 8] : w_SPO_DIN_B1[11: 4];
	assign CTL_IO_SPO_1_ctr_in[ 3: 0] = (disable__SPO_DAC12B_PINSWAP)? w_SPO_DIN_B1[ 3: 0] : w_SPO_DIN_B1[ 3: 0];
// 
wire [63:0] CTL_IO_SPO_1_mon_out;
	//assign w_SPO_MON_B1 = CTL_IO_SPO_1_mon_out;
	assign w_SPO_MON_B1[63:16] = (disable__SPO_DAC12B_PINSWAP)? CTL_IO_SPO_1_mon_out[63:16] : CTL_IO_SPO_1_mon_out[63:16];
	assign w_SPO_MON_B1[15:12] = (disable__SPO_DAC12B_PINSWAP)? CTL_IO_SPO_1_mon_out[15:12] : CTL_IO_SPO_1_mon_out[ 7: 4];
	assign w_SPO_MON_B1[11: 4] = (disable__SPO_DAC12B_PINSWAP)? CTL_IO_SPO_1_mon_out[11: 4] : CTL_IO_SPO_1_mon_out[15: 8];
	assign w_SPO_MON_B1[ 3: 0] = (disable__SPO_DAC12B_PINSWAP)? CTL_IO_SPO_1_mon_out[ 3: 0] : CTL_IO_SPO_1_mon_out[ 3: 0];
//
wire [63:0] CTL_IO_SPO_2_ctr_in = w_SPO_DIN_B2; 
wire [63:0] CTL_IO_SPO_2_mon_out;
//
	assign w_SPO_MON_B2 = CTL_IO_SPO_2_mon_out;
//
wire [63:0] CTL_IO_SPO_3_ctr_in = w_SPO_DIN_B3;
wire [63:0] CTL_IO_SPO_3_mon_out;
	assign w_SPO_MON_B3 = CTL_IO_SPO_3_mon_out;
//
wire CTL_IO_CON_en					= w_SPO_CON[0];
wire CTL_IO_CON_init 				= w_SPO_CON[1];
wire CTL_IO_CON_update 				= w_SPO_CON[2];
wire CTL_IO_CON_test 				= w_SPO_CON[3];
wire [2:0]  CTL_IO_CON_adrs_start	= w_SPO_CON[6:4];
wire [2:0]  CTL_IO_CON_num_bytes 	= w_SPO_CON[10:8];
wire [13:0] CTL_IO_CON_test_pdata	= w_SPO_CON[29:16];
//
wire CTL_IO_FLAG_SPO_0_init_done  ;
wire CTL_IO_FLAG_SPO_0_update_done;
wire CTL_IO_FLAG_SPO_0_test_done  ;
wire CTL_IO_FLAG_SPO_1_init_done  ;
wire CTL_IO_FLAG_SPO_1_update_done;
wire CTL_IO_FLAG_SPO_1_test_done  ;
wire CTL_IO_FLAG_SPO_2_init_done  ;
wire CTL_IO_FLAG_SPO_2_update_done;
wire CTL_IO_FLAG_SPO_2_test_done  ;
wire CTL_IO_FLAG_SPO_3_init_done  ;
wire CTL_IO_FLAG_SPO_3_update_done;
wire CTL_IO_FLAG_SPO_3_test_done  ;
//
wire CTL_IO_FLAG_SPO_X_init_done;
	assign CTL_IO_FLAG_SPO_X_init_done = 
		CTL_IO_FLAG_SPO_0_init_done  & 
		CTL_IO_FLAG_SPO_1_init_done  & 
		CTL_IO_FLAG_SPO_2_init_done  & 
		CTL_IO_FLAG_SPO_3_init_done  ;
wire CTL_IO_FLAG_SPO_X_update_done;
	assign CTL_IO_FLAG_SPO_X_update_done = 
		CTL_IO_FLAG_SPO_0_update_done & 
		CTL_IO_FLAG_SPO_1_update_done & 
		CTL_IO_FLAG_SPO_2_update_done & 
		CTL_IO_FLAG_SPO_3_update_done ;
wire CTL_IO_FLAG_SPO_X_test_done;
	assign CTL_IO_FLAG_SPO_X_test_done = 
		CTL_IO_FLAG_SPO_0_test_done  & 
		CTL_IO_FLAG_SPO_1_test_done  & 
		CTL_IO_FLAG_SPO_2_test_done  & 
		CTL_IO_FLAG_SPO_3_test_done  ;
//
wire CTL_IO_SPO_0_error;
wire CTL_IO_SPO_1_error;
wire CTL_IO_SPO_2_error;
wire CTL_IO_SPO_3_error;
//
assign w_SPO_FLAG = {
	{CTL_IO_FLAG_SPO_3_test_done,CTL_IO_FLAG_SPO_3_update_done,CTL_IO_FLAG_SPO_3_init_done,CTL_IO_CON_en&(~CTL_IO_SPO_3_error)},
	{CTL_IO_FLAG_SPO_2_test_done,CTL_IO_FLAG_SPO_2_update_done,CTL_IO_FLAG_SPO_2_init_done,CTL_IO_CON_en&(~CTL_IO_SPO_2_error)},
	{CTL_IO_FLAG_SPO_1_test_done,CTL_IO_FLAG_SPO_1_update_done,CTL_IO_FLAG_SPO_1_init_done,CTL_IO_CON_en&(~CTL_IO_SPO_1_error)},
	{CTL_IO_FLAG_SPO_0_test_done,CTL_IO_FLAG_SPO_0_update_done,CTL_IO_FLAG_SPO_0_init_done,CTL_IO_CON_en&(~CTL_IO_SPO_0_error)},
	4'b0,
	4'b0,
	4'b0,
	{CTL_IO_FLAG_SPO_X_test_done,CTL_IO_FLAG_SPO_X_update_done,CTL_IO_FLAG_SPO_X_init_done,CTL_IO_CON_en}
	};
// SPO_0
master_spi_SN74LV8153_pio master_spi_SN74LV8153_pio_SPO_0_inst (  
	.clk			(sys_clk),
	.reset_n		(reset_n & ~w_rst_spo),
	.en				(CTL_IO_CON_en),
	.init			(CTL_IO_CON_init),
	.update			(CTL_IO_CON_update),
	.test			(CTL_IO_CON_test),
	.adrs_start		(CTL_IO_CON_adrs_start),
	.num_bytes		(CTL_IO_CON_num_bytes),
	.test_pdata		(CTL_IO_CON_test_pdata),
	.ctr_in			(CTL_IO_SPO_0_ctr_in), //
	.mon_out		(CTL_IO_SPO_0_mon_out), //
	.spo_cdat		(CTL_SPO_CDAT0), //
	.spo_rst_n		(CTL_SPO_EN0), //
	.init_done		(CTL_IO_FLAG_SPO_0_init_done), //
	.update_done	(CTL_IO_FLAG_SPO_0_update_done), //
	.test_done		(CTL_IO_FLAG_SPO_0_test_done), //
	.error			(CTL_IO_SPO_0_error),
	.debug_out		()
);
// SPO_1
master_spi_SN74LV8153_pio master_spi_SN74LV8153_pio_SPO_1_inst (  
	.clk			(sys_clk),
	.reset_n		(reset_n & ~w_rst_spo),
	.en				(CTL_IO_CON_en),
	.init			(CTL_IO_CON_init),
	.update			(CTL_IO_CON_update),
	.test			(CTL_IO_CON_test),
	.adrs_start		(CTL_IO_CON_adrs_start),
	.num_bytes		(CTL_IO_CON_num_bytes),
	.test_pdata		(CTL_IO_CON_test_pdata),
	.ctr_in			(CTL_IO_SPO_1_ctr_in), //
	.mon_out		(CTL_IO_SPO_1_mon_out), //
	.spo_cdat		(CTL_SPO_CDAT1), //
	.spo_rst_n		(CTL_SPO_EN1), //
	.init_done		(CTL_IO_FLAG_SPO_1_init_done), //
	.update_done	(CTL_IO_FLAG_SPO_1_update_done), //
	.test_done		(CTL_IO_FLAG_SPO_1_test_done), //
	.error			(CTL_IO_SPO_1_error),
	.debug_out		()
);
// SPO_2
master_spi_SN74LV8153_pio master_spi_SN74LV8153_pio_SPO_2_inst (  
	.clk			(sys_clk),
	.reset_n		(reset_n & ~w_rst_spo),
	.en				(CTL_IO_CON_en),
	.init			(CTL_IO_CON_init),
	.update			(CTL_IO_CON_update),
	.test			(CTL_IO_CON_test),
	.adrs_start		(CTL_IO_CON_adrs_start),
	.num_bytes		(CTL_IO_CON_num_bytes),
	.test_pdata		(CTL_IO_CON_test_pdata),
	.ctr_in			(CTL_IO_SPO_2_ctr_in), //
	.mon_out		(CTL_IO_SPO_2_mon_out), //
	.spo_cdat		(CTL_SPO_CDAT2), //
	.spo_rst_n		(CTL_SPO_EN2), //
	.init_done		(CTL_IO_FLAG_SPO_2_init_done), //
	.update_done	(CTL_IO_FLAG_SPO_2_update_done), //
	.test_done		(CTL_IO_FLAG_SPO_2_test_done), //
	.error			(CTL_IO_SPO_2_error),
	.debug_out		()
);
// SPO_3
master_spi_SN74LV8153_pio master_spi_SN74LV8153_pio_SPO_3_inst (  
	.clk			(sys_clk),
	.reset_n		(reset_n & ~w_rst_spo),
	.en				(CTL_IO_CON_en),
	.init			(CTL_IO_CON_init),
	.update			(CTL_IO_CON_update),
	.test			(CTL_IO_CON_test),
	.adrs_start		(CTL_IO_CON_adrs_start),
	.num_bytes		(CTL_IO_CON_num_bytes),
	.test_pdata		(CTL_IO_CON_test_pdata),
	.ctr_in			(CTL_IO_SPO_3_ctr_in), //
	.mon_out		(CTL_IO_SPO_3_mon_out), //
	.spo_cdat		(CTL_SPO_CDAT3), //
	.spo_rst_n		(CTL_SPO_EN3), //
	.init_done		(CTL_IO_FLAG_SPO_3_init_done), //
	.update_done	(CTL_IO_FLAG_SPO_3_update_done), //
	.test_done		(CTL_IO_FLAG_SPO_3_test_done), //
	.error			(CTL_IO_SPO_3_error),//
	.debug_out		()
);
////

//}


/* TODO: DAC AD5754 */ //{

//// DAC_BIAS, DAC_A2A3 //

// DAC_BIAS
//wire [31:0] w_DAC_BIAS_CON   = ep11wire;
//wire [31:0] w_DAC_BIAS_DIN21 = ep16wire;
//wire [31:0] w_DAC_BIAS_DIN43 = ep17wire;
wire [31:0] w_DAC_BIAS_CON   = (w_mcs_ep_wi_en)? w_port_wi_11_1 : ep11wire;
wire [31:0] w_DAC_BIAS_DIN21 = (w_mcs_ep_wi_en)? w_port_wi_16_1 : ep16wire;
wire [31:0] w_DAC_BIAS_DIN43 = (w_mcs_ep_wi_en)? w_port_wi_17_1 : ep17wire;
//
wire [31:0] w_DAC_BIAS_FLAG;
assign ep31wire = w_DAC_BIAS_FLAG;
assign w_port_wo_31_1 = (w_mcs_ep_wo_en)? w_DAC_BIAS_FLAG : 32'hACAC_ACAC;
wire [31:0] w_DAC_BIAS_RB21;
assign ep36wire = w_DAC_BIAS_RB21;
assign w_port_wo_36_1 = (w_mcs_ep_wo_en)? w_DAC_BIAS_RB21 : 32'hACAC_ACAC;
wire [31:0] w_DAC_BIAS_RB43;
assign ep37wire = w_DAC_BIAS_RB43;
assign w_port_wo_37_1 = (w_mcs_ep_wo_en)? w_DAC_BIAS_RB43 : 32'hACAC_ACAC;
//
//wire [31:0] w_DAC_BIAS_TI    = ep50trig;
wire [31:0] w_DAC_BIAS_TI = (w_mcs_ep_ti_en)? w_port_ti_50_1 : ep50trig;
//
wire [31:0] w_DAC_BIAS_TO;
assign ep70trig = w_DAC_BIAS_TO;
assign w_port_to_70_1 = (w_mcs_ep_to_en)? w_DAC_BIAS_TO : 32'h0000_0000; //$$

// DAC_A2A3
//wire [31:0] w_DAC_A2A3_CON   = ep10wire;
//wire [31:0] w_DAC_A2A3_DIN21 = ep14wire;
//wire [31:0] w_DAC_A2A3_DIN43 = ep15wire;
wire [31:0] w_DAC_A2A3_CON   = (w_mcs_ep_wi_en)? w_port_wi_10_1 : ep10wire;
wire [31:0] w_DAC_A2A3_DIN21 = (w_mcs_ep_wi_en)? w_port_wi_14_1 : ep14wire;
wire [31:0] w_DAC_A2A3_DIN43 = (w_mcs_ep_wi_en)? w_port_wi_15_1 : ep15wire;
//
wire [31:0] w_DAC_A2A3_FLAG;
assign ep30wire = w_DAC_A2A3_FLAG;
assign w_port_wo_30_1 = (w_mcs_ep_wo_en)? w_DAC_A2A3_FLAG : 32'hACAC_ACAC;
wire [31:0] w_DAC_A2A3_RB21;
assign ep34wire = w_DAC_A2A3_RB21;
assign w_port_wo_34_1 = (w_mcs_ep_wo_en)? w_DAC_A2A3_RB21 : 32'hACAC_ACAC;
wire [31:0] w_DAC_A2A3_RB43;
assign ep35wire = w_DAC_A2A3_RB43;
assign w_port_wo_35_1 = (w_mcs_ep_wo_en)? w_DAC_A2A3_RB43 : 32'hACAC_ACAC;
//
//wire [31:0] w_DAC_A2A3_TI    = ep51trig;
wire [31:0] w_DAC_A2A3_TI = (w_mcs_ep_ti_en)? w_port_ti_51_1 : ep51trig;
//
wire [31:0] w_DAC_A2A3_TO;
assign ep71trig = w_DAC_A2A3_TO;
assign w_port_to_71_1 = (w_mcs_ep_to_en)? w_DAC_A2A3_TO : 32'h0000_0000; //$$
// IO pins for DAC_BIAS
wire DAC_BIAS_MISO  ; // DAC_BIAS_MISO
wire DAC_BIAS_MOSI  ; // DAC_BIAS_MOSI
wire DAC_BIAS_SYNB; // DAC_BIAS_SYNB
wire DAC_BIAS_SCLK ; // DAC_BIAS_SCLK
wire DAC_BIAS_CLRB  ; // DAC_BIAS_CLRB
wire DAC_BIAS_LD_B ; // DAC_BIAS_LD_B
// IO pins for DAC_A2A3
wire DAC_A2A3_MOSI; 
wire DAC_A2A3_SCLK; 
wire DAC_A2A3_SYNB; 
wire DAC_A2A3_MISO;

// test 
//wire [31:0] w_DAC_TEST_IN    = ep04wire;
wire [31:0] w_DAC_TEST_IN    = (w_mcs_ep_wi_en)? w_port_wi_04_1 : ep04wire;
//
wire [31:0] w_DAC_TEST_RB1;
assign ep32wire = w_DAC_TEST_RB1;
assign w_port_wo_32_1 = (w_mcs_ep_wo_en)? w_DAC_TEST_RB1 : 32'hACAC_ACAC;
wire [31:0] w_DAC_TEST_RB2;
assign ep33wire = w_DAC_TEST_RB2;
assign w_port_wo_33_1 = (w_mcs_ep_wo_en)? w_DAC_TEST_RB2 : 32'hACAC_ACAC;
wire [31:0] w_DAC_TEST_OUT;
assign ep24wire = w_DAC_TEST_OUT;
assign w_port_wo_24_1 = (w_mcs_ep_wo_en)? w_DAC_TEST_OUT : 32'hACAC_ACAC;
//
wire [31:0] w_DAC_BIAS_TEST_IN  = w_DAC_TEST_IN;
wire [31:0] w_DAC_BIAS_TEST_OUT;
wire [31:0] w_DAC_BIAS_TEST_RB1;
wire [31:0] w_DAC_BIAS_TEST_RB2;
//
wire [31:0] w_DAC_A2A3_TEST_IN  = w_DAC_TEST_IN;
wire [31:0] w_DAC_A2A3_TEST_OUT;
wire [31:0] w_DAC_A2A3_TEST_RB1;
wire [31:0] w_DAC_A2A3_TEST_RB2;
//
assign w_DAC_TEST_OUT = 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b10)? w_DAC_BIAS_TEST_OUT : 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b01)? w_DAC_A2A3_TEST_OUT : 
	32'b0;
assign w_DAC_TEST_RB1 = 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b10)? w_DAC_BIAS_TEST_RB1 : 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b01)? w_DAC_A2A3_TEST_RB1 : 
	32'b0;
assign w_DAC_TEST_RB2 = 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b10)? w_DAC_BIAS_TEST_RB2 : 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b01)? w_DAC_A2A3_TEST_RB2 : 
	32'b0;

// DAC AD5754 : DAC_BIAS 
//  master_spi_dac_AD5754__range_con
wire DAC_BIAS_en			= 				w_DAC_BIAS_CON[0];
wire DAC_BIAS_reset		= w_DAC_BIAS_TI[0];
wire DAC_BIAS_init		= w_DAC_BIAS_TI[1] | w_DAC_BIAS_CON[1];
wire DAC_BIAS_update		= w_DAC_BIAS_TI[2] | w_DAC_BIAS_CON[2];
wire DAC_BIAS_test		= w_DAC_BIAS_TI[3] | w_DAC_BIAS_CON[3];
wire [15:0] DAC_BIAS_conf =  				w_DAC_BIAS_CON[31:16];
//
wire DAC_BIAS_init_done	;
wire DAC_BIAS_update_done	;
wire DAC_BIAS_test_done	;
wire DAC_BIAS_error		;
//
wire [31:0] DAC_BIAS_DIN21 = w_DAC_BIAS_DIN21;
wire [31:0] DAC_BIAS_DIN43 = w_DAC_BIAS_DIN43;
//
wire [31:0] DAC_BIAS_RDBK21;
wire [31:0] DAC_BIAS_RDBK43;
//
master_spi_dac_AD5754__range_con master_spi_dac_AD5754_DAC_BIAS_inst ( 
	.clk		(sys_clk), // assume 10MHz or 100ns
	.reset_n	(reset_n & ~DAC_BIAS_reset & ~w_rst_bias), // input
	.en			(DAC_BIAS_en), // input
	//
	.conf		(DAC_BIAS_conf), // 16 bits
	//
	.init		(DAC_BIAS_init), // input
	.update		(DAC_BIAS_update), // input
	.test		(DAC_BIAS_test), // input
    //
	.test_sdi_pdata	(w_DAC_BIAS_TEST_IN ), // input
	.test_sdo_pdata	(w_DAC_BIAS_TEST_OUT), // out 
	.readback_pdata (w_DAC_BIAS_TEST_RB1), // out
	.readback1_pdata(w_DAC_BIAS_TEST_RB2), // out
	// DAC input
	.DAC_VALUE_IN1	(DAC_BIAS_DIN21[15: 0]), // input
	.DAC_VALUE_IN2	(DAC_BIAS_DIN21[31:16]), // input
	.DAC_VALUE_IN3	(DAC_BIAS_DIN43[15: 0]), // input
	.DAC_VALUE_IN4	(DAC_BIAS_DIN43[31:16]), // input
    // DAC readback
	.DAC_READBACK1	(DAC_BIAS_RDBK21[15: 0]),
	.DAC_READBACK2	(DAC_BIAS_RDBK21[31:16]), 
	.DAC_READBACK3	(DAC_BIAS_RDBK43[15: 0]),
	.DAC_READBACK4	(DAC_BIAS_RDBK43[31:16]), 
    // DAC control
    .SCLK			(DAC_BIAS_SCLK	),
    .SYNC_N			(DAC_BIAS_SYNB	),
    .DIN			(DAC_BIAS_MOSI		),
    .SDO			(DAC_BIAS_MISO		), // input
	// GPIO
	.CLR_DAC_N		(DAC_BIAS_CLRB		), // ext pin
	.LOAD_DAC_N		(DAC_BIAS_LD_B	), // ext pin
	.B2C_DAC		(),                // ext pin
    //
    .init_done		(DAC_BIAS_init_done	),
    .update_done	(DAC_BIAS_update_done	),
	.test_done		(DAC_BIAS_test_done	),
	.error			(DAC_BIAS_error		),
	.debug_out()
);
//
assign w_DAC_BIAS_RB21 = DAC_BIAS_RDBK21;
assign w_DAC_BIAS_RB43 = DAC_BIAS_RDBK43;
//
assign w_DAC_BIAS_FLAG = 
	{27'b0,DAC_BIAS_error,
	DAC_BIAS_test_done,DAC_BIAS_update_done,DAC_BIAS_init_done,DAC_BIAS_en}; 
//
assign w_DAC_BIAS_TO[0] = DAC_BIAS_en          ;
assign w_DAC_BIAS_TO[1] = DAC_BIAS_init_done   ;
assign w_DAC_BIAS_TO[2] = DAC_BIAS_update_done ;
assign w_DAC_BIAS_TO[3] = DAC_BIAS_test_done   ;
assign w_DAC_BIAS_TO[4] = DAC_BIAS_error       ;
assign w_DAC_BIAS_TO[31:5] = 27'b0;

// DAC AD5754 : DAC_A2A3 
//  master_spi_dac_AD5754__range_con
wire DAC_A2A3_en			= 				w_DAC_A2A3_CON[0];
wire DAC_A2A3_reset		= w_DAC_A2A3_TI[0];
wire DAC_A2A3_init		= w_DAC_A2A3_TI[1] | w_DAC_A2A3_CON[1];
wire DAC_A2A3_update		= w_DAC_A2A3_TI[2] | w_DAC_A2A3_CON[2];
wire DAC_A2A3_test		= w_DAC_A2A3_TI[3] | w_DAC_A2A3_CON[3];
wire [15:0] DAC_A2A3_conf =  				w_DAC_A2A3_CON[31:16];
//
wire DAC_A2A3_init_done	;
wire DAC_A2A3_update_done	;
wire DAC_A2A3_test_done	;
wire DAC_A2A3_error		;
//
wire [31:0] DAC_A2A3_DIN21 = w_DAC_A2A3_DIN21;
wire [31:0] DAC_A2A3_DIN43 = w_DAC_A2A3_DIN43;
//
wire [31:0] DAC_A2A3_RDBK21;
wire [31:0] DAC_A2A3_RDBK43;
//
master_spi_dac_AD5754__range_con master_spi_dac_AD5754_DAC_A2A3_inst ( 
	.clk		(sys_clk), // assume 10MHz or 100ns
	.reset_n	(reset_n & ~DAC_A2A3_reset & ~w_rst_bias), // input
	.en			(DAC_A2A3_en), // input
	//
	.conf		(DAC_A2A3_conf), // 16 bits
	//
	.init		(DAC_A2A3_init), // input
	.update		(DAC_A2A3_update), // input
	.test		(DAC_A2A3_test), // input
    //
	.test_sdi_pdata	(w_DAC_A2A3_TEST_IN ), // input
	.test_sdo_pdata	(w_DAC_A2A3_TEST_OUT), // out 
	.readback_pdata (w_DAC_A2A3_TEST_RB1), // out
	.readback1_pdata(w_DAC_A2A3_TEST_RB2), // out
	// DAC input
	.DAC_VALUE_IN1	(DAC_A2A3_DIN21[15: 0]), // input
	.DAC_VALUE_IN2	(DAC_A2A3_DIN21[31:16]), // input
	.DAC_VALUE_IN3	(DAC_A2A3_DIN43[15: 0]), // input
	.DAC_VALUE_IN4	(DAC_A2A3_DIN43[31:16]), // input
    // DAC readback
	.DAC_READBACK1	(DAC_A2A3_RDBK21[15: 0]),
	.DAC_READBACK2	(DAC_A2A3_RDBK21[31:16]), 
	.DAC_READBACK3	(DAC_A2A3_RDBK43[15: 0]),
	.DAC_READBACK4	(DAC_A2A3_RDBK43[31:16]), 
    // DAC control
    .SCLK			(DAC_A2A3_SCLK	),
    .SYNC_N			(DAC_A2A3_SYNB	),
    .DIN			(DAC_A2A3_MOSI	),
    .SDO			(DAC_A2A3_MISO	), // input
	// GPIO
	.CLR_DAC_N		(), // ext pin // none
	.LOAD_DAC_N		(), // ext pin // none
	.B2C_DAC		(), // ext pin // none
    //
    .init_done		(DAC_A2A3_init_done	),
    .update_done	(DAC_A2A3_update_done	),
	.test_done		(DAC_A2A3_test_done	),
	.error			(DAC_A2A3_error		),
	.debug_out()
);
//
assign w_DAC_A2A3_RB21 = DAC_A2A3_RDBK21;
assign w_DAC_A2A3_RB43 = DAC_A2A3_RDBK43;
//
assign w_DAC_A2A3_FLAG = 
	{27'b0,DAC_A2A3_error,
	DAC_A2A3_test_done,DAC_A2A3_update_done,DAC_A2A3_init_done,DAC_A2A3_en}; 
//
assign w_DAC_A2A3_TO[0] = DAC_A2A3_en          ;
assign w_DAC_A2A3_TO[1] = DAC_A2A3_init_done   ;
assign w_DAC_A2A3_TO[2] = DAC_A2A3_update_done ;
assign w_DAC_A2A3_TO[3] = DAC_A2A3_test_done   ;
assign w_DAC_A2A3_TO[4] = DAC_A2A3_error       ;
assign w_DAC_A2A3_TO[31:5] = 27'b0;
////

//}


/* TODO: SPIO : MCP23S17 */ //{
// support off-board spio IC 
// 4-wire spi 
// nets on sch in CMU-CPU
//
// [pins on b'rd] [nets on sch]  in CMU-CPU
//  TJ1-1          3V3
//  TJ1-2          GND
//  TJ1-3          CC8
//  TJ1-4          CC9
//  TJ1-5          CC10
//  TJ1-6          CC12
//
// pins on sub-boards for CMU       // 3G-MKCS <<<<<<
//  TJ1-1          3V3
//  TJ1-2          GND
//  TJ1-3          MOSI_AUX //
//  TJ1-4          SCLK_AUX
//  TJ1-5          SCSB_AUX //
//  TJ1-6          MISO_AUX
//
// pins on sub-boards for PGU (JFI) // 3G-CKMS
//  TK1-1          3V3
//  TK1-2          GND
//  TK1-3          AUX_CS_B
//  TK1-4          AUX_SCLK
//  TK1-5          AUX_MOSI
//  TK1-6          AUX_MISO
//

// ports for SPIO //{

wire  EXT_SPx_MOSI;
wire  EXT_SPx_SCLK;
wire  EXT_SPx_MISO;
//
//$$OBUF obuf__EXT_SPx_MOSI_inst (.O(o_B13_L2P         ), .I(EXT_SPx_MOSI ) ); // 
//$$OBUF obuf__EXT_SPx_SCLK_inst (.O(o_B13_L2N         ), .I(EXT_SPx_SCLK ) ); // 
//$$IBUF ibuf__EXT_SPx_MISO_inst (.I(i_B13_L4P         ), .O(EXT_SPx_MISO ) ); //
//
wire  EXT_SP0__CS_B; // map: S1_SPI_CSB0
wire  EXT_SP1__CS_B; // map: S1_SPI_CSB1
wire  EXT_SP2__CS_B; // map: S1_SPI_CSB2
wire  EXT_SP3__CS_B; // map: S2_SPI_CSB0
wire  EXT_SP4__CS_B; // map: S2_SPI_CSB1
wire  EXT_SP5__CS_B; // map: S2_SPI_CSB2
wire  EXT_SP6__CS_B; // map: S3_SPI_CSB0
wire  EXT_SP7__CS_B; // map: S3_SPI_CSB1
wire  EXT_SP8__CS_B; // map: S3_SPI_CSB2
wire  EXT_SP9__CS_B; // map: S4_SPI_CSB0
wire  EXT_SP10_CS_B; // map: S4_SPI_CSB1
wire  EXT_SP11_CS_B; // map: S4_SPI_CSB2
wire  EXT_SP12_CS_B; // map: S5_SPI_CSB0
wire  EXT_SP13_CS_B; // map: S5_SPI_CSB1
wire  EXT_SP14_CS_B; // map: S5_SPI_CSB2
wire  EXT_SP15_CS_B; // map: S6_SPI_CSB0
wire  EXT_SP16_CS_B; // map: S6_SPI_CSB1
wire  EXT_SP17_CS_B; // map: S6_SPI_CSB2
wire  EXT_SP18_CS_B; // map: S7_SPI_CSB0
wire  EXT_SP19_CS_B; // map: S7_SPI_CSB1
wire  EXT_SP20_CS_B; // map: S7_SPI_CSB2
wire  EXT_SP21_CS_B; // map: S8_SPI_CSB0
wire  EXT_SP22_CS_B; // map: S8_SPI_CSB1
wire  EXT_SP23_CS_B; // map: S8_SPI_CSB2
//
//$$OBUF obuf__EXT_SP0__CS_B_inst   (.O(o_B13_L4N         ), .I(EXT_SP0__CS_B   ) ); // 
//$$OBUF obuf__EXT_SP1__CS_B_inst   (.O(o_B13_L1P         ), .I(EXT_SP1__CS_B   ) ); // 
//$$OBUF obuf__EXT_SP2__CS_B_inst   (.O(o_B13_L5N         ), .I(EXT_SP2__CS_B   ) ); // 
//$$OBUF obuf__EXT_SP3__CS_B_inst   (.O(o_B13_L3P         ), .I(EXT_SP3__CS_B   ) ); // 
//$$OBUF obuf__EXT_SP4__CS_B_inst   (.O(o_B13_L3N         ), .I(EXT_SP4__CS_B   ) ); // 
//$$OBUF obuf__EXT_SP5__CS_B_inst   (.O(o_B13_L16P        ), .I(EXT_SP5__CS_B   ) ); // 
//$$OBUF obuf__EXT_SP6__CS_B_inst   (.O(o_B13_L16N        ), .I(EXT_SP6__CS_B   ) ); // 
//$$OBUF obuf__EXT_SP7__CS_B_inst   (.O(o_B13_L1N         ), .I(EXT_SP7__CS_B   ) ); // 
//$$OBUF obuf__EXT_SP8__CS_B_inst   (.O(o_B35_L10P        ), .I(EXT_SP8__CS_B   ) ); // 
//$$OBUF obuf__EXT_SP9__CS_B_inst   (.O(o_B35_L10N        ), .I(EXT_SP9__CS_B   ) ); // 
//$$OBUF obuf__EXT_SP10_CS_B_inst   (.O(o_B35_L8P         ), .I(EXT_SP10_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP11_CS_B_inst   (.O(o_B35_L8N         ), .I(EXT_SP11_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP12_CS_B_inst   (.O(o_B35_L5P         ), .I(EXT_SP12_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP13_CS_B_inst   (.O(o_B35_L5N         ), .I(EXT_SP13_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP14_CS_B_inst   (.O(o_B35_L12P_MRCC   ), .I(EXT_SP14_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP15_CS_B_inst   (.O(o_B35_L12N_MRCC   ), .I(EXT_SP15_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP16_CS_B_inst   (.O(o_B35_L4P         ), .I(EXT_SP16_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP17_CS_B_inst   (.O(o_B35_L4N         ), .I(EXT_SP17_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP18_CS_B_inst   (.O(o_B35_L6P         ), .I(EXT_SP18_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP19_CS_B_inst   (.O(o_B35_L6N         ), .I(EXT_SP19_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP20_CS_B_inst   (.O(o_B35_L1P         ), .I(EXT_SP20_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP21_CS_B_inst   (.O(o_B35_L1N         ), .I(EXT_SP21_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP22_CS_B_inst   (.O(o_B35_L13P_MRCC   ), .I(EXT_SP22_CS_B   ) ); // 
//$$OBUF obuf__EXT_SP23_CS_B_inst   (.O(o_B35_L13N_MRCC   ), .I(EXT_SP23_CS_B   ) ); // 

//}
  
// module //{

// enable logic 
//$$wire w_SPIO_en  --> move up

//
wire w_trig_SPIO_SPI_frame   = w_SPIO_TRIG_TI[1]; 
wire w_done_SPIO_SPI_frame;
wire w_done_SPIO_SPI_frame_TO;
wire w_busy_SPI_frame;
//
wire [7:0] w_SPIO_socket_en  = w_SPIO_CON_WI[31:24];
wire [2:0] w_SPIO_CS_en      = w_SPIO_CON_WI[18:16];
//
wire [2:0] w_SPIO_pin_adrs_A = w_SPIO_FDAT_WI[27:25];
wire       w_SPIO_R_W_bar    = w_SPIO_FDAT_WI[24]   ;
wire [7:0] w_SPIO_reg_adrs_A = w_SPIO_FDAT_WI[23:16];
wire [7:0] w_SPIO_wr_DA      = w_SPIO_FDAT_WI[15: 8];
wire [7:0] w_SPIO_wr_DB      = w_SPIO_FDAT_WI[ 7: 0];
//
wire [7:0] w_SPIO_rd_DA;
wire [7:0] w_SPIO_rd_DB;
//
wire       w_forced_pin_mode_en = w_SPIO_CON_WI[ 8];
wire       w_forced_sig_mosi    = w_SPIO_CON_WI[ 9];
wire       w_forced_sig_sclk    = w_SPIO_CON_WI[10];
wire       w_forced_sig_csel    = w_SPIO_CON_WI[11];

//
(* keep = "true" *) wire [2:0] w_S1_SPI_CSB;
(* keep = "true" *) wire [2:0] w_S2_SPI_CSB;
(* keep = "true" *) wire [2:0] w_S3_SPI_CSB;
(* keep = "true" *) wire [2:0] w_S4_SPI_CSB;
(* keep = "true" *) wire [2:0] w_S5_SPI_CSB;
(* keep = "true" *) wire [2:0] w_S6_SPI_CSB;
(* keep = "true" *) wire [2:0] w_S7_SPI_CSB;
(* keep = "true" *) wire [2:0] w_S8_SPI_CSB;
//
(* keep = "true" *) wire w_SPIOx_SCLK;
(* keep = "true" *) wire w_SPIOx_MOSI;
(* keep = "true" *) wire w_SPIOx_MISO = EXT_SPx_MISO;

wire [15:0] w_cnt_spio_trig_frame;

//
master_spi_mcp23s17__s8_cs3  master_spi_mcp23s17_inst (
	.clk				(sys_clk), // 10MHz (or 1MHz if failed...)
	.reset_n			(reset_n & w_SPIO_en & (~w_HW_reset)),
	//
	// trig control
	.i_trig_SPI_frame	    (w_trig_SPIO_SPI_frame), 
	.o_done_SPI_frame	    (w_done_SPIO_SPI_frame), 
	.o_done_SPI_frame_TO    (w_done_SPIO_SPI_frame_TO), 
	.o_busy_SPI_frame       (w_busy_SPI_frame     ),
	.o_cnt_spio_trig_frame  (w_cnt_spio_trig_frame), // [15:0] // $$
	
	// IO ports
	.o_S1_SPI_CSB   	(w_S1_SPI_CSB), // [2:0]
	.o_S2_SPI_CSB   	(w_S2_SPI_CSB), // [2:0]
	.o_S3_SPI_CSB   	(w_S3_SPI_CSB), // [2:0]
	.o_S4_SPI_CSB   	(w_S4_SPI_CSB), // [2:0]
	.o_S5_SPI_CSB   	(w_S5_SPI_CSB), // [2:0]
	.o_S6_SPI_CSB   	(w_S6_SPI_CSB), // [2:0]
	.o_S7_SPI_CSB   	(w_S7_SPI_CSB), // [2:0]
	.o_S8_SPI_CSB   	(w_S8_SPI_CSB), // [2:0]
	//
	.o_SPIOx_SCLK 		(w_SPIOx_SCLK), // EXT_SPx_SCLK
	.o_SPIOx_MOSI 		(w_SPIOx_MOSI), // EXT_SPx_MOSI 
	.i_SPIOx_MISO 		(w_SPIOx_MISO), // EXT_SPx_MISO
	
	// CS selection
	.i_socket_en        (w_SPIO_socket_en ), // socket_enable [7:0]
	.i_CS_en            (w_SPIO_CS_en     ), // frame_cs_enable [2:0]
	
	// forced pin mode :
	//   forced pin mode en
	//   EXT_SPx_MOSI_forced_sig
	//   EXT_SPx_SCLK_forced_sig
	.i_forced_pin_mode_en  (w_forced_pin_mode_en), //
	.i_forced_sig_mosi     (w_forced_sig_mosi   ), //
	.i_forced_sig_sclk     (w_forced_sig_sclk   ), //
	.i_forced_sig_csel     (w_forced_sig_csel   ), //
	
	// frame data
	.i_pin_adrs_A       (w_SPIO_pin_adrs_A), // [2:0] 
	.i_R_W_bar          (w_SPIO_R_W_bar   ), //       
	.i_reg_adrs_A       (w_SPIO_reg_adrs_A), // [7:0] 
	.i_wr_DA            (w_SPIO_wr_DA     ), // [7:0] 
	.i_wr_DB            (w_SPIO_wr_DB     ), // [7:0] 
	.o_rd_DA            (w_SPIO_rd_DA     ), // [7:0] 
	.o_rd_DB            (w_SPIO_rd_DB     ), // [7:0] 
	//
	.valid				()
);
//// note on test
// ... must enable and test hardware address selection IOCON
// LED test with CHECK_LED0 net (CS0_GPB7)
//   socket/ch enable          : w_SPIO_CON_WI  = 32'h01_01_00_01
//   IODIR frame 0x40_00_FF_7F : w_SPIO_FDAT_WI = 32'h00_00_FF_7F
//   GPIO  frame 0x40_12_00_80 : w_SPIO_FDAT_WI = 32'h00_12_00_80
//   GPIO  frame 0x40_12_00_00 : w_SPIO_FDAT_WI = 32'h00_12_00_00
// LED test with CHECK_LED1 net (CS1_GPB7)
//   socket/ch enable          : w_SPIO_CON_WI  = 32'h01_02_00_01
//   IODIR frame 0x40_00_FF_7F : w_SPIO_FDAT_WI = 32'h00_00_FF_7F
//   IODIR frame 0x41_00_XX_XX : w_SPIO_FDAT_WI = 32'h01_00_XX_XX // read NG // suspect no pullup on board circuit.
//   GPIO  frame 0x40_12_00_80 : w_SPIO_FDAT_WI = 32'h00_12_00_80
//   GPIO  frame 0x40_12_00_00 : w_SPIO_FDAT_WI = 32'h00_12_00_00

//}

// assignments //{
// chip/slave selection
assign EXT_SP0__CS_B = w_S1_SPI_CSB[0]; // map: S1_SPI_CSB0
assign EXT_SP1__CS_B = w_S1_SPI_CSB[1]; // map: S1_SPI_CSB1
assign EXT_SP2__CS_B = w_S1_SPI_CSB[2]; // map: S1_SPI_CSB2
assign EXT_SP3__CS_B = w_S2_SPI_CSB[0]; // map: S2_SPI_CSB0
assign EXT_SP4__CS_B = w_S2_SPI_CSB[1]; // map: S2_SPI_CSB1
assign EXT_SP5__CS_B = w_S2_SPI_CSB[2]; // map: S2_SPI_CSB2
assign EXT_SP6__CS_B = w_S3_SPI_CSB[0]; // map: S3_SPI_CSB0
assign EXT_SP7__CS_B = w_S3_SPI_CSB[1]; // map: S3_SPI_CSB1
assign EXT_SP8__CS_B = w_S3_SPI_CSB[2]; // map: S3_SPI_CSB2
assign EXT_SP9__CS_B = w_S4_SPI_CSB[0]; // map: S4_SPI_CSB0
assign EXT_SP10_CS_B = w_S4_SPI_CSB[1]; // map: S4_SPI_CSB1
assign EXT_SP11_CS_B = w_S4_SPI_CSB[2]; // map: S4_SPI_CSB2
assign EXT_SP12_CS_B = w_S5_SPI_CSB[0]; // map: S5_SPI_CSB0
assign EXT_SP13_CS_B = w_S5_SPI_CSB[1]; // map: S5_SPI_CSB1
assign EXT_SP14_CS_B = w_S5_SPI_CSB[2]; // map: S5_SPI_CSB2
assign EXT_SP15_CS_B = w_S6_SPI_CSB[0]; // map: S6_SPI_CSB0
assign EXT_SP16_CS_B = w_S6_SPI_CSB[1]; // map: S6_SPI_CSB1
assign EXT_SP17_CS_B = w_S6_SPI_CSB[2]; // map: S6_SPI_CSB2
assign EXT_SP18_CS_B = w_S7_SPI_CSB[0]; // map: S7_SPI_CSB0
assign EXT_SP19_CS_B = w_S7_SPI_CSB[1]; // map: S7_SPI_CSB1
assign EXT_SP20_CS_B = w_S7_SPI_CSB[2]; // map: S7_SPI_CSB2
assign EXT_SP21_CS_B = w_S8_SPI_CSB[0]; // map: S8_SPI_CSB0
assign EXT_SP22_CS_B = w_S8_SPI_CSB[1]; // map: S8_SPI_CSB1
assign EXT_SP23_CS_B = w_S8_SPI_CSB[2]; // map: S8_SPI_CSB2
// spi pin output
assign EXT_SPx_SCLK = w_SPIOx_SCLK;
assign EXT_SPx_MOSI = w_SPIOx_MOSI;
// frame data output
assign w_SPIO_FLAG_WO[31:27] = 5'b0;
assign w_SPIO_FLAG_WO[26:25]    = {w_busy_SPI_frame,w_done_SPIO_SPI_frame};
assign w_SPIO_FLAG_WO[24:16] = 9'b0;
assign w_SPIO_FLAG_WO[15:8]  = w_SPIO_rd_DA;
assign w_SPIO_FLAG_WO[ 7:0]  = w_SPIO_rd_DB;
// trig done output
assign w_SPIO_TRIG_TO[31:2]  = 30'b0;
assign w_SPIO_TRIG_TO[1]     = w_done_SPIO_SPI_frame_TO;
assign w_SPIO_TRIG_TO[0]     = 1'b0;
//}



//}


/* TODO: ADC_HS */ //{
//wire [31:0] w_ADC_HS_WI             = ep18wire;
wire [31:0] w_ADC_HS_WI = (w_mcs_ep_wi_en)? w_port_wi_18_1 : ep18wire;
//
(* keep = "true" *)
wire [31:0] w_ADC_HS_WO; 
assign ep38wire = w_ADC_HS_WO;
assign w_port_wo_38_1 = (w_mcs_ep_wo_en)? w_ADC_HS_WO : 32'hACAC_ACAC;
//
wire [31:0] w_ADC_HS_DOUT0;
wire [31:0] w_ADC_HS_DOUT1;
wire [31:0] w_ADC_HS_DOUT2;
wire [31:0] w_ADC_HS_DOUT3;
assign ep3Cwire = w_ADC_HS_DOUT0; 
assign ep3Dwire = w_ADC_HS_DOUT1;
assign ep3Ewire = w_ADC_HS_DOUT2;
assign ep3Fwire = w_ADC_HS_DOUT3;
assign w_port_wo_3C_1 = (w_mcs_ep_wo_en)? w_ADC_HS_DOUT0 : 32'hACAC_ACAC;
assign w_port_wo_3D_1 = (w_mcs_ep_wo_en)? w_ADC_HS_DOUT1 : 32'hACAC_ACAC;
assign w_port_wo_3E_1 = (w_mcs_ep_wo_en)? w_ADC_HS_DOUT2 : 32'hACAC_ACAC;
assign w_port_wo_3F_1 = (w_mcs_ep_wo_en)? w_ADC_HS_DOUT3 : 32'hACAC_ACAC;
//
wire [31:0] w_ADC_BASE_FREQ = ADC_BASE_FREQ;
assign ep39wire = w_ADC_BASE_FREQ;
assign w_port_wo_39_1 = (w_mcs_ep_wo_en)? w_ADC_BASE_FREQ : 32'hACAC_ACAC;
//
//wire [31:0] w_ADC_HS_TI             = ep58trig;
wire [31:0] w_ADC_HS_TI = (w_mcs_ep_ti_en)? w_port_ti_58_1 : ep58trig;
//
wire [31:0] w_ADC_HS_TO;
assign ep78trig = w_ADC_HS_TO;
assign w_port_to_78_1 = (w_mcs_ep_to_en)? w_ADC_HS_TO : 32'h0000_0000; //$$
//
wire [31:0] w_ADC_HS_DOUT0_PO;
wire [31:0] w_ADC_HS_DOUT1_PO;
wire [31:0] w_ADC_HS_DOUT2_PO;
wire [31:0] w_ADC_HS_DOUT3_PO;
//
assign epBCpipe = w_ADC_HS_DOUT0_PO; 
assign epBDpipe = w_ADC_HS_DOUT1_PO;
assign epBEpipe = w_ADC_HS_DOUT2_PO;
assign epBFpipe = w_ADC_HS_DOUT3_PO;
//
assign w_port_po_BC_1 = w_ADC_HS_DOUT0_PO; //$$
assign w_port_po_BD_1 = w_ADC_HS_DOUT1_PO; //$$
assign w_port_po_BE_1 = w_ADC_HS_DOUT2_PO; //$$
assign w_port_po_BF_1 = w_ADC_HS_DOUT3_PO; //$$
//
//$$wire w_ADC_HS_DOUT0_PO_RD = epBCrd;
//$$wire w_ADC_HS_DOUT1_PO_RD = epBDrd;
//$$wire w_ADC_HS_DOUT2_PO_RD = epBErd;
//$$wire w_ADC_HS_DOUT3_PO_RD = epBFrd;
wire w_ADC_HS_DOUT0_PO_RD = (w_mcs_ep_po_en)? w_rd_BC_1 : epBCrd;
wire w_ADC_HS_DOUT1_PO_RD = (w_mcs_ep_po_en)? w_rd_BD_1 : epBDrd;
wire w_ADC_HS_DOUT2_PO_RD = (w_mcs_ep_po_en)? w_rd_BE_1 : epBErd;
wire w_ADC_HS_DOUT3_PO_RD = (w_mcs_ep_po_en)? w_rd_BF_1 : epBFrd;

// fifo read clock switch 
//wire c_fifo_read = (w_mcs_ep_po_en)? mcs_clk : okClk;
wire c_fifo_read;
// note clock mux
// BUFGMUX in https://www.xilinx.com/support/documentation/user_guides/ug472_7Series_Clocking.pdf
BUFGMUX bufgmux_c_fifo_read_inst (
	.O(c_fifo_read), 
	.I0(okClk), 
	.I1(w_ck_pipe), //$$.I1(mcs_clk), 
	.S(w_mcs_ep_po_en) 
); 


// adc control 
(* keep = "true" *) wire      w_cnv_adc;
(* keep = "true" *) wire      w_clk_adc;
(* keep = "true" *) wire w_pin_test_adc;
(* keep = "true" *) wire  w_pin_dln_adc;
//
wire  w_dco_adc_0; // from pin ADC_00_DCO_N_ISO ANAL_CTRL35
wire w_dat2_adc_0; // from pin ADC_00_DA_P_ISO  ANAL_CTRL34
wire w_dat1_adc_0; // from pin ADC_00_DB_N_ISO  ANAL_CTRL33
//
wire  w_dco_adc_1;
wire w_dat2_adc_1;
wire w_dat1_adc_1;
//
wire  w_dco_adc_2;
wire w_dat2_adc_2;
wire w_dat1_adc_2;
//
wire  w_dco_adc_3;
wire w_dat2_adc_3;
wire w_dat1_adc_3;
//
(* keep = "true" *) wire w_hsadc_reset		= w_ADC_HS_TI[0]; // reset ADC control/serdes/fifo 
(* keep = "true" *) wire w_hsadc_init		= w_ADC_HS_TI[1] | w_ADC_HS_WI[1]; 
(* keep = "true" *) wire w_hsadc_update		= w_ADC_HS_TI[2] | w_ADC_HS_WI[2];
(* keep = "true" *) wire w_hsadc_test		= w_ADC_HS_TI[3] | w_ADC_HS_WI[3];
//
(* keep = "true" *) wire [17:0] fifo_adc0_din;
(* keep = "true" *) wire [17:0] fifo_adc1_din;
(* keep = "true" *) wire [17:0] fifo_adc2_din;
(* keep = "true" *) wire [17:0] fifo_adc3_din;
//
(* keep = "true" *) wire fifo_adc0_wr; // not used
(* keep = "true" *) wire fifo_adc1_wr; // not used
(* keep = "true" *) wire fifo_adc2_wr; // not used
(* keep = "true" *) wire fifo_adc3_wr; // not used
//
wire [17:0] fifo_adc0_dout	;
wire 		fifo_adc0_rd_en	;
wire 		fifo_adc0_valid	;
wire 		fifo_adc0_uflow	; 
wire 		fifo_adc0_pempty;
wire 		fifo_adc0_empty	;
wire 		fifo_adc0_wr_ack;
wire 		fifo_adc0_oflow	;
wire 		fifo_adc0_pfull	;
wire 		fifo_adc0_full	;
//
assign w_ADC_HS_WO[ 8] = fifo_adc0_pempty	;
assign w_ADC_HS_WO[ 9] = fifo_adc0_empty	;
assign w_ADC_HS_WO[10] = fifo_adc0_wr_ack	;
assign w_ADC_HS_WO[11] = fifo_adc0_oflow	;
assign w_ADC_HS_WO[12] = fifo_adc0_pfull	;
assign w_ADC_HS_WO[13] = fifo_adc0_full	;
//
wire [17:0] fifo_adc1_dout	;
wire 		fifo_adc1_rd_en	;
wire 		fifo_adc1_valid	;
wire 		fifo_adc1_uflow	;
wire 		fifo_adc1_pempty;
wire 		fifo_adc1_empty	;
wire 		fifo_adc1_wr_ack;
wire 		fifo_adc1_oflow	;
wire 		fifo_adc1_pfull	;
wire 		fifo_adc1_full	;
//
assign w_ADC_HS_WO[14] = fifo_adc1_pempty	;
assign w_ADC_HS_WO[15] = fifo_adc1_empty	;
assign w_ADC_HS_WO[16] = fifo_adc1_wr_ack	;
assign w_ADC_HS_WO[17] = fifo_adc1_oflow	;
assign w_ADC_HS_WO[18] = fifo_adc1_pfull	;
assign w_ADC_HS_WO[19] = fifo_adc1_full	;
//
wire [17:0] fifo_adc2_dout	;
wire 		fifo_adc2_rd_en	;
wire 		fifo_adc2_valid	;
wire 		fifo_adc2_uflow	;
wire 		fifo_adc2_pempty;
wire 		fifo_adc2_empty	;
wire 		fifo_adc2_wr_ack;
wire 		fifo_adc2_oflow	;
wire 		fifo_adc2_pfull	;
wire 		fifo_adc2_full	;
//
assign w_ADC_HS_WO[20] = fifo_adc2_pempty	;
assign w_ADC_HS_WO[21] = fifo_adc2_empty	;
assign w_ADC_HS_WO[22] = fifo_adc2_wr_ack	;
assign w_ADC_HS_WO[23] = fifo_adc2_oflow	;
assign w_ADC_HS_WO[24] = fifo_adc2_pfull	;
assign w_ADC_HS_WO[25] = fifo_adc2_full	;
//
wire [17:0] fifo_adc3_dout	;
wire 		fifo_adc3_rd_en	;
wire 		fifo_adc3_valid	;
wire 		fifo_adc3_uflow	;
wire 		fifo_adc3_pempty;
wire 		fifo_adc3_empty	;
wire 		fifo_adc3_wr_ack;
wire 		fifo_adc3_oflow	;
wire 		fifo_adc3_pfull	;
wire 		fifo_adc3_full	;
//
assign w_ADC_HS_WO[26] = fifo_adc3_pempty	;
assign w_ADC_HS_WO[27] = fifo_adc3_empty	;
assign w_ADC_HS_WO[28] = fifo_adc3_wr_ack	;
assign w_ADC_HS_WO[29] = fifo_adc3_oflow	;
assign w_ADC_HS_WO[30] = fifo_adc3_pfull	;
assign w_ADC_HS_WO[31] = fifo_adc3_full	;
//
wire w_hsadc_en		= w_ADC_HS_WI[0]; 
//
assign w_ADC_HS_DOUT0 = (w_hsadc_en)? {fifo_adc0_din,14'b0} : 32'b0;
assign w_ADC_HS_DOUT1 = (w_hsadc_en)? {fifo_adc1_din,14'b0} : 32'b0;
assign w_ADC_HS_DOUT2 = (w_hsadc_en)? {fifo_adc2_din,14'b0} : 32'b0;
assign w_ADC_HS_DOUT3 = (w_hsadc_en)? {fifo_adc3_din,14'b0} : 32'b0;
//
(* keep = "true" *) wire w_hsadc_error		; 
(* keep = "true" *) wire w_hsadc_init_done	; 
(* keep = "true" *) wire w_hsadc_update_done; 
(* keep = "true" *) wire w_hsadc_test_done	; 
//
assign w_ADC_HS_TO[ 0] = w_hsadc_en;
assign w_ADC_HS_TO[ 1] = w_hsadc_init_done	;
assign w_ADC_HS_TO[ 2] = w_hsadc_update_done;
assign w_ADC_HS_TO[ 3] = w_hsadc_test_done	;
assign w_ADC_HS_TO[ 4] = 1'b0;
assign w_ADC_HS_TO[ 5] = 1'b0;
assign w_ADC_HS_TO[ 6] = 1'b0;
assign w_ADC_HS_TO[ 7] = 1'b0;
assign w_ADC_HS_TO[ 8] = 1'b0;
assign w_ADC_HS_TO[ 9] = 1'b0;
assign w_ADC_HS_TO[10] = 1'b0;
assign w_ADC_HS_TO[11] = 1'b0;
assign w_ADC_HS_TO[12] = 1'b0;
assign w_ADC_HS_TO[13] = 1'b0;
assign w_ADC_HS_TO[14] = 1'b0;
assign w_ADC_HS_TO[15] = 1'b0;
assign w_ADC_HS_TO[16] = 1'b0;
assign w_ADC_HS_TO[17] = 1'b0;
assign w_ADC_HS_TO[18] = 1'b0;
assign w_ADC_HS_TO[19] = 1'b0;
//
assign w_ADC_HS_TO[20] = fifo_adc0_pempty	;
assign w_ADC_HS_TO[21] = fifo_adc0_empty	;
assign w_ADC_HS_TO[22] = fifo_adc0_pfull	;
assign w_ADC_HS_TO[23] = fifo_adc1_pempty	;
assign w_ADC_HS_TO[24] = fifo_adc1_empty	;
assign w_ADC_HS_TO[25] = fifo_adc1_pfull	;
assign w_ADC_HS_TO[26] = fifo_adc2_pempty	;
assign w_ADC_HS_TO[27] = fifo_adc2_empty	;
assign w_ADC_HS_TO[28] = fifo_adc2_pfull	;
assign w_ADC_HS_TO[29] = fifo_adc3_pempty	;
assign w_ADC_HS_TO[30] = fifo_adc3_empty	;
assign w_ADC_HS_TO[31] = fifo_adc3_pfull	;
//
assign  w_ADC_HS_WO[0] = w_hsadc_en & ~w_hsadc_error;
assign  w_ADC_HS_WO[1] = w_hsadc_init_done	;
assign  w_ADC_HS_WO[2] = w_hsadc_update_done;
assign  w_ADC_HS_WO[3] = w_hsadc_test_done	;
//
assign  w_ADC_HS_WO[7:4] = 4'b0; // not used
//
//wire [31:0] w_ADC_HS_UPD_SMP      = ep1Dwire; // loaded by init // 100
//wire [31:0] w_ADC_HS_SMP_PRD      = ep1Ewire; // loaded by init // 96ns/8ns = 12 ... 1/(96ns)=10.4167Msps
//wire [31:0] w_ADC_HS_DLY_TAP_OPT  = ep1Fwire; // loaded by init 
wire [31:0] w_ADC_HS_UPD_SMP     = (w_mcs_ep_wi_en)? w_port_wi_1D_1 : ep1Dwire;
wire [31:0] w_ADC_HS_SMP_PRD     = (w_mcs_ep_wi_en)? w_port_wi_1E_1 : ep1Ewire;
wire [31:0] w_ADC_HS_DLY_TAP_OPT = (w_mcs_ep_wi_en)? w_port_wi_1F_1 : ep1Fwire;
//
wire [9:0] w_in_delay_tap_serdes0 = w_ADC_HS_DLY_TAP_OPT[21:12]; // {ep1Fwire[31:27],ep1Fwire[26:22]} = {5'd15,5'd15}
wire [9:0] w_in_delay_tap_serdes1 = w_ADC_HS_DLY_TAP_OPT[31:22];
wire [9:0] w_in_delay_tap_serdes2 = w_ADC_HS_DLY_TAP_OPT[21:12]; // share
wire [9:0] w_in_delay_tap_serdes3 = w_ADC_HS_DLY_TAP_OPT[31:22]; // share
//
wire w_pin_test_frc_high = w_ADC_HS_DLY_TAP_OPT[0]; // loaded by init
wire w_pin_dlln_frc_low  = w_ADC_HS_DLY_TAP_OPT[1]; // loaded by init
wire w_pttn_cnt_up_en    = w_ADC_HS_DLY_TAP_OPT[2]; // loaded by init
//
control_adc_ddr_two_lane_LTC2387_reg_serdes_quad #( //$$ TODO: adc rev
	//.PERIOD_CLK_LOGIC_NS (8 ), // ns // for 125MHz @ clk_logic
	//.PERIOD_CLK_LOGIC_NS (5 ), // ns // for 200MHz @ clk_logic
	//.PERIOD_CLK_LOGIC_NS (4 ), // ns // for 250MHz @ clk_logic
	//.PERIOD_CLK_LOGIC_NS (4.76190476), // ns // for 210MHz @ clk_logic
	//.PERIOD_CLK_LOGIC_NS (5.5556 ), // ns // for 180MHz @ clk_logic
	.PERIOD_CLK_LOGIC_NS (5.12820513 ), // ns // for 195MHz @ clk_logic
	//
	//.DELAY_CLK (9), // 65ns min < 8ns*9=72ns @125MHz
	//.DELAY_CLK (14), // 65ns min < 5ns*14=70ns @200MHz
	//.DELAY_CLK (17), // 65ns min < 4ns*17=68ns @250MHz
	//.DELAY_CLK (14), // 65ns min < 4.76ns*14=66.7ns @210MHz
	//.DELAY_CLK (12), // 65ns min < 5.56ns*12=66.7ns @180MHz
	.DELAY_CLK (13), // 65ns min < 5.12820513ns*13=66.7ns @195MHz
	//
	.DAT1_OUTPUT_POLARITY(4'b0010), // ADC_01 inversion / PN swap 
	.DAT2_OUTPUT_POLARITY(4'b0010), // ADC_01 inversion / PN swap 
	.DCLK_OUTPUT_POLARITY(4'b0010), // ADC_01 inversion / PN swap 
	.MODE_ADC_CONTROL    (4'b0011) // enable adc1 adc0
	)  control_hsadc_quad_inst(
	.clk			(sys_clk), // assume 10MHz or 100ns
	.reset_n		(reset_n & ~w_hsadc_reset & ~w_rst_adc),
	.en				(w_hsadc_en), //
	//
	//.clk_logic		(clk_out4_125M), 
	.clk_logic		(base_adc_clk), 
	//
	.clk_fifo		(adc_fifo_clk), // for fifo write
	//$$.clk_bus		(okClk),         // for fifo read //$$ should be shared with MCS1
	.clk_bus		(c_fifo_read  ), // for fifo read //$$ should be shared with MCS1 
	.clk_ref_200M	(ref_200M_clk), // for serdes reference
	//
	.init			(w_hsadc_init	), //
	.update			(w_hsadc_update	), //
	.test			(w_hsadc_test	), //
	//
	.i_num_update_samples		(w_ADC_HS_UPD_SMP), 
	.i_sampling_period_count	(w_ADC_HS_SMP_PRD), 
	//
	.i_in_delay_tap_serdes0		(w_in_delay_tap_serdes0), // ({5'd15,5'd15}),
	.i_in_delay_tap_serdes1		(w_in_delay_tap_serdes1), // ({5'd15,5'd15}),
	.i_in_delay_tap_serdes2		(w_in_delay_tap_serdes2), // ({5'd15,5'd15}),
	.i_in_delay_tap_serdes3		(w_in_delay_tap_serdes3), // ({5'd15,5'd15}),
	//
	.i_pin_test_frc_high		(w_pin_test_frc_high ),
	.i_pin_dlln_frc_low 		(w_pin_dlln_frc_low  ),
	.i_pttn_cnt_up_en   		(w_pttn_cnt_up_en    ),
	//
	.o_cnv_adc				(     w_cnv_adc), //
	.o_clk_adc				(     w_clk_adc), //
	.o_pin_test_adc			(w_pin_test_adc), //
	.o_pin_duallane_adc		( w_pin_dln_adc), //
	//
	 .i_clk_in_adc0		(  w_dco_adc_0),
	.i_data_in_adc0		({w_dat2_adc_0,w_dat1_adc_0}),
	 .i_clk_in_adc1		(  w_dco_adc_1),
	.i_data_in_adc1		({w_dat2_adc_1,w_dat1_adc_1}),
	 .i_clk_in_adc2		(  w_dco_adc_2),
	.i_data_in_adc2		({w_dat2_adc_2,w_dat1_adc_2}),
	 .i_clk_in_adc3		(  w_dco_adc_3),
	.i_data_in_adc3		({w_dat2_adc_3,w_dat1_adc_3}),
	//
	 .o_data_in_fifo_0	(fifo_adc0_din		), // monitor
	 .o_data_in_fifo_1	(fifo_adc1_din		), // monitor
	 .o_data_in_fifo_2	(fifo_adc2_din		), // monitor
	 .o_data_in_fifo_3	(fifo_adc3_din		), // monitor
	 //
	      .o_wr_fifo_0	(fifo_adc0_wr		), // monitor
	      .o_wr_fifo_1	(fifo_adc1_wr		), // monitor
	      .o_wr_fifo_2	(fifo_adc2_wr		), // monitor
	      .o_wr_fifo_3	(fifo_adc3_wr		), // monitor
	 //
	.o_data_out_fifo_0	(fifo_adc0_dout		), // to usb endpoint
	      .i_rd_fifo_0	(fifo_adc0_rd_en	), // to usb endpoint
	   .o_valid_fifo_0	(fifo_adc0_valid	),
	   .o_uflow_fifo_0	(fifo_adc0_uflow	),
	  .o_pempty_fifo_0	(fifo_adc0_pempty	),
	   .o_empty_fifo_0	(fifo_adc0_empty	),
	  .o_wr_ack_fifo_0	(fifo_adc0_wr_ack	),
	   .o_oflow_fifo_0	(fifo_adc0_oflow	),
	   .o_pfull_fifo_0	(fifo_adc0_pfull	),
	    .o_full_fifo_0	(fifo_adc0_full		),
	//
	.o_data_out_fifo_1	(fifo_adc1_dout		), // to usb endpoint
	      .i_rd_fifo_1	(fifo_adc1_rd_en	), // to usb endpoint
	   .o_valid_fifo_1	(fifo_adc1_valid	),
	   .o_uflow_fifo_1	(fifo_adc1_uflow	),
	  .o_pempty_fifo_1	(fifo_adc1_pempty	),
	   .o_empty_fifo_1	(fifo_adc1_empty	),
	  .o_wr_ack_fifo_1	(fifo_adc1_wr_ack	),
	   .o_oflow_fifo_1	(fifo_adc1_oflow	),
	   .o_pfull_fifo_1	(fifo_adc1_pfull	),
	    .o_full_fifo_1	(fifo_adc1_full		),
	//
	.o_data_out_fifo_2	(fifo_adc2_dout		), // to usb endpoint
	      .i_rd_fifo_2	(fifo_adc2_rd_en	), // to usb endpoint
	   .o_valid_fifo_2	(fifo_adc2_valid	),
	   .o_uflow_fifo_2	(fifo_adc2_uflow	),
	  .o_pempty_fifo_2	(fifo_adc2_pempty	),
	   .o_empty_fifo_2	(fifo_adc2_empty	),
	  .o_wr_ack_fifo_2	(fifo_adc2_wr_ack	),
	   .o_oflow_fifo_2	(fifo_adc2_oflow	),
	   .o_pfull_fifo_2	(fifo_adc2_pfull	),
	    .o_full_fifo_2	(fifo_adc2_full		),
	//
	.o_data_out_fifo_3	(fifo_adc3_dout		), // to usb endpoint
	      .i_rd_fifo_3	(fifo_adc3_rd_en	), // to usb endpoint
	   .o_valid_fifo_3	(fifo_adc3_valid	),
	   .o_uflow_fifo_3	(fifo_adc3_uflow	),
	  .o_pempty_fifo_3	(fifo_adc3_pempty	),
	   .o_empty_fifo_3	(fifo_adc3_empty	),
	  .o_wr_ack_fifo_3	(fifo_adc3_wr_ack	),
	   .o_oflow_fifo_3	(fifo_adc3_oflow	),
	   .o_pfull_fifo_3	(fifo_adc3_pfull	),
	    .o_full_fifo_3	(fifo_adc3_full		),
	//
	.init_done		(w_hsadc_init_done	),  //
	.update_done	(w_hsadc_update_done),  //
	.test_done		(w_hsadc_test_done	),  //
	.error			(w_hsadc_error),        //
	.debug_out		()
);
//
assign w_ADC_HS_DOUT0_PO = {fifo_adc0_dout,14'b0}; // 18-bit fifo - 32-bit end-point
assign fifo_adc0_rd_en = w_ADC_HS_DOUT0_PO_RD;
//
assign w_ADC_HS_DOUT1_PO = {fifo_adc1_dout,14'b0}; // 18-bit fifo - 32-bit end-point
assign fifo_adc1_rd_en = w_ADC_HS_DOUT1_PO_RD;
//
assign w_ADC_HS_DOUT2_PO = {fifo_adc2_dout,14'b0}; // 18-bit fifo - 32-bit end-point
assign fifo_adc2_rd_en = w_ADC_HS_DOUT2_PO_RD;
//
assign w_ADC_HS_DOUT3_PO = {fifo_adc3_dout,14'b0}; // 18-bit fifo - 32-bit end-point
assign fifo_adc3_rd_en = w_ADC_HS_DOUT3_PO_RD;
////

//}


/* TODO: TEST COUNTER : test_counter_wrapper */ //{

// module //{
wire [7:0]  test_shift_pattern;
wire [7:0]  count1;
wire        count1eq00;
wire        count1eq80;
wire        reset1;
wire        disable1;
wire [7:0]  count2;
wire        count2eqFF;
wire        reset2;
wire        up2;
wire        down2;
wire        autocount2;
//
test_counter_wrapper  test_counter_wrapper_inst (
	.sys_clk       (sys_clk),
	.reset_n       (reset_n & (~w_HW_reset)),
	//
	.o_count1      (count1),
	.reset1        (reset1),
	.disable1      (disable1),
	.o_count1eq00  (count1eq00),
	.o_count1eq80  (count1eq80),
	//
	.o_count2      (count2),
	.reset2        (reset2    ),
	.up2           (up2       ),
	.down2         (down2     ),
	.autocount2    (autocount2),
	.o_count2eqFF  (count2eqFF),
	//             
	.o_test        (test_shift_pattern) // circular right shift pattern
);
//}

// assignment //{

// 
assign reset1     = w_TEST_CON[0]; //$$
assign disable1   = w_TEST_CON[1]; //$$
assign autocount2 = w_TEST_CON[2]; //$$
//
assign w_TEST_OUT[7:0]  = count1[7:0];
assign w_TEST_OUT[15:8] = count2[7:0];

// 
//  wire [2:0] count2_trig_ext_data;
//  assign reset2     = w_TEST_TI[0] | count2_trig_ext_data[0];
//  assign up2        = w_TEST_TI[1] | count2_trig_ext_data[1];
//  assign down2      = w_TEST_TI[2] | count2_trig_ext_data[2];
assign reset2     = w_TEST_TI[0];
assign up2        = w_TEST_TI[1];
assign down2      = w_TEST_TI[2];

//
assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};

//assign w_TEST_FLAG_WO[15:0] = {count2[7:0], count1[7:0]}; 
//  assign w_TEST_FLAG_WO[15:0] = 
//  	(w_SSPI_TEST_mode_en)? 
//  	w_SSPI_TEST_WO[15:0]       :
//  	{count2[7:0], count1[7:0]} ; //$$ share with SSPI_TEST
//  assign w_TEST_FLAG_WO[23]    = w_SSPI_TEST_mode_en; //$$
//
//  assign w_TEST_FLAG_WO[22:20] = 3'b0; //$$ not yet used
//
//  assign w_SSPI_TEST_WO[31:16] = 16'b0; //$$ not used


//}

//}


/* TODO: LED */ //{

// function for LED //{
function [7:0] xem7310_led;
input [7:0] a;
integer i;
begin
	for(i=0; i<8; i=i+1) begin: for_xem7310_led
		// inverted and high-Z
		// to turn on LED ... logic '0' 
		xem7310_led[i] = (a[i]==1'b1) ? (1'b0) : (1'bz);
	end
end
endfunction
//}

// assignment for LED //{
wire [7:0] w_test_led = count1 ^ test_shift_pattern; // from test counter
assign led = xem7310_led(w_test_led);
//}

//}


/* TODO: pulse loopback test */ //{

//wire [31:0] w_TEST_TI_HS = ep41trig;
//wire [31:0] w_TEST_TI_HS = (w_mcs_ep_ti_en)? w_port_ti_41_1 : ep41trig;
//
(* keep = "true" *) wire w_pulse_out;
(* keep = "true" *) wire w_pulse_loopback_in;
//
wire test_clk = base_adc_clk;
//
(* keep = "true" *) wire w_enable__pulse_loopback = w_TEST_CON[7];
//
(* keep = "true" *) wire w_trig__pulse_shot = w_TEST_TI_HS[1] | w_TEST_TI[8];
//
(* keep = "true" *) reg [7:0] r_cnt__pulse_period;
(* keep = "true" *) reg [7:0] r_cnt__pulse_width;
(* keep = "true" *) reg [7:0] r_cnt__pulse_num;
//
(* keep = "true" *) wire [7:0] w_set__pulse_period = w_TEST_CON[31:24];
(* keep = "true" *) wire [7:0] w_set__pulse_width  = w_TEST_CON[23:16];
(* keep = "true" *) wire [7:0] w_set__pulse_num    = w_TEST_CON[15:8];
//
reg r_pulse_out;
	assign w_pulse_out = r_pulse_out;
//
always @(posedge test_clk, negedge reset_n) begin
	if (!reset_n) begin
		r_pulse_out         <= 1'b0; 
		r_cnt__pulse_period <= 8'b0; 
		r_cnt__pulse_width  <= 8'b0; 
		r_cnt__pulse_num    <= 8'b0; 
		end
	else begin
		//
		if (!w_enable__pulse_loopback) begin
			r_pulse_out         <= 1'b0; 
			r_cnt__pulse_period <= 8'b0; 
			r_cnt__pulse_width  <= 8'b0; 
			r_cnt__pulse_num    <= 8'b0; 
			end
		else if (w_trig__pulse_shot) begin 
			r_pulse_out         <= 1'b0; 
			r_cnt__pulse_period <= (w_set__pulse_period==0)? r_cnt__pulse_period : w_set__pulse_period; 
			r_cnt__pulse_width  <= (w_set__pulse_width ==0)? r_cnt__pulse_width  : w_set__pulse_width ; 
			r_cnt__pulse_num    <= (w_set__pulse_num   ==0)? r_cnt__pulse_num    : w_set__pulse_num   ; 
			end
		else begin 
			if (r_cnt__pulse_width  >0)
				r_pulse_out         <= 1'b1; 
			else 
				r_pulse_out         <= 1'b0; 
			//
			if (r_cnt__pulse_period >1)
				r_cnt__pulse_period <= r_cnt__pulse_period - 1; 
			else if (r_cnt__pulse_period == 1 && r_cnt__pulse_num >1)
				r_cnt__pulse_period <= w_set__pulse_period; 
			else 
				r_cnt__pulse_period <= 8'b0;
			//
			if (r_cnt__pulse_width  >1)
				r_cnt__pulse_width  <= r_cnt__pulse_width  - 1; 
			else if (r_cnt__pulse_period == 1 && r_cnt__pulse_num >1)
				r_cnt__pulse_width  <= w_set__pulse_width ; 
			else 
				r_cnt__pulse_width <= 8'b0;
			//
			if (r_cnt__pulse_period == 1 && r_cnt__pulse_num >0)
				r_cnt__pulse_num    <= r_cnt__pulse_num    - 1; 
			end
		//
		end
end
//
(* keep = "true" *) reg [15:0] r_cnt__pulse_loopback;
	assign w_TEST_OUT[31:16] = r_cnt__pulse_loopback; 
//
//(* keep = "true" *) wire w_reset__pulse_loopback_cnt = w_TEST_TI_125M[0] | w_TEST_TI[9];
(* keep = "true" *) wire w_reset__pulse_loopback_cnt = w_TEST_TI_HS[0] | w_TEST_TI[9];
//
reg [1:0] r_smp__pulse_loopback_in;
wire w_rise__pulse_loopback_in = ~r_smp__pulse_loopback_in[1]&r_smp__pulse_loopback_in[0];
//
always @(posedge test_clk, negedge reset_n) begin
	if (!reset_n) begin
		r_smp__pulse_loopback_in <= 2'b0;
		r_cnt__pulse_loopback    <= 16'd0; 
		end
	else begin
		//
		r_smp__pulse_loopback_in <= {r_smp__pulse_loopback_in[0], w_pulse_loopback_in};
		//
		if (w_reset__pulse_loopback_cnt) begin
			r_cnt__pulse_loopback <= 16'd0; 
			end
		else if (w_rise__pulse_loopback_in) begin
			r_cnt__pulse_loopback <= r_cnt__pulse_loopback + 16'd1;
			end 
		//
		end
end
////

//}


/* TODO: TEMP SENSOR */ //{
// support MAX6576ZUT+T
// temp signal count by 12MHz
// net in sch: FPGA_IO_B
// pin: i_B35_L6P
// check signal on debugger 
// test
(* keep = "true" *) wire w_temp_sig;
(* keep = "true" *) reg r_temp_sig;
(* keep = "true" *) reg r_toggle_temp_sig;
(* keep = "true" *) wire w_rise_temp_sig = ~r_temp_sig & w_temp_sig;
//reg [15:0] r_subcnt_temp_sig_period;
//reg [15:0] r_period_temp_sig_period;
//
wire tmps_clk = lan_io_clk;
// 
//
always @(posedge tmps_clk, negedge reset_n) begin
	if (!reset_n) begin
		r_temp_sig     <= 1'b0;
		r_toggle_temp_sig  <= 1'b0;
		end
	else begin
		//
		r_temp_sig     <= w_temp_sig;
		//
		if (w_rise_temp_sig) begin 
			r_toggle_temp_sig <= ~r_toggle_temp_sig;
			end
		end
end
//}


/* TODO: TP for TXEM7310 */ //{
(* keep = "true" *) wire [7:0] TP_out;
(* keep = "true" *) wire [7:0] TP_tri; // enable high-Z
(* keep = "true" *) wire [7:0] TP_in;

//assign TP_out[0] = 1'b1;     // for EEPROM test 
//assign TP_out[1] = 1'b0;     // for EEPROM test 
//assign TP_out[2] = 1'b0;    // for EEPROM test
assign TP_out[3] = 1'b0; 
assign TP_out[4] = 1'b0; 
assign TP_out[5] = 1'b0; 
assign TP_out[6] = 1'b0; 
assign TP_out[7] = 1'b0; 

//assign TP_tri[0] = 1'b0; // '0' enable output // for EEPROM test
//assign TP_tri[1] = 1'b0; // '0' enable output // for EEPROM test
//assign TP_tri[2] = 1'b1;                      // for EEPROM test
assign TP_tri[3] = 1'b1; 
assign TP_tri[4] = 1'b1; 
assign TP_tri[5] = 1'b1; 
assign TP_tri[6] = 1'b1; 
assign TP_tri[7] = 1'b1; 


//}


/* TODO: EEPROM : 11AA160T-I/TT */ //{
// support 11AA160T-I/TT U59
// io signal, open-drain
// note that 10K ohm pull up is located on board.
// net in sch: FPGA_IO_A
// pin: io_B13_L1N

//// high-Z control style ports //{
wire  MEM_SIO_out; //  = 1'b1;
wire  MEM_SIO_tri; //  = 1'b1; // enable high-Z
wire  MEM_SIO_in;
//}

//// path control
// wire w_sel__H_LAN_for_EEPROM_fifo = w_MCS_SETUP_WI[8];
// wire w_sel__H_EEPROM_on_TP        = w_MCS_SETUP_WI[9];


// fifo read clock //{
wire c_eeprom_fifo_clk; // clock mux between lan and slave-spi end-points
//
BUFGMUX bufgmux_c_eeprom_fifo_clk_inst (
	.O(c_eeprom_fifo_clk), 
	//.I0(base_sspi_clk), // base_sspi_clk for slave_spi_mth_brd // 104MHz
	.I0(okClk        ), // USB  // 108MHz
	.I1(w_ck_pipe    ), // LAN from lan_endpoint_wrapper_inst      // 72MHz
	.S(w_sel__H_LAN_for_EEPROM_fifo) 
);
//}

// endpoint wires and mux //{
wire [31:0] w_MEM_WI      = (w_sel__H_LAN_for_EEPROM_fifo)? w_port_wi_13_1 : ep13wire;                                        
wire [31:0] w_MEM_FDAT_WI = (w_sel__H_LAN_for_EEPROM_fifo)? w_port_wi_12_1 : ep12wire;                                        
wire [31:0] w_MEM_TI      = w_port_ti_53_1 | ep53trig; 
wire [31:0] w_MEM_TO; 
	assign ep73trig       = w_MEM_TO; 
	assign w_port_to_73_1 = w_MEM_TO; 
wire [31:0] w_MEM_PI = (w_sel__H_LAN_for_EEPROM_fifo)? w_port_pi_93_1 : ep93pipe;
wire w_MEM_PI_wr = w_wr_93_1 | ep93wr;                  
wire [31:0] w_MEM_PO; 
	assign epB3pipe       = w_MEM_PO; 
	assign w_port_po_B3_1 = w_MEM_PO; 
wire w_MEM_PO_rd = w_rd_B3_1 | epB3rd; 

//}

// wires for module //{
wire w_MEM_rst;
wire w_MEM_fifo_rst;
wire w_MEM_valid;
//
wire w_trig_frame   ;
wire w_done_frame   ;
wire w_done_frame_TO;
//
wire [7:0] w_frame_data_CMD    ;
wire [7:0] w_frame_data_ADH    ;
wire [7:0] w_frame_data_ADL    ;
wire [7:0] w_frame_data_STA_in ;
wire [7:0] w_frame_data_STA_out;
//
wire [11:0] w_num_bytes_DAT    ;
wire        w_con_disable_SBP;
//
wire [7:0] w_frame_data_DAT_wr   ;
wire [7:0] w_frame_data_DAT_rd   ;
wire       w_frame_data_DAT_wr_en;
wire       w_frame_data_DAT_rd_en;
//
wire w_SCIO_DI;
wire w_SCIO_DO;
wire w_SCIO_OE;
//}

//
control_eeprom__11AA160T  control_eeprom__11AA160T_inst (
	//
	.clk     (sys_clk                ), //	input  wire // 10MHz
	.reset_n (reset_n & (~w_MEM_rst) & (~w_HW_reset)), //	input  wire // TI
	
	// controls //{
	.i_trig_frame     (w_trig_frame   ), //	input  wire                    // TI
	.o_done_frame     (w_done_frame   ), //	output wire                    // TO
	.o_done_frame_TO  (w_done_frame_TO), //	output wire  // trig out @ clk // TO
	//	
	.i_en_SBP         (~w_con_disable_SBP  ), //	input  wire  // enable SBP (stand-by pulse) // WI
	//
	.i_frame_data_SHD (8'h55           ), //	input  wire [7:0]  // fixed
	.i_frame_data_DVA (8'hA0           ), //	input  wire [7:0]  // fixed
	.i_frame_data_CMD (w_frame_data_CMD    ), //	input  wire [7:0]  // WI
	.i_frame_data_ADH (w_frame_data_ADH    ), //	input  wire [7:0]  // WI
	.i_frame_data_ADL (w_frame_data_ADL    ), //	input  wire [7:0]  // WI
	.i_frame_data_STA (w_frame_data_STA_in ), //	input  wire [7:0]  // WI
	.o_frame_data_STA (w_frame_data_STA_out), //	output wire [7:0]  // TO
	//	
	.i_num_bytes_DAT  (w_num_bytes_DAT     ), //	input  wire [11:0] // WI
	//}
	
	// FIFO/PIPE interface //{
	.i_reset_fifo             (w_MEM_fifo_rst), //	input  wire    // TI
	//
	.i_frame_data_DAT         (w_frame_data_DAT_wr   ), //	input  wire [7:0] // PI
	.o_frame_data_DAT         (w_frame_data_DAT_rd   ), //	output wire [7:0] // PO
	//
	.i_frame_data_DAT_wr_en   (w_frame_data_DAT_wr_en), // input wire // control for i_frame_data_DAT
	.i_frame_data_DAT_wr_clk  (c_eeprom_fifo_clk     ), // input wire // control for i_frame_data_DAT
	.i_frame_data_DAT_rd_en   (w_frame_data_DAT_rd_en), // input wire // control for o_frame_data_DAT	
	.i_frame_data_DAT_rd_clk  (c_eeprom_fifo_clk     ), // input wire // control for o_frame_data_DAT	
	//}
	
	// IO ports //{
	.i_SCIO_DI        (w_SCIO_DI), // input  wire 
	.o_SCIO_DO        (w_SCIO_DO), // output wire 
	.o_SCIO_OE        (w_SCIO_OE), // output wire 
	//}

	.valid            (w_MEM_valid)//	output wire 
);

// assignments //{

assign w_num_bytes_DAT               = w_MEM_WI[11:0]; // 12-bit 
assign w_con_disable_SBP             = w_MEM_WI[15]  ; // 1-bit

assign w_frame_data_CMD              = w_MEM_FDAT_WI[ 7: 0]; // 8-bit
assign w_frame_data_STA_in           = w_MEM_FDAT_WI[15: 8]; // 8-bit
assign w_frame_data_ADL              = w_MEM_FDAT_WI[23:16]; // 8-bit
assign w_frame_data_ADH              = w_MEM_FDAT_WI[31:24]; // 8-bit

assign w_MEM_rst      = w_MEM_TI[0];
assign w_MEM_fifo_rst = w_MEM_TI[1];
assign w_trig_frame   = w_MEM_TI[2];

assign w_MEM_TO[0]     = w_MEM_valid    ;
assign w_MEM_TO[1]     = w_done_frame   ;
assign w_MEM_TO[2]     = w_done_frame_TO;
assign w_MEM_TO[ 7: 3] =  5'b0;
assign w_MEM_TO[15: 8] = w_frame_data_STA_out; 
assign w_MEM_TO[31:16] = 16'b0;

assign w_frame_data_DAT_wr    = w_MEM_PI[7:0]; // 8-bit
assign w_frame_data_DAT_wr_en = w_MEM_PI_wr;

assign w_MEM_PO[7:0]          = w_frame_data_DAT_rd; // 8-bit
assign w_MEM_PO[31:8]         = {24{w_frame_data_DAT_rd[7]}}; // rev signed expansion for compatibility
assign w_frame_data_DAT_rd_en = w_MEM_PO_rd;

//// ports
assign w_SCIO_DI   = ( w_sel__H_EEPROM_on_TP)?   TP_in[2] : MEM_SIO_in ; // switching
//                     
assign TP_out[2]   = ( w_sel__H_EEPROM_on_TP)?  w_SCIO_DO : 1'b0 ; // test TP 
assign TP_tri[2]   = ( w_sel__H_EEPROM_on_TP)? ~w_SCIO_OE : 1'b1 ; // test TP 
//
assign TP_out[0]   = ( w_sel__H_EEPROM_on_TP)? 1'b1 : 1'b0 ; // for test power (3.3V) on signal line
assign TP_out[1]   = ( w_sel__H_EEPROM_on_TP)? 1'b0 : 1'b0 ; // for test power (GND)  on signal line
assign TP_tri[0]   = ( w_sel__H_EEPROM_on_TP)? 1'b0 : 1'b1 ; // for test power on signal line
assign TP_tri[1]   = ( w_sel__H_EEPROM_on_TP)? 1'b0 : 1'b1 ; // for test power on signal line
//                     
assign MEM_SIO_out = (~w_sel__H_EEPROM_on_TP)?  w_SCIO_DO : 1'b0 ; // dedicated port
assign MEM_SIO_tri = (~w_sel__H_EEPROM_on_TP)? ~w_SCIO_OE : 1'b1 ; // dedicated port


//}

////

//}


///TODO: //-------------------------------------------------------//

/* TODO: okHost : ok_endpoint_wrapper */ //{
//$$ Endpoints
// Wire In 		0x00 - 0x1F
// Wire Out 	0x20 - 0x3F
// Trigger In 	0x40 - 0x5F
// Trigger Out 	0x60 - 0x7F
// Pipe In 		0x80 - 0x9F
// Pipe Out 	0xA0 - 0xBF
ok_endpoint_wrapper  ok_endpoint_wrapper_inst (
	.okUH (okUH ), //input  wire [4:0]   okUH, // external pins
	.okHU (okHU ), //output wire [2:0]   okHU, // external pins
	.okUHU(okUHU), //inout  wire [31:0]  okUHU, // external pins
	.okAA (okAA ), //inout  wire         okAA, // external pin
	// Wire In 		0x00 - 0x1F
	.ep00wire(ep00wire), // output wire [31:0]
	.ep01wire(ep01wire), // output wire [31:0]
	.ep02wire(ep02wire), // output wire [31:0]
	.ep03wire(ep03wire), // output wire [31:0]
	.ep04wire(ep04wire), // output wire [31:0]
	.ep05wire(ep05wire), // output wire [31:0]
	.ep06wire(ep06wire), // output wire [31:0]
	.ep07wire(ep07wire), // output wire [31:0]
	.ep08wire(ep08wire), // output wire [31:0]
	.ep09wire(ep09wire), // output wire [31:0]
	.ep0Awire(ep0Awire), // output wire [31:0]
	.ep0Bwire(ep0Bwire), // output wire [31:0]
	.ep0Cwire(ep0Cwire), // output wire [31:0]
	.ep0Dwire(ep0Dwire), // output wire [31:0]
	.ep0Ewire(ep0Ewire), // output wire [31:0]
	.ep0Fwire(ep0Fwire), // output wire [31:0]
	.ep10wire(ep10wire), // output wire [31:0]
	.ep11wire(ep11wire), // output wire [31:0]
	.ep12wire(ep12wire), // output wire [31:0]
	.ep13wire(ep13wire), // output wire [31:0]
	.ep14wire(ep14wire), // output wire [31:0]
	.ep15wire(ep15wire), // output wire [31:0]
	.ep16wire(ep16wire), // output wire [31:0]
	.ep17wire(ep17wire), // output wire [31:0]
	.ep18wire(ep18wire), // output wire [31:0]
	.ep19wire(ep19wire), // output wire [31:0]
	.ep1Awire(ep1Awire), // output wire [31:0]
	.ep1Bwire(ep1Bwire), // output wire [31:0]
	.ep1Cwire(ep1Cwire), // output wire [31:0]
	.ep1Dwire(ep1Dwire), // output wire [31:0]
	.ep1Ewire(ep1Ewire), // output wire [31:0]
	.ep1Fwire(ep1Fwire), // output wire [31:0]
	// Wire Out 	0x20 - 0x3F
	.ep20wire(ep20wire), // input wire [31:0]
	.ep21wire(ep21wire), // input wire [31:0]
	.ep22wire(ep22wire), // input wire [31:0]
	.ep23wire(ep23wire), // input wire [31:0]
	.ep24wire(ep24wire), // input wire [31:0]
	.ep25wire(ep25wire), // input wire [31:0]
	.ep26wire(ep26wire), // input wire [31:0]
	.ep27wire(ep27wire), // input wire [31:0]
	.ep28wire(ep28wire), // input wire [31:0]
	.ep29wire(ep29wire), // input wire [31:0]
	.ep2Awire(ep2Awire), // input wire [31:0]
	.ep2Bwire(ep2Bwire), // input wire [31:0]
	.ep2Cwire(ep2Cwire), // input wire [31:0]
	.ep2Dwire(ep2Dwire), // input wire [31:0]
	.ep2Ewire(ep2Ewire), // input wire [31:0]
	.ep2Fwire(ep2Fwire), // input wire [31:0]
	.ep30wire(ep30wire), // input wire [31:0]
	.ep31wire(ep31wire), // input wire [31:0]
	.ep32wire(ep32wire), // input wire [31:0]
	.ep33wire(ep33wire), // input wire [31:0]
	.ep34wire(ep34wire), // input wire [31:0]
	.ep35wire(ep35wire), // input wire [31:0]
	.ep36wire(ep36wire), // input wire [31:0]
	.ep37wire(ep37wire), // input wire [31:0]
	.ep38wire(ep38wire), // input wire [31:0]
	.ep39wire(ep39wire), // input wire [31:0]
	.ep3Awire(ep3Awire), // input wire [31:0]
	.ep3Bwire(ep3Bwire), // input wire [31:0]
	.ep3Cwire(ep3Cwire), // input wire [31:0]
	.ep3Dwire(ep3Dwire), // input wire [31:0]
	.ep3Ewire(ep3Ewire), // input wire [31:0]
	.ep3Fwire(ep3Fwire), // input wire [31:0]
	// Trigger In 	0x40 - 0x5F
	.ep40ck(ep40ck), .ep40trig(ep40trig), // input wire, output wire [31:0],
	.ep41ck(ep41ck), .ep41trig(ep41trig), // input wire, output wire [31:0],
	.ep42ck(ep42ck), .ep42trig(ep42trig), // input wire, output wire [31:0],
	.ep43ck(ep43ck), .ep43trig(ep43trig), // input wire, output wire [31:0],
	.ep44ck(ep44ck), .ep44trig(ep44trig), // input wire, output wire [31:0],
	.ep45ck(ep45ck), .ep45trig(ep45trig), // input wire, output wire [31:0],
	.ep46ck(ep46ck), .ep46trig(ep46trig), // input wire, output wire [31:0],
	.ep47ck(ep47ck), .ep47trig(ep47trig), // input wire, output wire [31:0],
	.ep48ck(ep48ck), .ep48trig(ep48trig), // input wire, output wire [31:0],
	.ep49ck(ep49ck), .ep49trig(ep49trig), // input wire, output wire [31:0],
	.ep4Ack(ep4Ack), .ep4Atrig(ep4Atrig), // input wire, output wire [31:0],
	.ep4Bck(ep4Bck), .ep4Btrig(ep4Btrig), // input wire, output wire [31:0],
	.ep4Cck(ep4Cck), .ep4Ctrig(ep4Ctrig), // input wire, output wire [31:0],
	.ep4Dck(ep4Dck), .ep4Dtrig(ep4Dtrig), // input wire, output wire [31:0],
	.ep4Eck(ep4Eck), .ep4Etrig(ep4Etrig), // input wire, output wire [31:0],
	.ep4Fck(ep4Fck), .ep4Ftrig(ep4Ftrig), // input wire, output wire [31:0],
	.ep50ck(ep50ck), .ep50trig(ep50trig), // input wire, output wire [31:0],
	.ep51ck(ep51ck), .ep51trig(ep51trig), // input wire, output wire [31:0],
	.ep52ck(ep52ck), .ep52trig(ep52trig), // input wire, output wire [31:0],
	.ep53ck(ep53ck), .ep53trig(ep53trig), // input wire, output wire [31:0],
	.ep54ck(ep54ck), .ep54trig(ep54trig), // input wire, output wire [31:0],
	.ep55ck(ep55ck), .ep55trig(ep55trig), // input wire, output wire [31:0],
	.ep56ck(ep56ck), .ep56trig(ep56trig), // input wire, output wire [31:0],
	.ep57ck(ep57ck), .ep57trig(ep57trig), // input wire, output wire [31:0],
	.ep58ck(ep58ck), .ep58trig(ep58trig), // input wire, output wire [31:0],
	.ep59ck(ep59ck), .ep59trig(ep59trig), // input wire, output wire [31:0],
	.ep5Ack(ep5Ack), .ep5Atrig(ep5Atrig), // input wire, output wire [31:0],
	.ep5Bck(ep5Bck), .ep5Btrig(ep5Btrig), // input wire, output wire [31:0],
	.ep5Cck(ep5Cck), .ep5Ctrig(ep5Ctrig), // input wire, output wire [31:0],
	.ep5Dck(ep5Dck), .ep5Dtrig(ep5Dtrig), // input wire, output wire [31:0],
	.ep5Eck(ep5Eck), .ep5Etrig(ep5Etrig), // input wire, output wire [31:0],
	.ep5Fck(ep5Fck), .ep5Ftrig(ep5Ftrig), // input wire, output wire [31:0],
	// Trigger Out 	0x60 - 0x7F
	.ep60ck(ep60ck), .ep60trig(ep60trig), // input wire, input wire [31:0],
	.ep61ck(ep61ck), .ep61trig(ep61trig), // input wire, input wire [31:0],
	.ep62ck(ep62ck), .ep62trig(ep62trig), // input wire, input wire [31:0],
	.ep63ck(ep63ck), .ep63trig(ep63trig), // input wire, input wire [31:0],
	.ep64ck(ep64ck), .ep64trig(ep64trig), // input wire, input wire [31:0],
	.ep65ck(ep65ck), .ep65trig(ep65trig), // input wire, input wire [31:0],
	.ep66ck(ep66ck), .ep66trig(ep66trig), // input wire, input wire [31:0],
	.ep67ck(ep67ck), .ep67trig(ep67trig), // input wire, input wire [31:0],
	.ep68ck(ep68ck), .ep68trig(ep68trig), // input wire, input wire [31:0],
	.ep69ck(ep69ck), .ep69trig(ep69trig), // input wire, input wire [31:0],
	.ep6Ack(ep6Ack), .ep6Atrig(ep6Atrig), // input wire, input wire [31:0],
	.ep6Bck(ep6Bck), .ep6Btrig(ep6Btrig), // input wire, input wire [31:0],
	.ep6Cck(ep6Cck), .ep6Ctrig(ep6Ctrig), // input wire, input wire [31:0],
	.ep6Dck(ep6Dck), .ep6Dtrig(ep6Dtrig), // input wire, input wire [31:0],
	.ep6Eck(ep6Eck), .ep6Etrig(ep6Etrig), // input wire, input wire [31:0],
	.ep6Fck(ep6Fck), .ep6Ftrig(ep6Ftrig), // input wire, input wire [31:0],
	.ep70ck(ep70ck), .ep70trig(ep70trig), // input wire, input wire [31:0],
	.ep71ck(ep71ck), .ep71trig(ep71trig), // input wire, input wire [31:0],
	.ep72ck(ep72ck), .ep72trig(ep72trig), // input wire, input wire [31:0],
	.ep73ck(ep73ck), .ep73trig(ep73trig), // input wire, input wire [31:0],
	.ep74ck(ep74ck), .ep74trig(ep74trig), // input wire, input wire [31:0],
	.ep75ck(ep75ck), .ep75trig(ep75trig), // input wire, input wire [31:0],
	.ep76ck(ep76ck), .ep76trig(ep76trig), // input wire, input wire [31:0],
	.ep77ck(ep77ck), .ep77trig(ep77trig), // input wire, input wire [31:0],
	.ep78ck(ep78ck), .ep78trig(ep78trig), // input wire, input wire [31:0],
	.ep79ck(ep79ck), .ep79trig(ep79trig), // input wire, input wire [31:0],
	.ep7Ack(ep7Ack), .ep7Atrig(ep7Atrig), // input wire, input wire [31:0],
	.ep7Bck(ep7Bck), .ep7Btrig(ep7Btrig), // input wire, input wire [31:0],
	.ep7Cck(ep7Cck), .ep7Ctrig(ep7Ctrig), // input wire, input wire [31:0],
	.ep7Dck(ep7Dck), .ep7Dtrig(ep7Dtrig), // input wire, input wire [31:0],
	.ep7Eck(ep7Eck), .ep7Etrig(ep7Etrig), // input wire, input wire [31:0],
	.ep7Fck(ep7Fck), .ep7Ftrig(ep7Ftrig), // input wire, input wire [31:0],
	// Pipe In 		0x80 - 0x9F
	.ep80wr(ep80wr), .ep80pipe(ep80pipe), // output wire, output wire [31:0],
	.ep81wr(ep81wr), .ep81pipe(ep81pipe), // output wire, output wire [31:0],
	.ep82wr(ep82wr), .ep82pipe(ep82pipe), // output wire, output wire [31:0],
	.ep83wr(ep83wr), .ep83pipe(ep83pipe), // output wire, output wire [31:0],
	.ep84wr(ep84wr), .ep84pipe(ep84pipe), // output wire, output wire [31:0],
	.ep85wr(ep85wr), .ep85pipe(ep85pipe), // output wire, output wire [31:0],
	.ep86wr(ep86wr), .ep86pipe(ep86pipe), // output wire, output wire [31:0],
	.ep87wr(ep87wr), .ep87pipe(ep87pipe), // output wire, output wire [31:0],
	.ep88wr(ep88wr), .ep88pipe(ep88pipe), // output wire, output wire [31:0],
	.ep89wr(ep89wr), .ep89pipe(ep89pipe), // output wire, output wire [31:0],
	.ep8Awr(ep8Awr), .ep8Apipe(ep8Apipe), // output wire, output wire [31:0],
	.ep8Bwr(ep8Bwr), .ep8Bpipe(ep8Bpipe), // output wire, output wire [31:0],
	.ep8Cwr(ep8Cwr), .ep8Cpipe(ep8Cpipe), // output wire, output wire [31:0],
	.ep8Dwr(ep8Dwr), .ep8Dpipe(ep8Dpipe), // output wire, output wire [31:0],
	.ep8Ewr(ep8Ewr), .ep8Epipe(ep8Epipe), // output wire, output wire [31:0],
	.ep8Fwr(ep8Fwr), .ep8Fpipe(ep8Fpipe), // output wire, output wire [31:0],
	.ep90wr(ep90wr), .ep90pipe(ep90pipe), // output wire, output wire [31:0],
	.ep91wr(ep91wr), .ep91pipe(ep91pipe), // output wire, output wire [31:0],
	.ep92wr(ep92wr), .ep92pipe(ep92pipe), // output wire, output wire [31:0],
	.ep93wr(ep93wr), .ep93pipe(ep93pipe), // output wire, output wire [31:0],
	.ep94wr(ep94wr), .ep94pipe(ep94pipe), // output wire, output wire [31:0],
	.ep95wr(ep95wr), .ep95pipe(ep95pipe), // output wire, output wire [31:0],
	.ep96wr(ep96wr), .ep96pipe(ep96pipe), // output wire, output wire [31:0],
	.ep97wr(ep97wr), .ep97pipe(ep97pipe), // output wire, output wire [31:0],
	.ep98wr(ep98wr), .ep98pipe(ep98pipe), // output wire, output wire [31:0],
	.ep99wr(ep99wr), .ep99pipe(ep99pipe), // output wire, output wire [31:0],
	.ep9Awr(ep9Awr), .ep9Apipe(ep9Apipe), // output wire, output wire [31:0],
	.ep9Bwr(ep9Bwr), .ep9Bpipe(ep9Bpipe), // output wire, output wire [31:0],
	.ep9Cwr(ep9Cwr), .ep9Cpipe(ep9Cpipe), // output wire, output wire [31:0],
	.ep9Dwr(ep9Dwr), .ep9Dpipe(ep9Dpipe), // output wire, output wire [31:0],
	.ep9Ewr(ep9Ewr), .ep9Epipe(ep9Epipe), // output wire, output wire [31:0],
	.ep9Fwr(ep9Fwr), .ep9Fpipe(ep9Fpipe), // output wire, output wire [31:0],
	// Pipe Out 	0xA0 - 0xBF
	.epA0rd(epA0rd), .epA0pipe(epA0pipe), // output wire, input wire [31:0],
	.epA1rd(epA1rd), .epA1pipe(epA1pipe), // output wire, input wire [31:0],
	.epA2rd(epA2rd), .epA2pipe(epA2pipe), // output wire, input wire [31:0],
	.epA3rd(epA3rd), .epA3pipe(epA3pipe), // output wire, input wire [31:0],
	.epA4rd(epA4rd), .epA4pipe(epA4pipe), // output wire, input wire [31:0],
	.epA5rd(epA5rd), .epA5pipe(epA5pipe), // output wire, input wire [31:0],
	.epA6rd(epA6rd), .epA6pipe(epA6pipe), // output wire, input wire [31:0],
	.epA7rd(epA7rd), .epA7pipe(epA7pipe), // output wire, input wire [31:0],
	.epA8rd(epA8rd), .epA8pipe(epA8pipe), // output wire, input wire [31:0],
	.epA9rd(epA9rd), .epA9pipe(epA9pipe), // output wire, input wire [31:0],
	.epAArd(epAArd), .epAApipe(epAApipe), // output wire, input wire [31:0],
	.epABrd(epABrd), .epABpipe(epABpipe), // output wire, input wire [31:0],
	.epACrd(epACrd), .epACpipe(epACpipe), // output wire, input wire [31:0],
	.epADrd(epADrd), .epADpipe(epADpipe), // output wire, input wire [31:0],
	.epAErd(epAErd), .epAEpipe(epAEpipe), // output wire, input wire [31:0],
	.epAFrd(epAFrd), .epAFpipe(epAFpipe), // output wire, input wire [31:0],
	.epB0rd(epB0rd), .epB0pipe(epB0pipe), // output wire, input wire [31:0],
	.epB1rd(epB1rd), .epB1pipe(epB1pipe), // output wire, input wire [31:0],
	.epB2rd(epB2rd), .epB2pipe(epB2pipe), // output wire, input wire [31:0],
	.epB3rd(epB3rd), .epB3pipe(epB3pipe), // output wire, input wire [31:0],
	.epB4rd(epB4rd), .epB4pipe(epB4pipe), // output wire, input wire [31:0],
	.epB5rd(epB5rd), .epB5pipe(epB5pipe), // output wire, input wire [31:0],
	.epB6rd(epB6rd), .epB6pipe(epB6pipe), // output wire, input wire [31:0],
	.epB7rd(epB7rd), .epB7pipe(epB7pipe), // output wire, input wire [31:0],
	.epB8rd(epB8rd), .epB8pipe(epB8pipe), // output wire, input wire [31:0],
	.epB9rd(epB9rd), .epB9pipe(epB9pipe), // output wire, input wire [31:0],
	.epBArd(epBArd), .epBApipe(epBApipe), // output wire, input wire [31:0],
	.epBBrd(epBBrd), .epBBpipe(epBBpipe), // output wire, input wire [31:0],
	.epBCrd(epBCrd), .epBCpipe(epBCpipe), // output wire, input wire [31:0],
	.epBDrd(epBDrd), .epBDpipe(epBDpipe), // output wire, input wire [31:0],
	.epBErd(epBErd), .epBEpipe(epBEpipe), // output wire, input wire [31:0],
	.epBFrd(epBFrd), .epBFpipe(epBFpipe), // output wire, input wire [31:0],
	// 
	.okClk(okClk)//output wire okClk // sync with write/read of pipe
	);
////

//}

///TODO: //-------------------------------------------------------//


/* TODO: BANK signals */

//$$  // DAC_A2A3 //{
//$$  OBUF obuf_DAC_A2A3_MOSI_inst (.O(o_B13_L1P), .I(DAC_A2A3_MOSI) ); // DAC_A2A3_MOSI // CC8
//$$  OBUF obuf_DAC_A2A3_SCLK_inst (.O(o_B13_L4N), .I(DAC_A2A3_SCLK) ); // DAC_A2A3_SCLK // CC9
//$$  OBUF obuf_DAC_A2A3_SYNB_inst (.O(o_B13_L4P), .I(DAC_A2A3_SYNB) ); // DAC_A2A3_SYNB // CC10
//$$  IBUF ibuf_DAC_A2A3_MISO_inst (.I(i_B13_L2P), .O(DAC_A2A3_MISO) ); // DAC_A2A3_MISO // CC12
//$$  //}

//// mux DAC_A2A3 and SPIO //{

// [pins on b'rd] [nets on sch]  in CMU-CPU
//  TJ1-1          3V3
//  TJ1-2          GND
//  TJ1-3          CC8
//  TJ1-4          CC9
//  TJ1-5          CC10
//  TJ1-6          CC12
//
// pins on sub-boards for CMU       // 3G-MKCS <<<<<<
//  TJ1-1          3V3
//  TJ1-2          GND
//  TJ1-3          MOSI_AUX //
//  TJ1-4          SCLK_AUX
//  TJ1-5          SCSB_AUX //
//  TJ1-6          MISO_AUX

wire w_CC8  = (w_SPIO_en)? ( (~w_pin_swap_CSB_MOSI)? EXT_SPx_MOSI  : EXT_SP0__CS_B) : DAC_A2A3_MOSI;
wire w_CC9  = (w_SPIO_en)? ( (~w_pin_swap_CSB_MOSI)? EXT_SPx_SCLK  : EXT_SPx_SCLK ) : DAC_A2A3_SCLK;
wire w_CC10 = (w_SPIO_en)? ( (~w_pin_swap_CSB_MOSI)? EXT_SP0__CS_B : EXT_SPx_MOSI ) : DAC_A2A3_SYNB;
wire w_CC12;
assign DAC_A2A3_MISO = (~w_SPIO_en)? w_CC12 : 1'b0;
assign EXT_SPx_MISO  = ( w_SPIO_en)? w_CC12 : 1'b0;

OBUF obuf_CC8__inst (.O(o_B13_L1P), .I(w_CC8 ) ); // CC8
OBUF obuf_CC9__inst (.O(o_B13_L4N), .I(w_CC9 ) ); // CC9
OBUF obuf_CC10_inst (.O(o_B13_L4P), .I(w_CC10) ); // CC10
IBUF ibuf_CC12_inst (.I(i_B13_L2P), .O(w_CC12) ); // CC12

// note MISO may have pullup. check xdc. i_B13_L2P

//}


// DAC_BIAS //{
IBUF ibuf_DAC_BIAS_MISO_inst (.I(i_B34_L4N), .O(DAC_BIAS_MISO  ) ); // BIAS_SDO
OBUF obuf_DAC_BIAS_MOSI_inst (.O(o_B34_L4P), .I(DAC_BIAS_MOSI  ) ); // BIAS_SDI
OBUF obuf_DAC_BIAS_SYNB_inst (.O(o_B34_L2P), .I(DAC_BIAS_SYNB) ); // BIAS_SYNCn
OBUF obuf_DAC_BIAS_SCLK_inst (.O(o_B34_L2N), .I(DAC_BIAS_SCLK ) ); // BIAS_SCLK
//
OBUF obuf_DAC_BIAS_CLRB_inst (.O(o_B34_L10N ), .I(DAC_BIAS_CLRB  ) ); // BIAS_CLR_B
OBUF obuf_DAC_BIAS_LD_B_inst (.O(o_B34_L10P ), .I(DAC_BIAS_LD_B ) ); // BIAS_LOAD_B
//}

// DWAVE //{

// dwave warm-up test 
(* keep = "true" *) wire w_disable__dwave_warmup = w_TEST_CON[5]; // default for warm-up
// output 1 for dwave disable 
// output 0 for dwave warm-up by DC current
wire w_dwave_idle_output = (w_disable__dwave_warmup)? 1'b1 : 1'b0;
//
OBUF obuf_dwave_w1f1_inst (.O(o_B34_L20P), .I((dwave_en)? ~dwave_w1f1 : w_dwave_idle_output) ); // DWAVE_F01 // inverted
OBUF obuf_dwave_w1f2_inst (.O(o_B34_L20N), .I((dwave_en)? ~dwave_w1f2 : w_dwave_idle_output) ); // DWAVE_F02 // inverted
OBUF obuf_dwave_w1f3_inst (.O(o_B34_L3P ), .I((dwave_en)? ~dwave_w1f3 : w_dwave_idle_output) ); // DWAVE_F03 // inverted
//OBUF obuf_dwave_w1f1_inst (.O(o_B34_L20P), .I((dwave_en)? ~dwave_w1f1 : 1'b1) ); // DWAVE_F01 // inverted
//OBUF obuf_dwave_w1f2_inst (.O(o_B34_L20N), .I((dwave_en)? ~dwave_w1f2 : 1'b1) ); // DWAVE_F02 // inverted
//OBUF obuf_dwave_w1f3_inst (.O(o_B34_L3P ), .I((dwave_en)? ~dwave_w1f3 : 1'b1) ); // DWAVE_F03 // inverted
////$$OBUF obuf_dwave_w2f1_inst (.O(o_B34_L3N ), .I((dwave_en)? ~dwave_w2f1 : 1'b1) ); // DWAVE_F11 // inverted
////$$OBUF obuf_dwave_w2f2_inst (.O(o_B34_L9P ), .I((dwave_en)? ~dwave_w2f2 : 1'b1) ); // DWAVE_F12 // inverted
////$$OBUF obuf_dwave_w2f3_inst (.O(o_B34_L9N ), .I((dwave_en)? ~dwave_w2f3 : 1'b1) ); // DWAVE_F13 // inverted
//
// swap test 
//(* keep = "true" *) wire w_disable__dwave2_pin_swap = w_TEST_CON[6]; // default for swap
(* keep = "true" *) wire w_disable__dwave2_pin_swap = w_TEST_CON[6]|~w_TEST_CON[4]; // default for non-swap
//  w_TEST_CON[6] w_TEST_CON[4] = 0 0 : disable__dwave2_pin_swap
//  w_TEST_CON[6] w_TEST_CON[4] = 0 1 : enable__dwave2_pin_swap
//  w_TEST_CON[6] w_TEST_CON[4] = 1 0 : disable__dwave2_pin_swap
//  w_TEST_CON[6] w_TEST_CON[4] = 1 1 : disable__dwave2_pin_swap
//  
//
wire w_dwave_w2f1 = (w_disable__dwave2_pin_swap)? dwave_w2f1 : dwave_w2f3;
wire w_dwave_w2f2 = (w_disable__dwave2_pin_swap)? dwave_w2f2 : dwave_w2f2;
wire w_dwave_w2f3 = (w_disable__dwave2_pin_swap)? dwave_w2f3 : dwave_w2f1;
//
OBUF obuf_dwave_w2f1_inst (.O(o_B34_L3N ), .I((dwave_en)? ~w_dwave_w2f1 : w_dwave_idle_output) ); // DWAVE_F11 // inverted
OBUF obuf_dwave_w2f2_inst (.O(o_B34_L9P ), .I((dwave_en)? ~w_dwave_w2f2 : w_dwave_idle_output) ); // DWAVE_F12 // inverted
OBUF obuf_dwave_w2f3_inst (.O(o_B34_L9N ), .I((dwave_en)? ~w_dwave_w2f3 : w_dwave_idle_output) ); // DWAVE_F13 // inverted
//OBUF obuf_dwave_w2f1_inst (.O(o_B34_L3N ), .I((dwave_en)? ~w_dwave_w2f1 : 1'b1) ); // DWAVE_F11 // inverted
//OBUF obuf_dwave_w2f2_inst (.O(o_B34_L9P ), .I((dwave_en)? ~w_dwave_w2f2 : 1'b1) ); // DWAVE_F12 // inverted
//OBUF obuf_dwave_w2f3_inst (.O(o_B34_L9N ), .I((dwave_en)? ~w_dwave_w2f3 : 1'b1) ); // DWAVE_F13 // inverted
//

//}

//SPO //{
OBUF obuf_SPCD0_inst (.O(o_B13_L5P), .I(CTL_SPO_CDAT0) ); // CC0 // SPO_CD0
OBUF obuf_SPEN0_inst (.O(o_B13_L5N), .I(CTL_SPO_EN0  ) ); // CC1 // SPO_EN0
OBUF obuf_SPCD1_inst (.O(o_B13_L3P), .I(CTL_SPO_CDAT1) ); // CC2 // SPO_CD1
OBUF obuf_SPEN1_inst (.O(o_B13_L3N), .I(CTL_SPO_EN1  ) ); // CC3 // SPO_EN1
//
OBUF obuf_SPCD2_inst (.O(o_B13_L16P), .I(CTL_SPO_CDAT2) ); // CC4 // SPO_CD2
OBUF obuf_SPEN2_inst (.O(o_B13_L16N), .I(CTL_SPO_EN2  ) ); // CC5 // SPO_EN2
OBUF obuf_SPCD3_inst (.O(o_B34_L5P ), .I(CTL_SPO_CDAT3) ); // CC6 // SPO_CD3
OBUF obuf_SPEN3_inst (.O(o_B34_L5N ), .I(CTL_SPO_EN3  ) ); // CC7 // SPO_EN3
// 

//}

// CC //{
assign RB3 = DAC_A2A3_MISO;                       // CC12 // RBCK3 or DAC_A2A3_MISO
IBUF ibuf_RB2_inst (.I(i_B35_L6N  ), .O(RB2  ) ); // CC13 // RBCK2
IBUF ibuf_RB1_inst (.I(i_B35_L19P ), .O(RB1  ) ); // CC14 // RBCK1
IBUF ibuf_UNB_inst (.I(i_B35_L19N ), .O(UNBAL) ); // CC15 // UNBAL
//
IBUF ibuf_D_R_inst (.I(i_B13_L6P      ), .O(D_R  ) ); // CC16 // D_R
IBUF ibuf_C_R_inst (.I(i_B13_L6N      ), .O(C_R  ) ); // CC17 // C_R
IBUF ibuf_B_R_inst (.I(i_B13_L11N_SRCC), .O(B_R  ) ); // CC18 // B_R 
IBUF ibuf_A_R_inst (.I(i_B13_L11P_SRCC), .O(A_R  ) ); // CC19 // A_R
//
IBUF ibuf_D_D_inst (.I(i_B34_L6P), .O(D_D  ) ); // CC20 // D_D
IBUF ibuf_C_D_inst (.I(i_B34_L6N), .O(C_D  ) ); // CC21 // C_D
IBUF ibuf_B_D_inst (.I(i_B34_L8P), .O(B_D  ) ); // CC22 // B_D
IBUF ibuf_A_D_inst (.I(i_B34_L8N), .O(A_D  ) ); // CC23 // A_D
//

//}

/* ADC */ //{
// nets for BUFG clocks
wire c_dco_adc_0;
wire c_dco_adc_1;
wire c_dco_adc_2;
wire c_dco_adc_3;
// adc0
IBUFDS ibufds_ADC_00_DCO_inst (.I(c_B34D_L11P_SRCC), .IB(c_B34D_L11N_SRCC), .O(c_dco_adc_0) );
BUFG     bufg_ADC_00_DCO_inst (.I(c_dco_adc_0), .O(w_dco_adc_0) ); 
IBUFDS ibufds_ADC_00_DA_inst (.I(i_B34D_L18P), .IB(i_B34D_L18N), .O(w_dat2_adc_0) );
IBUFDS ibufds_ADC_00_DB_inst (.I(i_B34D_L22P), .IB(i_B34D_L22N), .O(w_dat1_adc_0) );
// adc1
IBUFDS ibufds_ADC_01_DCO_inst (.I(c_B34D_L14P_SRCC), .IB(c_B34D_L14N_SRCC), .O(c_dco_adc_1) ); // check polarity
BUFG     bufg_ADC_01_DCO_inst (.I(c_dco_adc_1), .O(w_dco_adc_1) ); 
IBUFDS ibufds_ADC_01_DA_inst (.I(i_B34D_L16P), .IB(i_B34D_L16N), .O(w_dat2_adc_1) );
IBUFDS ibufds_ADC_01_DB_inst (.I(i_B34D_L17P), .IB(i_B34D_L17N), .O(w_dat1_adc_1) );
// adc2 ... not used
IBUFDS ibufds_ADC_10_DCO_inst (.I(c_B34D_L12P_MRCC), .IB(c_B34D_L12N_MRCC), .O(c_dco_adc_2) );
BUFG     bufg_ADC_10_DCO_inst (.I(c_dco_adc_2), .O(w_dco_adc_2) ); 
IBUFDS ibufds_ADC_10_DA_inst (.I(i_B34D_L7P), .IB(i_B34D_L7N), .O(w_dat2_adc_2) );
IBUFDS ibufds_ADC_10_DB_inst (.I(i_B34D_L1P), .IB(i_B34D_L1N), .O(w_dat1_adc_2) );
// adc3 ... not used
IBUFDS ibufds_ADC_11_DCO_inst (.I(c_B34D_L13P_MRCC), .IB(c_B34D_L13N_MRCC), .O(c_dco_adc_3) ); 
BUFG     bufg_ADC_11_DCO_inst (.I(c_dco_adc_3), .O(w_dco_adc_3) ); 
IBUFDS ibufds_ADC_11_DA_inst (.I(i_B34D_L15P), .IB(i_B34D_L15N), .O(w_dat2_adc_3) );
IBUFDS ibufds_ADC_11_DB_inst (.I(i_B34D_L23P), .IB(i_B34D_L23N), .O(w_dat1_adc_3) );
//
wire w_ADC_XX_CLK = w_clk_adc;
OBUFDS obufds_ADC_XX_CLK_inst 		(.O(o_B34D_L19P), .OB(o_B34D_L19N), .I(w_ADC_XX_CLK)		); // ADC_XX_CLK
//
wire w_ADC_XX_CNV;
	assign w_ADC_XX_CNV = (w_enable__pulse_loopback)? w_pulse_out : w_cnv_adc;
OBUFDS obufds_ADC_XX_CNV_inst 		(.O(o_B34D_L21P), .OB(o_B34D_L21N), .I(w_ADC_XX_CNV)		); // ADC_XX_CNV
//
OBUF obuf_ADC_XX_TEST_B_inst		(.O(o_B34_L24P), .I(~w_pin_test_adc)); // ADC_XX_TEST_B
OBUF obuf_ADC_XX_DUAL_LANE_B_inst 	(.O(o_B34_L24N), .I(~w_pin_dln_adc)	); // ADC_XX_DUAL_LANE_B
// loopback test
wire w_OSC_IN;
	assign w_pulse_loopback_in = w_OSC_IN;
IBUFDS ibufds_OSC_IN_inst (.I(i_B35D_L12P_MRCC), .IB(i_B35D_L12N_MRCC), .O(w_OSC_IN) ); 
////

//}

// LAN signals to mux //{

wire  PT_BASE_EP_LAN_MOSI ; 
wire  PT_BASE_EP_LAN_SCLK ; 
wire  PT_BASE_EP_LAN_CS_B ; 
wire  PT_BASE_EP_LAN_INT_B;
wire  PT_BASE_EP_LAN_RST_B; 
wire  PT_BASE_EP_LAN_MISO ;

wire  PT_FMOD_EP_LAN_MOSI ; 
wire  PT_FMOD_EP_LAN_SCLK ; 
wire  PT_FMOD_EP_LAN_CS_B ; 
wire  PT_FMOD_EP_LAN_INT_B;
wire  PT_FMOD_EP_LAN_RST_B; 
wire  PT_FMOD_EP_LAN_MISO ;

// output mux
assign PT_BASE_EP_LAN_MOSI  = ( w_sel__H_LAN_on_BASE_BD)? EP_LAN_MOSI  : 1'b0;
assign PT_BASE_EP_LAN_SCLK  = ( w_sel__H_LAN_on_BASE_BD)? EP_LAN_SCLK  : 1'b0;
assign PT_BASE_EP_LAN_CS_B  = ( w_sel__H_LAN_on_BASE_BD)? EP_LAN_CS_B  : 1'b1;
assign PT_BASE_EP_LAN_RST_B = ( w_sel__H_LAN_on_BASE_BD)? EP_LAN_RST_B : 1'b1;

assign PT_FMOD_EP_LAN_MOSI  = (~w_sel__H_LAN_on_BASE_BD)? EP_LAN_MOSI  : 1'b0;
assign PT_FMOD_EP_LAN_SCLK  = (~w_sel__H_LAN_on_BASE_BD)? EP_LAN_SCLK  : 1'b0;
assign PT_FMOD_EP_LAN_CS_B  = (~w_sel__H_LAN_on_BASE_BD)? EP_LAN_CS_B  : 1'b1;
assign PT_FMOD_EP_LAN_RST_B = (~w_sel__H_LAN_on_BASE_BD)? EP_LAN_RST_B : 1'b1;

// input mux
assign EP_LAN_INT_B = (~w_sel__H_LAN_on_BASE_BD)? PT_FMOD_EP_LAN_INT_B  : PT_BASE_EP_LAN_INT_B;
assign EP_LAN_MISO  = (~w_sel__H_LAN_on_BASE_BD)? PT_FMOD_EP_LAN_MISO   : PT_BASE_EP_LAN_MISO;

//}

// LAN pin on BASE board //{
//wire w_LAN_PWDN; // unused 
// input  wire			i_B13_SYS_CLK_MC2, //$$ LAN_PWDN // unused in WIZ850io // used in WIZ820io
// output wire			o_B35_L21N,        //$$ LAN_RSTn
// output wire			o_B35_IO0,         //$$ LAN_SCSn
// output wire			o_B35_IO25,        //$$ LAN_SCLK
// output wire			o_B35_L24P,        //$$ LAN_MOSI
// input  wire			i_B35_L24N,        //$$ LAN_INTn
// input  wire			i_B35_L21P,        //$$ LAN_MISO

OBUF obuf__LAN_MOSI__inst (.O( o_B35_L24P ), .I(PT_BASE_EP_LAN_MOSI ) ); // 
OBUF obuf__LAN_SCLK__inst (.O( o_B35_IO25 ), .I(PT_BASE_EP_LAN_SCLK ) ); // 
OBUF obuf__LAN_CS_B__inst (.O( o_B35_IO0  ), .I(PT_BASE_EP_LAN_CS_B ) ); // 
IBUF ibuf__LAN_INT_B_inst (.I( i_B35_L24N ), .O(PT_BASE_EP_LAN_INT_B) ); //
OBUF obuf__LAN_RST_B_inst (.O( o_B35_L21N ), .I(PT_BASE_EP_LAN_RST_B) ); // 
IBUF ibuf__LAN_MISO__inst (.I( i_B35_L21P ), .O(PT_BASE_EP_LAN_MISO ) ); //


// OBUF obuf__LAN_MOSI__inst (.O(o_B13_L11N_SRCC ), .I(PT_BASE_EP_LAN_MOSI ) ); // 
// OBUF obuf__LAN_SCLK__inst (.O(o_B13_L6N       ), .I(PT_BASE_EP_LAN_SCLK ) ); // 
// OBUF obuf__LAN_CS_B__inst (.O(o_B13_L6P       ), .I(PT_BASE_EP_LAN_CS_B ) ); // 
// IBUF ibuf__LAN_INT_B_inst (.I(i_B13_L13P_MRCC ), .O(PT_BASE_EP_LAN_INT_B) ); //
// OBUF obuf__LAN_RST_B_inst (.O(o_B13_L15P      ), .I(PT_BASE_EP_LAN_RST_B) ); // 
// IBUF ibuf__LAN_MISO__inst (.I(i_B13_L15N      ), .O(PT_BASE_EP_LAN_MISO ) ); //

// OBUF obuf_o_B35_L24P_inst  (.O(o_B35_L24P ), .I(w_LAN_MOSI) ); // w_LAN_MOSI
// OBUF obuf_o_B35_IO25_inst  (.O(o_B35_IO25 ), .I(w_LAN_SCLK) ); // w_LAN_SCLK
// OBUF obuf_o_B35_IO0_inst   (.O(o_B35_IO0  ), .I(w_LAN_SCSn) ); // w_LAN_SCSn
// IBUF ibuf_i_B35_L24N_inst  (.I(i_B35_L24N ), .O(w_LAN_INTn) ); // w_LAN_INTn
// OBUF obuf_o_B35_L21N_inst  (.O(o_B35_L21N ), .I(w_LAN_RSTn) ); // w_LAN_RSTn
// IBUF ibuf_i_B35_L21P_inst  (.I(i_B35_L21P ), .O(w_LAN_MISO) ); // w_LAN_MISO

//}

// TEMP SENSOR //{
IBUF ibuf_i_B35_L6P_inst  (.I(i_B35_L6P ), .O(w_temp_sig) ); // w_temp_sig
//}

// EEPROM //{
IOBUF iobuf__MEM_SIO_inst(.IO(io_B13_L1N  ), .T(MEM_SIO_tri), .I(MEM_SIO_out ), .O(MEM_SIO_in ) ); //
//}


/* TODO: reserved signals : compatible with TXEM7310 */

//// LAN pin on FPGA module //{
//	output wire  o_B15_L6P , // # H17    EP_LAN_PWDN 
//	output wire  o_B15_L7P , // # J22    EP_LAN_MOSI
//	output wire  o_B15_L7N , // # H22    EP_LAN_SCLK
//	output wire  o_B15_L8P , // # H20    EP_LAN_CS_B
//	input  wire  i_B15_L8N , // # G20    EP_LAN_INT_B
//	output wire  o_B15_L9P , // # K21    EP_LAN_RST_B
//	input  wire  i_B15_L9N , // # K22    EP_LAN_MISO

wire  EP_LAN_PWDN  = 1'b0; // test // unused // fixed
OBUF obuf__EP_LAN_PWDN__inst (.O( o_B15_L6P ), .I( EP_LAN_PWDN   ) ); // 

OBUF obuf__EP_LAN_MOSI__inst (.O( o_B15_L7P ), .I( PT_FMOD_EP_LAN_MOSI  ) ); // 
OBUF obuf__EP_LAN_SCLK__inst (.O( o_B15_L7N ), .I( PT_FMOD_EP_LAN_SCLK  ) ); // 
OBUF obuf__EP_LAN_CS_B__inst (.O( o_B15_L8P ), .I( PT_FMOD_EP_LAN_CS_B  ) ); // 
IBUF ibuf__EP_LAN_INT_B_inst (.I( i_B15_L8N ), .O( PT_FMOD_EP_LAN_INT_B ) ); //
OBUF obuf__EP_LAN_RST_B_inst (.O( o_B15_L9P ), .I( PT_FMOD_EP_LAN_RST_B ) ); // 
IBUF ibuf__EP_LAN_MISO__inst (.I( i_B15_L9N ), .O( PT_FMOD_EP_LAN_MISO  ) ); //

//}

//// TP on FPGA module //{
//	inout  wire  io_B15_L1P , // # H13    TP0 // test for eeprom : VCC_3.3V
//	inout  wire  io_B15_L1N , // # G13    TP1 // test for eeprom : VSS_GND
//	inout  wire  io_B15_L2P , // # G15    TP2 // test for eeprom : SCIO
//	inout  wire  io_B15_L2N , // # G16    TP3 // test for eeprom : NA
//	inout  wire  io_B15_L3P , // # J14    TP4 // test for eeprom : VCC_3.3V
//	inout  wire  io_B15_L3N , // # H14    TP5 // test for eeprom : VSS_GND
//	inout  wire  io_B15_L5P , // # J15    TP6 // test for eeprom : NA
//	inout  wire  io_B15_L5N , // # H15    TP7 // test for eeprom : NA

IOBUF iobuf__TP0__inst(.IO(io_B15_L1P  ), .T(TP_tri[0]), .I(TP_out[0] ), .O(TP_in[0] ) ); //
IOBUF iobuf__TP1__inst(.IO(io_B15_L1N  ), .T(TP_tri[1]), .I(TP_out[1] ), .O(TP_in[1] ) ); //
IOBUF iobuf__TP2__inst(.IO(io_B15_L2P  ), .T(TP_tri[2]), .I(TP_out[2] ), .O(TP_in[2] ) ); //
IOBUF iobuf__TP3__inst(.IO(io_B15_L2N  ), .T(TP_tri[3]), .I(TP_out[3] ), .O(TP_in[3] ) ); //
IOBUF iobuf__TP4__inst(.IO(io_B15_L3P  ), .T(TP_tri[4]), .I(TP_out[4] ), .O(TP_in[4] ) ); //
IOBUF iobuf__TP5__inst(.IO(io_B15_L3N  ), .T(TP_tri[5]), .I(TP_out[5] ), .O(TP_in[5] ) ); //
IOBUF iobuf__TP6__inst(.IO(io_B15_L5P  ), .T(TP_tri[6]), .I(TP_out[6] ), .O(TP_in[6] ) ); //
IOBUF iobuf__TP7__inst(.IO(io_B15_L5N  ), .T(TP_tri[7]), .I(TP_out[7] ), .O(TP_in[7] ) ); //


//}

//// ADC on module //{
//	input  wire  i_B15_L10P, // # H20    AUX_AD11P
//	input  wire  i_B15_L10N, // # G20    AUX_AD11N

//}

endmodule
