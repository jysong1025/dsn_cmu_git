//------------------------------------------------------------------------
// dwave_control.v
//  for dwave logic
// 
//  8f, 4-level design
//	    base clock 80MHz or 12.5ns
//
//		targets freq  --> count 
//		10kHz 100us   --> 8000
//		20kHz 50us    --> 4000
//		40kHz 25us    --> 2000
//		100kHz 10us   --> 800
//		200kHz 5us    --> 400
//		400kHz 2.5us  --> 200
//		1MHz  1us     --> 80
//		2MHz  500ns   --> 40
//		4MHz  250ns   --> 20
//		10MHz 100ns   --> 8
//		note that 16bit counter is enough.
//		
//  - pulse generation info
//  	>> 4-level scheme .
//  	pulse_en:____-----------------------------------------------------------------_____
//  	8f:      _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
//  	index:        1 2 3 4 5 6 7 8
//		...
//		in-phase control 
//  	f_1:     ---------------____------------____------------____------------____------- [pulse_out_f1]
//  	f_2:     -------------________--------________--------________--------________----- [pulse_out_f1_ref]
//  	f_3:     -----__----____________----____________----____________----__________----- [pulse_out_f2]
//  	L_0:     -------____------------____------------____------------____--------------- 
//  	L_1:     -----__----__--------__----__--------__----__--------__----__------------- 
//  	L_2:     -------------__----__--------__----__--------__----__--------__----__----- 
//  	L_3:     ---------------____------------____------------____------------____------- 
//  	...
//		quadrature-phase control
//  	f_4:     -----__------------____------------____------------____------------__----- [pulse_out_f3]
//  	f_5:     -----____--------________--------________--------________--------____----- [pulse_out_f3_ref]
//  	f_6:     -----______----____________----____________----____________----______----- [pulse_out_f4]
//  	L_0:     -----------____------------____------------____------------____----------- 
//  	L_1:     ---------__----__--------__----__--------__----__--------__----__--------- 
//  	L_2:     -------__--------__----__--------__----__--------__----__--------__------- 
//  	L_3:     -----__------------____------------____------------____------------__----- 
//
//	10MHz ex
//		>>>f1_cnt_period = 8
//		>>>f1_pulse_index = 1 // 1 2 ~ 8 // start polocation
//		f1_on =   2 3
//		>>>f1_on_trig = 1
//		>>>f1_off_trig = 3
//		f1_ref
//		f1_bar
//		>>>f2_cnt_period = 8
//		>>>f2_pulse_index = 3 // 1 2 ~ 8 // start polocation
//		f2_on = 1 2 3 4
//		>>>f2_on_trig = 1
//		>>>f2_off_trig = 3
//		others: pulse_en
//
//  - IO
//		clk // 10MHz for reg control 
//		reset_n
//		en
//		clk_dwave // 80MHz for generating waveform. 
//		init 
//		update 
//		test 
//		pulse_period // for future
//		pulse_start_loc // for future
//		pulse_on_trig_loc // for future
//		pulse_off_trig_loc // for future
//		test_datain
//		test_datain_type
//		test_datain_port_sel
//		pulse_out_f#_ref (duty 50%)
//		pulse_out_f#_ref_n (duty 50%)
//		pulse_out_f# (duty adjustable)
//		pulse_out_f#_n
//					#:1~4
//
//  - external end-points
//		DWAVE_CON = {test_datain[15:0], ..., test_datain_port[1:0], test_datain_type[1:0], test, update, init, en}
//					test_datain_type[1:0] with test
//						0 for r_cnt_pulse#[15:0]
//						1 for r_loc_start#[15:0]
//						2 for r_loc_trig_on#[15:0]
//						3 for r_loc_trig_off#[15:0]
//					test_datain_port[1:0] with test
//						0 for pulse0
//						1 for pulse1
//						2 for pulse2
//						3 for pulse3
//					test_datain[3:0] with update 
//						r_pulse_en
//  	DWAVE_FLAG = {... , test_done,update_done,init_done,en}
//  	DWAVE_DIN ... reseved
//  	DWAVE_DOUT ... reseved
//
//  - reg
//		r_cnt_pulse#[15:0]
//		r_loc_start#[15:0]
//		r_loc_trig_on#[15:0]
//		r_loc_trig_off#[15:0]
//		r_subpulse_idx#[15:0]
//		r_pulse_en# // enable for r_subpulse_idx# // trig by update
//
//  - timing 
//  	base clock 80MHz or 12.5ns 
//  	...
//		r_pulse_en#			____-----------------------------------------------------------------_____
//		r_cnt_pulse#		88888888888888888888888888888888888888888888888888888888888888888888888888
//		r_loc_start#		33333333333333333333333333333333333333333333333333333333333333333333333333
//		r_loc_trig_on#		11111111111111111111111111111111111111111111111111111111111111111111111111
//		r_loc_trig_off#		33333333333333333333333333333333333333333333333333333333333333333333333333
//  	r_subpulse_idx#		00003456781234567812345678123456781234567812345678123456781234567812300000
//		pulse_out_f#_ref	_______----____----____----____----____----____----____----____----_______
//		pulse_out_f#_ref_n	___________----____----____----____----____----____----____----____--_____
//		pulse_out_f#		___________--______--______--______--______--______--______--______--_____
//		pulse_out_f#_n		_____------__------__------__------__------__------__------__------__-____
//		
//

	
`timescale 1ns / 1ps
module dwave_control (  
	// 
	input wire clk, // assume 10MHz or 100ns
	input wire reset_n,
    input wire en,
	//
	input wire clk_dwave, // 80MHz for generating waveform
    // control
    input wire init, // rising
    input wire update, // rising
    input wire test, // rising
	// test
    input wire [31:0] test_datain, //$$
	input wire [1:0] test_datain_type,
	input wire [1:0] test_datain_port, 
	//
	// trig interface
	//	r_pulse_off		// ti44[0]  DWAVE_PULSE_OFF
	input wire i_trig_pulse_off,
	//	r_pulse_on_cont	// ti44[1]  DWAVE_PULSE_ON
	input wire i_trig_pulse_on_cont,
	//	r_pulse_on_num	// ti44[2]  DWAVE_PULSE_ON_NUM
	input wire i_trig_pulse_on_num,
	//	r_set_parameters// ti44[3]  DWAVE_SET_PARAM 
	input wire i_trig_set_parameters,
	// wire in/out to share in trig interface 
	input wire  [31:0] i_wire_datain_by_trig, // 32b
	output wire [31:0] o_wire_dataout_by_trig, // 32b
	//
	//	r_cnt_period[15:0]	// ti44[16] DWAVE_SET_CNT_PERIOD ; wi04  SET_DATA_BY_TRIG 
							// ti44[24] DWAVE_READ_CNT_PERIOD; wo3B  READ_DATA_BY_TRIG
	input wire i_trig_wr_cnt_period,
	input wire i_trig_rd_cnt_period,
	//
	//	r_cnt_diff[15:0]	// ti44[17] DWAVE_SET_CNT_PERIOD ; wi04  SET_DATA_BY_TRIG 
							// ti44[25] DWAVE_READ_CNT_PERIOD; wo3B  READ_DATA_BY_TRIG
	input wire i_trig_wr_cnt_diff,
	input wire i_trig_rd_cnt_diff,
	//
	//	r_num_pulses[15:0]	// ti44[18] DWAVE_SET_CNT_PERIOD ; wi04  SET_DATA_BY_TRIG 
							// ti44[26] DWAVE_READ_CNT_PERIOD; wo3B  READ_DATA_BY_TRIG
	input wire i_trig_wr_num_pulses,
	input wire i_trig_rd_num_pulses,
	// i_phase disable // q_phase disable  
	//   f1, f2 ... i_phase ... for disable ... 0x3 ..3
	//   f3, f4 ... q_phase ... for disable ... 0xC ..12
	input wire i_trig_wr_output_dis, 
	input wire i_trig_rd_output_dis,
	//
	// GPIO
	output wire pulse_out_f1_ref,
	output wire pulse_out_f1_ref_n,
	output wire pulse_out_f1,
	output wire pulse_out_f1_n,
	output wire pulse_out_f2_ref,
	output wire pulse_out_f2_ref_n,
	output wire pulse_out_f2,
	output wire pulse_out_f2_n,
	output wire pulse_out_f3_ref,
	output wire pulse_out_f3_ref_n,
	output wire pulse_out_f3,
	output wire pulse_out_f3_n,
	output wire pulse_out_f4_ref,
	output wire pulse_out_f4_ref_n,
	output wire pulse_out_f4,
	output wire pulse_out_f4_n,
    // flag
    output wire init_done,
    output wire update_done,
	output wire test_done,
	output wire error,
	output wire [7:0] debug_out
);
//


////
// parameters for trig interface 
//
//	.i_trig_pulse_off      (),
//	.i_trig_pulse_on_cont  (),
//	.i_trig_pulse_on_num   (),
//	.i_trig_set_parameters (),
//
reg r_pulse_en_trig ;
reg r_pulse_en_num  ;
//
reg [31:0] r_cnt_pulses;  //$$
//
reg [31:0] r_cnt_period ; //$$
reg [31:0] r_cnt_diff   ; //$$
reg [31:0] r_num_pulses ; //$$
//
reg [3:0]  r_output_dis ;
//
reg [31:0] r_wire_dataout_by_trig;
	assign o_wire_dataout_by_trig = r_wire_dataout_by_trig;
//
//
//$$always @(posedge clk, negedge reset_n)
always @(posedge clk_dwave, negedge reset_n)
	if (!reset_n) begin
		r_pulse_en_trig  <= 1'b0;
		end 
	else if (!en) begin
		r_pulse_en_trig  <= 1'b0;
		end
	else begin 
		//
		if      (i_trig_pulse_off)
			r_pulse_en_trig  <= 1'b0;
		else if (i_trig_pulse_on_cont)
			r_pulse_en_trig  <= 1'b1;
		//
		end
//
//
always @(posedge clk_dwave, negedge reset_n)
	if (!reset_n) begin
		r_pulse_en_num   <= 1'b0;
		r_cnt_pulses     <= 32'b0;
		end 
	else if (!en) begin
		r_pulse_en_num   <= 1'b0;
		r_cnt_pulses     <= 32'b0;
		end
	else if (i_trig_pulse_off) begin
		r_pulse_en_num   <= 1'b0;
		r_cnt_pulses     <= 32'b0;
		end
	else if (i_trig_pulse_on_num) begin
		r_pulse_en_num   <= 1'b0;
		r_cnt_pulses     <=  r_num_pulses * r_cnt_period; // count down  r_num_pulses
		end
	else if (r_cnt_pulses > 0) begin
		r_pulse_en_num   <= 1'b1; // start pulse
		r_cnt_pulses     <= r_cnt_pulses - 1;
		end
	else begin 
		r_pulse_en_num   <= 1'b0; // end pulse
		r_cnt_pulses     <= 32'b0;
		end
//
//	.i_wire_datain_by_trig   (), // 32b
//	.o_wire_dataout_by_trig   (), // 32b
//
//	.i_trig_wr_cnt_period  (),
//	.i_trig_rd_cnt_period  (),
//	.i_trig_wr_cnt_diff    (),
//	.i_trig_rd_cnt_diff    (),
//	.i_trig_wr_num_pulses  (),
//	.i_trig_rd_num_pulses  (),
//
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_cnt_period  <= 32'd80; //$$
		r_cnt_diff    <= 32'd60; //$$
		r_num_pulses  <= 32'd10; //$$
		r_output_dis  <= 4'b0;
		//
		r_wire_dataout_by_trig  <= 32'b0;
		end 
	else if (!en) begin
		r_cnt_period  <= 32'd80; //$$
		r_cnt_diff    <= 32'd60; //$$
		r_num_pulses  <= 32'd10; //$$
		r_output_dis  <= 4'b0;
		//
		r_wire_dataout_by_trig  <= 32'b0;
		end
	else begin 
		//
		// trig_wr
		if (i_trig_wr_cnt_period)
			r_cnt_period      <= i_wire_datain_by_trig; //$$
		//
		if (i_trig_wr_cnt_diff)
			r_cnt_diff        <= i_wire_datain_by_trig; //$$
		//                                            
		if (i_trig_wr_num_pulses)                     
			r_num_pulses      <= i_wire_datain_by_trig; //$$
		//
		if (i_trig_wr_output_dis)
			r_output_dis      <= i_wire_datain_by_trig[3:0]; 
		//
		// trig_rd
		if      (i_trig_rd_cnt_period) 
			r_wire_dataout_by_trig  <= r_cnt_period; //$$
		else if (i_trig_rd_cnt_diff) 
			r_wire_dataout_by_trig  <= r_cnt_diff;   //$$
		else if (i_trig_rd_num_pulses) 
			r_wire_dataout_by_trig  <= r_num_pulses; //$$
		else if (i_trig_rd_output_dis) 
			r_wire_dataout_by_trig  <= {28'b0, r_output_dis};
		//
		end
//
//
//
////


////
// inner parameters for dwave_core
//
(* keep = "true" *) reg [31:0] r_cnt_pulse1;    //$$
(* keep = "true" *) reg [31:0] r_loc_start1;    //$$
(* keep = "true" *) reg [31:0] r_loc_trig_on1;  //$$
(* keep = "true" *) reg [31:0] r_loc_trig_off1; //$$
//
(* keep = "true" *) reg [31:0] r_cnt_pulse2;    //$$
(* keep = "true" *) reg [31:0] r_loc_start2;    //$$
(* keep = "true" *) reg [31:0] r_loc_trig_on2;  //$$
(* keep = "true" *) reg [31:0] r_loc_trig_off2; //$$
//
(* keep = "true" *) reg [31:0] r_cnt_pulse3;    //$$
(* keep = "true" *) reg [31:0] r_loc_start3;    //$$
(* keep = "true" *) reg [31:0] r_loc_trig_on3;  //$$
(* keep = "true" *) reg [31:0] r_loc_trig_off3; //$$
//
(* keep = "true" *) reg [31:0] r_cnt_pulse4;    //$$
(* keep = "true" *) reg [31:0] r_loc_start4;    //$$
(* keep = "true" *) reg [31:0] r_loc_trig_on4;  //$$
(* keep = "true" *) reg [31:0] r_loc_trig_off4; //$$
//
(* keep = "true" *) reg		   r_pulse_en1; 
(* keep = "true" *) reg		   r_pulse_en2; 
(* keep = "true" *) reg		   r_pulse_en3; 
(* keep = "true" *) reg		   r_pulse_en4; 
//
////


////
// call pulse generator 
//
localparam NUM_dwave_core = 4;
genvar dwave_core_idx;
//
wire [NUM_dwave_core-1:0] w_dwave_pulse_en;
//
wire [31:0] w_dwave_cnt_pulse    [0:NUM_dwave_core-1]; //$$
wire [31:0] w_dwave_loc_start    [0:NUM_dwave_core-1]; //$$
wire [31:0] w_dwave_loc_trig_on  [0:NUM_dwave_core-1]; //$$
wire [31:0] w_dwave_loc_trig_off [0:NUM_dwave_core-1]; //$$
//
wire [NUM_dwave_core-1:0] w_dwave_pulse_out_f_ref  ;
wire [NUM_dwave_core-1:0] w_dwave_pulse_out_f_ref_n;
wire [NUM_dwave_core-1:0] w_dwave_pulse_out_f      ;
wire [NUM_dwave_core-1:0] w_dwave_pulse_out_f_n    ;
//
//
generate 
for (dwave_core_idx=0;dwave_core_idx<NUM_dwave_core;dwave_core_idx=dwave_core_idx+1) begin: dwave_core
//
dwave_core  dwave_core_inst (
	//
	.reset_n	(reset_n),
	.en			(en),
	//
	.clk_dwave	(clk_dwave), // 80MHz for generating waveform
	// control
	.i_pulse_en		(w_dwave_pulse_en    [dwave_core_idx]),
	//
	.i_cnt_pulse   	(w_dwave_cnt_pulse   [dwave_core_idx]), 
	.i_loc_start   	(w_dwave_loc_start   [dwave_core_idx]), 
	.i_loc_trig_on 	(w_dwave_loc_trig_on [dwave_core_idx]),  
	.i_loc_trig_off	(w_dwave_loc_trig_off[dwave_core_idx]), 
	// IO
	.o_pulse_out_f_ref		(w_dwave_pulse_out_f_ref  [dwave_core_idx]),
	.o_pulse_out_f_ref_n	(w_dwave_pulse_out_f_ref_n[dwave_core_idx]),
	.o_pulse_out_f			(w_dwave_pulse_out_f      [dwave_core_idx]),
	.o_pulse_out_f_n		(w_dwave_pulse_out_f_n    [dwave_core_idx]),
	// flag	
	.debug_out		()
);
//
end
endgenerate
//
//
assign w_dwave_pulse_en    [0] =     r_pulse_en1 | r_pulse_en_trig | r_pulse_en_num ;
assign w_dwave_cnt_pulse   [0] =    r_cnt_pulse1;   
assign w_dwave_loc_start   [0] =    r_loc_start1;   
assign w_dwave_loc_trig_on [0] =  r_loc_trig_on1;
assign w_dwave_loc_trig_off[0] = r_loc_trig_off1;
//
assign w_dwave_pulse_en    [1] =     r_pulse_en2 | r_pulse_en_trig | r_pulse_en_num ;
assign w_dwave_cnt_pulse   [1] =    r_cnt_pulse2;   
assign w_dwave_loc_start   [1] =    r_loc_start2;   
assign w_dwave_loc_trig_on [1] =  r_loc_trig_on2;
assign w_dwave_loc_trig_off[1] = r_loc_trig_off2;
//
assign w_dwave_pulse_en    [2] =     r_pulse_en3 | r_pulse_en_trig | r_pulse_en_num ;
assign w_dwave_cnt_pulse   [2] =    r_cnt_pulse3;   
assign w_dwave_loc_start   [2] =    r_loc_start3;   
assign w_dwave_loc_trig_on [2] =  r_loc_trig_on3;
assign w_dwave_loc_trig_off[2] = r_loc_trig_off3;
//
assign w_dwave_pulse_en    [3] =     r_pulse_en4 | r_pulse_en_trig | r_pulse_en_num ;
assign w_dwave_cnt_pulse   [3] =    r_cnt_pulse4;   
assign w_dwave_loc_start   [3] =    r_loc_start4;   
assign w_dwave_loc_trig_on [3] =  r_loc_trig_on4;
assign w_dwave_loc_trig_off[3] = r_loc_trig_off4;
//
////


////
// pulse output assignment
//
assign pulse_out_f1_ref		= (r_output_dis[0]) ? 1'b0 : w_dwave_pulse_out_f_ref  [0];
assign pulse_out_f1_ref_n	= (r_output_dis[0]) ? 1'b0 : w_dwave_pulse_out_f_ref_n[0];
assign pulse_out_f1			= (r_output_dis[0]) ? 1'b0 : w_dwave_pulse_out_f      [0];
assign pulse_out_f1_n		= (r_output_dis[0]) ? 1'b0 : w_dwave_pulse_out_f_n    [0];
//
assign pulse_out_f2_ref		= (r_output_dis[1]) ? 1'b0 : w_dwave_pulse_out_f_ref  [1];
assign pulse_out_f2_ref_n	= (r_output_dis[1]) ? 1'b0 : w_dwave_pulse_out_f_ref_n[1];
assign pulse_out_f2			= (r_output_dis[1]) ? 1'b0 : w_dwave_pulse_out_f      [1];
assign pulse_out_f2_n		= (r_output_dis[1]) ? 1'b0 : w_dwave_pulse_out_f_n    [1];
//
assign pulse_out_f3_ref		= (r_output_dis[2]) ? 1'b0 : w_dwave_pulse_out_f_ref  [2];
assign pulse_out_f3_ref_n	= (r_output_dis[2]) ? 1'b0 : w_dwave_pulse_out_f_ref_n[2];
assign pulse_out_f3			= (r_output_dis[2]) ? 1'b0 : w_dwave_pulse_out_f      [2];
assign pulse_out_f3_n		= (r_output_dis[2]) ? 1'b0 : w_dwave_pulse_out_f_n    [2];
//
assign pulse_out_f4_ref		= (r_output_dis[3]) ? 1'b0 : w_dwave_pulse_out_f_ref  [3];
assign pulse_out_f4_ref_n	= (r_output_dis[3]) ? 1'b0 : w_dwave_pulse_out_f_ref_n[3];
assign pulse_out_f4			= (r_output_dis[3]) ? 1'b0 : w_dwave_pulse_out_f      [3];
assign pulse_out_f4_n		= (r_output_dis[3]) ? 1'b0 : w_dwave_pulse_out_f_n    [3];
//
////


////
(* keep = "true" *) reg [7:0] state; 
//
parameter 	RESET_STATE		= 8'h00; // reset state
//
parameter 	INIT_STATE 		= 8'h01; // init state 
parameter 	UPDATE_STATE 	= 8'h02; // update state 
parameter 	TEST_STATE 		= 8'h03; // test state 
//
parameter 	SEQ_START 		= 8'hfd; // start of seq
parameter 	SEQ_END 		= 8'hfe; // end of seq
parameter 	SEQ_END_WAIT	= 8'hff; // end of seq


////
// RESET_STATE -> SEQ_START (wait for triggers) -> (options) -> SEQ_END -> SEQ_START ...
//
//
assign debug_out = state;


// init packet
reg r_init_trig;
reg r_init_done;
reg [1:0] r_init;
reg r_init_done_smp;

// update packet
reg r_update_trig;
reg r_update_done;
reg [1:0] r_update;
reg r_update_done_smp;
	
// test packet
reg r_test_trig;
reg r_test_done;
reg [1:0] r_test;
reg r_test_done_smp;
//
assign init_done = r_init_done;
assign update_done = r_update_done;
assign test_done = r_test_done;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
        r_init <= 2'b0;
		r_init_done_smp <= 1'b0;
		//
        r_update <= 2'b0;
		r_update_done_smp <= 1'b0;
		//
        r_test <= 2'b0;
		r_test_done_smp <= 1'b0;
        end
    else if (en) begin
        r_init <= {r_init[0], init};
        r_init_done_smp <= r_init_done;
		//
        r_update <= {r_update[0], update};
        r_update_done_smp <= r_update_done;
		//
        r_test <= {r_test[0], test};
        r_test_done_smp <= r_test_done;
        end 
    else begin
        r_init <= 2'b0;
		r_init_done_smp <= 1'b0;
		//
        r_update <= 2'b0;
		r_update_done_smp <= 1'b0;
		//
        r_test <= 2'b0;
		r_test_done_smp <= 1'b0;
        end
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
        r_init_trig <= 1'b0;
        r_update_trig <= 1'b0;
        r_test_trig <= 1'b0;
        end
    else if (en) begin
		if (!r_init_trig)
			r_init_trig <= ~r_init[1]&r_init[0];
		else if (~r_init_done_smp&r_init_done)
			r_init_trig <= 1'b0;
		//
		if (!r_update_trig)
			r_update_trig <= ~r_update[1]&r_update[0];
		else if (~r_update_done_smp&r_update_done)
			r_update_trig <= 1'b0;
		//
		if (!r_test_trig)
			r_test_trig <= ~r_test[1]&r_test[0];
		else if (~r_test_done_smp&r_test_done)
			r_test_trig <= 1'b0;
        end 
    else begin
        r_init_trig <= 1'b0;
        r_update_trig <= 1'b0;
        r_test_trig <= 1'b0;
        end

		
		
reg r_error; 
assign error = r_error;
//
(* keep = "true" *) reg r_init_busy;		
(* keep = "true" *) reg r_update_busy;		
(* keep = "true" *) reg r_test_busy;		
		
// process state 
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		state       		<= RESET_STATE;
		//
		r_cnt_pulse1		<= 32'd0; //$$
		r_loc_start1		<= 32'd0; //$$
		r_loc_trig_on1		<= 32'd0; //$$
		r_loc_trig_off1		<= 32'd0; //$$
		//
		r_cnt_pulse2		<= 32'd0; //$$
		r_loc_start2		<= 32'd0; //$$
		r_loc_trig_on2		<= 32'd0; //$$
		r_loc_trig_off2		<= 32'd0; //$$
		//
		r_cnt_pulse3		<= 32'd0; //$$
		r_loc_start3		<= 32'd0; //$$
		r_loc_trig_on3		<= 32'd0; //$$
		r_loc_trig_off3		<= 32'd0; //$$
		//
		r_cnt_pulse4		<= 32'd0; //$$
		r_loc_start4		<= 32'd0; //$$
		r_loc_trig_on4		<= 32'd0; //$$
		r_loc_trig_off4		<= 32'd0; //$$
		//
		r_pulse_en1			<= 1'b0; 
		r_pulse_en2			<= 1'b0; 
		r_pulse_en3			<= 1'b0; 
		r_pulse_en4			<= 1'b0; 
		//
        r_init_done     	<= 1'b0;
		r_update_done     	<= 1'b0;
		r_test_done     	<= 1'b0;
		//
        r_init_busy     	<= 1'b0;
		r_update_busy     	<= 1'b0;
		r_test_busy     	<= 1'b0;
		//
		r_error				<=1'b0;
		end 
	else case (state)
		RESET_STATE : begin
			if (en) begin
				state 			<= SEQ_START; 
				end
			else begin 
				// do ... during reset state
				end
			end
		// start seq
		SEQ_START : begin
			//
			if (!en) begin 
				r_init_done		<= 1'b0;
				r_init_busy 	<= 1'b0;
				r_update_done	<= 1'b0;
				r_update_busy 	<= 1'b0;
				r_test_done		<= 1'b0;
				r_test_busy 	<= 1'b0;
                state   		<= RESET_STATE;
				end
			else if (r_init_trig) begin
				r_init_done		<= 1'b0;
				r_init_busy 	<= 1'b1;
                state   		<= INIT_STATE;
				end
			//
			else if (r_update_trig) begin
				r_update_done	<= 1'b0;
				r_update_busy 	<= 1'b1;
                state   		<= UPDATE_STATE;
				end
			//
			else if (r_test_trig) begin
				r_test_done		<= 1'b0;
				r_test_busy 	<= 1'b1;
                state   		<= TEST_STATE;
				end
			else begin 
				state   		<= state;
				//
				// do alternative setting by trig
				if (i_trig_set_parameters) begin 
					r_cnt_pulse1 	<= r_cnt_period;
					r_cnt_pulse2 	<= r_cnt_period;
					r_cnt_pulse3 	<= r_cnt_period;
					r_cnt_pulse4 	<= r_cnt_period;
					//
					r_loc_start1	<= 32'd0      + 32'd1;  // period_count_0_8 + 1  //$$
					r_loc_start2	<= 32'd0      + 32'd1;  // period_count_0_8 + 1  //$$
					r_loc_start3	<= r_cnt_diff + 32'd1;  // period_count_6_8 + 1  //$$
					r_loc_start4	<= r_cnt_diff + 32'd1;  // period_count_6_8 + 1  //$$
					//              
					r_loc_trig_on1	<= ((r_cnt_period*1)+4)>>3; // period_count_1_8 //$$
					r_loc_trig_on2	<= ((r_cnt_period*7)+4)>>3; // period_count_7_8 //$$
					r_loc_trig_on3	<= ((r_cnt_period*1)+4)>>3; // period_count_1_8 //$$
					r_loc_trig_on4	<= ((r_cnt_period*7)+4)>>3; // period_count_7_8 //$$
					//              
					r_loc_trig_off1	<= ((r_cnt_period*3)+4)>>3; // period_count_3_8 //$$
					r_loc_trig_off2	<= ((r_cnt_period*5)+4)>>3; // period_count_5_8 //$$
					r_loc_trig_off3	<= ((r_cnt_period*3)+4)>>3; // period_count_3_8 //$$
					r_loc_trig_off4	<= ((r_cnt_period*5)+4)>>3; // period_count_5_8 //$$
					//              
					r_pulse_en1		<= 1'b0; 
					r_pulse_en2		<= 1'b0; 
					r_pulse_en3		<= 1'b0; 
					r_pulse_en4		<= 1'b0; 
					end 
				//
				end 
			//
			end
		//
		// init state 
		INIT_STATE : begin
			state           	<= SEQ_END;
			// do ... during init state
			r_cnt_pulse1		<= 32'd0; //$$
			r_loc_start1		<= 32'd0; //$$
			r_loc_trig_on1		<= 32'd0; //$$
			r_loc_trig_off1		<= 32'd0; //$$
			//
			r_cnt_pulse2		<= 32'd0; //$$
			r_loc_start2		<= 32'd0; //$$
			r_loc_trig_on2		<= 32'd0; //$$
			r_loc_trig_off2		<= 32'd0; //$$
			//
			r_cnt_pulse3		<= 32'd0; //$$
			r_loc_start3		<= 32'd0; //$$
			r_loc_trig_on3		<= 32'd0; //$$
			r_loc_trig_off3		<= 32'd0; //$$
			//
			r_cnt_pulse4		<= 32'd0; //$$
			r_loc_start4		<= 32'd0; //$$
			r_loc_trig_on4		<= 32'd0; //$$
			r_loc_trig_off4		<= 32'd0; //$$
			//
			r_pulse_en1			<= 1'b0; 
			r_pulse_en2			<= 1'b0; 
			r_pulse_en3			<= 1'b0; 
			r_pulse_en4			<= 1'b0; 
			end		
		//
		// update state 
		UPDATE_STATE : begin
			state           	<= SEQ_END;
			// do ... during update state
			r_pulse_en1			<= test_datain[0]; 
			r_pulse_en2			<= test_datain[1]; 
			r_pulse_en3			<= test_datain[2]; 
			r_pulse_en4			<= test_datain[3]; 
			end		
		//
		// test state 		
		TEST_STATE : begin
			state           	<= SEQ_END; // next state
			// do ... during update state
			r_cnt_pulse1		<= (test_datain_port==2'b00&&test_datain_type==2'b00)? test_datain :    r_cnt_pulse1;
			r_loc_start1		<= (test_datain_port==2'b00&&test_datain_type==2'b01)? test_datain :    r_loc_start1;
			r_loc_trig_on1		<= (test_datain_port==2'b00&&test_datain_type==2'b10)? test_datain :  r_loc_trig_on1;
			r_loc_trig_off1		<= (test_datain_port==2'b00&&test_datain_type==2'b11)? test_datain : r_loc_trig_off1;
			//
			r_cnt_pulse2		<= (test_datain_port==2'b01&&test_datain_type==2'b00)? test_datain :    r_cnt_pulse2;
			r_loc_start2		<= (test_datain_port==2'b01&&test_datain_type==2'b01)? test_datain :    r_loc_start2;
			r_loc_trig_on2		<= (test_datain_port==2'b01&&test_datain_type==2'b10)? test_datain :  r_loc_trig_on2;
			r_loc_trig_off2		<= (test_datain_port==2'b01&&test_datain_type==2'b11)? test_datain : r_loc_trig_off2;
			//
			r_cnt_pulse3		<= (test_datain_port==2'b10&&test_datain_type==2'b00)? test_datain :    r_cnt_pulse3;
			r_loc_start3		<= (test_datain_port==2'b10&&test_datain_type==2'b01)? test_datain :    r_loc_start3;
			r_loc_trig_on3		<= (test_datain_port==2'b10&&test_datain_type==2'b10)? test_datain :  r_loc_trig_on3;
			r_loc_trig_off3		<= (test_datain_port==2'b10&&test_datain_type==2'b11)? test_datain : r_loc_trig_off3;
			//
			r_cnt_pulse4		<= (test_datain_port==2'b11&&test_datain_type==2'b00)? test_datain :    r_cnt_pulse4;
			r_loc_start4		<= (test_datain_port==2'b11&&test_datain_type==2'b01)? test_datain :    r_loc_start4;
			r_loc_trig_on4		<= (test_datain_port==2'b11&&test_datain_type==2'b10)? test_datain :  r_loc_trig_on4;
			r_loc_trig_off4		<= (test_datain_port==2'b11&&test_datain_type==2'b11)? test_datain : r_loc_trig_off4;
			end		
		//
		// end of seq
		SEQ_END : begin
			// check done 
			if (r_init_busy) begin // init packet was sent!
				r_init_done		<= 1'b1;
				r_init_busy		<= 1'b0;
				end
			if (r_update_busy) begin // update packet was sent!
				r_update_done	<= 1'b1;
				r_update_busy	<= 1'b0;
				end
			if (r_test_busy) begin // test packet was sent!
				r_test_done		<= 1'b1;
				r_test_busy		<= 1'b0;
				end
			//
			state 				<= SEQ_END_WAIT; // return to start of seq
			end
		SEQ_END_WAIT : begin // wait for clearing the previous trigger.
			state 				<= SEQ_START; // return to start of seq
			end
		//
		default  : begin
			state 			<= RESET_STATE;
			end
	endcase
	
endmodule
