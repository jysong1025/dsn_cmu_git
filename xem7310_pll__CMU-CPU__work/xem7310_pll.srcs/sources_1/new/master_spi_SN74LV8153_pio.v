//------------------------------------------------------------------------
// master_spi_SN74LV8153_pio.v
//  for SN74LV8153 Single-Wire Serial Data Input, serial to parallel interface 
//  	http://www.ti.com/lit/ds/symlink/sn74lv8153.pdf
//  support parallel input and montor output
//
//  - IO
// input  wire [63:0] ctr_in,
// output wire [63:0] mon_out,
// output wire spo_cdat,
// output wire spo_rst_n,
//
//  - reg 
// CTL_IO	CTL_IO_CON	wi11	control for CMU_PLAN_A_IO
//			CTL_IO_CON = {2'b0, test_pdata[13:0], 5'b0, num_bytes[2:0], 1'b0, adrs_start[2:0], test,update,init,en}
//						en: enable SP_CON block.
//						init: generate reset signals 
//						update: reserved for pipe io.
//						test: signal test. 
//						adrs_start[2:0] : start address of SN74LV8153
//						num_bytes[2:0] : set (numbers of bytes to increase) - 1
//							0 for  1 bytes
//							1 for  2 bytes
//							2 for  3 bytes
//							...
//							7 for  8 bytes
//						test_pdata[13:0] : test packet data 14 bits
//							test_pdata[13] : 1st frame A0
//							test_pdata[12] : 1st frame A1
//							test_pdata[11] : 1st frame A2
//							test_pdata[10] : 1st frame D0
//							test_pdata[ 9] : 1st frame D1
//							test_pdata[ 8] : 1st frame D2
//							test_pdata[ 7] : 1st frame D3
//							test_pdata[ 6] : 2nd frame A0
//							test_pdata[ 5] : 2nd frame A1
//							test_pdata[ 4] : 2nd frame A2
//							test_pdata[ 3] : 2nd frame D4
//							test_pdata[ 2] : 2nd frame D5
//							test_pdata[ 1] : 2nd frame D6
//							test_pdata[ 0] : 2nd frame D7
// CTL_IO	CTL_IO_FLAG	wo28	flag for for CMU_PLAN_A_IO
//			CTL_IO_FLAG = {..., test_done,update_done,init_done,en}
//
//  - serial data format or timing 
//  	SN74LV8153PWR/TSSOP-20   3.3V 5V  2~24Kbps ... 12kHz pulse based operation design...
//  	reset 200ns
//  	12kHz = 83.333us
//  	10MHz/12kHz = 833.333333 
//  	10MHz/834 = 11.9904077 kHz or 83.4us
//  	// http://www.ti.com/lit/ds/symlink/sn74lv8153.pdf
//  	// http://www.eleparts.co.kr/EPX3JLUN  2660
//
//  	<timing chart - test process>
//  	based on 12kHz = 83.333us
//  	            222211111111110000000000
//  	            321098765432109876543210
//  	test_trig  _------------------------__
//  	RST_B      --_------------------------ // clear every test packet
//  	D          -----_-AAADDDD-_-AAADDDD---
//  	               SSS0120123SSS0124567S
//  			       PTT       PTT       P
//  			        01        01
//
//  	<timing chart - update process>
//  	based on 12kHz = 83.333us
//  	            222211111111110000000000
//  	            321098765432109876543210
//  	test_trig  _------------------------__
//  	RST_B      --------------------------- // not clear with update_trig
//  	D          -----_-AAADDDD-_-AAADDDD---
//  	               SSS0120123SSS0124567S
//  			       PTT       PTT       P
//  			        01        01
//
//  	<timing chart - init process>
//  	en         ________-----------------------_______
//  	init_trig  ___________------_____------__________
//  	RST_B      ____________----------_--------_______
//  	D          ___________--------------------_______
//
//------------------------------------------------------------------------

`timescale 1ns / 1ps
module master_spi_SN74LV8153_pio (  
	// 
	input wire clk, // assume 10MHz or 100ns
	input wire reset_n,
    input wire en,
	//
	// control
    input wire init, // rising
    input wire update, // rising
    input wire test, // rising
	//
	input wire [2:0] adrs_start, // start address of SN74LV8153
	input wire [2:0] num_bytes, // numbers of bytes to increase
	input wire [13:0] test_pdata, // 14 bit test data packet
	//
	// IO for multiple SN74LV8153 in a single bus.
	input  wire [63:0] ctr_in,
	output wire [63:0] mon_out,
	output wire spo_cdat,
	output wire spo_rst_n,
	//
    // flag
    output wire init_done,
    output wire update_done,
	output wire test_done,
	output wire error,
	output wire [7:0] debug_out
);

// local clock - low speed
// 10MHz/834 = 11.9904077 kHz or 83.4us
// clock enable for 11.9904077 kHz
reg r_clken_12kHz; 
parameter CLKEN_12KHZ_PERIOD_SET = 10'd0834; // 
reg [9:0] r_cnt_clk_12kHz;
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
        r_clken_12kHz <= 1'b0;
		r_cnt_clk_12kHz <= 10'b0;
        end
    else if (en) begin
		if (r_cnt_clk_12kHz > 0) begin
			r_clken_12kHz = 1'b0;
			r_cnt_clk_12kHz <= r_cnt_clk_12kHz - 1;
			end 
		else begin 
			r_clken_12kHz = 1'b1;
			r_cnt_clk_12kHz <= CLKEN_12KHZ_PERIOD_SET - 1;
			end
        end 
    else begin
        r_clken_12kHz <= 1'b0;
		r_cnt_clk_12kHz <= CLKEN_12KHZ_PERIOD_SET - 1; //$$
        end
//

(* keep = "true" *) reg [7:0] r_cnt_packet;
//  	SN74LV8153
//  	<timing chart>
//  	based on 12kHz = 83.333us
//  	            222211111111110000000000
//  	            321098765432109876543210
//  	test_trig  _------------------------__
//
//  	RST_B      --_------------------------
//  	term	    abc
//
//  	D          -----_-AAADDDD-_-AAADDDD---
//  	               SSS0120123SSS0124567S
//  			       PTT       PTT       P
//  			        01        01
//  	term	    a   b                  c
parameter 	LEN_PACKET	= 8'd24; //

// function of wave_sdat_rstb: SDAT*_RSTB
// wave_sdat_rstb(1,1,22,payload)	
//
function [LEN_PACKET-1:0] wave_sdat_rstb;
input [7:0] a,b,c; // terms
input payload; // 1 bit 
integer ii;
begin
	for(ii=LEN_PACKET-1; ii>LEN_PACKET-1-a; ii=ii-1) begin
		wave_sdat_rstb[ii] = 1'b1;
	end
	for(ii=LEN_PACKET-1-a; ii>LEN_PACKET-1-a-b; ii=ii-1) begin
		wave_sdat_rstb[ii] = payload;
	end
	for(ii=0; ii<c; ii=ii+1) begin
		wave_sdat_rstb[ii] = 1'b1;
	end
end
endfunction 

// function of wave_sclk_data: SCLK*_DATA
// wave_sclk_data(4,19,1,payload)	
//
function [LEN_PACKET-1:0] wave_sclk_data;
input [7:0] a,b,c; // terms
input [18:0] payload; // 19 bits
integer ii;
begin
	for(ii=LEN_PACKET-1; ii>LEN_PACKET-1-a; ii=ii-1) begin
		wave_sclk_data[ii] = 1'b1;
	end
	for(ii=LEN_PACKET-1-a; ii>LEN_PACKET-1-a-b; ii=ii-1) begin
		wave_sclk_data[ii] = payload[ii-c];
	end
	for(ii=0; ii<c; ii=ii+1) begin
		wave_sclk_data[ii] = 1'b1;
	end
end
endfunction 

// function of packet_shift_left
function [LEN_PACKET-1:0] packet_shift_left;
input [LEN_PACKET-1:0] packet_str; 
input data_s_in; // data shift in
begin
	packet_shift_left = {packet_str[LEN_PACKET-2:0], data_s_in};
end
endfunction 

// packet register for SN74LV8153
reg [LEN_PACKET-1:0] r_SDAT_RSTB;
reg [LEN_PACKET-1:0] r_SCLK_DATA;
// monitoring 
(* keep = "true" *) wire SDAT_RSTB = r_SDAT_RSTB[LEN_PACKET-1];
(* keep = "true" *) wire SCLK_DATA = r_SCLK_DATA[LEN_PACKET-1];

// state register
(* keep = "true" *) reg [7:0] state; 
(* keep = "true" *) reg [7:0] state_after_packet; 
//
parameter 	INIT_PACKET_SET	= 8'hcf; 
//
parameter 	UPDATE_PACKET_SET	= 8'hdf; // make update packet
//
parameter 	TEST_PACKET_SET		= 8'he0; // make test packet
//
parameter 	PACKET_GEN		= 8'hf8; // send a packet
//
parameter 	INIT_STATE 		= 8'hfc; // reset state
//
parameter 	SEQ_START 		= 8'hfd; // start of seq
parameter 	SEQ_END 		= 8'hfe; // end of seq
parameter 	SEQ_END_WAIT	= 8'hff; // end of seq
//

reg [2:0] r_idx_byte;
wire [2:0] w_adrs;
//
wire [7:0] w_ctr_in_sel = ctr_in[(6'd8*(adrs_start+r_idx_byte)) +: 6'd8];
wire [7:0] w_data;
//
reg [63:0] r_mon_out;
//
assign mon_out = r_mon_out;

////
// INIT_STATE -> SEQ_START (wait for triggers) -> (options) -> SEQ_END -> SEQ_START ...
//
// initial seq: triggered by r_init_trig
// update seq: triggered by r_update_trig
// test packet seq: triggered by r_test_trig 
//
//
assign debug_out = state;


// init packet
reg r_init_trig;
reg r_init_done;
reg [1:0] r_init;
reg r_init_done_smp;

// update packet
reg r_update_trig;
reg r_update_done;
reg [1:0] r_update;
reg r_update_done_smp;
	
// test packet
reg r_test_trig;
reg r_test_done;
reg [1:0] r_test;
reg r_test_done_smp;

// busy
reg r_init_busy;		
reg r_update_busy;		
reg r_test_busy;		

// done
assign init_done = r_init_done;
assign update_done = r_update_done;
assign test_done = r_test_done;


// assign outputs 
assign spo_rst_n = (en&init_done)? SDAT_RSTB : 1'b0;
assign spo_cdat = (en&init_done)? SCLK_DATA : 1'b0;
	
// process sampling
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
        r_init <= 2'b0;
		r_init_done_smp <= 1'b0;
		//
        r_update <= 2'b0;
		r_update_done_smp <= 1'b0;
		//
        r_test <= 2'b0;
		r_test_done_smp <= 1'b0;
        end
    else if (en) begin
        r_init <= {r_init[0], init};
        r_init_done_smp <= r_init_done;
		//
        r_update <= {r_update[0], update};
        r_update_done_smp <= r_update_done;
		//
        r_test <= {r_test[0], test};
        r_test_done_smp <= r_test_done;
        end 
    else begin
        r_init <= 2'b0;
		r_init_done_smp <= 1'b0;
		//
        r_update <= 2'b0;
		r_update_done_smp <= 1'b0;
		//
        r_test <= 2'b0;
		r_test_done_smp <= 1'b0;
        end

// process trig 
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
        r_init_trig <= 1'b0;
        r_update_trig <= 1'b0;
        r_test_trig <= 1'b0;
        end
    else if (en) begin
		if (!r_init_trig)
			r_init_trig <= ~r_init[1]&r_init[0];
		else if (~r_init_done_smp&r_init_done)
			r_init_trig <= 1'b0;
		//
		if (!r_update_trig)
			r_update_trig <= ~r_update[1]&r_update[0];
		else if (~r_update_done_smp&r_update_done)
			r_update_trig <= 1'b0;
		//
		if (!r_test_trig)
			r_test_trig <= ~r_test[1]&r_test[0];
		else if (~r_test_done_smp&r_test_done)
			r_test_trig <= 1'b0;
        end 
    else begin
        r_init_trig <= 1'b0;
        r_update_trig <= 1'b0;
        r_test_trig <= 1'b0;
        end
		
reg r_error; // update error
assign error = r_error;
//

//
assign w_adrs = adrs_start+r_idx_byte;
assign w_data = w_ctr_in_sel;

// process state 
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		state       		<= INIT_STATE;
		state_after_packet	<= INIT_STATE;
		r_cnt_packet		<= 8'b0;
		//
		r_idx_byte			<= 3'b0;
		r_mon_out			<= 64'b0;
		//
		r_SDAT_RSTB			<= {(LEN_PACKET) {1'b1}};
		r_SCLK_DATA			<= {(LEN_PACKET) {1'b1}};
		//
		r_error				<=1'b0;
		//
        r_init_done     	<= 1'b0;
		r_update_done     	<= 1'b0;
		r_test_done     	<= 1'b0;
		//
        r_init_busy     	<= 1'b0;
		r_update_busy     	<= 1'b0;
		r_test_busy     	<= 1'b0;
		//
		r_error				<=1'b0;
		end 
	else case (state)
		// r_clken_12kHz for slow clock enable
		// wait for stable state ... may need more times.
		INIT_STATE : if (r_clken_12kHz) begin
			if (en) state <= SEQ_START; // wait for ADC not busy
			end
		// start seq
		SEQ_START : if (r_clken_12kHz) begin
			//
			r_error				<=1'b0;
			//
			if (en && r_init_trig) begin
				r_init_done	<= 1'b0;
				r_init_busy <= 1'b1;
                state   	<= INIT_PACKET_SET; // wait for en
				end
			//
			else if (en && r_update_trig) begin
				r_update_done	<= 1'b0;
				r_update_busy <= 1'b1;
				state   	<= UPDATE_PACKET_SET; // wait for en
				//
				r_idx_byte	<= 3'b0;
				end
			//
			else if (en && r_test_trig) begin
				r_test_done	<= 1'b0;
				r_test_busy <= 1'b1;
                state   	<= TEST_PACKET_SET; // wait for en
				end
			end
		//
		// init packet
		INIT_PACKET_SET : if (r_clken_12kHz) begin
			//
			r_SDAT_RSTB			<= wave_sdat_rstb(1,1,22, 1'b0);
			r_SCLK_DATA			<= wave_sclk_data(4,19,1,19'b11_1111111_1_11_1111111); // init payload 
			//
			r_cnt_packet    	<= LEN_PACKET;
			//
			state_after_packet	<= SEQ_END; // no readback 
			//
			state           	<= PACKET_GEN;			
			end
		//
		// update packet
		UPDATE_PACKET_SET : if (r_clken_12kHz) begin
			//
			r_SDAT_RSTB			<= wave_sdat_rstb(1,1,22, 1'b1); // not clear with update packets 
			//r_SCLK_DATA			<= wave_sclk_data(4,19,1,{2'b01,adrs_start[2:0],w_ctr_in_sel[7:4], 3'b1_01, adrs_start[2:0],w_ctr_in_sel[3:0]}); 
			//r_SCLK_DATA			<= wave_sclk_data(4,19,1,{2'b01,(adrs_start[2:0]+r_idx_byte[2:0]),w_ctr_in_sel[7:4], 3'b1_01,(adrs_start[2:0]+r_idx_byte[2:0]),w_ctr_in_sel[3:0]}); 
			r_SCLK_DATA			<= wave_sclk_data(4,19,1,{2'b01,w_adrs[0],w_adrs[1],w_adrs[2], w_data[0],w_data[1],w_data[2],w_data[3],
			                                            3'b1_01,w_adrs[0],w_adrs[1],w_adrs[2], w_data[4],w_data[5],w_data[6],w_data[7]});
			//
			r_cnt_packet    	<= LEN_PACKET;
			//
			if (r_idx_byte == num_bytes) begin
				state_after_packet	<= SEQ_END; // go to end after the final packet
				end
			else begin
				state_after_packet	<= UPDATE_PACKET_SET; // send again after packet_gen
				r_idx_byte			<= r_idx_byte + 3'd01;
				end
			//
			state           	<= PACKET_GEN;			
			//
			r_mon_out[(6'd8*(adrs_start+r_idx_byte)) +: 6'd8]	<= w_ctr_in_sel; // update mon_out
			//
			end 
		//
		// test packet
		TEST_PACKET_SET : if (r_clken_12kHz) begin
			//
			r_SDAT_RSTB			<= wave_sdat_rstb(1,1,22, 1'b0); // clear output registers with test packets
			r_SCLK_DATA			<= wave_sclk_data(4,19,1,{2'b01,test_pdata[13:7],3'b1_01,test_pdata[6:0]}); 
			//
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= SEQ_END; // no readback 
			//
			state           	<= PACKET_GEN;
			end
		//
		// send a packet 
		PACKET_GEN : if (r_clken_12kHz) begin
			if (r_cnt_packet == 0) begin // all sent
				state       <= state_after_packet; // set state to return
				end 
			else if (r_cnt_packet > 0) begin
				r_SDAT_RSTB      	<= packet_shift_left(r_SDAT_RSTB, 1'b1); // move towards MSB 
				r_SCLK_DATA    		<= packet_shift_left(r_SCLK_DATA, 1'b1); // move towards MSB 
				//
				r_cnt_packet <= r_cnt_packet - 1;            
				//
				if (SDAT_RSTB == 1'b0) // clear with reset 
					r_mon_out		<= 64'b0;
				//
				end
			end
		//
		// end of seq
		SEQ_END : if (r_clken_12kHz) begin
			// check done 
			if (r_init_busy) begin // init packet was sent!
				r_init_done		<= 1'b1;
				r_init_busy		<= 1'b0;
				end
			if (r_update_busy) begin // update packet was sent!
				r_update_done	<= 1'b1;
				r_update_busy	<= 1'b0;
				end
			if (r_test_busy) begin // test packet was sent!
				r_test_done		<= 1'b1;
				r_test_busy		<= 1'b0;
				end
			//
			state_after_packet	<= INIT_STATE;
			state 				<= SEQ_END_WAIT; // return to start of seq
			end
		SEQ_END_WAIT : if (r_clken_12kHz) begin // wait for clearing the previous trigger.
			state <= SEQ_START; // return to start of seq
			end
		default  : if (r_clken_12kHz) begin
			state <= INIT_STATE;
			r_error				<=1'b1;
			end
	endcase
 
	
endmodule
