`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: tb_dwave_control
// 
// http://www.asic-world.com/verilog/art_testbench_writing1.html
// http://www.asic-world.com/verilog/art_testbench_writing2.html
// https://www.xilinx.com/support/documentation/sw_manuals/xilinx2016_1/ug937-vivado-design-suite-simulation-tutorial.pdf
//
//////////////////////////////////////////////////////////////////////////////////


module tb_dwave_control;
reg clk; // assume 10MHz or 100ns
reg reset_n;
reg en;
//
reg clk_bus; //$$ 9.92ns for USB3.0	
reg clk_dwave; // 80MHz or 12.5ns for dwave
//
reg init;
reg update;
reg test;
//
wire init_done;
wire update_done;
wire test_done;
//
reg [15:0] test_datain;
reg [1:0] test_datain_type;
reg [1:0] test_datain_port;
//	
reg r_trig_pulse_off      = 1'b0;
reg r_trig_pulse_on_cont  = 1'b0;
reg r_trig_pulse_on_num   = 1'b0;
reg r_trig_set_parameters = 1'b0;

// DUT
wire sclk;
dwave_control dwave_control_inst(
	.clk(clk), // assume 10MHz or 100ns
	.reset_n(reset_n),
	.en(en),
	//
	.clk_dwave(clk_dwave),
	//
	.init(init),
	.update(update),
	.test(test),
	//
	.test_datain(test_datain),
	.test_datain_type(test_datain_type),
	.test_datain_port(test_datain_port),
	//
	//
	.i_trig_pulse_off      (r_trig_pulse_off    ),
	.i_trig_pulse_on_cont  (r_trig_pulse_on_cont),
	.i_trig_pulse_on_num   (r_trig_pulse_on_num ),
	.i_trig_set_parameters (r_trig_set_parameters),
	.i_wire_datain_by_trig   (), // 32b
	.o_wire_dataout_by_trig  (), // 32b
	.i_trig_wr_cnt_period  (),
	.i_trig_rd_cnt_period  (),
	.i_trig_wr_cnt_diff    (),
	.i_trig_rd_cnt_diff    (),
	.i_trig_wr_num_pulses  (),
	.i_trig_rd_num_pulses  (),
	//
	//
	.init_done(init_done),
	.update_done(update_done),
	.test_done(test_done),
	.error(),
	.debug_out()
);

initial begin
#0		clk = 1'b0;
		reset_n = 1'b0;
		en = 1'b0;
		clk_bus = 1'b0;
		clk_dwave = 1'b0;
		init = 1'b0;
		update = 1'b0;
		test = 1'b0;
		//
		test_datain = 16'd00;
		test_datain_type = 2'b00;
		test_datain_port = 2'b00;
//
//#10		test = 1'b1;
//		test_sdi_pdata = 32'b1010_0101_1001_0110_1100_1010_0011_0101;
#200	reset_n = 1'b1;
#200	en = 1'b1;
//
// f1
#1000	test = 1'b0;
		test_datain = 16'd08;
		test_datain_type = 2'b00;
		test_datain_port = 2'b00;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd01;
		test_datain_type = 2'b01;
		test_datain_port = 2'b00;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd01;
		test_datain_type = 2'b10;
		test_datain_port = 2'b00;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd03;
		test_datain_type = 2'b11;
		test_datain_port = 2'b00;
#1000	test = 1'b1;
//
// f2
#1000	test = 1'b0;
		test_datain = 16'd08;
		test_datain_type = 2'b00;
		test_datain_port = 2'b01;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd01;
		test_datain_type = 2'b01;
		test_datain_port = 2'b01;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd07;
		test_datain_type = 2'b10;
		test_datain_port = 2'b01;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd05;
		test_datain_type = 2'b11;
		test_datain_port = 2'b01;
#1000	test = 1'b1;
//
// f3
#1000	test = 1'b0;
		test_datain = 16'd08;
		test_datain_type = 2'b00;
		test_datain_port = 2'b10;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd07;
		test_datain_type = 2'b01;
		test_datain_port = 2'b10;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd01;
		test_datain_type = 2'b10;
		test_datain_port = 2'b10;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd03;
		test_datain_type = 2'b11;
		test_datain_port = 2'b10;
#1000	test = 1'b1;
//
// f4
#1000	test = 1'b0;
		test_datain = 16'd08;
		test_datain_type = 2'b00;
		test_datain_port = 2'b11;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd07;
		test_datain_type = 2'b01;
		test_datain_port = 2'b11;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd07;
		test_datain_type = 2'b10;
		test_datain_port = 2'b11;
#1000	test = 1'b1;
//
#1000	test = 1'b0;
		test_datain = 16'd05;
		test_datain_type = 2'b11;
		test_datain_port = 2'b11;
#1000	test = 1'b1;
//
//
#1000	update = 1'b0;
		test_datain[3:0] = 4'b1111;
#1000	update = 1'b1;
//
//
#1000	update = 1'b0;
		test_datain[3:0] = 4'b0000;
#1000	update = 1'b1;
//
//
//#15_000	init = 1'b1;
//		test = 1'b0;
//#30_000	update = 1'b1;
//		test = 1'b1;
    @(posedge update_done);
#1000;
//
// test trig interface
//	.i_trig_pulse_off      (),
//	.i_trig_pulse_on_cont  (),
	@(posedge clk);
	r_trig_pulse_on_cont = 1'b1;
	@(posedge clk);
	r_trig_pulse_on_cont = 1'b0;
#1000;
	@(posedge clk);
	r_trig_pulse_off = 1'b1;
	@(posedge clk);
	r_trig_pulse_off = 1'b0;
#1000;
//
//	.i_trig_pulse_on_num   (),
	@(posedge clk);
	r_trig_pulse_on_num = 1'b1;
	@(posedge clk);
	r_trig_pulse_on_num = 1'b0;
#(100*100);
#1000;
//	
	@(posedge clk);
	r_trig_pulse_on_num = 1'b1;
	@(posedge clk);
	r_trig_pulse_on_num = 1'b0;
#1000;
//	
	@(posedge clk);
	r_trig_pulse_off = 1'b1;
	@(posedge clk);
	r_trig_pulse_off = 1'b0;
#1000;
	//
	@(posedge clk);
	r_trig_set_parameters = 1'b1;
	@(posedge clk);
	r_trig_set_parameters = 1'b0;
	//
	@(posedge clk);
	r_trig_pulse_on_num = 1'b1;
	@(posedge clk);
	r_trig_pulse_on_num = 1'b0;
#(100*100);
#1000;
	//
	//
	// pulse on / pulse off
	@(posedge clk);
	r_trig_pulse_on_cont = 1'b1;
	@(posedge clk);
	r_trig_pulse_on_cont = 1'b0;
#1000;
#(100*100);
	//
	@(posedge clk);
	r_trig_pulse_off = 1'b1;
	@(posedge clk);
	r_trig_pulse_off = 1'b0;
#1000;
#(100*100);
	//
	//
	// pulse on / pulse off
	@(posedge clk);
	r_trig_pulse_on_cont = 1'b1;
	@(posedge clk);
	r_trig_pulse_on_cont = 1'b0;
#1000;
#(100*100);
	//
	@(posedge clk);
	r_trig_pulse_off = 1'b1;
	@(posedge clk);
	r_trig_pulse_off = 1'b0;
#1000;
#(100*100);
	//
	//
	// pulse on / pulse off
	@(posedge clk);
	r_trig_pulse_on_cont = 1'b1;
	@(posedge clk);
	r_trig_pulse_on_cont = 1'b0;
#1000;
#(100*100);
	//
	@(posedge clk);
	r_trig_pulse_off = 1'b1;
	@(posedge clk);
	r_trig_pulse_off = 1'b0;
#1000;
#(100*100);
	// 
	//
	// new freq 1MHz : setup / pulse on / pulse off
	//
	//
	// new freq 5MHz : setup / pulse on / pulse off
	//
	//
    $finish;

end

always
#50 	clk = ~clk; // toggle every 50ns --> clock 100ns 

always
#4.96 	clk_bus = ~clk_bus; // toggle every 4.96ns --> clock 9.92ns for USB3.0	 

//always
//#6.25 	clk_dwave = ~clk_dwave; // 80MHz or 12.5ns for dwave
always
#3.125 	clk_dwave = ~clk_dwave; // 160MHz or 6.25ns for dwave


//initial begin
	//$dumpfile ("waveform.vcd"); 
	//$dumpvars; 
//end 
  
//initial  begin
	//$display("\t\t time,\t clk,\t reset_n,\t en"); 
	//$monitor("%d,\t%b,\t%b,\t%b,\t%d",$time,clk,reset_n,en); 
//end 
  
//initial 
	//#200_000 $finish; // 200us = 200_000 ns
	//#1000 $finish; // 1us = 1000 ns
	//#1000_000 $finish; // 1ms = 1000_000 ns
	
endmodule
